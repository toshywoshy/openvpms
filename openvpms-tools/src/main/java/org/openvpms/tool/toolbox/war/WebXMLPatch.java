/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

/**
 * Replaces the <param-value>openvpms</param-value> with one based on the name of the war.
 *
 * @author Tim Anderson
 */
class WebXMLPatch extends LineReplacingPatch {

    /**
     * The war name.
     */
    private final String name;

    /**
     * The line to match.
     */
    private static final String LINE = "<param-value>openvpms</param-value>";

    /**
     * Constructs a {@link WebXMLPatch}.
     *
     * @param name the war name
     */
    public WebXMLPatch(String name) {
        this.name = name;
    }

    /**
     * Performs replacement on a line of text.
     *
     * @param line the line
     * @return the (possibly modified) line
     */
    @Override
    protected String replace(String line) {
        if (line.contains(LINE)) {
            line = line.replace(LINE, "<param-value>" + name + "</param-value>");
        }
        return line;
    }
}
