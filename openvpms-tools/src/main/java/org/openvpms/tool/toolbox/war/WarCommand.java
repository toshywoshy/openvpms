/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.war;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.tool.toolbox.AbstractCommand;
import org.openvpms.tool.toolbox.config.ConfigProperties;
import org.openvpms.tool.toolbox.util.PathHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

/**
 * Patches the openvpms.war with properties from openvpms.properties.
 *
 * @author Tim Anderson
 */
@Command(name = "war", header = "Create the web archive")
public class WarCommand extends AbstractCommand {

    /**
     * The war file name.
     */
    @Option(names = {"-n", "--name"}, description = "The war file name", defaultValue = "openvpms.war")
    String name;

    /**
     * The source zip.
     */
    @Option(names = {"-a", "--archive"}, description = "The archive path", defaultValue = MASTER_ZIP_PATH)
    String archive;

    /**
     * The output directory.
     */
    @Option(names = {"-d", "--dir"}, required = true, description = "Output directory", defaultValue = OUTPUT_DIR_PATH)
    String dir;

    /**
     * The default master zip path.
     */
    private static final String MASTER_ZIP_PATH = "${openvpms.home}/webapps/openvpms-master.zip";

    /**
     * The default output directory.
     */
    private static final String OUTPUT_DIR_PATH = "${openvpms.home}/webapps";


    /**
     * Initialises the command.
     *
     * @throws Exception for any error
     */
    @Override
    protected void init() throws Exception {
        ConfigProperties properties = loadProperties();
        checkProperties(properties);
    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result = 1;
        String war = getWarName();
        if (war != null) {
            Path target = patch(war);
            Path relative = PathHelper.getPathRelativeToCWD(target);
            System.out.printf("Created %s\n\n", relative);
            try {
                Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rw-------");
                Files.setPosixFilePermissions(target, permissions);
            } catch (Throwable exception) {
                System.err.printf("\nThe file %s should have restrictive permissions to protect passwords\n\n",
                                  relative);
            }
            result = 0;
        }
        return result;
    }

    /**
     * Returns the patches to apply to the war.
     *
     * @param name the war name, minus suffix
     * @return the patches, keyed on the paths that they apply to
     */
    protected Map<String, Patch> getPatches(String name) {
        Map<String, Patch> result = new HashMap<>();
        result.put("WEB-INF/classes/openvpms.properties", new CopyPropertiesPatch(getPropertiesPath()));
        if (!"openvpms".equals(name)) {
            result.put("WEB-INF/web.xml", new WebXMLPatch(name));
            result.put("WEB-INF/classes/log4j2.xml", new Log4JPatch(name));
        }
        return result;
    }

    /**
     * Returns the war name.
     *
     * @return the war name, minus any extension
     */
    private String getWarName() {
        String result = null;
        if (name != null) {
            String war = StringUtils.removeEndIgnoreCase(name, ".war");
            if (!war.matches("[a-zA-Z]+[a-zA-Z0-9_\\-]*")) {
                System.err.printf("%s is not a valid name\n", name);
            } else {
                result = war;
            }
        } else {
            result = "openvpms";
        }
        return result;
    }

    /**
     * Generates a .war from the specified archive.
     *
     * @param name the war file name, minus any extension.
     * @return the path of the output war
     * @throws IOException for any I/O error
     */
    private Path patch(String name) throws IOException {
        Path tempJarFile = Files.createTempFile(Paths.get(dir), "openvpms", ".war");
        if (OUTPUT_DIR_PATH.equals(dir)) {
            dir = PathHelper.replaceHome(dir);
        }
        Path result = Paths.get(dir, name + ".war");
        Map<String, Patch> patches = getPatches(name);

        if (MASTER_ZIP_PATH.equals(archive)) {
            dir = PathHelper.replaceHome(archive);
        }
        try (JarFile jar = new JarFile(archive);
             OutputStream out = Files.newOutputStream(tempJarFile)) {
            JarStream tempJar = new JarStream(out);

            for (Enumeration<JarEntry> entries = jar.entries(); entries.hasMoreElements(); ) {
                JarEntry entry = entries.nextElement();
                try (InputStream inputStream = jar.getInputStream(entry)) {
                    Patch patch = patches.remove(entry.getName());
                    if (patch == null) {
                        // copy the file as is
                        tempJar.putNextEntry(entry);
                        IOUtils.copy(inputStream, tempJar);
                    } else {
                        tempJar.putNextEntry(new JarEntry(entry.getName()));
                        // the new file will have different size, CRC
                        patch.apply(inputStream, tempJar);
                    }
                    tempJar.closeEntry();
                }
            }
            tempJar.realClose();

            Files.move(tempJarFile, result, StandardCopyOption.REPLACE_EXISTING);
        }
        if (!patches.isEmpty()) {
            System.err.println("Not all patches were applied: " + StringUtils.join(patches.keySet(), ", "));
        }

        return result;
    }


    private static class JarStream extends JarOutputStream {

        /**
         * Constructs a {@link JarStream}.
         *
         * @param out the actual output stream
         * @throws IOException if an I/O error has occurred
         */
        public JarStream(OutputStream out) throws IOException {
            super(out);
        }

        /**
         * Closes the ZIP output stream as well as the stream being filtered.
         */
        @Override
        public void close() {

        }

        public void realClose() throws IOException {
            super.close();
        }
    }
}
