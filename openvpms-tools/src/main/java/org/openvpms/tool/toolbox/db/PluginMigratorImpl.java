/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.db;

import com.atlassian.plugin.JarPluginArtifact;
import org.apache.commons.io.FileUtils;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.db.service.PluginMigrator;
import org.openvpms.tool.toolbox.plugin.PluginArtifactHelper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Migrates plugins.
 * <p/>
 * This installs plugins from the specified plugins directory, if they have been installed previously.
 *
 * @author Tim Anderson
 */
public class PluginMigratorImpl implements PluginMigrator {

    /**
     * The context loader.
     */
    private final ContextLoader loader;

    /**
     * The plugins directory.
     */
    private final String dir;

    /**
     * Constructs a {@link PluginMigratorImpl}.
     *
     * @param loader the context loader
     * @param dir    the plugins directory
     */
    public PluginMigratorImpl(ContextLoader loader, String dir) {
        this.loader = loader;
        this.dir = dir;
    }

    /**
     * Migrates plugins.
     */
    @Override
    public void migrate() {
        Map<String, JarPluginArtifact> availablePlugins = getAvailablePlugins();
        PluginDAO dao = loader.getContext().getBean(PluginDAO.class);
        Iterator<Plugin> plugins = dao.getPlugins();
        while (plugins.hasNext()) {
            Plugin plugin = plugins.next();
            String key = plugin.getKey();
            JarPluginArtifact artifact = availablePlugins.get(key);
            if (artifact != null) {
                try (InputStream stream = artifact.getInputStream()) {
                    dao.save(key, artifact.getName(), stream);
                } catch (IOException exception) {
                    throw new IllegalStateException("Failed to update plugin " + artifact.getName() + ": "
                                                    + exception.getMessage(), exception);
                }
            }
        }
    }

    /**
     * Returns the available plugins.
     *
     * @return the available plugins, keyed on plugin key
     */
    private Map<String, JarPluginArtifact> getAvailablePlugins() {
        Map<String, JarPluginArtifact> result = new HashMap<>();
        for (File file : FileUtils.listFiles(new File(dir), new String[]{"jar"}, false)) {
            JarPluginArtifact artifact = new JarPluginArtifact(file);
            if (artifact.containsJavaExecutableCode()) {
                String key = PluginArtifactHelper.getPluginKey(artifact);
                if (key != null) {
                    result.put(key, artifact);
                }
            }
        }
        return result;
    }

}