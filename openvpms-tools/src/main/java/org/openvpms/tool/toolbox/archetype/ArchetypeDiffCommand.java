/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.archetype;

import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import org.openvpms.tools.archetype.diff.ArchDiff;
import org.openvpms.tools.archetype.io.DescriptorLoader;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

/**
 * Command to compare archetypes.
 *
 * @author Tim Anderson
 */
@Command(name = "--diff", description = "Compare archetypes",
        footer = "\n\nThe version1 and version2 arguments specify the older and newer versions of archetypes.\n" +
                 "They may be:\n" +
                 ". an .adl file\n" +
                 ". a directory. All .adl files in the directory will be read.\n" +
                 ". database (or db) - all archetypes in the database will be read\n" +
                 "\n" +
                 "Examples:\n" +
                 "1. Compare a directory with archetypes in the database\n" +
                 "> toolbox archetype --diff -v ../archetypes\n" +
                 "\n" +
                 "2. Compare a file with a prior version in the database\n" +
                 "> toolbox archetype --diff -v db ../archetypes/contact.location.adl\n" +
                 "\n" +
                 "3. Compare two directories containing archetypes\n" +
                 "> toolbox archetype --diff 2.1/archetypes 2.2/archetypes\n" +
                 "\n")
public class ArchetypeDiffCommand extends AbstractApplicationContextCommand {

    /**
     * The first version path, or "db" to load archetypes from the database.
     */
    @CommandLine.Parameters(index = "0", description = "The first version to compare.", arity = "1")
    String version1;

    /**
     * The second version path, or "db" to load archetypes from the database.
     */
    @CommandLine.Parameters(index = "1", description = "The second version to compare.", arity = "1")
    String version2;

    /**
     * If true, don't recurse subdirectories.
     */
    @Option(names = {"--no-recurse", "-n"}, description = "Disable search of subdirectories.")
    boolean norecurse;

    /**
     * If {@code true} display verbose information.
     */
    @Option(names = {"--verbose", "-v"}, description = "Displays verbose info to the console.")
    boolean verbose;

    /**
     * Constructs an {@link ArchetypeDiffCommand}.
     */
    public ArchetypeDiffCommand() {
        super(true); // can still list if the database is not up-to-date, unless there are breaking schema changes
    }

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        ArchDiff diff = new ArchDiff(getBean(IArchetypeRuleService.class));
        boolean recurse = !norecurse;
        DescriptorLoader oldVersion = diff.getDescriptorLoader(version1, recurse);
        DescriptorLoader newVersion = diff.getDescriptorLoader(version2, recurse);
        diff.compare(oldVersion, newVersion, verbose);
        return 0;
    }
}
