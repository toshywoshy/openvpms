/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.ssl;

import org.openvpms.tool.toolbox.AbstractCommand;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Command to test SSL connections.
 * <p/>
 * Adapted from https://confluence.atlassian.com/download/attachments/117455/SSLPoke.java
 *
 * @author Tim Anderson
 */
@Command(name = "--check", description = "Check SSL connectivity")
public class SSLCommand extends AbstractCommand {

    /**
     * The host to connect to.
     */
    @CommandLine.Parameters(index = "0", arity = "1")
    String host;

    /**
     * The port to connect to.
     */
    @CommandLine.Parameters(index = "1", arity = "1")
    int port;

    /**
     * The number of calls.
     */
    @CommandLine.Option(names = "-n", description = "Number of calls to make", defaultValue = "1")
    int count;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        if (count < 0) {
            count = 1;
        }
        int success = 0;
        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        System.out.println("Connecting to " + host + " port " + port);
        for (int i = 0; i < count; ++i) {
            if (count > 1) {
                System.out.printf("%d... ", i + 1);
            }
            if (i > 0) {
                Thread.sleep(1000);
            }
            try (SSLSocket socket = (SSLSocket) factory.createSocket(host, port)) {
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();

                // Write a test byte to get a reaction
                out.write(1);
                while (in.available() > 0) {
                    System.out.print(in.read());
                }
                System.out.println("Successfully connected");
                socket.close();
                success++;
            } catch (Throwable exception) {
                System.out.print("Failed: ");
                exception.printStackTrace();
            }
        }
        if (count > 1) {
            double percent = success > 0 ? (((double) count * 100) / success) : 0;
            System.out.printf("%d of %d calls were successful (%.1f%%)\n", success, count, percent);
        }
        return success == count ? 0 : 1;
    }
}
