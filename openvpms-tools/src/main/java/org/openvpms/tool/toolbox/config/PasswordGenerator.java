/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.apache.commons.lang3.ArrayUtils;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Helper to generate a default password for MySQL.
 * <p/>
 * Note that this excludes special characters that may cause problems when used in shells. For a stronger password,
 * users can always enter their own.
 *
 * @author Tim Anderson
 */
public class PasswordGenerator {

    /**
     * Secure random instance.
     */
    private final Random random = new SecureRandom();

    /**
     * Uppercase characters.
     */
    static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * Lowercase characters.
     */
    static final String LOWER = UPPER.toLowerCase();

    /**
     * Numeric characters.
     */
    static final String NUMERIC = "0123456789";

    /**
     * Special characters. This excludes characters that will likely cause issues in shells.
     */
    static final String SPECIAL = ":@.,/+-=";

    /**
     * All characters.
     */
    static final String ALL = UPPER + LOWER + NUMERIC + SPECIAL;

    /**
     * Generates a password between 8 and 16 characters long, containing at least 2 uppercase, 2 lowercase, 2 numbers
     * and 2 special characters.
     *
     * @return the password
     */
    public String generate() {
        StringBuilder builder = new StringBuilder();
        builder.append(generate(UPPER, 2));
        builder.append(generate(LOWER, 2));
        builder.append(generate(NUMERIC, 2));
        builder.append(generate(SPECIAL, 2));
        int remaining = random.nextInt(16 - builder.length());
        if (remaining != 0) {
            builder.append(generate(ALL, remaining));
        }
        char[] chars = builder.toString().toCharArray();
        ArrayUtils.shuffle(chars, random);
        return String.valueOf(chars);
    }

    /**
     * Generates a random string from the specified characters.
     *
     * @param characters the characters to select from
     * @param length     the string length
     * @return the random string
     */
    private String generate(String characters, int length) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            result.append(characters.charAt(random.nextInt(characters.length())));
        }
        return result.toString();
    }
}