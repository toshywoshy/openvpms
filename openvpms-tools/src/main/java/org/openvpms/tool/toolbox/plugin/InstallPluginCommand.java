/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.plugin;

import com.atlassian.plugin.JarPluginArtifact;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.tool.toolbox.AbstractApplicationContextCommand;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Command to install plugins.
 *
 * @author Tim Anderson
 */
@Command(name = "--install", description = "Installs a plugin")
public class InstallPluginCommand extends AbstractApplicationContextCommand {

    /**
     * The plugin file.
     */
    @CommandLine.Parameters(description = "the .jar file", index = "0", arity = "1")
    File file;

    /**
     * Runs the command.
     *
     * @return {@code 0} indicates success, non-zero failure
     * @throws Exception for any error
     */
    @Override
    protected int run() throws Exception {
        int result = 1;
        JarPluginArtifact artifact = new JarPluginArtifact(file);
        if (artifact.containsJavaExecutableCode()) {
            String key = PluginArtifactHelper.getPluginKey(artifact);
            if (key != null) {
                PluginDAO dao = getBean(PluginDAO.class);
                try (InputStream stream = new FileInputStream(file)) {
                    dao.save(key, file.getName(), stream);
                    System.out.printf("Installed plugin with key %s\n", key);
                    result = 0;
                }
            }
        } else {
            System.err.printf("%s is not a recognised plugin\n", file.getName());
        }
        return result;
    }

}