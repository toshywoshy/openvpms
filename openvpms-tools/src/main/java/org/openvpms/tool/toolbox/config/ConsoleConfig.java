/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tool.toolbox.config;

import org.apache.commons.lang3.StringUtils;

import java.io.Console;
import java.util.Properties;

/**
 * Implementation of {@link Config} that prompts for values from the console.
 *
 * @author Tim Anderson
 */
class ConsoleConfig implements Config {

    /**
     * The console.
     */
    private final Console console;

    /**
     * The fallback configuration.
     */
    private final FallbackConfig config;

    /**
     * Constructs a {@link ConsoleConfig}.
     *
     * @param console    the console
     * @param properties the properties
     * @param defaults   the default properties
     */
    public ConsoleConfig(Console console, Properties properties, Properties defaults) {
        this.console = console;
        config = new FallbackConfig(properties, defaults);
    }

    /**
     * Returns the JDBC driver class name.
     *
     * @return the JDBC driver class name. May be {@code null}
     */
    @Override
    public String getDriver() {
        return config.getDriver();
    }

    /**
     * Returns the database URL.
     *
     * @return the database URL. May be {@code null}
     */
    @Override
    public String getUrl() {
        return prompt("Database URL", config.getUrl());
    }

    /**
     * Returns the database username.
     *
     * @return the database username. May be {@code null}
     */
    @Override
    public String getUsername() {
        return prompt("Database user", config.getUsername());
    }

    /**
     * Returns the database password.
     *
     * @return the database password. May be {@code null}
     */
    @Override
    public String getPassword() {
        return prompt("Database password", config.getPassword());
    }

    /**
     * Returns the reporting database URL.
     *
     * @return the reporting database URL. May be {@code null}
     */
    @Override
    public String getReportingUrl() {
        return prompt("Reporting database URL", config.getReportingUrl());
    }

    /**
     * Returns the reporting username.
     *
     * @return the reporting username. May be {@code null}
     */
    @Override
    public String getReportingUsername() {
        return prompt("Reporting database user", config.getReportingUsername());
    }

    /**
     * Returns the reporting password.
     *
     * @return the reporting password. May be {@code null}
     */
    @Override
    public String getReportingPassword() {
        return prompt("Reporting database password", config.getReportingPassword());
    }

    /**
     * Returns the encryption key.
     *
     * @return the encryption key. May be {@code null}
     */
    @Override
    public String getKey() {
        return config.getKey();
    }

    /**
     * Prompts for user input and returns the entered value.
     *
     * @param prompt       the prompt
     * @param defaultValue the default value. May be {@code null}
     * @return the entered value
     */
    private String prompt(String prompt, String defaultValue) {
        String result = null;
        boolean done = false;
        while (!done) {
            String line = defaultValue != null ? prompt + " [" + defaultValue + "]: " : prompt + ": ";
            console.printf(line);
            result = console.readLine();
            if (!StringUtils.isEmpty(result)) {
                done = true;
            } else if (defaultValue != null) {
                result = defaultValue;
                done = true;
            }
        }
        return result;
    }

}