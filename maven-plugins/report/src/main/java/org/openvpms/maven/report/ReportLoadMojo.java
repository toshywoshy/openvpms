/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.report;


import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.service.lookup.LookupService;
import org.openvpms.maven.archetype.AbstractHibernateMojo;
import org.openvpms.report.tools.TemplateLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.File;


/**
 * Loads report templates using the {@link TemplateLoader}.
 */
@Mojo(name = "load")
public class ReportLoadMojo extends AbstractHibernateMojo {

    /**
     * The templates file(s).
     */
    @Parameter(required = true)
    private File[] files;

    /**
     * Loads report templates from the specified directory.
     *
     * @throws MojoExecutionException if an unexpected problem occurs
     */
    protected void doExecute() throws MojoExecutionException {
        if (files == null || files.length == 0) {
            throw new MojoExecutionException("No files specified");
        }
        for (File file : files) {
            if (!file.exists()) {
                throw new MojoExecutionException("File does not exist: " + file);
            }
        }
        try {
            ApplicationContext context = getContext();
            IArchetypeService service = context.getBean(IArchetypeRuleService.class);
            DocumentHandlers handlers = context.getBean(DocumentHandlers.class);
            PlatformTransactionManager transactionManager = context.getBean(PlatformTransactionManager.class);
            LookupService lookups = context.getBean(LookupService.class);
            TemplateLoader loader = new TemplateLoader(service, handlers, transactionManager, lookups);
            for (File file : files) {
                getLog().info("Loading: " + file.getPath());
                loader.load(file.getPath());
            }
        } catch (Exception exception) {
            throw new MojoExecutionException("Failed to load report templates", exception);
        }
    }

    /**
     * Returns the application context paths used to create the Spring application context.
     *
     * @return the context paths
     */
    @Override
    protected String[] getContextPaths() {
        return new String[]{APPLICATION_CONTEXT, "reportContext.xml"};
    }

}