/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.archetype;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptors;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptors;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionTypeDescriptor;
import org.openvpms.tools.archetype.io.ArchetypeIOHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

/**
 * Mojo to generate a openvpms-archetypes.properties in META-INF containing a checksum of the archetypes.
 * <p/>
 * This can be used to determine if archetypes need to be loaded into the database.
 * <p/>
 * If archetypes are customised, the {@link #override} flag should be set to indicate that the generated
 * checksum is overriding the default.
 * .
 *
 * @author Tim Anderson
 */
@Mojo(name = "checksum")
public class ArchetypeChecksumMojo extends AbstractMojo {

    /**
     * The directory to source archetypes from.
     */
    @Parameter(required = true)
    private File dir;

    /**
     * The assertion types file. If not specified, defaults to {@code dir/assertionTypes.xml}
     */
    @Parameter
    private File assertionTypes;

    /**
     * Determines if the override property is being generated. This should be set {@code true} if archetypes are
     * customised.
     */
    @Parameter
    private boolean override;

    /**
     * The maven project to interact with.
     */
    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    /**
     * Executes the mojo.
     *
     * @throws MojoExecutionException if an unexpected problem occurs
     */
    @Override
    public void execute() throws MojoExecutionException {
        if (dir == null || !dir.exists()) {
            throw new MojoExecutionException("Directory not found: " + dir);
        }
        if (!dir.isDirectory()) {
            throw new MojoExecutionException("Not a directory: " + dir);
        }
        ArchetypeChecksumGenerator generator = new ArchetypeChecksumGenerator();
        try {
            List<AssertionTypeDescriptor> assertionTypeDescriptors = getAssertionTypeDescriptors();
            List<ArchetypeDescriptor> archetypes = getArchetypeDescriptors();
            int checksum = generator.generate(assertionTypeDescriptors, archetypes);
            Properties properties = new Properties();
            String key = (override) ? "checksum.override" : "checksum.default";
            properties.put(key, Integer.toHexString(checksum));
            File metaInf = new File(project.getBuild().getOutputDirectory(), "META-INF");
            if (!metaInf.exists() && !metaInf.mkdirs()) {
                throw new MojoExecutionException("Failed to create " + metaInf);
            }
            File file = new File(metaInf, "openvpms-archetypes.properties");
            try (FileOutputStream out = new FileOutputStream(file)) {
                properties.store(out, "");
            }
        } catch (MojoExecutionException exception) {
            throw exception;
        } catch (Exception throwable) {
            throw new MojoExecutionException("Failed to calculate archetype checksum", throwable);
        }
    }

    /**
     * Returns the assertion type descriptors.
     *
     * @return the assertion type descriptors
     * @throws FileNotFoundException if the file cannot be found
     */
    private List<AssertionTypeDescriptor> getAssertionTypeDescriptors() throws FileNotFoundException {
        File file = (assertionTypes != null) ? assertionTypes : new File(dir, "assertionTypes.xml");
        AssertionTypeDescriptors descriptors = AssertionTypeDescriptors.read(new FileInputStream(file));
        return new ArrayList<>(descriptors.getAssertionTypeDescriptors().values());
    }

    /**
     * Returns the archetype descriptors.
     *
     * @return the archetype descriptors
     * @throws IOException            for any I/O error
     * @throws MojoExecutionException if no archetypes were found
     */
    private List<ArchetypeDescriptor> getArchetypeDescriptors() throws IOException, MojoExecutionException {
        List<ArchetypeDescriptor> result = new ArrayList<>();
        Collection<File> files = ArchetypeIOHelper.getArchetypeFiles(dir, true);

        if (files.isEmpty()) {
            throw new MojoExecutionException("No archetypes found");
        }
        for (File file : files) {
            ArchetypeDescriptors descriptors = ArchetypeDescriptors.read(new FileInputStream(file));
            result.addAll(descriptors.getArchetypeDescriptors().values());
        }
        return result;
    }
}