/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.maven.archetype;


import org.openvpms.component.model.archetype.ActionTypeDescriptor;
import org.openvpms.component.model.archetype.ArchetypeDescriptor;
import org.openvpms.component.model.archetype.AssertionDescriptor;
import org.openvpms.component.model.archetype.AssertionProperty;
import org.openvpms.component.model.archetype.AssertionTypeDescriptor;
import org.openvpms.component.model.archetype.NamedProperty;
import org.openvpms.component.model.archetype.NodeDescriptor;
import org.openvpms.component.model.archetype.PropertyList;
import org.openvpms.component.model.archetype.PropertyMap;
import org.openvpms.component.model.object.IMObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Generates a checksum for a set of archetypes in order to determine if archetypes need to be loaded.
 *
 * @author Tim Anderson
 */
public class ArchetypeChecksumGenerator {

    /**
     * Generates the checksum for a set of archetypes.
     *
     * @param assertionTypes the assertion type descriptors
     * @param archetypes     the archetype descriptors
     * @return the check sum
     */
    public int generate(List<AssertionTypeDescriptor> assertionTypes, List<ArchetypeDescriptor> archetypes) {
        Checksum checksum = new CRC32();
        for (AssertionTypeDescriptor assertionType : sort(assertionTypes)) {
            generate(assertionType, checksum);
        }
        for (ArchetypeDescriptor archetype : sort(archetypes)) {
            generate(archetype, checksum);
        }
        return (int) checksum.getValue();
    }

    /**
     * Generates the checksum for an assertion type descriptor.
     *
     * @param assertionType the assertion type descriptor
     * @param checksum      the check sum
     */
    private void generate(AssertionTypeDescriptor assertionType, Checksum checksum) {
        add(assertionType.getName(), checksum);
        add(assertionType.getPropertyArchetype(), checksum);
        for (ActionTypeDescriptor actionType : sort(assertionType.getActionTypes())) {
            add(actionType.getName(), checksum);
            add(actionType.getClassName(), checksum);
            add(actionType.getMethodName(), checksum);
        }
    }

    /**
     * Generates the checksum for an archetype descriptor.
     *
     * @param archetype the archetype descriptor
     * @param checksum  the check sum
     */
    private void generate(ArchetypeDescriptor archetype, Checksum checksum) {
        add(archetype.getName(), checksum);
        add(archetype.getArchetypeType(), checksum);
        add(archetype.getDisplayName(), checksum);
        add(archetype.getDescription(), checksum);
        add(archetype.getClassName(), checksum);
        add(archetype.isLatest(), checksum);
        add(archetype.isPrimary(), checksum);
        add(archetype.isSingleton(), checksum);
        for (NodeDescriptor node : sort(archetype.getNodeDescriptors())) {
            generate(node, checksum);
        }
    }

    /**
     * Generates the checksum for a node descriptor.
     *
     * @param node     the node descriptor
     * @param checksum the check sum
     */
    private void generate(NodeDescriptor node, Checksum checksum) {
        add(node.getName(), checksum);
        add(node.getDescription(), checksum);
        add(node.getDefaultValue(), checksum);
        add(node.isDerived(), checksum);
        add(node.getDerivedValue(), checksum);
        add(node.getDisplayName(), checksum);
        add(node.getFilter(), checksum);
        add(node.getIndex(), checksum);
        add(node.getMaxCardinality(), checksum);
        add(node.getMinCardinality(), checksum);
        add(node.getMaxLength(), checksum);
        add(node.getMinLength(), checksum);
        add(node.getPath(), checksum);
        add(node.getType(), checksum);
        add(node.isHidden(), checksum);
        add(node.isReadOnly(), checksum);
        add(node.getBaseName(), checksum);
        add(node.isParentChild(), checksum);

        for (AssertionDescriptor descriptor : sort(node.getAssertionDescriptors().values())) {
            add(descriptor.getName(), checksum);
            add(descriptor.getDescription(), checksum);
            add(descriptor.getErrorMessage(), checksum);
            add(descriptor.getIndex(), checksum);
            for (NamedProperty property : sort(descriptor.getPropertyMap().getProperties().values())) {
                generate(property, checksum);
            }
        }
    }

    /**
     * Generates the checksum for a property.
     *
     * @param property the property
     * @param checksum the checksum
     */
    private void generate(NamedProperty property, Checksum checksum) {
        add(property.getName(), checksum);
        add(property.getDescription(), checksum);
        if (property instanceof AssertionProperty) {
            AssertionProperty assertionProperty = (AssertionProperty) property;
            add(assertionProperty.getType(), checksum);
            add(assertionProperty.getValue(), checksum);
        } else if (property instanceof PropertyList) {
            PropertyList list = (PropertyList) property;
            for (NamedProperty child : sort(list.getProperties())) {
                generate(child, checksum);
            }
        } else if (property instanceof PropertyMap) {
            PropertyMap map = (PropertyMap) property;
            for (NamedProperty child : sort(map.getProperties().values())) {
                generate(child, checksum);
            }
        }
    }

    /**
     * Sorts a collection of objects on name.
     *
     * @param objects the objects to sort
     * @return the sorted objects
     */
    private <T extends IMObject> List<T> sort(Collection<T> objects) {
        List<T> result = new ArrayList<>(objects);
        result.sort(Comparator.comparing(IMObject::getName));
        return result;
    }

    /**
     * Adds a string value to the checksum.
     *
     * @param value    the value. May be {@code null}
     * @param checksum the checksum
     */
    private void add(String value, Checksum checksum) {
        if (value != null) {
            byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
            checksum.update(bytes, 0, bytes.length);
        }
    }

    /**
     * Adds a boolean value to the checksum.
     *
     * @param value    the value
     * @param checksum the checksum
     */
    private void add(boolean value, Checksum checksum) {
        checksum.update(value ? 1 : 0);
    }

    /**
     * Adds a value to the checksum.
     *
     * @param value    the value
     * @param checksum the checksum
     */
    private void add(int value, Checksum checksum) {
        checksum.update(value);
    }

    /**
     * Adds a value to the checksum.
     *
     * @param value    the value. May be {@code null}
     * @param checksum the checksum
     */
    private void add(Object value, Checksum checksum) {
        if (value != null) {
            add(value.toString(), checksum);
        }
    }
}