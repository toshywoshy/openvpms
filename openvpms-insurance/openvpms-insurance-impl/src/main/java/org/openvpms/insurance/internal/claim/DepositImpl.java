/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.internal.claim;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.act.ActIdentity;
import org.openvpms.component.business.domain.im.act.BeanActDecorator;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.insurance.claim.Deposit;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * Default implementation of {@link Deposit}.
 *
 * @author Tim Anderson
 */
class DepositImpl extends BeanActDecorator implements Deposit {

    /**
     * Constructs a {@link DepositImpl}.
     *
     * @param peer    the peer to delegate to
     * @param factory the bean factory
     */
    public DepositImpl(Act peer, IMObjectBeanFactory factory) {
        super(peer, factory);
    }

    /**
     * Returns the deposit identifier.
     *
     * @return the deposit identifier
     */
    @Override
    public String getDepositId() {
        ActIdentity identity = getBean().getObject("depositId", ActIdentity.class);
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * The deposit date.
     *
     * @return the deposit date
     */
    @Override
    public OffsetDateTime getDate() {
        return DateRules.toOffsetDateTime(getBean().getDate("startTime"));
    }

    /**
     * The deposit amount.
     *
     * @return the deposit amount
     */
    @Override
    public BigDecimal getAmount() {
        return getBean().getBigDecimal("amount", BigDecimal.ZERO);
    }
}
