/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.claim;

import org.openvpms.insurance.exception.InsuranceException;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

/**
 * Updates the state of a {@link GapClaim}.
 *
 * @author Tim Anderson
 */
public interface GapClaimUpdater extends ClaimUpdater {

    /**
     * Updates the gap claim with the benefit.
     * <p>
     * This is only valid when the gap status is {@link GapClaim.GapStatus#PENDING}.<br/>
     * This sets the gap status to {@link GapClaim.GapStatus#RECEIVED}.
     *
     * @param amount the benefit amount
     * @param notes  notes associated with the benefit amount
     * @throws IllegalStateException if the gap status is not {@link GapClaim.GapStatus#PENDING}
     */
    GapClaimUpdater benefit(BigDecimal amount, String notes);

    /**
     * Updates the gap claim with the vet benefit.
     *
     * @param amount the vet benefit amount
     */
    GapClaimUpdater vetBenefitAmount(BigDecimal amount);

    /**
     * Determines if there is deposit with the specified identifier.
     *
     * @param id the deposit identifier
     * @return {@code true} if the deposit exists, otherwise {@code false}
     */
    boolean hasDeposit(String id);

    /**
     * Adds a deposit.
     *
     * @param id        the deposit identifier
     * @param archetype the deposit identifier archetype. Must have an <em>actIdentity.insuranceDeposit*</em> prefix
     * @param date      the date the deposit was paid
     * @param amount    the deposit amount
     */
    GapClaimUpdater deposit(String id, String archetype, OffsetDateTime date, BigDecimal amount);

    /**
     * Updates the claim.
     *
     * @return the claim
     * @throws InsuranceException if the claim cannot be updated
     */
    @Override
    GapClaim update();
}
