/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.insurance.claim;

import org.openvpms.component.model.party.Party;
import org.openvpms.insurance.exception.InsuranceException;

/**
 * Updates the state of a {@link Claim}.
 *
 * @author Tim Anderson
 */
public interface ClaimUpdater {

    /**
     * Sets the claim identifier, issued by the insurer.
     * <p>
     * A claim can have a single identifier issued by an insurer. To avoid duplicates, each insurance service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.insuranceClaim</em> prefix.
     * @param id        the claim identifier
     * @return this
     * @throws InsuranceException if the identifier cannot be set
     */
    ClaimUpdater insurerId(String archetype, String id);

    /**
     * Changes the policy for a claim. This can be used if a policy was submitted with an incorrect insurer or policy
     * number.
     *
     * @param insurer      the insurer
     * @param policyNumber the policy number
     * @return this
     */
    ClaimUpdater policy(Party insurer, String policyNumber);

    /**
     * Sets the claim status.
     *
     * @param status the claim status
     * @return this
     */
    ClaimUpdater status(Claim.Status status);

    /**
     * Sets the claim status, along with any message from the insurer.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     */
    ClaimUpdater status(Claim.Status status, String message);

    /**
     * Sets a message on the claim. This may be used by insurance service to convey to users the status of the claim,
     * or why a claim was declined.
     *
     * @param message the message. May be {@code null}
     * @return this
     */
    ClaimUpdater message(String message);

    /**
     * Updates the claim.
     *
     * @return the claim
     * @throws InsuranceException if the claim cannot be updated
     */
    Claim update();

}
