/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.laboratory.TestLaboratoryFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.practice.Location;
import org.openvpms.laboratory.service.DeviceBuilder;
import org.openvpms.laboratory.service.Devices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DeviceBuilderImpl}.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class DeviceBuilderImplTestCase extends ArchetypeServiceTest {

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The laboratory factory.
     */
    @Autowired
    private TestLaboratoryFactory laboratoryFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * Tests the {@link DeviceBuilder#addLocation(Location)} method and {@link DeviceBuilder#locations(Location...)}.
     * methods.
     */
    @Test
    public void testLocations() {
        Devices devices = new DevicesImpl(getArchetypeService(), transactionManager, domainService);
        DeviceBuilderImpl builder1 = new DeviceBuilderImpl(devices, getArchetypeService(), transactionManager,
                                                           domainService);
        Location location1 = domainService.create(practiceFactory.createLocation(), Location.class);
        Location location2 = domainService.create(practiceFactory.createLocation(), Location.class);
        Laboratory laboratory = domainService.create(laboratoryFactory.createLaboratory(), Laboratory.class);

        // create a device with two locations
        String id = UUID.randomUUID().toString();
        Device device1 = builder1.laboratory(laboratory)
                .name("zdevice")
                .deviceId("entityIdentity.laboratoryDeviceTest", id)
                .addLocation(location1)
                .addLocation(location2)
                .build();

        // verify both locations are present
        IMObject object1 = get(device1);
        List<IMObject> locations1 = getBean(object1).getTargets("locations");
        assertEquals(2, locations1.size());
        assertTrue(locations1.contains(location1));
        assertTrue(locations1.contains(location2));

        // update the device, and make location2 the only location
        DeviceBuilderImpl builder2 = new DeviceBuilderImpl(devices, getArchetypeService(), transactionManager,
                                                           domainService);
        Device device2 = builder2.laboratory(laboratory)
                .deviceId("entityIdentity.laboratoryDeviceTest", id)
                .locations(location2)
                .build();
        IMObject object2 = get(device2);

        // verify the object was updated
        assertEquals(object1, object2);

        // verify only location2 is present
        List<IMObject> locations2 = getBean(object2).getTargets("locations");
        assertEquals(1, locations2.size());
        assertFalse(locations2.contains(location1));
        assertTrue(locations2.contains(location2));
    }
}
