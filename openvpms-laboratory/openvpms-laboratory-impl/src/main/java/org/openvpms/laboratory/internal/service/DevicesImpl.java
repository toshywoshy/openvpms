/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.laboratory.service.DeviceBuilder;
import org.openvpms.laboratory.service.Devices;
import org.openvpms.laboratory.service.LaboratoryService;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link Devices}.
 *
 * @author Tim Anderson
 */
public class DevicesImpl implements Devices {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link DevicesImpl}.
     *
     * @param service            the archetype service
     * @param transactionManager the transaction manager
     * @param domainService      the domain object service
     */
    public DevicesImpl(ArchetypeService service, PlatformTransactionManager transactionManager,
                       DomainService domainService) {
        this.service = service;
        this.transactionManager = transactionManager;
        this.domainService = domainService;
    }

    /**
     * Returns a laboratory device.
     * <p>
     * Devices managed by an {@link LaboratoryService} must have a unique code.
     *
     * @param archetype the device identity archetype. Must be an <em>entityIdentity.laboratoryDevice*</em>
     * @param deviceId  the device identifier
     * @return the device, or {@code null} if none is found. The returned device may be inactive
     */
    @Override
    public Device getDevice(String archetype, String deviceId) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, LaboratoryArchetypes.DEVICE);
        Join identity = root.join("deviceId", archetype);
        identity.on(builder.equal(identity.get("identity"), deviceId));
        query.orderBy(builder.asc(root.get("id")));
        Entity match = service.createQuery(query).getFirstResult();
        return (match != null) ? domainService.create(match, Device.class) : null;
    }

    /**
     * Returns all devices with the specified device identity archetype.
     *
     * @param archetype  the device identity archetype. Must be an <em>entityIdentity.laboratoryDevice*</em>
     * @param activeOnly if {@code true}, only return active devices
     * @return the tests
     */
    @Override
    public List<Device> getDevices(String archetype, boolean activeOnly) {
        List<Device> result = new ArrayList<>();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, LaboratoryArchetypes.DEVICE);
        root.join("deviceId", archetype);
        if (activeOnly) {
            query.where(builder.equal(root.get("active"), true));
        }
        query.orderBy(builder.asc(root.get("id")));
        for (Entity entity : service.createQuery(query).getResultList()) {
            result.add(domainService.create(entity, Device.class));
        }
        return result;
    }

    /**
     * Returns a builder to build devices.
     *
     * @return the device builder
     */
    @Override
    public DeviceBuilder getDeviceBuilder() {
        return new DeviceBuilderImpl(this, service, transactionManager, domainService);
    }

}
