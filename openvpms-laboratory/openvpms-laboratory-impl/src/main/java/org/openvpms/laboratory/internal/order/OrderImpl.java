/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.order;

import org.apache.commons.lang.StringUtils;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.InvestigationActStatus;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.laboratory.Device;
import org.openvpms.domain.laboratory.InvestigationType;
import org.openvpms.domain.laboratory.Laboratory;
import org.openvpms.domain.laboratory.Test;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Visit;
import org.openvpms.domain.practice.Location;
import org.openvpms.laboratory.exception.InvestigationNotFound;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.internal.i18n.LaboratoryMessages;
import org.openvpms.laboratory.internal.report.ReportBuilderImpl;
import org.openvpms.laboratory.internal.report.ReportImpl;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.report.Report;
import org.openvpms.laboratory.report.ReportBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Default implementation of {@link Order}.
 *
 * @author Tim Anderson
 */
public class OrderImpl implements Order {

    /**
     * The order.
     */
    private final Act order;

    /**
     * The bean wrapping the order
     */
    private final IMObjectBean bean;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The investigation type.
     */
    private InvestigationType investigationType;

    /**
     * The laboratory device.
     */
    private Device device;

    /**
     * The laboratory.
     */
    private Laboratory laboratory;

    /**
     * The patient.
     */
    private Patient patient;

    /**
     * The customer.
     */
    private Customer customer;

    /**
     * The location.
     */
    private Location location;

    /**
     * Allowed statuses when cancelling an order.
     */
    private static final EnumSet<Status> CANCEL_STATUSES
            = EnumSet.of(Status.PENDING, Status.SUBMITTING, Status.ERROR, Status.CANCELLED);

    /**
     * The order id node.
     */
    private static final String ORDER_ID = "orderId";

    /**
     * The type node.
     */
    private static final String TYPE = "type";

    /**
     * Constructs an {@link OrderImpl}.
     *
     * @param order              the order
     * @param service            the archetype service
     * @param patientRules       the patient rules
     * @param domainService      the domain object service
     * @param transactionManager the transaction manager
     * @param handlers           the document handlers
     * @param rules              the document rules
     */
    public OrderImpl(Act order, ArchetypeService service, PatientRules patientRules, DomainService domainService,
                     PlatformTransactionManager transactionManager, DocumentHandlers handlers,
                     DocumentRules rules) {
        this.order = order;
        this.service = service;
        this.patientRules = patientRules;
        this.domainService = domainService;
        this.transactionManager = transactionManager;
        this.handlers = handlers;
        this.rules = rules;
        bean = service.getBean(this.order);
    }

    /**
     * Returns the investigation identifier.
     *
     * @return the investigation identifier
     */
    @Override
    public long getInvestigationId() {
        ActIdentity identity = bean.getObject("investigationId", ActIdentity.class);
        return identity != null ? Long.parseLong(identity.getIdentity()) : -1;
    }

    /**
     * Returns a universally unique identifier (UUID) for the order.
     *
     * @return the stringified form of the UUID
     */
    @Override
    public String getUUID() {
        return order.getLinkId();
    }

    /**
     * Returns the order identifier, issued by the laboratory.
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    @Override
    public String getOrderId() {
        ActIdentity identity = getOrderIdentity();
        return identity != null ? identity.getIdentity() : null;
    }

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @throws LaboratoryException if the identifier cannot be set
     */
    @Override
    public void setOrderId(String archetype, String id) {
        ActIdentity identity = getOrderIdentity();
        if (identity == null) {
            identity = service.create(archetype, ActIdentity.class);
            bean.addValue(ORDER_ID, identity);
        } else if (!identity.isA(archetype)) {
            throw new LaboratoryException(LaboratoryMessages.differentOrderIdentifierArchetype(
                    identity.getArchetype(), archetype));
        }
        identity.setIdentity(id);
        bean.save();
    }

    /**
     * Returns the identity for the order, issued by the laboratory.
     *
     * @return the order identifier, or {@code null} if none has been issued
     */
    @Override
    public ActIdentity getOrderIdentity() {
        return bean.getObject(ORDER_ID, ActIdentity.class);
    }

    /**
     * Sets the order identifier, issued by the laboratory.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param identity the order identifier
     */
    @Override
    public void setOrderIdentity(ActIdentity identity) {
        ActIdentity current = getOrderIdentity();
        if (current != null) {
            bean.removeValue(ORDER_ID, current);
        }
        bean.addValue(ORDER_ID, identity);
        bean.save();
    }

    /**
     * Returns the order type.
     *
     * @return the order type
     */
    @Override
    public Type getType() {
        return Type.valueOf(bean.getString(TYPE));
    }

    /**
     * Returns the request status.
     *
     * @return the request status
     */
    @Override
    public Status getStatus() {
        return Status.valueOf(order.getStatus());
    }

    /**
     * Sets the request status.
     *
     * @param status the new request status
     * @throws LaboratoryException if the status cannot be set
     */
    @Override
    public void setStatus(Status status) {
        setStatus(status, null);
    }

    /**
     * Sets the request status, along with any message from the laboratory.
     *
     * @param status  the status
     * @param message the message. May be {@code null}
     * @throws LaboratoryException if the status cannot be set
     */
    @Override
    public void setStatus(Status status, String message) {
        Type type = getType();
        if (type == Type.CANCEL && !CANCEL_STATUSES.contains(status)) {
            throw new LaboratoryException(LaboratoryMessages.invalidCancelStatus(status));
        }
        Status existing = getStatus();
        if (status != existing) {
            if (existing == Status.ERROR && type != Type.CANCEL) {
                throw new LaboratoryException(LaboratoryMessages.orderWithErrorStatusMustBeCancelled());
            }
            order.setStatus(status.name());
            if (status == Status.CONFIRM) {
                if (type == Type.NEW) {
                    setInvestigationOrderStatus(InvestigationActStatus.CONFIRM, message);
                }
            }
            if (status == Status.SUBMITTED) {
                if (type == Type.NEW) {
                    setInvestigationOrderStatus(InvestigationActStatus.SENT, message);
                }
            } else if (status == Status.COMPLETED) {
                if (type == Type.NEW) {
                    setInvestigationOrderStatus(InvestigationActStatus.RECEIVED, message);
                }
            } else if (status == Status.ERROR) {
                setInvestigationOrderStatus(InvestigationActStatus.ERROR, message);
            } else if (status == Status.CANCELLED) {
                bean.setValue(TYPE, Type.CANCEL.name());
                order.setStatus(Status.CANCELLED.name());
                withTransaction(() -> {
                    Act investigation = getInvestigation();
                    if (investigation != null && !ActStatus.CANCELLED.equals(investigation.getStatus())) {
                        // NOTE: the investigation could be POSTED due to record locking. It may still be CANCELLED.
                        investigation.setStatus(ActStatus.CANCELLED);
                        setMessage(investigation, message);
                        service.save(investigation);
                    }
                    bean.save();
                });
            } else {
                bean.save();
            }
        }
    }

    /**
     * Returns the investigation.
     *
     * @return the investigation. May be {@code null}
     */
    public Act getInvestigation() {
        return getInvestigation(false);
    }

    /**
     * Returns the date when the order was created.
     *
     * @return the date
     */
    @Override
    public OffsetDateTime getCreated() {
        return DateRules.toOffsetDateTime(order.getActivityStartTime());
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type.
     */
    @Override
    public InvestigationType getInvestigationType() {
        if (investigationType == null) {
            investigationType = getTarget("investigationType", InvestigationType.class);
        }
        return investigationType;
    }

    /**
     * Returns the tests being ordered.
     *
     * @return the tests being ordered
     */
    @Override
    public List<Test> getTests() {
        List<Test> result = new ArrayList<>();
        for (Entity entity : bean.getTargets("tests", Entity.class)) {
            result.add(domainService.create(entity, Test.class));
        }
        return result;
    }

    /**
     * Returns the device to use to perform the tests.
     *
     * @return the device, {@code null} if no device was specified
     */
    @Override
    public Device getDevice() {
        if (device == null) {
            IMObject object = bean.getTarget("device");
            if (object != null) {
                device = domainService.create(object, Device.class);
            }
        }
        return device;
    }

    /**
     * Returns the laboratory used to perform the tests.
     *
     * @return the laboratory
     */
    @Override
    public Laboratory getLaboratory() {
        if (laboratory == null) {
            laboratory = getTarget("laboratory", Laboratory.class);
        }
        return laboratory;
    }

    /**
     * Returns the patient that the order is for.
     *
     * @return the patient
     */
    @Override
    public Patient getPatient() {
        if (patient == null) {
            patient = getTarget("patient", Patient.class);
        }
        return patient;
    }

    /**
     * Returns the customer responsible for the patient.
     *
     * @return the customer. May be {@code null}
     */
    @Override
    public Customer getCustomer() {
        if (customer == null) {
            Party owner = patientRules.getOwner(order);
            if (owner != null) {
                customer = domainService.create(owner, Customer.class);
            }
        }
        return customer;
    }

    /**
     * Returns the clinician responsible for the claim.
     *
     * @return the clinician
     */
    @Override
    public User getClinician() {
        return bean.getTarget("clinician", User.class);
    }

    /**
     * Returns the practice location where the order was placed.
     *
     * @return the practice location
     */
    @Override
    public Location getLocation() {
        if (location == null) {
            location = getTarget("location", Location.class);
        }
        return location;
    }

    /**
     * Returns the patient visit.
     *
     * @return the patient visit. May be {@code null}
     */
    @Override
    public Visit getVisit() {
        Act investigation = getInvestigation(false);
        if (investigation != null) {
            Act act = service.getBean(investigation).getSource("event", Act.class);
            return (act != null) ? domainService.create(act, Visit.class) : null;
        }
        return null;
    }

    /**
     * Returns the user that placed the order.
     *
     * @return the user that placed the order
     */
    @Override
    public User getUser() {
        return bean.getObject("createdBy", User.class);
    }

    /**
     * Returns the order notes.
     *
     * @return the order notes. May be {@code null}
     */
    @Override
    public String getNotes() {
        return bean.getString("description");
    }

    /**
     * Returns the laboratory report for the order.
     *
     * @return the report
     * @throws InvestigationNotFound if the investigation cannot be found
     */
    @Override
    public Report getReport() {
        Act investigation = getInvestigation(true);
        return domainService.create(investigation, ReportImpl.class);
    }

    /**
     * Returns a report builder to build the report for this order.
     *
     * @return the report builder
     */
    @Override
    public ReportBuilder getReportBuilder() {
        return new ReportBuilderImpl(getInvestigation(true), order, service, domainService, handlers, rules,
                                     transactionManager);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof OrderImpl) {
            return ((OrderImpl) obj).order.equals(order);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return order.hashCode();
    }

    /**
     * Saves the order and investigation in a transaction.
     * <p/>
     * The status and message are ignored if the investigation is cancelled, or the status is unchanged.
     *
     * @param status  the new investigation status
     * @param message the new investigation message. May be {@code null}
     */
    private void setInvestigationOrderStatus(String status, String message) {
        withTransaction(() -> {
            Act investigation = getInvestigation();
            if (investigation != null) {
                if (!status.equals(investigation.getStatus2())
                    && !ActStatus.CANCELLED.equals(investigation.getStatus())) {
                    investigation.setStatus2(status);
                    setMessage(investigation, message);
                    service.save(investigation);
                }
            }
            bean.save();
        });
    }

    /**
     * Sets the investigation message.
     *
     * @param investigation the investigation
     * @param message       the message. May be {@code null}
     */
    private void setMessage(Act investigation, String message) {
        IMObjectBean bean = service.getBean(investigation);
        if (message != null) {
            message = StringUtils.abbreviate(message, bean.getMaxLength("message"));
        }
        bean.setValue("message", message);
        bean.save();
    }

    /**
     * Return the investigation associated with the order.
     *
     * @param fail if {@code true}, fail with an {@link InvestigationNotFound} if the investigation no longer exists
     * @return the investigation, or {@code null} if the investigation was not found and {@code fail == false}
     * @throws InvestigationNotFound if the investigation was not found and {@code fail == true}
     */
    private DocumentAct getInvestigation(boolean fail) {
        DocumentAct result = bean.getSource("investigation", DocumentAct.class);
        if (result == null && fail) {
            throw new InvestigationNotFound(LaboratoryMessages.investigationNotFound(getInvestigationId()));
        }
        return result;
    }

    private <T> T getTarget(String name, Class<T> type) {
        IMObject object = bean.getTarget(name);
        if (object == null) {
            throw new IllegalStateException("Order has no " + name + ": " + order.getId());
        }
        return domainService.create(object, type);
    }

    /**
     * Runs code in a transaction.
     *
     * @param runnable the code to execute
     */
    private void withTransaction(Runnable runnable) {
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                runnable.run();
            }
        });
    }

}
