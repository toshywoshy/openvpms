/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.openvpms.archetype.component.dispatcher.Queue;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.domain.laboratory.Laboratory;

/**
 * A queue of orders for a {@link Laboratory}.
 *
 * @author Tim Anderson
 */
class OrderQueue extends Queue<Act, Entity> {

    /**
     * The order service.
     */
    private final OrderService service;

    /**
     * Constructs an {@link OrderQueue}.
     *
     * @param laboratory the laboratory that this queue manages orders for
     * @param service    the order service
     */
    public OrderQueue(Entity laboratory, OrderService service) {
        super(laboratory);
        this.service = service;
    }

    /**
     * Returns the interval to wait after a failure.
     *
     * @return the interval, in seconds
     */
    @Override
    public int getRetryInterval() {
        return 30;
    }

    /**
     * Queues an order for cancellation.
     *
     * @param order the order to add
     * @return {@code true} if it was queued
     */
    public boolean cancel(Act order) {
        return service.cancel(order);
    }

    /**
     * Returns a string representation of the owner.
     *
     * @param owner the queue owner
     * @return a string representation of the owner
     */
    @Override
    protected String toString(Entity owner) {
        return owner.getArchetype() + ":" + owner.getId();
    }

    /**
     * Retrieves the next act.
     *
     * @param owner the owner of the queue
     * @return the next act, or {@code null} if there is none
     */
    @Override
    protected Act getNext(Entity owner) {
        return service.next(owner);
    }
}
