/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.internal.order.OrderImpl;
import org.openvpms.laboratory.order.Order;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Factory for {@link Order} instances, given an <em>act.laboratoryOrder</em>.
 *
 * @author Tim Anderson
 */
class OrderFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The patient rules.
     */
    private final PatientRules patientRules;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The transaction manager.
     */
    private final PlatformTransactionManager transactionManager;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document rules.
     */
    private final DocumentRules documentRules;

    /**
     * Constructs an {@link OrderFactory}.
     *
     * @param service            the archetype service
     * @param patientRules       the patient rules
     * @param domainService      the domain object service
     * @param transactionManager the transaction manager
     */
    public OrderFactory(ArchetypeService service, PatientRules patientRules, DomainService domainService,
                        PlatformTransactionManager transactionManager) {
        this.service = service;
        this.patientRules = patientRules;
        this.domainService = domainService;
        this.transactionManager = transactionManager;
        this.handlers = new DocumentHandlers(service);  // these need the plugin archetype service
        this.documentRules = new DocumentRules(service);
    }

    /**
     * Creates an order.
     *
     * @param act the order act
     * @return the order
     */
    public Order create(Act act) {
        return new OrderImpl(act, service, patientRules, domainService, transactionManager, handlers, documentRules);
    }
}
