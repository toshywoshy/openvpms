/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.dispatcher;

import org.openvpms.archetype.component.dispatcher.Dispatcher;
import org.openvpms.archetype.component.dispatcher.Queue;
import org.openvpms.archetype.component.dispatcher.Queues;
import org.openvpms.archetype.rules.laboratory.LaboratoryRules;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.order.OrderConfirmation;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of {@link OrderDispatcher}.
 * <p/>
 * Dispatching doesn't start till the application context is started, to ensure plugins are available.
 *
 * @author Tim Anderson
 */
public class OrderDispatcherImpl extends Dispatcher<Act, Entity, OrderQueue> implements OrderDispatcher {

    /**
     * The laboratory services.
     */
    private final LaboratoryServices laboratoryServices;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * The order service.
     */
    private final OrderService orderService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(OrderDispatcherImpl.class);


    /**
     * Constructs a {@link OrderDispatcherImpl}.
     *
     * @param laboratoryServices the laboratory services
     * @param service            the archetype service
     * @param domainService      the domain object service
     * @param rules              the laboratory rules
     */
    public OrderDispatcherImpl(LaboratoryServices laboratoryServices, IArchetypeService service,
                               DomainService domainService, PracticeService practiceService, LaboratoryRules rules) {
        this(laboratoryServices, service, domainService, practiceService, new OrderService(service, rules));
    }

    /**
     * Constructs a {@link OrderDispatcherImpl}.
     *
     * @param laboratoryServices the laboratory services
     * @param service            the archetype service
     * @param domainService      the domain object service
     * @param orderService       the order service
     */
    protected OrderDispatcherImpl(LaboratoryServices laboratoryServices, IArchetypeService service,
                                  DomainService domainService, PracticeService practiceService,
                                  OrderService orderService) {
        this(laboratoryServices, service, domainService, practiceService, orderService,
             new OrderQueues(orderService, service));
    }

    /**
     * Constructs a {@link OrderDispatcherImpl}.
     *
     * @param laboratoryServices the laboratory services
     * @param service            the archetype service
     * @param domainService      the domain object service
     * @param orderService       the order service
     * @param orderQueues        the order queues
     */
    protected OrderDispatcherImpl(LaboratoryServices laboratoryServices, IArchetypeService service,
                                  DomainService domainService, PracticeService practiceService,
                                  OrderService orderService, Queues<Entity, OrderQueue> orderQueues) {
        super(practiceService, log);
        this.laboratoryServices = laboratoryServices;
        this.service = service;
        this.domainService = domainService;
        this.orderService = orderService;
        init(orderQueues);
    }

    /**
     * Creates a laboratory order.
     * <p/>
     * If the investigation has its <em>confirmOrder</em> set to {@code true}, the order will be created for
     * manual submission, otherwise it will be queued.
     *
     * @param investigation the investigation to place the order for
     */
    @Override
    public void create(Act investigation) {
        IMObjectBean bean = service.getBean(investigation);
        boolean confirmOrder = bean.getBoolean("confirmOrder");
        Act order = bean.getTarget("order", Act.class);
        if (order == null) {
            if (confirmOrder) {
                orderService.createOrder(investigation, false);
            } else {
                orderService.createOrder(investigation, true);
                schedule();
            }
        }
    }

    /**
     * Places an order synchronously.
     *
     * @param investigation the investigation
     * @return the order confirmation, or {@code null} if no confirmation is required
     * @throws LaboratoryException if the laboratory cannot be contacted
     */
    @Override
    public Confirmation order(Act investigation) {
        IMObjectBean bean = service.getBean(investigation);
        Act orderAct = bean.getTarget("order", Act.class);
        Entity laboratory = bean.getTarget("laboratory", Entity.class);
        if (laboratory == null) {
            throw new IllegalStateException("Investigation has no laboratory");
        }
        if (orderAct == null) {
            orderAct = orderService.createOrder(investigation, false);
        }
        Order order = domainService.create(orderAct, Order.class);
        if (!Order.Type.NEW.equals(order.getType())) {
            throw new IllegalStateException("Expected NEW order but got " + order.getType());
        }

        LaboratoryService laboratoryService = laboratoryServices.getService(laboratory);

        if (Order.Status.PENDING.equals(order.getStatus())) {
            laboratoryService.order(order);
        }
        OrderConfirmation confirmation = null;
        if (Order.Status.CONFIRM.equals(order.getStatus())) {
            confirmation = laboratoryService.getOrderConfirmation(order);
        }
        return (confirmation != null) ? new Confirmation(order, confirmation, laboratoryService) : null;
    }

    /**
     * Queues an order cancellation.
     *
     * @param investigation the investigation to cancel
     */
    @Override
    public void cancel(Act investigation) {
        IMObjectBean bean = service.getBean(investigation);
        Act order = bean.getTarget("order", Act.class);

        if (order != null) {
            Entity laboratory = service.getBean(order).getTarget("laboratory", Entity.class);
            if (laboratory != null) {
                OrderQueue queue = getQueue(laboratory);
                if (queue != null && queue.cancel(order)) {
                    schedule();
                }
            } else {
                log.error("No laboratory for for order=" + order.getId());
            }
        } else {
            log.error("No order for investigation=" + investigation.getId());
        }
    }

    /**
     * Processes an object.
     * <p/>
     * If the object is successfully processed, {@link Queue#processed()} should be invoked, otherwise an
     * exception should be raised.
     *
     * @param object the object to process
     * @param queue  the queue
     */
    @Override
    protected void process(Act object, OrderQueue queue) {
        Entity laboratory = queue.getOwner();
        LaboratoryService laboratoryService = laboratoryServices.getService(laboratory);
        Order order = domainService.create(object, Order.class);
        IMObjectBean bean = service.getBean(object);
        if (Order.Type.NEW.name().equals(bean.getString("type"))) {
            laboratoryService.order(order);
        } else {
            laboratoryService.cancel(order);
        }
        queue.processed();
    }

    /**
     * Returns a string representation of an object.
     *
     * @param object the object
     * @return a string representation of the object
     */
    @Override
    protected String toString(Act object) {
        return object.getObjectReference().toString();
    }
}
