/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.service;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.laboratory.order.Order;
import org.openvpms.laboratory.service.OrderQuery;
import org.openvpms.laboratory.service.Orders;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Default implementation of {@link Orders}.
 *
 * @author Tim Anderson
 */
public class OrdersImpl implements Orders {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The order factory.
     */
    private final OrderFactory factory;

    /**
     * Constructs an {@link OrdersImpl}.
     *
     * @param service            the archetype service
     * @param patientRules       the patient rules
     * @param factory            the domain object service
     * @param transactionManager the transaction manager
     */
    public OrdersImpl(ArchetypeService service, PatientRules patientRules, DomainService factory,
                      PlatformTransactionManager transactionManager) {
        this.service = service;
        this.factory = new OrderFactory(service, patientRules, factory, transactionManager);
    }

    /**
     * Returns an order for an investigation.
     *
     * @param id the investigation identifier
     * @return the order or {@code null} if none is found
     */
    @Override
    public Order getOrder(long id) {
        // NOTE: the investigation id is stored on the order to allow order retrieval after an investigation has been
        // deleted. This may be necessary for laboratories to cancel an order associated with a deleted investigation
        return getQuery().investigationId(id).getFirstResult();
    }

    /**
     * Returns an order, given its laboratory id.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @return the order or {@code null} if none is found
     */
    @Override
    public Order getOrder(String archetype, String id) {
        return getQuery().orderId(archetype, id).getFirstResult();
    }

    /**
     * Returns the order associated with an investigation.
     *
     * @param investigation the investigation
     * @return the corresponding order, or {@code null} if none exists
     */
    public Order getOrder(Act investigation) {
        IMObjectBean bean = service.getBean(investigation);
        Act order = bean.getTarget("order", Act.class);
        return order != null ? factory.create(order) : null;
    }

    /**
     * Returns an order query.
     *
     * @return the order query
     */
    @Override
    public OrderQuery getQuery() {
        return new OrderQueryImpl(service, factory);
    }

}
