/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.internal.report;

import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.document.DocumentDecorator;
import org.openvpms.component.model.document.Document;

import java.io.IOException;
import java.io.InputStream;

/**
 * A wrapper a {@link Document} that decompresses the content if required.
 *
 * @author Tim Anderson
 */
class DecompressingDocument extends DocumentDecorator {

    /**
     * The underlying document.
     */
    private final Document document;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * Constructs a {@link DecompressingDocument}.
     *
     * @param document the document
     * @param handlers the document handlers
     */
    public DecompressingDocument(Document document, DocumentHandlers handlers) {
        super(document);
        this.document = document;
        this.handlers = handlers;
    }

    /**
     * Returns the document content.
     *
     * @return the content
     * @throws IOException for any I/O error
     */
    @Override
    public InputStream getContent() throws IOException {
        // need to return the decompressed content
        DocumentHandler handler = handlers.get(document);
        return handler.getContent(document);
    }

    /**
     * Sets the document content.
     *
     * @param stream a stream of the content
     * @throws UnsupportedOperationException if invoked - this document is read-only
     */
    @Override
    public void setContent(InputStream stream) {
        throw new UnsupportedOperationException("This document may only be updated via the ReportBuilder");
    }
}