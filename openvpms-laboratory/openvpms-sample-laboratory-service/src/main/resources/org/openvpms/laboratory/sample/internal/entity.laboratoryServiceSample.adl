<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="entity.laboratoryServiceSample.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.common.Entity" displayName="Sample Laboratory Service">
        <node name="id" path="/id" type="java.lang.Long" hidden="true" readOnly="true"/>
        <node name="name" type="java.lang.String" path="/name" minCardinality="1" maxLength="100"
              defaultValue="'Sample Laboratory Service'"/>
        <node name="description" type="java.lang.String" path="/description"/>
        <node name="active" path="/active" type="java.lang.Boolean" defaultValue="true()"/>
        <node name="orderDelay" path="/details/orderDelay" type="java.lang.Integer" defaultValue="30"
              description="The no. of seconds to wait before submitting an order, or 0 to act synchronously"/>
        <node name="resultsDelay" path="/details/resultsDelay" type="java.lang.Integer" defaultValue="30"
              description="The no. of seconds to wait before generating results after submission, or 0 to act synchronously"/>
        <node name="cancelDelay" path="/details/cancelDelay" type="java.lang.Integer" defaultValue="30"
              description="The no. of seconds to wait before cancelling an order, or 0 to act synchronously"/>
        <node name="locations" path="/entityLinks" type="java.util.HashSet" baseName="EntityLink"
              minCardinality="0" maxCardinality="*" filter="entityLink.laboratoryLocation"/>
    </archetype>
</archetypes>
