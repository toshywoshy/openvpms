/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import org.openvpms.component.model.document.Document;

import java.math.BigDecimal;

/**
 * A result for an individual analyte.
 *
 * @author Tim Anderson
 */
public interface Result {

    /**
     * Result status.
     *
     * @author Tim Anderson
     */
    enum Status {
        PENDING,
        IN_PROGRESS,
        COMPLETED,
        CANCELLED
    }

    /**
     * Returns the result identifier.
     *
     * @return the result identifier
     */
    String getResultId();

    /**
     * Returns the result status.
     *
     * @return the result status
     */
    Status getStatus();

    /**
     * Returns an identifier for the analyte.
     *
     * @return the analyte identifier. May be {@code null}
     */
    String getAnalyteCode();

    /**
     * Returns the name of the analyte for which the result is being reported.
     *
     * @return the analyte name
     */
    String getAnalyteName();

    /**
     * Returns the numerical result for the analyte.
     *
     * @return the numerical result for the analyte. May be {@code null}
     */
    BigDecimal getValue();

    /**
     * Returns the result for the analyte, as text.
     *
     * @return the result text. May be {@code null}
     */
    String getResult();

    /**
     * Returns the unit of measure for the analyte result.
     *
     * @return the unit of measure. May be {@code null}
     */
    String getUnits();

    /**
     * Returns the qualifier.
     *
     * @return the qualifier. May be {@code null}
     */
    String getQualifier();

    /**
     * Returns the low reference range value for the analyte.
     *
     * @return the low reference range value. May be {@code null}
     */
    BigDecimal getLowRange();

    /**
     * Returns the high reference range value for the analyte.
     *
     * @return the high reference range value. May be {@code null}
     */
    BigDecimal getHighRange();

    /**
     * Returns the extreme low reference range value for the analyte.
     *
     * @return the extreme low reference range value. May be {@code null}
     */
    BigDecimal getExtremeLowRange();

    /**
     * Returns the extreme high reference range value for the analyte.
     *
     * @return the extreme high reference range value. May be {@code null}
     */
    BigDecimal getExtremeHighRange();

    /**
     * Determines if the result is out of range.
     *
     * @return {@code true} if the result is out of range, otherwise {@code false}
     */
    boolean getOutOfRange();

    /**
     * Returns the reference range.
     *
     * @return the reference range. May be {@code null}
     */
    String getReferenceRange();

    /**
     * Returns a short narrative of the result.
     *
     * @return the result narrative. May be {@code null}
     */
    String getNotes();

    /**
     * Returns the result image.
     *
     * @return the document, or {@code null} if the result has no image.
     */
    Document getImage();

}
