/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.laboratory.exception.LaboratoryException;

/**
 * Locates {@link LaboratoryService} instances.
 *
 * @author Tim Anderson
 */
public interface LaboratoryServices {

    /**
     * Returns the laboratory for the specified investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the laboratory, or {@code null} if the investigation type is not associated with a laboratory at the
     * specified location
     */
    Entity getLaboratory(Entity investigationType, Party location);

    /**
     * Returns the laboratory service for the specified investigation type and practice location.
     *
     * @param investigationType the investigation type
     * @param location          the practice location
     * @return the laboratory service, or {@code null} if the investigation type is not associated with a laboratory
     * service at the specified location
     * @throws LaboratoryException if the service is unavailable
     */
    LaboratoryService getService(Entity investigationType, Party location);

    /**
     * Returns the laboratory service for the specified laboratory.
     *
     * @param laboratory the laboratory. Must be an <em>entity.laboratoryService*</em>
     * @return the laboratory service
     * @throws LaboratoryException if the service is unavailable
     */
    LaboratoryService getService(Entity laboratory);

}
