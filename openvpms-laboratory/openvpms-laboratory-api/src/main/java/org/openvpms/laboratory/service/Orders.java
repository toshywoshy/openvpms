/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.laboratory.order.Order;

/**
 * Orders service.
 *
 * @author Tim Anderson
 */
public interface Orders {

    /**
     * Returns an order for an investigation.
     *
     * @param id the investigation identifier
     * @return the order or {@code null} if none is found
     */
    Order getOrder(long id);

    /**
     * Returns an order, given its laboratory id.
     * <p>
     * An order can have a single identifier issued by a laboratory. To avoid duplicates, each laboratory service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.laboratoryOrder</em> prefix.
     * @param id        the order identifier
     * @return the order or {@code null} if none is found
     */
    Order getOrder(String archetype, String id);

    /**
     * Returns an order query.
     *
     * @return the order query
     */
    OrderQuery getQuery();

}
