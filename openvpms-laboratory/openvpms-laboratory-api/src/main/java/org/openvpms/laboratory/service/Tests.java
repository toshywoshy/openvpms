/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.domain.laboratory.Test;

import java.util.List;

/**
 * Manages laboratory tests.
 *
 * @author Tim Anderson
 */
public interface Tests {

    /**
     * Returns a laboratory test.
     * <p>
     * Tests managed by an {@link LaboratoryService} must have a unique code.
     *
     * @param archetype the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param code      the test code
     * @return the test, or {@code null} if none is found. The returned test may be inactive
     */
    Test getTest(String archetype, String code);

    /**
     * Returns all tests with the specified test code archetype.
     *
     * @param archetype  the test code archetype. Must be an <em>entityIdentity.laboratoryTest*</em>
     * @param activeOnly if {@code true}, only return active tests
     * @return the tests
     */
    List<Test> getTests(String archetype, boolean activeOnly);

    /**
     * Returns a builder to build tests.
     *
     * @return the test builder
     */
    TestBuilder getTestBuilder();

}
