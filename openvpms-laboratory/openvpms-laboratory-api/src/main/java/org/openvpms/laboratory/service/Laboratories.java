/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.domain.laboratory.Laboratory;

import java.util.List;

/**
 * Manages laboratories.
 *
 * @author Tim Anderson
 */
public interface Laboratories {

    /**
     * Returns a laboratory given its reference.
     *
     * @param reference the reference
     * @return the corresponding laboratory, or {@code null} if none is found
     */
    Laboratory getLaboratory(Reference reference);

    /**
     * Return a laboratory corresponding to an object.
     *
     * @param object the object
     * @return the corresponding laboratory
     */
    Laboratory getLaboratory(IMObject object);

    /**
     * Returns laboratories of the specified archetype.
     *
     * @param archetype  the archetype. Must be an <em>entity.laboratoryService*</em>
     * @param activeOnly if {@code true}, only return active laboratories
     * @return the laboratories
     */
    List<Laboratory> getLaboratories(String archetype, boolean activeOnly);

}
