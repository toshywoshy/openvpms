/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.service;

import org.openvpms.domain.laboratory.Device;

import java.util.List;

/**
 * Manages laboratory devices.
 *
 * @author Tim Anderson
 */
public interface Devices {

    /**
     * Returns a laboratory device.
     * <p>
     * Devices managed by an {@link LaboratoryService} must have a unique code.
     *
     * @param archetype the device identity archetype. Must be an <em>entityIdentity.laboratoryDevice*</em>
     * @param deviceId  the device identifier
     * @return the device, or {@code null} if none is found. The returned device may be inactive
     */
    Device getDevice(String archetype, String deviceId);

    /**
     * Returns all devices with the specified device identity archetype.
     *
     * @param archetype  the device identity archetype. Must be an <em>entityIdentity.laboratoryDevice*</em>
     * @param activeOnly if {@code true}, only return active devices
     * @return the tests
     */
    List<Device> getDevices(String archetype, boolean activeOnly);

    /**
     * Returns a builder to build devices.
     *
     * @return the device builder
     */
    DeviceBuilder getDeviceBuilder();

}
