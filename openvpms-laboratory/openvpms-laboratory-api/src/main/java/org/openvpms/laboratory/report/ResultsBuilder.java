/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import org.openvpms.domain.laboratory.Test;

import java.time.OffsetDateTime;

/**
 * Builds {@link Results}.
 *
 * @author Tim Anderson
 */
public interface ResultsBuilder {

    /**
     * Sets the results status.
     * <p/>
     * If not set, the status will be derived from the results.
     *
     * @param status the status
     * @return this
     */
    ResultsBuilder status(Result.Status status);

    /**
     * Sets the date/time when the results where produced.
     *
     * @param date the date/time
     * @return this
     */
    ResultsBuilder date(OffsetDateTime date);

    /**
     * Sets the test that the results are for.
     *
     * @param test the test. May be {@code null}
     * @return this
     */
    ResultsBuilder test(Test test);

    /**
     * Sets the result category code.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @param code the result category code
     * @return this
     */
    ResultsBuilder categoryCode(String code);

    /**
     * Sets the result category name.
     * <p/>
     * This can be used classify results that aren't associated with a test.
     *
     * @param name the result category name
     * @return this
     */
    ResultsBuilder categoryName(String name);

    /**
     * Sets a short narrative of the results.
     *
     * @param notes the notes. May be {@code null}
     * @return this
     */
    ResultsBuilder notes(String notes);

    /**
     * Returns a {@link Result} builder.
     * <p/>
     * This updates the result with the specified id if it exists, or creates a new result if it doesn't.     *
     *
     * @param id the result identifier. This need only be unique within a single {@link Results}
     * @return the result builder
     */
    ResultBuilder result(String id);

    /**
     * Builds the {@link Results}, adding it to the report.
     * <p/>
     * The builder is reset.
     *
     * @return the results
     */
    Results build();

    /**
     * Builds the {@link Results}, adding it to the report.
     *
     * @return the parent builder
     */
    ReportBuilder add();

}
