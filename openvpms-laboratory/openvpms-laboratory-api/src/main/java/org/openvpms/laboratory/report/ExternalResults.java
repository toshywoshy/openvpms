/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.report;

import org.openvpms.laboratory.exception.LaboratoryException;
import org.openvpms.laboratory.resource.Resources;

/**
 * Used to represent laboratory results that are available externally.
 *
 * @author Tim Anderson
 */
public interface ExternalResults {

    /**
     * Determines if the results may be viewed in a browser.
     *
     * @return {@code true} if the results may be viewed in a browser, otherwise {@code false}
     * @throws LaboratoryException for any error
     */
    boolean canView();

    /**
     * Returns the view for the external results.
     *
     * @return the view of the results, or {@code null} if this is not supported
     * @throws LaboratoryException for any error
     */
    View getView();

    /**
     * Determines if the results can be retrieved as documents.
     * <p/>
     * If supported, these are available via {@link #getDocuments()}.
     *
     * @return {@code true} if the results can be retrieved as documents, otherwise {@code false}
     * @throws LaboratoryException for any error
     */
    boolean hasDocuments();

    /**
     * Returns the external documents.
     *
     * @return the external documents
     * @throws LaboratoryException for any error
     */
    Resources getDocuments();

}
