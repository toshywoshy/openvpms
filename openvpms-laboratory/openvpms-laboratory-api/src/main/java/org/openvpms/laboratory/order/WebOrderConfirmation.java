/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.order;

/**
 * Confirmation for an order done via the browser.
 * <p/>
 * The URL will displayed in an <em>iframe</em>, and must send a message indicating it has completed via
 * <a href="https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage">Window.postMessage</a>
 * <p/>
 * Only messages from the same origin as the URL will be accepted.
 *
 * @author Tim Anderson
 */
public interface WebOrderConfirmation extends OrderConfirmation {

    /**
     * Returns the order confirmation URL.
     *
     * @return the order confirmation URL
     */
    String getUrl();

    /**
     * Determines if the message from {@code Window.postMessage} indicates that the order was confirmed.
     *
     * @param message the message
     * @return {@code true} if the message indicates the order was confirmed, otherwise {@code false}
     */
    boolean isConfirmed(String message);

    /**
     * Returns the window width.
     *
     * @return the window width, in pixels
     */
    int getWidth();

    /**
     * Returns the window height.
     *
     * @return the window height, in pixels
     */
    int getHeight();
}
