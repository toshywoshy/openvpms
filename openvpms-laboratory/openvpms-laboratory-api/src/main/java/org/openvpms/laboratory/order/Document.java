/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.laboratory.order;

import java.io.IOException;
import java.io.InputStream;

/**
 * Provides access to the document to include when submitting samples for an {@link Order}.
 * <p/>
 * Documents should be PDF, DOCX or ODT format.
 *
 * @author Tim Anderson
 */
public interface Document {

    /**
     * Returns a display name for the document.
     *
     * @return the document display name
     */
    String getDisplayName();

    /**
     * Returns the document file name.
     *
     * @return the file name
     */
    String getName();

    /**
     * Returns the document mime type.
     *
     * @return the mime type
     */
    String getMimeType();

    /**
     * Returns a stream to the submission document.
     *
     * @return the stream
     * @throws IOException for any I/O error
     */
    InputStream getInputStream() throws IOException;

}
