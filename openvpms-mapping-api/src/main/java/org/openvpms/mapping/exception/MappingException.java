/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.exception;

import org.openvpms.component.exception.OpenVPMSException;
import org.openvpms.component.i18n.Message;

/**
 * Mapping exception.
 *
 * @author Tim Anderson
 */
public class MappingException extends OpenVPMSException {

    /**
     * Constructs a {@link MappingException}.
     *
     * @param message the exception message
     */
    public MappingException(Message message) {
        super(message);
    }

    /**
     * Constructs a {@link MappingException}.
     *
     * @param message the exception message
     * @param cause   the cause
     */
    public MappingException(Message message, Throwable cause) {
        super(message, cause);
    }
}
