/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.model;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Abstract implementation of {@link Targets}.
 * <p/>
 * This can be used where the number of available targets is small.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTargets implements Targets {

    /**
     * The display name.
     */
    private final String displayName;

    /**
     * Constructs an {@link AbstractTargets}.
     *
     * @param displayName the display name
     */
    public AbstractTargets(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Returns a display name for the target object type.
     *
     * @return the display name
     */
    @Override
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Creates a target.
     *
     * @param identity the target object identifier
     * @param name     the target object display name
     * @param active   determines if the target object is active
     * @return a new target
     */
    @Override
    public Target create(String identity, String name, boolean active) {
        return new DefaultTarget(identity, name, active);
    }

    /**
     * Returns a target given its identifier.
     *
     * @param identity the target object identifier
     * @return the target, or {@code null} if none exists
     */
    @Override
    public Target getTarget(String identity) {
        Target result = null;
        for (Target target : getTargets()) {
            if (identity.equals(target.getId())) {
                result = target;
                break;
            }
        }
        return result;
    }

    /**
     * Returns the available targets.
     *
     * @param mappings    the mappings
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve, or {@code -1} to retrieve all results
     * @return the available targets
     */
    @Override
    public List<Target> getTargets(Mappings mappings, String name, boolean unmapped, int firstResult, int maxResults) {
        List<Target> results = new ArrayList<>();
        int matches = 0;
        for (Target target : getTargets()) {
            if (matches(target, mappings, name, unmapped)) {
                if (matches >= firstResult) {
                    results.add(target);
                }
                matches++;
                if (maxResults != -1 && results.size() == maxResults) {
                    break;
                }
            }
        }
        return results;
    }

    /**
     * Returns a count of the targets matching the criteria.
     *
     * @param mappings the mappings
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of targets matching the criteria
     */
    @Override
    public int count(Mappings mappings, String name, boolean unmapped) {
        int matches = 0;
        for (Target target : getTargets()) {
            if (matches(target, mappings, name, unmapped)) {
                matches++;
            }
        }
        return matches;
    }

    /**
     * Returns the available targets.
     *
     * @return the available targets
     */
    protected abstract Collection<Target> getTargets();

    /**
     * Determines if a target matches the specified criteria.
     *
     * @param target   the target
     * @param mappings the existing mappings
     * @param name     the name to match, or {@code null} to match all names
     * @param unmapped if {@code true}, only include unmapped targets
     * @return {@code true} if the target matches, otherwise {@code false}
     */
    private boolean matches(Target target, Mappings mappings, String name, boolean unmapped) {
        boolean result = false;
        if (!unmapped || mappings.getMapping(target.getId()) == null) {
            if (name == null) {
                result = true;
            } else {
                String targetName = target.getName();
                result = (targetName != null && StringUtils.containsIgnoreCase(targetName, name));
            }
        }
        return result;
    }
}
