/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.mapping.model;


import java.util.Objects;

/**
 * Default implementation of {@link Target}.
 *
 * @author Tim Anderson
 */
public class DefaultTarget implements Target {

    /**
     * The target object identifier.
     */
    private final String id;

    /**
     * The target object display name.
     */
    private final String name;

    /**
     * Determines if the target object is active.
     */
    private final boolean active;

    /**
     * Constructs a {@link DefaultTarget}.
     *
     * @param id     the target object identifier
     * @param name   the target object display name
     * @param active determines if the target object is active.
     */
    public DefaultTarget(long id, String name, boolean active) {
        this(Long.toString(id), name, active);
    }

    /**
     * Constructs a {@link DefaultTarget}.
     *
     * @param id     the target object identifier
     * @param name   the target object display name
     * @param active determines if the target object is active.
     */
    public DefaultTarget(String id, String name, boolean active) {
        this.id = id;
        this.name = name;
        this.active = active;
    }

    /**
     * Returns the target object identifier.
     *
     * @return the target object identifier
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * Returns a display name for the target object.
     *
     * @return a display name for the target object
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Determines if the target object is active.
     *
     * @return {@code true} if the target object is active, {@code false} if it is inactive
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj instanceof DefaultTarget) {
            DefaultTarget other = (DefaultTarget) obj;
            // need to do name comparisons, as both the id and name are stored. The name check forces the store
            // to update
            return Objects.equals(id, other.getId()) && Objects.equals(name, other.getName());
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
