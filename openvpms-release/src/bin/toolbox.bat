@echo off

setlocal

set "CURRENT_DIR=%cd%"
if not "%OPENVPMS_HOME%" == "" goto gotHome
set "OPENVPMS_HOME=%CURRENT_DIR%"
if exist "%OPENVPMS_HOME%\bin\setenv.bat" goto okHome
cd ..
set "OPENVPMS_HOME=%cd%"
cd "%CURRENT_DIR%"
:gotHome
set "ENVSCRIPT=%OPENVPMS_HOME%\bin\setenv.bat"
if exist "%ENVSCRIPT%" goto okHome
echo Cannot find "%ENVSCRIPT%"
goto end
:okHome

call "%ENVSCRIPT%"

java -classpath "%CLASSPATH%" "-Dopenvpms.home=%OPENVPMS_HOME%" "-Dlog4j.configurationFile=%OPENVPMS_HOME%/conf/log4j2.xml" org.openvpms.tool.toolbox.Toolbox %*%

:end