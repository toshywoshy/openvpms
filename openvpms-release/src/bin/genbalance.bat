@echo off

call setenv.bat

java -Xmx512m -Dlog4j.configurationFile=file:../conf/log4j2.xml org.openvpms.archetype.tools.account.AccountBalanceTool --context ../conf/applicationContext.xml %*%