@echo off

call setenv.bat

java -classpath %CLASSPATH% -Dlog4j.configurationFile=file:../conf/log4j2.xml org.openvpms.archetype.tools.reminder.ReminderTool --context ../conf/applicationContext.xml %*%
