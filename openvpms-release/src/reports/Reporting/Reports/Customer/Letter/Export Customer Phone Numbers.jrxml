<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Export Customer Phone Numbers" pageWidth="792" pageHeight="612" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="792" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="ca8d8887-bdd7-456c-a56e-f8d80f29fe77">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="0.75"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="OpenVPMS"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" vTextAlign="Middle" fontSize="10"/>
	<parameter name="Species" class="java.lang.String">
		<parameterDescription><![CDATA[Species selection]]></parameterDescription>
	</parameter>
	<parameter name="Breed" class="java.lang.String">
		<parameterDescription><![CDATA[Breed selection]]></parameterDescription>
	</parameter>
	<parameter name="DOB Start" class="java.util.Date">
		<parameterDescription><![CDATA[Date of Birth from]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date("01/01/1980")]]></defaultValueExpression>
	</parameter>
	<parameter name="DOB End" class="java.util.Date">
		<parameterDescription><![CDATA[Date of Birth to]]></parameterDescription>
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="Classification" class="java.lang.String">
		<parameterDescription><![CDATA[Acct Type or Category]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="PracticeLocation" class="java.lang.String">
		<parameterDescription><![CDATA[Practice Location]]></parameterDescription>
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="SMS" class="java.lang.Boolean">
		<parameterDescription><![CDATA[SMS only]]></parameterDescription>
		<defaultValueExpression><![CDATA[true]]></defaultValueExpression>
	</parameter>
	<parameter name="Explain" class="java.lang.Boolean">
		<parameterDescription><![CDATA[Display explanation ?]]></parameterDescription>
		<defaultValueExpression><![CDATA[false]]></defaultValueExpression>
	</parameter>
	<queryString language="SQL">
		<![CDATA[SELECT customer.entity_id              AS id,
       last_name.value                                  AS lastname,
       first_name.value                                 AS firstname,
       title_lookup.name                                AS title,
       customer.description                             AS description,
       c_phone.name                                     AS phoneName,
       CONCAT(IFNULL(area_code.value, ''), phone.value) AS phone,
       sms.value                                        AS sms,
       IFNULL(contact_rank, 0)                          AS contact_rank
FROM entities customer
         LEFT JOIN entity_details last_name
                   ON last_name.entity_id = customer.entity_id
                       AND last_name.name = 'lastName'
         LEFT JOIN entity_details first_name
                   ON first_name.entity_id = customer.entity_id
                       AND first_name.name = 'firstName'
         LEFT JOIN entity_details title
                   ON title.entity_id = customer.entity_id AND title.name = 'title'
         LEFT JOIN lookups title_lookup
                   ON title_lookup.code = title.value
                       AND title_lookup.arch_short_name = 'lookup.personTitle'
         LEFT JOIN entity_links l_location
                   ON l_location.source_id = customer.entity_id
                       AND l_location.arch_short_name = 'entityLink.customerLocation'
         LEFT JOIN entities location
                   ON location.entity_id = l_location.target_id
         JOIN (SELECT c1.party_id,
                      SUBSTRING_INDEX(GROUP_CONCAT(c1.contact_id ORDER BY c1.contact_id DESC), ',', 1) AS contact_id,
                      SUBSTRING_INDEX(GROUP_CONCAT(c1.contact_rank ORDER BY c1.contact_rank DESC), ',',
                                      1)                                                               AS contact_rank
               FROM (SELECT c_contact.party_id,
                            c_contact.contact_id,
                            IF((preferred.value = 'true'), IF((sms.value = 'true'), 30, 10),
                               IF((sms.value = 'true'), 20, 0)) AS contact_rank
                     FROM contacts c_contact
                              JOIN contact_details phone
                                      ON phone.contact_id = c_contact.contact_id
                                          AND phone.name = 'telephoneNumber'
                              LEFT JOIN contact_details preferred
                                        ON preferred.contact_id = c_contact.contact_id
                                            AND preferred.name = 'preferred'
                              LEFT JOIN contact_details sms
                                        ON sms.contact_id = c_contact.contact_id
                                            AND sms.name = 'sms'
                     WHERE c_contact.arch_short_name = 'contact.phoneNumber'
                              AND (NOT $P{SMS} OR sms.value = 'true')) c1
                        LEFT JOIN (
                   SELECT c_contact.party_id,
                          c_contact.contact_id,
                          IF((preferred.value = 'true'), IF((sms.value = 'true'), 30, 10),
                             IF((sms.value = 'true'), 20, 0)) AS contact_rank
                   FROM contacts c_contact
                            JOIN contact_details phone
                                      ON phone.contact_id = c_contact.contact_id
                                          AND phone.name = 'telephoneNumber'
                            LEFT JOIN contact_details preferred
                                      ON preferred.contact_id = c_contact.contact_id
                                          AND preferred.name = 'preferred'
                            LEFT JOIN contact_details sms
                                      ON sms.contact_id = c_contact.contact_id
                                          AND sms.name = 'sms'
                   WHERE c_contact.arch_short_name = 'contact.phoneNumber'
                              AND (NOT $P{SMS} OR sms.value = 'true')) c2
                                  ON c1.party_id = c2.party_id
                                      AND ((c2.contact_rank > c1.contact_rank)
                                          OR c2.contact_rank = c1.contact_rank AND c2.contact_id < c1.contact_id)
               WHERE c2.party_id IS NULL
               GROUP BY c1.party_id) unique_contact
              ON unique_contact.party_id = customer.entity_id
         LEFT JOIN contacts c_phone
                   ON c_phone.contact_id = unique_contact.contact_id
         LEFT JOIN contact_details area_code
                   ON area_code.contact_id = unique_contact.contact_id
                       AND area_code.name = 'areaCode'
         LEFT JOIN contact_details phone
                   ON phone.contact_id = unique_contact.contact_id
                       AND phone.name = 'telephoneNumber'
         LEFT JOIN contact_details sms
                   ON sms.contact_id = unique_contact.contact_id
                       AND sms.name = 'sms'
WHERE customer.arch_short_name = 'party.customerperson'
  AND customer.active = 1
  AND EXISTS(SELECT 1
             FROM entity_relationships r_owner
                      JOIN entities patient
                           ON patient.entity_id = r_owner.target_id
                      LEFT JOIN entity_details birth_date
                                ON birth_date.entity_id = patient.entity_id
                                    AND birth_date.name = 'dateOfBirth'
                      LEFT JOIN entity_details species
                                ON species.entity_id = patient.entity_id
                                    AND species.name = 'species'
                      LEFT JOIN lookups species_lookup
                                ON species_lookup.code = species.value
                                    AND species_lookup.arch_short_name = 'lookup.species'
                      LEFT JOIN entity_details breed
                                ON breed.entity_id = patient.entity_id
                                    AND breed.name = 'breed'
                      LEFT JOIN lookups breed_lookup
                                ON breed_lookup.code = breed.value
                                    AND breed_lookup.arch_short_name = 'lookup.breed'
             WHERE r_owner.source_id = customer.entity_id
               AND r_owner.arch_short_name = 'entityRelationship.patientOwner'
               AND r_owner.active_end_time IS NULL
               AND patient.active = 1
               AND species_lookup.name LIKE CONCAT(IFNULL($P{Species}, ''), '%')
               AND ((breed_lookup.name IS NULL AND $P{Breed} IS NULL) OR
                    (breed_lookup.name LIKE CONCAT(IFNULL($P{Breed}, ''), '%')))
               AND ((birth_date.value >= $P{DOB Start} AND birth_date.value < DATE_ADD($P{DOB End}, INTERVAL 1 DAY))
                 OR birth_date.value IS NULL))
  AND (IFNULL($P{Classification}, 1) OR EXISTS(
        SELECT 1
        FROM entity_classifications c_classification
                 JOIN lookups l_type
                      ON l_type.lookup_id = c_classification.lookup_id
                          AND l_type.arch_short_name IN ('lookup.customerAccountType', 'lookup.customerType')
                          AND l_type.name LIKE CONCAT($P{Classification}, '%')
        WHERE c_classification.entity_id = customer.entity_id))
  AND IFNULL(location.name, '-') LIKE CONCAT(IFNULL($P{PracticeLocation}, ''), '%')
ORDER BY customer.name, customer.entity_id]]>
	</queryString>
	<field name="id" class="java.lang.Long"/>
	<field name="lastname" class="java.lang.String"/>
	<field name="firstname" class="java.lang.String"/>
	<field name="title" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="phoneName" class="java.lang.String"/>
	<field name="phone" class="java.lang.String"/>
	<field name="sms" class="java.lang.String"/>
	<field name="contact_rank" class="java.lang.Integer"/>
	<group name="id-group" minHeightToStartNewPage="18">
		<groupExpression><![CDATA[$F{id}]]></groupExpression>
		<groupHeader>
			<band height="19">
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="0" y="0" width="75" height="13" uuid="ccd5131d-9550-4efb-81e7-996c7fefa95d"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="75" y="0" width="75" height="13" uuid="af3aab31-ba9c-4906-ae39-838efa2944d1"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{title}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="150" y="0" width="76" height="13" uuid="16c11acd-fea5-4dea-8b88-9541848d2d1b"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{lastname}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="226" y="0" width="75" height="13" uuid="61029f88-919b-4ec3-bdae-24498f2d8e76"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{firstname}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" pattern="" isBlankWhenNull="true">
					<reportElement key="textField" x="301" y="0" width="150" height="13" uuid="62806c41-eb86-4aeb-8a58-19cc6b0a6f4c"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{description}.replace( '\n', ' ' )]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="451" y="0" width="76" height="13" uuid="a17be48a-b960-4d4a-b6af-951b4e8a3d30"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{phoneName}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" isBlankWhenNull="true">
					<reportElement key="textField" x="527" y="0" width="75" height="13" uuid="77e70472-4e48-4ecc-a575-7105e1bf4446"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{phone} != null ? $F{phone}.replaceAll("[\\s\\-()]", "").replaceAll("[^\\d\\+].*", "") : null]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" pattern="" isBlankWhenNull="true">
					<reportElement key="textField-2" x="640" y="0" width="75" height="13" uuid="dea11cef-0045-45b4-8851-4c7ba6f1c8ba">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{contact_rank}]]></textFieldExpression>
				</textField>
				<textField textAdjust="StretchHeight" pattern="" isBlankWhenNull="true">
					<reportElement key="textField-2" x="602" y="0" width="38" height="13" uuid="5451a438-263c-4d97-a2a5-98e5a087fd9f">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font size="8"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{sms}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="4" width="75" height="11" uuid="cff93c63-cfc0-42b9-a1da-e24f474262b6"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[CLIENTID]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-2" x="75" y="4" width="75" height="11" uuid="05603129-970c-458f-bcb0-6a80f8e5d6c6"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[TITLE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="150" y="4" width="76" height="11" uuid="e21c9fcd-a811-4e64-8856-5f6a330b72ad"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[LASTNAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="226" y="4" width="75" height="11" uuid="accc7096-5c3a-4a4f-97b6-125135c4ebaa"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[FIRSTNAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="301" y="4" width="150" height="11" uuid="2eaf724b-7b3c-44e0-9932-597495bd9eb0"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[ADDRESS]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="451" y="4" width="76" height="11" uuid="740c82b1-2616-42ff-815b-1e9dd9735b0b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[PHONE-NAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="527" y="4" width="75" height="11" uuid="774aa61d-158a-4008-9a4e-e6df19a9fe55"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[PHONE-NUMBER]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="640" y="4" width="75" height="11" uuid="9f7fb141-38d1-4406-9aa9-e4d92d28718e">
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[RANK]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="602" y="4" width="38" height="11" uuid="01387466-d289-41a0-9f65-2cf7c4e801b8">
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="8"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[SMS]]></text>
			</staticText>
		</band>
	</columnHeader>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="302" splitType="Prevent">
			<staticText>
				<reportElement x="0" y="1" width="308" height="21" isRemoveLineWhenBlank="true" uuid="347d9bf5-f8c9-4ce9-8136-8bed52701e0f">
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
					<printWhenExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()==0)]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[No Data Found - Check Parameters]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="107" width="790" height="194" isRemoveLineWhenBlank="true" uuid="8603168f-aecb-45a5-89fe-5887a916b007">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<printWhenExpression><![CDATA[$P{Explain}]]></printWhenExpression>
				</reportElement>
				<textElement>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[This report is designed for exporting customer phone contact data for use by marketing organisations.
It finds the customers who match the selection conditions.
The period is inclusive - ie sales from 00:00 on the start date through 23:59:59 on the end date.
All the text selection fields entered have % added so that leaving them blank will find all, and entering say breed %terrier will find all species containing 'terrier' in their species name, and account type 'c' will include all types starting C. The selection is case-insensitive.
The Patient DOB selection is inclusive (ie from <= DOB<= to), but where Patients have no DOB they will be included.
If there is no Practice Location selection then customers with no Practice Location set will be included. Using the selection '-' will select just those with no Practice Location set. The same applies for the Account Type/Category selection.
Only active customers with active patients are included.
Where the customer has multiple contacts, these are ranked as follows:
  If preferred and SMS allowed - score 30; If SMS allowed - score 20; If preferred - score 10; else score 0.
The highest rank contact is used, and if more than one has the highest rank, then the newest is used.
Note that prior to using in mail-merge or other programs, you must remove the information and explanation lines following the data lines.
]]></text>
			</staticText>
			<frame>
				<reportElement x="0" y="21" width="630" height="86" uuid="4f5a3c58-fb51-4570-bcf4-9f1156955f31"/>
				<textField>
					<reportElement x="75" y="14" width="75" height="15" uuid="ace8d18e-c2e4-48d0-beac-0bb9e29eb262">
						<property name="local_mesure_unitheight" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{Species}==null)?"%":$P{Species}+"%"]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="226" y="14" width="75" height="15" uuid="2c794ae1-89a9-44c4-bbb7-c7682c3e2ba1">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{Breed}==null)?"%":$P{Breed}+"%"]]></textFieldExpression>
				</textField>
				<textField pattern="MM/dd/yyyy">
					<reportElement x="75" y="29" width="75" height="14" uuid="5dd261db-e178-4d61-9af0-7a9f31a54e16">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{DOB Start})]]></textFieldExpression>
				</textField>
				<textField pattern="MM/dd/yyyy">
					<reportElement x="226" y="29" width="75" height="14" uuid="e07f7da8-8551-40ff-b8a3-cd5c7dca91e4">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($P{DOB End})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="0" width="74" height="14" uuid="adf331e8-4aeb-4c6d-a9c5-226d127d84cd">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Parameters]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="14" width="74" height="15" uuid="af10e8f0-0e90-4acf-b6f3-986b2fcf83c5">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Species]]></text>
				</staticText>
				<staticText>
					<reportElement x="0" y="29" width="74" height="14" uuid="c69f26ad-33e0-4294-9cb5-dd5564063336">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[DOB From]]></text>
				</staticText>
				<staticText>
					<reportElement x="150" y="14" width="76" height="15" uuid="18467c5d-a264-4817-a0ca-75767d2d550b">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Breed]]></text>
				</staticText>
				<staticText>
					<reportElement x="150" y="29" width="76" height="14" uuid="61dcf8fd-49b6-4221-ae45-dbc74643f954">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[DOB To]]></text>
				</staticText>
				<staticText>
					<reportElement x="451" y="14" width="76" height="15" uuid="f37e2d3e-855d-42b8-92f7-eb6db5a3028f">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Generated]]></text>
				</staticText>
				<textField pattern="dd/MM/yyyy h.mm a">
					<reportElement x="527" y="14" width="94" height="15" uuid="d1fab493-c3fa-491f-9e6e-f0751fd03d61">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="0" y="43" width="74" height="14" uuid="981c69dc-e06e-49f7-b868-4acef1fe96b2">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Acct.Type/Cat]]></text>
				</staticText>
				<textField>
					<reportElement x="75" y="43" width="75" height="14" uuid="515ca780-6d35-4d50-b3c8-0e3198db0746">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{Classification}==null)?"%":$P{Classification}+"%"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="150" y="43" width="76" height="14" uuid="336ae9b8-e6f0-4437-aedd-2f5051cd3777">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Practice Locn]]></text>
				</staticText>
				<textField>
					<reportElement x="226" y="43" width="75" height="14" uuid="4e0631eb-b113-418e-92ca-8ed50edd3223">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[($P{PracticeLocation}==null)?"%":$P{PracticeLocation}+"%"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="451" y="43" width="76" height="14" uuid="0ebef0c2-5385-42d0-b6b7-435356e880d1">
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
						<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[SMS Only]]></text>
				</staticText>
				<textField pattern="dd/MM/yyyy h.mm a">
					<reportElement x="527" y="43" width="94" height="14" uuid="09e86fc8-a93e-4d32-ab6f-0fa45dd3eb39">
						<property name="local_mesure_unity" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.y" value="px"/>
						<property name="com.jaspersoft.studio.unit.height" value="px"/>
					</reportElement>
					<textElement>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{SMS}]]></textFieldExpression>
				</textField>
			</frame>
		</band>
	</summary>
</jasperReport>
