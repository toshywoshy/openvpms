<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Prescription Letter" pageWidth="595" pageHeight="842" columnWidth="519" leftMargin="38" rightMargin="38" topMargin="28" bottomMargin="28" uuid="774b9ad9-b9f2-4a91-8e52-56c94700044f">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="One Empty Record"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="12"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<parameter name="dataSource" class="org.openvpms.report.jasper.DataSource" isForPrompting="false"/>
	<parameter name="Medication" class="java.lang.String">
		<defaultValueExpression><![CDATA["Drug Name, Strength and Form"]]></defaultValueExpression>
	</parameter>
	<parameter name="Quantity" class="java.lang.String">
		<defaultValueExpression><![CDATA["Quantity of Medication"]]></defaultValueExpression>
	</parameter>
	<parameter name="Instructions for use" class="java.lang.String">
		<defaultValueExpression><![CDATA["Instructions for Use"]]></defaultValueExpression>
	</parameter>
	<parameter name="Expiry" class="java.lang.String">
		<defaultValueExpression><![CDATA["Expiry Date"]]></defaultValueExpression>
	</parameter>
	<parameter name="Number of Repeats - If Needed:" class="java.lang.String">
		<defaultValueExpression><![CDATA["Number of Repeats - If Needed"]]></defaultValueExpression>
	</parameter>
	<field name="startTime" class="java.util.Date"/>
	<field name="endTime" class="java.util.Date"/>
	<field name="OpenVPMS.location.letterhead.target.subreport" class="java.lang.String"/>
	<field name="OpenVPMS.location.name" class="java.lang.String"/>
	<field name="[party:getTelephone(party:getLetterheadContacts($OpenVPMS.location))]" class="java.lang.String"/>
	<field name="[openvpms:get(party:getPatientOwner(.),&apos;id&apos;)]" class="java.lang.Long"/>
	<field name="[openvpms:lookup(party:getPatientOwner(.),&apos;title&apos;)]" class="java.lang.String"/>
	<field name="[openvpms:get(party:getPatientOwner(.),&apos;title.name&apos;)]" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="[openvpms:get(party:getPatientOwner(.),&apos;initials&apos;)]" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="[openvpms:get(party:getPatientOwner(.),&apos;firstName&apos;)]" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="[openvpms:get(party:getPatientOwner(.),&apos;lastName&apos;)]" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="[party:getPartyFullName(.)]" class="java.lang.String"/>
	<field name="[openvpms:get(party:getPatientOwner(.),&apos;companyName&apos;)]" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="[party:getBillingAddress(.)]" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="[normalize-space(party:getCorrespondenceAddress(party:getPatientOwner(.)))]" class="java.lang.String"/>
	<field name="[party:getHomeTelephone(party:getPatientOwner(.))]" class="java.lang.String"/>
	<field name="[party:getMobileTelephone(party:getPatientOwner(.))]" class="java.lang.String"/>
	<field name="[party:getEmailAddress(party:getPatientOwner(.))]" class="java.lang.String"/>
	<field name="patient.entity.name" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="patient.entity.id" class="java.lang.Long"/>
	<field name="patient.entity.description" class="java.lang.String"/>
	<field name="patient.entity.species" class="java.lang.String"/>
	<field name="patient.entity.breed" class="java.lang.String"/>
	<field name="patient.entity.colour" class="java.lang.String"/>
	<field name="patient.entity.sex" class="java.lang.String"/>
	<field name="patient.entity.age" class="java.lang.String"/>
	<field name="[date:format(openvpms:get(., &quot;patient.entity.dateOfBirth&quot;), &quot;dd/MM/yyyy&quot;)]" class="java.lang.String"/>
	<field name="[party:getPatientWeight(.)]" class="java.lang.String"/>
	<field name="[party:getPatientDesexStatus(.)]" class="java.lang.String"/>
	<field name="[party:getPatientMicrochip(.)]" class="java.lang.String"/>
	<field name="[user:format(openvpms:get(., &apos;clinician.entity&apos;), &apos;long&apos;)]" class="java.lang.String"/>
	<field name="product.entity.name" class="java.lang.String"/>
	<field name="product.entity.printedName" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="clinician.entity.name" class="java.lang.String">
		<fieldDescription><![CDATA[ClinicianName]]></fieldDescription>
	</field>
	<field name="clinician.entity.description" class="java.lang.String">
		<fieldDescription><![CDATA[Clinician Description]]></fieldDescription>
	</field>
	<field name="clinician.entity.qualifications" class="java.lang.String">
		<fieldDescription><![CDATA[Clinician Qualifications]]></fieldDescription>
	</field>
	<field name="[party:getEmailAddress($OpenVPMS.location]" class="java.lang.String">
		<fieldDescription><![CDATA[LocationEmailAddress]]></fieldDescription>
	</field>
	<variable name="CustomerFullName" class="java.lang.String" resetType="None">
		<variableExpression><![CDATA[(($F{[openvpms:get(party:getPatientOwner(.),'title.name')]} == null) ? "": $F{[openvpms:get(party:getPatientOwner(.),'title.name')]}) + " " + (($F{[openvpms:get(party:getPatientOwner(.),'firstName')]} == null) ? "": $F{[openvpms:get(party:getPatientOwner(.),'firstName')]}) + " " + $F{[openvpms:get(party:getPatientOwner(.),'lastName')]}]]></variableExpression>
	</variable>
	<group name="customerIdGroup">
		<groupExpression><![CDATA[$F{[openvpms:get(party:getPatientOwner(.),'id')]}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="307" splitType="Stretch">
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="-20" y="0" width="559" height="25" isPrintWhenDetailOverflows="true" uuid="7e7e81a7-e624-4204-b16e-92c3698a1d56">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("patient")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" positionType="Float" x="140" y="29" width="79" height="14" uuid="7b03a3f4-eeeb-45b9-a74e-f9020ffdf841">
					<property name="local_mesure_unitheight" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format(new Date())]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="29" width="70" height="14" uuid="66107f7c-f2b6-42da-815a-6b86ed567a96">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="140" y="135" width="379" height="14" uuid="073319f2-a014-444f-aabc-e92f9cb1fa8e">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{[party:getPartyFullName(.)]}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="140" y="265" width="379" height="14" uuid="9d76c96a-d11e-4efc-b2f6-2495ec22a47c">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{patient.entity.species} + " " + $F{patient.entity.description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="140" y="245" width="379" height="14" uuid="c1bb657d-1540-4571-a7df-dc204589a7bb">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{patient.entity.name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="140" y="155" width="379" height="14" uuid="7ecab764-4ae1-4d99-b436-9e568ef3e21d">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{[normalize-space(party:getCorrespondenceAddress(party:getPatientOwner(.)))]}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="117" width="519" height="14" uuid="759f1871-c558-4463-bcb1-bb35ec20fec7">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Owner Details:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="225" width="140" height="14" uuid="9ae5585d-23e1-48ed-b9a3-12f345f98160">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Patient Details:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="140" y="175" width="379" height="14" uuid="bfd486fd-6e91-443e-b974-c2afeb007470">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{[party:getHomeTelephone(party:getPatientOwner(.))]}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="57" width="180" height="14" uuid="9943c28e-3c63-4589-9256-2fc67b15053c">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<text><![CDATA[Dear Pharmacy,]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="140" y="285" width="379" height="14" uuid="95759a2e-6377-4886-9ddf-dff664253d7d">
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{[date:format(openvpms:get(., "patient.entity.dateOfBirth"), "dd/MM/yyyy")]} + "  " + $F{patient.entity.age}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="0" y="92" width="170" height="14" uuid="0a6d2868-53e9-4925-9255-122a2b92ff7a">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
				</reportElement>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Please supply:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="135" width="70" height="14" uuid="307374bd-8837-4bbb-99e7-ba9b3a4f3bc7">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Name:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="155" width="70" height="14" uuid="1915af70-7501-4f9b-b472-f952fac95885">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Address:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="175" width="70" height="14" uuid="eecebdcd-cdf6-42bd-a9b7-4f34bb3f09b9">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Phone:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="201" width="519" height="14" uuid="dd3f0c92-458f-4d4c-a4ee-95fd9dfb8e8b">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[With the following prescription medication for:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="245" width="70" height="14" uuid="9e4aa4f3-b795-4616-a7a6-ea0170ccd0e6">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Name:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="265" width="70" height="14" uuid="07e4b128-0f33-457f-892d-bf6fb9671f47">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Description:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="285" width="70" height="14" uuid="f6c7fb81-6c8f-4810-b06a-211c40756ff1">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[DOB:]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="309" splitType="Immediate">
			<staticText>
				<reportElement positionType="Float" x="100" y="239" width="420" height="14" uuid="f5c52470-8916-4538-9ed1-a97f5a051b54"/>
				<text><![CDATA[____________________________________________________________________________________]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="10" width="518" height="14" uuid="23acf0b5-1c56-4f5f-a4e4-1327e46ae191">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Prescription Details:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="32" width="518" height="14" uuid="316f3672-97e0-436d-95dd-236894e31a9c">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Medication:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="69" width="518" height="14" uuid="56f1817d-ee75-4679-8d73-6331b1ed97fa">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Quantity:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="106" width="518" height="14" uuid="1edbdf17-24ed-4e43-82e1-b29a51bc1aa2">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Instructions for use:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="371" y="144" width="49" height="14" uuid="1ec349cc-c36d-41d7-854a-079f494fbc3b">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Expiry:]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="-1" y="174" width="520" height="30" uuid="5d969ec9-782c-4be5-a9ad-fda73919263a"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[FOR ANIMAL TREATMENT ONLY
KEEP OUT OF REACH OF CHILDREN]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="0" y="214" width="519" height="14" uuid="4eda3a99-2e70-4c69-ba3f-e20e8067292c">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="12" isBold="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Yours sincerely,]]></text>
			</staticText>
			<staticText>
				<reportElement positionType="Float" x="-1" y="239" width="100" height="14" uuid="3a97dc21-0d67-4f28-90a6-c80fe35e99f0"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Signature:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="0" y="258" width="420" height="14" uuid="f5991516-405d-430f-b329-3a4c181aa10c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{clinician.entity.name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="0" y="273" width="420" height="14" uuid="33608e53-bced-455b-abf0-7285ad99ad3f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{clinician.entity.description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-1" positionType="Float" x="0" y="288" width="420" height="14" uuid="72166a12-2b11-4365-9449-02b19bd20602"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textFieldExpression><![CDATA[$F{clinician.entity.qualifications}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="49" width="518" height="14" uuid="d4c99b30-6f85-4662-8bbe-10616a8ea9d0">
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<textFieldExpression><![CDATA[$P{Medication}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="86" width="518" height="14" uuid="e7154288-60b3-439a-ae6e-0cdab9c22ef9">
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<textFieldExpression><![CDATA[$P{Quantity}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement positionType="Float" x="0" y="124" width="518" height="14" uuid="25220abb-9088-479d-90d7-e50e2ba43457">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$P{Instructions for use}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement positionType="Float" isPrintRepeatedValues="false" x="430" y="144" width="74" height="14" isRemoveLineWhenBlank="true" uuid="e52f2f07-3295-4397-b518-9f08166e3358">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<textFieldExpression><![CDATA[$P{Expiry}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement positionType="Float" x="239" y="144" width="132" height="14" uuid="4e3d3d6f-cf8b-40e2-965f-34e46a7461e6">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<textFieldExpression><![CDATA[$P{Number of Repeats - If Needed:}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="144" width="230" height="14" uuid="36e6f577-bfbe-4bcd-ace0-ffe8dcb3169f">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Number of Repeats (If Required):]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
