<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Receipt" pageWidth="612" pageHeight="792" columnWidth="554" leftMargin="29" rightMargin="29" topMargin="26" bottomMargin="26" isSummaryWithPageHeaderAndFooter="true" resourceBundle="localisation.reports" uuid="a44a233a-11bd-424c-8579-8687a668c705">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="com.jaspersoft.studio.unit." value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageHeight" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.pageWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.topMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.bottomMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.leftMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.rightMargin" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnWidth" value="pixel"/>
	<property name="com.jaspersoft.studio.unit.columnSpacing" value="pixel"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="org.openvpms.archetype.function.party.PartyFunctions"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="Base" isDefault="true" hTextAlign="Left" hImageAlign="Left" vTextAlign="Middle" vImageAlign="Middle" fontSize="9"/>
	<parameter name="dataSource" class="org.openvpms.report.jasper.IMObjectCollectionDataSource" isForPrompting="false"/>
	<parameter name="IsEmail" class="java.lang.Boolean" isForPrompting="false">
		<parameterDescription><![CDATA[If true, indicates the report is being emailed, to enable different formatting]]></parameterDescription>
		<defaultValueExpression><![CDATA[Boolean.FALSE]]></defaultValueExpression>
	</parameter>
	<field name="createdBy.name" class="java.lang.String"/>
	<field name="customer.entity.name" class="java.lang.String"/>
	<field name="customer.entity.companyName" class="java.lang.String"/>
	<field name="customer.entity.lastName" class="java.lang.String"/>
	<field name="customer.entity.firstName" class="java.lang.String"/>
	<field name="customer.entity.title" class="java.lang.String"/>
	<field name="customer.entity.initials" class="java.lang.String"/>
	<field name="customer.entity.id" class="java.lang.Long"/>
	<field name="startTime" class="java.util.Date"/>
	<field name="id" class="java.lang.Long"/>
	<field name="amount" class="java.math.BigDecimal"/>
	<field name="notes" class="java.lang.String"/>
	<field name="[party:getBillingAddress(.)]" class="java.lang.String"/>
	<field name="[party:getAccountBalance(.)]" class="java.math.BigDecimal"/>
	<field name="[party:getPartyFullName(.)]" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.subreport" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.lastPageFooter" class="java.lang.String"/>
	<field name="OpenVPMS.location.letterhead.target.receiptMsg" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="OpenVPMS.location.letterhead.target.invoicePay" class="java.lang.String"/>
	<field name="reference" class="java.lang.String"/>
	<field name="status.code" class="java.lang.String"/>
	<field name="hide" class="java.lang.Boolean"/>
	<variable name="CustomerFullName" class="java.lang.String" resetType="None">
		<variableExpression><![CDATA[(($F{customer.entity.title} == null) ? "": $F{customer.entity.title}) + " " + (($F{customer.entity.firstName} == null) ? "": $F{customer.entity.firstName}) + " " + $F{customer.entity.lastName}]]></variableExpression>
	</variable>
	<variable name="isFinalised" class="java.lang.Boolean" calculation="First">
		<variableExpression><![CDATA["POSTED".equals($F{status.code})]]></variableExpression>
		<initialValueExpression><![CDATA[Boolean.FALSE]]></initialValueExpression>
	</variable>
	<background>
		<band height="565" splitType="Stretch">
			<staticText>
				<reportElement x="184" y="28" width="185" height="536" forecolor="#FF0000" uuid="a22b7086-05d1-46fa-99a5-494d83afcad5">
					<printWhenExpression><![CDATA[$F{hide}]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="Right">
					<font size="94" isBold="true"/>
				</textElement>
				<text><![CDATA[Cancelled]]></text>
			</staticText>
		</band>
	</background>
	<title>
		<band height="146" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" positionType="Float" x="473" y="74" width="81" height="13" uuid="f8538d39-cf4a-4ffe-9704-32a194f3ec00">
					<property name="local_mesure_unitheight" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[DateFormat.getDateInstance(DateFormat.SHORT, $P{REPORT_LOCALE}).format($F{startTime})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" positionType="Float" x="399" y="87" width="72" height="14" uuid="0ccd4997-b7b0-4b9e-b2d3-9d30f02ecd0e">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Reference]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" positionType="Float" x="399" y="74" width="72" height="13" uuid="0246c60a-5644-4cd9-adbe-994d3b7bf150">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-4" positionType="Float" x="473" y="87" width="81" height="14" uuid="b119bd3e-7a64-4ace-9b28-b0c355df8ed2">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reference}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" x="399" y="61" width="72" height="13" uuid="7bb83848-d9b6-45ac-80e5-bb1dff292525">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Customer ID]]></text>
			</staticText>
			<textField>
				<reportElement positionType="Float" x="473" y="61" width="81" height="13" uuid="be3c02df-b70b-448d-9b38-641c4030cd02">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement verticalAlignment="Middle">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer.entity.id}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Master">
				<reportElement positionType="Float" x="473" y="114" width="81" height="13" uuid="aca809c7-4cea-40de-95ae-9c031150bff4"/>
				<textElement textAlignment="Left"/>
				<textFieldExpression><![CDATA["Page 1 of "+$V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField textAdjust="StretchHeight">
				<reportElement key="staticText-1" positionType="Float" x="388" y="24" width="166" height="37" uuid="301dd7b8-e1f4-4580-841f-e965f4cc1045">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font size="16" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{text.receiptTitle}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-2" positionType="Float" x="399" y="101" width="72" height="13" uuid="51c162b5-3fb1-448a-862a-7562e6e72761">
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Document No]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-4" positionType="Float" x="473" y="101" width="81" height="13" uuid="07ee8d74-e7e1-4612-87da-40a1ffb7de30">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="local_mesure_unity" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{id}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement key="" positionType="Float" mode="Opaque" x="388" y="114" width="83" height="32" forecolor="#FF0000" backcolor="#FFFFFF" uuid="3f996092-bcfc-4967-a777-56f4b03ec674">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineColor="#FFFFFF"/>
					<leftPen lineColor="#FFFFFF"/>
					<bottomPen lineColor="#FFFFFF"/>
					<rightPen lineColor="#FFFFFF"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" markup="html">
					<font size="18" isBold="true" isItalic="false" isUnderline="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{isFinalised} ? "":"DRAFT"]]></textFieldExpression>
			</textField>
			<subreport>
				<reportElement positionType="Float" x="0" y="24" width="381" height="103" uuid="c07152ea-5595-4e27-8b9e-22d6ec694cc6">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="Addr1">
					<subreportParameterExpression><![CDATA[$F{customer.entity.companyName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="Addr2">
					<subreportParameterExpression><![CDATA[$V{CustomerFullName}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="Addr3">
					<subreportParameterExpression><![CDATA[$F{[party:getBillingAddress(.)]}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+" AddressBlock"+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" isPrintWhenDetailOverflows="true" uuid="36a89086-0bb5-41a1-860a-6c2b8dab92e6">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</title>
	<pageHeader>
		<band height="66" splitType="Stretch">
			<printWhenExpression><![CDATA[$V{PAGE_NUMBER}>1]]></printWhenExpression>
			<textField>
				<reportElement key="staticText-1" positionType="Float" x="175" y="24" width="204" height="27" uuid="3459bb10-a68f-4dab-ab0f-961ba445c65b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="16" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$R{text.receiptTitle}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement positionType="Float" x="441" y="24" width="67" height="12" uuid="bfd70080-43ea-4b7a-a7d1-524dd1a725f1">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Right">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement positionType="Float" x="508" y="24" width="46" height="12" uuid="8029e70d-003b-431b-9c5a-2ae891739ecf"/>
				<textElement textAlignment="Left">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[" of " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<subreport isUsingCache="false">
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" isPrintWhenDetailOverflows="true" uuid="284ec033-b81c-4841-9be0-3441d929707a">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<subreportParameter name="IsEmail">
					<subreportParameterExpression><![CDATA[$P{IsEmail}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="pageNo">
					<subreportParameterExpression><![CDATA[$V{PAGE_NUMBER}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("customer")]]></dataSourceExpression>
				<subreportExpression><![CDATA[(($F{OpenVPMS.location.letterhead.target.subreport}==null)?"Letterhead":$F{OpenVPMS.location.letterhead.target.subreport})+(($P{JASPER_REPORT}.getPageWidth()<450)?"-A5":"")+".jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="34" splitType="Stretch">
			<subreport isUsingCache="true">
				<reportElement key="subreport-1" x="0" y="0" width="554" height="34" uuid="d5350d46-c4ed-4f2f-8267-f6517c1ad416">
					<property name="local_mesure_unitwidth" value="pixel"/>
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
				</reportElement>
				<dataSourceExpression><![CDATA[$P{dataSource}.getDataSource("items")]]></dataSourceExpression>
				<subreportExpression><![CDATA["Receipt Items.jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="25">
			<textField textAdjust="StretchHeight" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="staticText-1" stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="0" y="0" width="554" height="24" uuid="25f52d32-af2a-4afd-ac69-29af749ca95a">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<printWhenExpression><![CDATA[($F{OpenVPMS.location.letterhead.target.lastPageFooter}==null)?Boolean.FALSE:
($F{OpenVPMS.location.letterhead.target.lastPageFooter}.trim().length()>0)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="9" isBold="true" isItalic="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{OpenVPMS.location.letterhead.target.lastPageFooter}]]></textFieldExpression>
			</textField>
		</band>
	</lastPageFooter>
	<summary>
		<band height="101" splitType="Immediate">
			<line>
				<reportElement key="line-2" x="0" y="1" width="554" height="1" uuid="80b583c1-37da-4637-9248-9ff2010aeb84">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
			</line>
			<textField textAdjust="StretchHeight" isBlankWhenNull="true">
				<reportElement key="staticText-1" positionType="Float" x="0" y="40" width="554" height="24" uuid="b7674ef1-a852-46f5-b460-8f59cd32abea">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<printWhenExpression><![CDATA[($F{OpenVPMS.location.letterhead.target.receiptMsg}==null)?Boolean.FALSE:
($F{OpenVPMS.location.letterhead.target.receiptMsg}.trim().length()>0)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="9" isBold="true" isItalic="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{OpenVPMS.location.letterhead.target.receiptMsg}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-9" positionType="Float" x="284" y="14" width="168" height="15" isRemoveLineWhenBlank="true" uuid="f0164e5b-5353-4cc2-8bb1-8c58728267cd">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<printWhenExpression><![CDATA[$V{isFinalised}&&($F{[party:getAccountBalance(.)]}!=BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font size="9" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Your account balance is]]></text>
			</staticText>
			<textField textAdjust="ScaleFont" pattern="¤ #,##0.00" isBlankWhenNull="false">
				<reportElement key="textField-8" positionType="Float" x="465" y="14" width="82" height="15" isRemoveLineWhenBlank="true" uuid="5cd89500-163e-4293-9860-2090b027e19d">
					<property name="local_mesure_unitx" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="px"/>
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
					<property name="com.jaspersoft.studio.unit.x" value="px"/>
					<printWhenExpression><![CDATA[$V{isFinalised}&&($F{[party:getAccountBalance(.)]}!=BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font size="9" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{[party:getAccountBalance(.)]}]]></textFieldExpression>
			</textField>
			<subreport overflowType="Stretch">
				<reportElement stretchType="RelativeToTallestObject" x="-29" y="76" width="612" height="24" isRemoveLineWhenBlank="true" isPrintWhenDetailOverflows="true" uuid="60a4f684-bdc0-4e4f-9760-6b31e30597ff"/>
				<subreportParameter name="isSubReport">
					<subreportParameterExpression><![CDATA[TRUE( )]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[$P{dataSource}.getExpressionDataSource("eftpos:printableReceipts()")]]></dataSourceExpression>
				<subreportExpression><![CDATA["EFTPOS Receipt.jrxml"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
