<?xml version="1.0" encoding="UTF-8"  ?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<!-- This file loads all the documents - ie all templates that are not report templates accessed via 
  Reporting|Reports.
  You may have customised versions of these templates - if these have the same template name and content
  file name as the standard ones then these will be overwritten.
  If you do have customised versions, then you have two choices:
    1) do not load the documents using this file
    2) edit this file to delete or comment out the templates that you do not wish to load. To make this 
       easier the template below are grouped together
  
  Note that the Letterhead facility is used by the majority of these templates, this normally provides 
  all the customisation required without the need to customised the actual jrxml files.
  Hence if you have customised Customer Documents (ie Invoice, Counter Sale, etc), but no others, then 
  it is worth editing this file to delete/comment out the Customer Documents, but load the remainder
  -->

<templates>
    <!-- Customer Emails -->
    <email-template name="Customer Base Email" description="Customer Base Email"
                    path="Customer/Email/Customer Base Email.jrxml" mimeType="text/xml" system="false"
                    subjectType="XPATH" subject="concat('From ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Invoice Email" description="Customer Invoice Email"
                    path="Customer/Email/Customer Invoice Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Invoice from ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Receipt Email" description="Customer Receipt Email"
                    path="Customer/Email/Customer Receipt Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Receipt from ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Refund Email" description="Customer Refund Email"
                    path="Customer/Email/Customer Refund Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Refund from ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Statement Email" description="Customer Statement Email"
                    path="Customer/Email/Customer Statement Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Statement from ',$location.name)" contentSource="$customer"/>

    <!-- Patient Emails -->
    <email-template name="Patient Base Email" description="Patient Base Email"
                    path="Patient/Email/Patient Base Email.jrxml" mimeType="text/xml" system="false"
                    subjectType="XPATH" subject="concat('From ',$location.name)" contentSource="$patient"/>
    <email-template name="Patient Estimate Email" description="Patient Estimate Email"
                    path="Patient/Email/Patient Estimate Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Estimate from ',$location.name)" contentSource="$patient"/>
    <email-template name="Patient Owner History Email" description="Patient Owner History Email"
                    path="Patient/Email/Patient Owner History Email.jrxml" mimeType="text/xml"
                    subjectType="XPATH" subject="concat('Patient History from ',$location.name)"
                    contentSource="$patient"/>
    <email-template name="Patient Referral History Email" description="Patient Referral History Email"
                    path="Patient/Email/Patient Referral History Email.jrxml" mimeType="text/xml"
                    subjectType="XPATH" subject="concat('Patient History from ',$location.name)"
                    contentSource="$patient"/>

    <!-- Reminder Emails -->
    <email-template name="Patient Grouped Email Reminder" description="Patient Grouped Email Reminder"
                    path="Reporting/Reminders/Email/Patient Grouped Email Reminder.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Reminder from ' , $location.name)"/>
    <email-template name="Patient Single Email Reminder" description="Patient Single Email Reminder"
                    path="Reporting/Reminders/Email/Patient Single Email Reminder.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Reminder from ' , $location.name)"/>
    <email-template name="Patient Single Collection Email Reminder" description="Patient Single Collection Email Reminder"
                    path="Reporting/Reminders/Email/Patient Single Collection Email Reminder.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Reminder from ' , $location.name)"/>

    <!-- Customer Financial Documents -->
    <template name="Counter Sale" archetype="act.customerAccountChargesCounter" reportType="CUSTOMER"
              description="Counter Sale (A5)" path="Customer/Counter Sale/A5/Counter Sale.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Credit" archetype="act.customerAccountChargesCredit" reportType="CUSTOMER" description="Credit (A5)"
              path="Customer/Credit/A5/Credit.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Credit Adjustment" archetype="act.customerAccountCreditAdjust" reportType="CUSTOMER"
              description="Credit Adjustment (A5)" path="Customer/Credit Adjustment/A5/Credit Adjustment.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Debit Adjustment" archetype="act.customerAccountDebitAdjust" reportType="CUSTOMER"
              description="Debit Adjustment (A5)" path="Customer/Debit Adjustment/A5/Debit Adjustment.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="EFTPOS Receipt" archetype="act.EFTPOSReceiptCustomer" reportType="CUSTOMER"
              description="EFTPOS Receipt (A5)"
              path="Customer/EFTPOS Receipt/A5/EFTPOS Receipt.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Estimate" archetype="act.customerEstimation" reportType="CUSTOMER" description="Estimate (A4)"
              path="Customer/Estimate/A4/Estimate.jrxml" mimeType="text/xml" docType="document.other">
        <email-template name="Patient Estimate Email"/>
    </template>
    <template name="Estimate Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Estimate Items (A5)"
              path="Customer/Estimate/A5/Estimate Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Estimate Items-PT" archetype="SUBREPORT" reportType="CUSTOMER" description="Estimate Items-PT (A5)"
              path="Customer/Estimate/A5/Estimate Items-PT.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice" archetype="act.customerAccountChargesInvoice" reportType="CUSTOMER"
              description="Invoice (A5)" path="Customer/Invoice/A5/Invoice.jrxml" mimeType="text/xml"
              docType="document.other">
        <email-template name="Customer Invoice Email"/>
    </template>
    <template name="Invoice Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Items (A5)"
              path="Customer/Invoice/A5/Invoice Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Items-PT" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Items-PT (A5)"
              path="Customer/Invoice/A5/Invoice Items-PT.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Notes" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Notes (A5)"
              path="Customer/Invoice/A5/Invoice Notes.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Reminders" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Reminders (A5)"
              path="Customer/Invoice/A5/Invoice Reminders.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Appointments" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Appointments (A5)"
              path="Customer/Invoice/A5/Invoice Appointments.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Receipt" archetype="act.customerAccountPayment" reportType="CUSTOMER" description="Receipt (A5)"
              path="Customer/Receipt/A5/Receipt.jrxml" mimeType="text/xml" docType="document.other">
        <email-template name="Customer Receipt Email"/>
    </template>
    <template name="Receipt Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Receipt Items (A5)"
              path="Customer/Receipt/A5/Receipt Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Refund" archetype="act.customerAccountRefund" reportType="CUSTOMER" description="Refund (A5)"
              path="Customer/Refund/A5/Refund.jrxml" mimeType="text/xml" docType="document.other">
        <email-template name="Customer Refund Email"/>
    </template>
    <template name="Refund Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Refund Items (A5)"
              path="Customer/Refund/A5/Refund Items.jrxml" mimeType="text/xml" docType="document.other"/>

    <!-- Customer Documents - Statements -->
    <template name="Statement" archetype="act.customerAccountOpeningBalance" reportType="CUSTOMER"
              description="Statement (A5)" path="Customer/Statement/A5/Statement.jrxml" mimeType="text/xml"
              docType="document.other">
        <email-template name="Customer Statement Email"/>
    </template>
    <template name="Statement Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Statement Items (A5)"
              path="Customer/Statement/A5/Statement Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Statement Items-PT" archetype="SUBREPORT" reportType="CUSTOMER" description="Statement Items-PT (A5)"
              path="Customer/Statement/A5/Statement Items-PT.jrxml" mimeType="text/xml" docType="document.other"/>

<!-- Customer Documents - Pharmacy items -->
    <template name="Pharmacy Order" archetype="act.customerOrderPharmacy" reportType="WORKFLOW"
              description="Pharmacy Order (A5)" path="Workflow/Customer Order/Pharmacy Order/A5/Pharmacy Order.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Pharmacy Order Items" archetype="SUBREPORT" reportType="WORKFLOW" description="Pharmacy Order Items (A5)"
              path="Workflow/Customer Order/Pharmacy Order/A5/Pharmacy Order Items.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Pharmacy Return" archetype="act.customerReturnPharmacy" reportType="WORKFLOW"
              description="Pharmacy Return (A5)" path="Workflow/Customer Order/Pharmacy Return/A5/Pharmacy Return.jrxml"
              mimeType="text/xml" docType="document.other"/>


<!-- Patient Documents -->
    <template name="Desexing Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Desexing Certificate (A5)" path="Patient/Documents/A5/Desexing Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Euthanasia Consent" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Euthanasia Consent (A5)" path="Patient/Documents/A4/Euthanasia Consent.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="General Admission Form" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="General Admission Form (A5)" path="Patient/Documents/A5/General Admission Form.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Microchip Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Microchip Certificate (A5)" path="Patient/Documents/A5/Microchip Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Prescription External" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Prescription Letter External (A5)" path="Patient/Documents/A5/Prescription External.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Rabies Vaccination Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Rabies Vaccination Certificate (A5)" path="Patient/Documents/A5/Rabies Vaccination Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Referral Letter" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Referral Letter (A5)" path="Patient/Documents/A5/Referral Letter.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Surgical Admission" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Surgical Admission (A5)" path="Patient/Documents/A5/Surgical Admission.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Vaccination Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Vaccination Certificate (A5)" path="Patient/Documents/A5/Vaccination Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Vaccination Certificate (Reminders)" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Vaccination Certificate with upcoming reminders (A5)"
              path="Patient/Documents/A5/Vaccination Certificate with reminders.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Vaccination Reminders" archetype="SUBREPORT" reportType="PATIENT"
              description="Vaccination Reminders SubReport (A5)" path="Patient/Documents/A5/Vaccination Reminders.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Patient Image" archetype="act.patientDocumentImage" reportType="PATIENT"
              description="Patient Image (A5)" path="Patient/Documents/A5/Patient Image.jrxml" mimeType="text/xml"
              docType="document.other"/>

    <!-- Patient Labels -->
    <template name="Drug Label" archetype="act.patientMedication" reportType="PATIENT" description="Drug Label (54mm x 70mm)"
              path="Patient/Labels/Label 54x70.jrxml" mimeType="text/xml" docType="document.other" orientation="LANDSCAPE"/>

    <!-- Patient Medical Records -->
    <template name="Medical Records" archetype="act.patientClinicalEvent" reportType="PATIENT"
              description="Used by Patients|Medical Records|Summary|Print button (A5)"
              path="Patient/Medical Records/A5/Medical Records.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Owner History Email"/>
    </template>
    <template name="Problems" archetype="act.patientClinicalProblem" reportType="PATIENT"
              description="Used by Patients|Medical Records|Problems|Print button (A5)"
              path="Patient/Medical Records/A5/Problems.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Prescription" archetype="act.patientPrescription" reportType="PATIENT"
              description="Prescription (A5)" path="Patient/Medical Records/A5/Prescription.jrxml" mimeType="text/xml"
              docType="document.other"/>

    <!-- Patient Insurance Documents -->
    <template name="Insurance Claim" archetype="act.patientInsuranceClaim" reportType="PATIENT"
              description="Patient insurance claim (A5)"
              path="Patient/Insurance/A5/Insurance Claim.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Items" archetype="SUBREPORT" reportType="PATIENT"
              description="Patient insurance claim items (A5)"
              path="Patient/Insurance/A5/Insurance Claim Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Invoice" archetype="INSURANCE_CLAIM_INVOICE" reportType="PATIENT"
              description="Invoice for attachment to patient insurance claims (A5)"
              path="Patient/Insurance/A5/Insurance Claim Invoice.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Invoice Items" archetype="SUBREPORT" reportType="PATIENT"
              description="Invoice Items for attachment to patient insurance claims (A5)"
              path="Patient/Insurance/A5/Insurance Claim Invoice Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Medical Records" archetype="INSURANCE_CLAIM_MEDICAL_RECORDS" reportType="PATIENT"
              description="Medical records for attachment to patient insurance claims (A5)"
              path="Patient/Insurance/A5/Insurance Claim Medical Records.jrxml"
              mimeType="text/xml" docType="document.other"/>

    <!-- Patient Reminder documents -->
    <template name="Patient Reminders Report" archetype="act.patientReminder" reportType="PATIENT"
              description="Used by Reporting|Reminders|Report button (A5)"
              path="Reporting/Reminders/A5/Patient Reminders.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Customer Grouped Reminder" archetype="GROUPED_REMINDERS" reportType="CUSTOMER"
              description="Customer Grouped Reminder (A5)" path="Reporting/Reminders/A5/Customer Grouped Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
    </template>
    <template name="Patient Grouped Reminder" archetype="GROUPED_REMINDERS" reportType="PATIENT"
              description="Patient Grouped Reminder (A5)" path="Reporting/Reminders/A5/Patient Grouped Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Grouped Email Reminder"/>
    </template>
    <template name="Patient Single Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Single Reminder (A5)" path="Reporting/Reminders/A5/Patient Single Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Email Reminder"/>
    </template>
    <template name="Patient Single Overdue Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Single Overdue Reminder (A5)"
              path="Reporting/Reminders/A5/Patient Single Overdue Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Email Reminder"/>
    </template>
    <template name="Patient Desexing Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Desexing Reminder (A5)" path="Reporting/Reminders/A5/Patient Desexing Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Email Reminder"/>
    </template>
    <template name="Patient Collection Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Collection Reminder (A5)"
              path="Reporting/Reminders/A5/Patient Collection Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Collection Email Reminder"/>
    </template>
    <template name="Patient Collection Overdue Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Collection Overdue Reminder (A5)"
              path="Reporting/Reminders/A5/Patient Collection Overdue Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Collection Email Reminder"/>
    </template>

<!-- Supplier Documents -->
    <template name="Supplier Credit" archetype="act.supplierAccountChargesCredit" reportType="SUPPLIER"
              description="Supplier Credit (A5)" path="Supplier/Credit/A5/Supplier Credit.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Delivery" archetype="act.supplierDelivery" reportType="SUPPLIER" description="Delivery (A5)"
              path="Supplier/Delivery/A5/Delivery.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Delivery Items" archetype="SUBREPORT" reportType="SUPPLIER" description="Delivery Items (A5)"
              path="Supplier/Delivery/A5/Delivery Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Invoice" archetype="act.supplierAccountChargesInvoice" reportType="SUPPLIER"
              description="Supplier Invoice (A5)" path="Supplier/Invoice/A5/Supplier Invoice.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Supplier Invoice Items" archetype="SUBREPORT" reportType="SUPPLIER"
              description="Supplier Invoice Items (A5)" path="Supplier/Invoice/A5/Supplier Invoice Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Refund" archetype="act.supplierAccountRefund" reportType="SUPPLIER"
              description="Supplier Refund (A5)" path="Supplier/Refund/A5/Supplier Refund.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Supplier Refund Items" archetype="SUBREPORT" reportType="SUPPLIER"
              description="Supplier Refund Items (A5)" path="Supplier/Refund/A5/Supplier Refund Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Remittance" archetype="act.supplierAccountPayment" reportType="SUPPLIER"
              description="Supplier Remittance (A5)" path="Supplier/Remittance/A5/Supplier Remittance.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Remittance Items" archetype="SUBREPORT" reportType="SUPPLIER"
              description="Supplier Remittance Items (A5)" path="Supplier/Remittance/A5/Supplier Remittance Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Return" archetype="act.supplierReturn" reportType="SUPPLIER"
              description="Supplier Return (A5)" path="Supplier/Return/A5/Supplier Return.jrxml"
              mimeType="text/xml" docType="document.other"/>

<!-- Supplier Documents - Orders -->
    <template name="Order" archetype="act.supplierOrder" reportType="SUPPLIER" description="Order (A5)"
              path="Supplier/Order/A5/Order.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Order Items" archetype="SUBREPORT" reportType="SUPPLIER" description="Order Items (A5)"
              path="Supplier/Order/A5/Order Items.jrxml" mimeType="text/xml" docType="document.other"/>


<!-- Print Stock Adjustments & Transfers, Messages, Appointment & Task -->
    <template name="Stock Adjustment" archetype="act.stockAdjust" reportType="PRODUCT" description="Stock Adjustment (A5)"
              path="Product/Stock Adjustment/A5/Stock Adjustment.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Stock Adjustment Items" archetype="SUBREPORT" reportType="PRODUCT"
              description="Stock Adjustment Items (A5)" path="Product/Stock Adjustment/A5/Stock Adjustment Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Stock Transfer" archetype="act.stockTransfer" reportType="PRODUCT" description="Stock Transfer (A5)"
              path="Product/Stock Transfer/A5/Stock Transfer.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Stock Transfer Items" archetype="SUBREPORT" reportType="PRODUCT"
              description="Stock Transfer Items (A5)" path="Product/Stock Transfer/A5/Stock Transfer Items.jrxml"
              mimeType="text/xml" docType="document.other"/>

    <template name="Message" archetype="act.userMessage" reportType="WORKFLOW" description="Message (A5)"
              path="Workflow/Message/A5/Message.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Audit Message" archetype="act.auditMessage" reportType="WORKFLOW" description="Audit Message (A5)"
              path="Workflow/Message/A5/Audit Message.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="System Message" archetype="act.systemMessage" reportType="WORKFLOW" description="System Message (A5)"
              path="Workflow/Message/A5/System Message.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Appointment" archetype="act.customerAppointment" reportType="WORKFLOW"
              description="Appointment (A5)" path="Workflow/Appointment/A5/Appointment.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Task" archetype="act.customerTask" reportType="WORKFLOW" description="Task (A5)"
              path="Workflow/Task/A5/Task.jrxml" mimeType="text/xml" docType="document.other"/>


<!-- Reports got using Print buttons -->
    <template name="Customer Account Balance Report" archetype="CUSTOMER_BALANCE" reportType="FINANCIAL"
              description="Used by Reporting|Debtors|Report button (A5)"
              path="Reporting/Debtors/A5/Customer Account Balance.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Bank Deposit" archetype="act.bankDeposit" reportType="FINANCIAL"
              description="Used by Reporting|Deposits|Print button (A5)"
              path="Reporting/Deposits/A5/Bank Deposit.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Till Balance" archetype="act.tillBalance" reportType="FINANCIAL"
              description="Used by Reporting|Till Balancing|Print button (A5)"
              path="Reporting/Till Balancing/A5/Till Balance.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Work In Progress Report" archetype="WORK_IN_PROGRESS_CHARGES" reportType="FINANCIAL"
              description="Used by Reporting|Charges|Work In Progress|Report button (A5)"
              path="Reporting/Charges/Work In Progress/A5/Work In Progress.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Charges Report" archetype="CHARGES" reportType="FINANCIAL"
              description="Used by Reporting|Charges|Search|Report button (A5)"
              path="Reporting/Charges/Search/A5/Charges.jrxml"
              mimeType="text/xml" docType="document.other"/>


<!-- Letterhead items -->
    <template name="Letterhead" archetype="SUBREPORT" reportType="WORKFLOW" description="Letterhead (A4)"
              path="Letterhead/A4/Letterhead.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Letterhead-A5" archetype="SUBREPORT" reportType="WORKFLOW" description="Letterhead (A5)"
              path="Letterhead/A5/Letterhead-A5.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Letterhead AddressBlock" archetype="SUBREPORT" reportType="WORKFLOW" description="Letterhead AddressBlock (A4)"
              path="Letterhead/A4/Letterhead AddressBlock.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Letterhead AddressBlock-A5" archetype="SUBREPORT" reportType="WORKFLOW" description="Letterhead AddressBlock (A5)"
              path="Letterhead/A5/Letterhead AddressBlock-A5.jrxml" mimeType="text/xml" docType="document.other"/>

</templates>
