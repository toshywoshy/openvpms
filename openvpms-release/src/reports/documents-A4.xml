<?xml version="1.0" encoding="UTF-8"  ?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<!-- This file loads all the documents - ie all templates that are not report templates accessed via 
  Reporting|Reports.
  You may have customised versions of these templates - if these have the same template name and content
  file name as the standard ones then these will be overwritten.
  If you do have customised versions, then you have two choices:
    1) do not load the documents using this file
    2) edit this file to delete or comment out the templates that you do not wish to load. To make this 
       easier the template below are grouped together
  
  Note that the Letterhead facility is used by the majority of these templates, this normally provides 
  all the customisation required without the need to customised the actual jrxml files.
  Hence if you have customised Customer Documents (ie Invoice, Counter Sale, etc), but no others, then 
  it is worth editing this file to delete/comment out the Customer Documents, but load the remainder
  -->

<templates>
    <!-- Customer Emails -->
    <email-template name="Customer Base Email" description="Customer Base Email"
                    path="Customer/Email/Customer Base Email.jrxml" mimeType="text/xml" system="false"
                    subjectType="XPATH" subject="concat('From ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Invoice Email" description="Customer Invoice Email"
                    path="Customer/Email/Customer Invoice Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Invoice from ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Receipt Email" description="Customer Receipt Email"
                    path="Customer/Email/Customer Receipt Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Receipt from ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Refund Email" description="Customer Refund Email"
                    path="Customer/Email/Customer Refund Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Refund from ',$location.name)" contentSource="$customer"/>
    <email-template name="Customer Statement Email" description="Customer Statement Email"
                    path="Customer/Email/Customer Statement Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Statement from ',$location.name)" contentSource="$customer"/>

    <!-- Patient Emails -->
    <email-template name="Patient Base Email" description="Patient Base Email"
                    path="Patient/Email/Patient Base Email.jrxml" mimeType="text/xml" system="false"
                    subjectType="XPATH" subject="concat('From ',$location.name)" contentSource="$patient"/>
    <email-template name="Patient Estimate Email" description="Patient Estimate Email"
                    path="Patient/Email/Patient Estimate Email.jrxml" mimeType="text/xml" system="true"
                    subjectType="XPATH" subject="concat('Estimate from ',$location.name)" contentSource="$patient"/>
    <email-template name="Patient Owner History Email" description="Patient Owner History Email"
                    path="Patient/Email/Patient Owner History Email.jrxml" mimeType="text/xml"
                    subjectType="XPATH" subject="concat('Patient History from ',$location.name)"
                    contentSource="$patient"/>
    <email-template name="Patient Referral History Email" description="Patient Referral History Email"
                    path="Patient/Email/Patient Referral History Email.jrxml" mimeType="text/xml"
                    subjectType="XPATH" subject="concat('Patient History from ',$location.name)"
                    contentSource="$patient"/>

    <!-- Reminder Emails -->
    <email-template name="Patient Grouped Email Reminder" description="Patient Grouped Email Reminder"
                    path="Reporting/Reminders/Email/Patient Grouped Email Reminder.jrxml" mimeType="text/xml"
                    system="true" subjectType="XPATH" subject="concat('Reminder from ' , $location.name)"/>
    <email-template name="Patient Single Email Reminder" description="Patient Single Email Reminder"
                    path="Reporting/Reminders/Email/Patient Single Email Reminder.jrxml" mimeType="text/xml"
                    system="true" subjectType="XPATH" subject="concat('Reminder from ' , $location.name)"/>
    <email-template name="Patient Single Collection Email Reminder" description="Patient Single Email Reminder"
                    path="Reporting/Reminders/Email/Patient Single Collection Email Reminder.jrxml" mimeType="text/xml"
                    system="true" subjectType="XPATH" subject="concat('Reminder from ' , $location.name)"/>

    <!-- Customer Financial Documents -->
    <template name="Counter Sale" archetype="act.customerAccountChargesCounter" reportType="CUSTOMER"
              description="Counter Sale (A4)" path="Customer/Counter Sale/A4/Counter Sale.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Credit" archetype="act.customerAccountChargesCredit" reportType="CUSTOMER" description="Credit (A4)"
              path="Customer/Credit/A4/Credit.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Credit Adjustment" archetype="act.customerAccountCreditAdjust" reportType="CUSTOMER"
              description="Credit Adjustment (A4)" path="Customer/Credit Adjustment/A4/Credit Adjustment.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Debit Adjustment" archetype="act.customerAccountDebitAdjust" reportType="CUSTOMER"
              description="Debit Adjustment (A4)" path="Customer/Debit Adjustment/A4/Debit Adjustment.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="EFTPOS Receipt" archetype="act.EFTPOSReceiptCustomer" reportType="CUSTOMER"
              description="EFTPOS Receipt (A4)"
              path="Customer/EFTPOS Receipt/A4/EFTPOS Receipt.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Estimate" archetype="act.customerEstimation" reportType="CUSTOMER" description="Estimate (A4)"
              path="Customer/Estimate/A4/Estimate.jrxml" mimeType="text/xml" docType="document.other">
        <email-template name="Patient Estimate Email"/>
    </template>
    <template name="Estimate Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Estimate Items (A4)"
              path="Customer/Estimate/A4/Estimate Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Estimate Items-PT" archetype="SUBREPORT" reportType="CUSTOMER" description="Estimate Items-PT (A4)"
              path="Customer/Estimate/A4/Estimate Items-PT.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice" archetype="act.customerAccountChargesInvoice" reportType="CUSTOMER"
              description="Invoice (A4)" path="Customer/Invoice/A4/Invoice.jrxml" mimeType="text/xml"
              docType="document.other">
        <email-template name="Customer Invoice Email"/>
    </template>
    <template name="Invoice Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Items (A4)"
              path="Customer/Invoice/A4/Invoice Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Items-PT" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Items-PT (A4)"
              path="Customer/Invoice/A4/Invoice Items-PT.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Notes" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Notes (A4)"
              path="Customer/Invoice/A4/Invoice Notes.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Reminders" archetype="SUBREPORT" reportType="CUSTOMER" description="Invoice Reminders (A4)"
              path="Customer/Invoice/A4/Invoice Reminders.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Invoice Appointments" archetype="SUBREPORT" reportType="CUSTOMER"
              description="Invoice Appointments (A4)"
              path="Customer/Invoice/A4/Invoice Appointments.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Receipt" archetype="act.customerAccountPayment" reportType="CUSTOMER" description="Receipt (A4)"
              path="Customer/Receipt/A4/Receipt.jrxml" mimeType="text/xml" docType="document.other">
        <email-template name="Customer Receipt Email"/>
    </template>
    <template name="Receipt Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Receipt Items (A4)"
              path="Customer/Receipt/A4/Receipt Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Refund" archetype="act.customerAccountRefund" reportType="CUSTOMER" description="Refund (A4)"
              path="Customer/Refund/A4/Refund.jrxml" mimeType="text/xml" docType="document.other">
        <email-template name="Customer Refund Email"/>
    </template>
    <template name="Refund Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Refund Items (A4)"
              path="Customer/Refund/A4/Refund Items.jrxml" mimeType="text/xml" docType="document.other"/>

    <!-- Customer Documents - Statements -->
    <template name="Statement" archetype="act.customerAccountOpeningBalance" reportType="CUSTOMER"
              description="Statement (A4)" path="Customer/Statement/A4/Statement.jrxml" mimeType="text/xml"
              docType="document.other">
        <email-template name="Customer Statement Email"/>
    </template>
    <template name="Statement Items" archetype="SUBREPORT" reportType="CUSTOMER" description="Statement Items (A4)"
              path="Customer/Statement/A4/Statement Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Statement Items-PT" archetype="SUBREPORT" reportType="CUSTOMER"
              description="Statement Items-PT (A4)"
              path="Customer/Statement/A4/Statement Items-PT.jrxml" mimeType="text/xml" docType="document.other"/>

    <!-- Customer Documents - Pharmacy items -->
    <template name="Pharmacy Order" archetype="act.customerOrderPharmacy" reportType="WORKFLOW"
              description="Pharmacy Order (A4)" path="Workflow/Customer Order/Pharmacy Order/A4/Pharmacy Order.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Pharmacy Order Items" archetype="SUBREPORT" reportType="WORKFLOW"
              description="Pharmacy Order Items (A4)"
              path="Workflow/Customer Order/Pharmacy Order/A4/Pharmacy Order Items.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Pharmacy Return" archetype="act.customerReturnPharmacy" reportType="WORKFLOW"
              description="Pharmacy Return (A4)" path="Workflow/Customer Order/Pharmacy Return/A4/Pharmacy Return.jrxml"
              mimeType="text/xml" docType="document.other"/>


    <!-- Patient Documents -->
    <template name="Desexing Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Desexing Certificate (A4)" path="Patient/Documents/A4/Desexing Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Euthanasia Consent" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Euthanasia Consent (A4)" path="Patient/Documents/A4/Euthanasia Consent.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="General Admission Form" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="General Admission Form (A4)" path="Patient/Documents/A4/General Admission Form.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Microchip Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Microchip Certificate (A4)" path="Patient/Documents/A4/Microchip Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Prescription External" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Prescription Letter External (A4)" path="Patient/Documents/A4/Prescription External.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Rabies Vaccination Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Rabies Vaccination Certificate (A4)"
              path="Patient/Documents/A4/Rabies Vaccination Certificate.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Referral Letter" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Referral Letter (A4)" path="Patient/Documents/A4/Referral Letter.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Surgical Admission" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Surgical Admission Letter (A4)" path="Patient/Documents/A4/Surgical Admission.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Vaccination Certificate" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Vaccination Certificate (A4)" path="Patient/Documents/A4/Vaccination Certificate.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Vaccination Certificate (Reminders)" archetype="act.patientDocumentForm" reportType="PATIENT"
              description="Vaccination Certificate with upcoming reminders (A4)"
              path="Patient/Documents/A4/Vaccination Certificate with reminders.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Vaccination Reminders" archetype="SUBREPORT" reportType="PATIENT"
              description="Vaccination Reminders subreport (A4)" path="Patient/Documents/A4/Vaccination Reminders.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Patient Image" archetype="act.patientDocumentImage" reportType="PATIENT"
              description="Patient Image (A4)" path="Patient/Documents/A4/Patient Image.jrxml" mimeType="text/xml"
              docType="document.other"/>

    <!-- Patient Labels -->
    <template name="Drug Label" archetype="act.patientMedication" reportType="PATIENT"
              description="Drug Label (54mm x 70mm)" path="Patient/Labels/Label 54x70.jrxml" mimeType="text/xml"
              docType="document.other" orientation="LANDSCAPE"/>

    <!-- Patient Medical Records -->
    <template name="Medical Records" archetype="act.patientClinicalEvent" reportType="PATIENT"
              description="Used by Patients|Medical Records|Summary|Print button (A4)"
              path="Patient/Medical Records/A4/Medical Records.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Owner History Email"/>
    </template>
    <template name="Problems" archetype="act.patientClinicalProblem" reportType="PATIENT"
              description="Used by Patients|Medical Records|Problems|Print button (A4)"
              path="Patient/Medical Records/A4/Problems.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Prescription" archetype="act.patientPrescription" reportType="PATIENT"
              description="Prescription (A4)" path="Patient/Medical Records/A4/Prescription.jrxml" mimeType="text/xml"
              docType="document.other"/>

    <!-- Patient Insurance Documents -->
    <template name="Insurance Claim" archetype="act.patientInsuranceClaim" reportType="PATIENT"
              description="Patient insurance claim (A4)"
              path="Patient/Insurance/A4/Insurance Claim.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Items" archetype="SUBREPORT" reportType="PATIENT"
              description="Patient insurance claim items (A4)"
              path="Patient/Insurance/A4/Insurance Claim Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Invoice" archetype="INSURANCE_CLAIM_INVOICE" reportType="PATIENT"
              description="Invoice for attachment to patient insurance claims (A4)"
              path="Patient/Insurance/A4/Insurance Claim Invoice.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Insurance Claim Invoice Items" archetype="SUBREPORT" reportType="PATIENT"
              description="Invoice Items for attachment to patient insurance claims (A4)"
              path="Patient/Insurance/A4/Insurance Claim Invoice Items.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Insurance Claim Medical Records" archetype="INSURANCE_CLAIM_MEDICAL_RECORDS" reportType="PATIENT"
              description="Medical records for attachment to patient insurance claims (A4)"
              path="Patient/Insurance/A4/Insurance Claim Medical Records.jrxml"
              mimeType="text/xml" docType="document.other"/>

    <!-- Patient Reminder documents -->
    <template name="Patient Reminders Report" archetype="act.patientReminder" reportType="PATIENT"
              description="Used by Reporting|Reminders|Report button (A4)"
              path="Reporting/Reminders/A4/Patient Reminders.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Customer Grouped Reminder" archetype="GROUPED_REMINDERS" reportType="CUSTOMER"
              description="Customer Grouped Reminder (A4)" path="Reporting/Reminders/A4/Customer Grouped Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
    </template>
    <template name="Patient Grouped Reminder" archetype="GROUPED_REMINDERS" reportType="PATIENT"
              description="Patient Grouped Reminder (A4)" path="Reporting/Reminders/A4/Patient Grouped Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Grouped Email Reminder"/>
    </template>
    <template name="Patient Single Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Single Reminder (A4)" path="Reporting/Reminders/A4/Patient Single Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Email Reminder"/>
    </template>
    <template name="Patient Single Overdue Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Single Overdue Reminder (A4)"
              path="Reporting/Reminders/A4/Patient Single Overdue Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Email Reminder"/>
    </template>
    <template name="Patient Desexing Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Desexing Reminder (A4)" path="Reporting/Reminders/A4/Patient Desexing Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Email Reminder"/>
    </template>
    <template name="Patient Collection Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Collection Reminder (A4)"
              path="Reporting/Reminders/A4/Patient Collection Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Collection Email Reminder"/>
    </template>
    <template name="Patient Collection Overdue Reminder" archetype="act.patientDocumentLetter" reportType="PATIENT"
              description="Patient Collection Overdue Reminder (A4)"
              path="Reporting/Reminders/A4/Patient Collection Overdue Reminder.jrxml"
              mimeType="text/xml" docType="document.other">
        <email-template name="Patient Single Collection Email Reminder"/>
    </template>

    <!-- Supplier Documents -->
    <template name="Supplier Credit" archetype="act.supplierAccountChargesCredit" reportType="SUPPLIER"
              description="Supplier Credit (A4)" path="Supplier/Credit/A4/Supplier Credit.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Delivery" archetype="act.supplierDelivery" reportType="SUPPLIER" description="Delivery (A4)"
              path="Supplier/Delivery/A4/Delivery.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Delivery Items" archetype="SUBREPORT" reportType="SUPPLIER" description="Delivery Items (A4)"
              path="Supplier/Delivery/A4/Delivery Items.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Invoice" archetype="act.supplierAccountChargesInvoice" reportType="SUPPLIER"
              description="Supplier Invoice (A4)" path="Supplier/Invoice/A4/Supplier Invoice.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Supplier Invoice Items" archetype="SUBREPORT" reportType="SUPPLIER"
              description="Supplier Invoice Items (A4)" path="Supplier/Invoice/A4/Supplier Invoice Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Refund" archetype="act.supplierAccountRefund" reportType="SUPPLIER"
              description="Supplier Refund (A4)" path="Supplier/Refund/A4/Supplier Refund.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Supplier Refund Items" archetype="SUBREPORT" reportType="SUPPLIER"
              description="Supplier Refund Items (A4)" path="Supplier/Refund/A4/Supplier Refund Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Remittance" archetype="act.supplierAccountPayment" reportType="SUPPLIER"
              description="Supplier Remittance (A4)" path="Supplier/Remittance/A4/Supplier Remittance.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Remittance Items" archetype="SUBREPORT" reportType="SUPPLIER"
              description="Supplier Remittance Items (A4)" path="Supplier/Remittance/A4/Supplier Remittance Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Supplier Return" archetype="act.supplierReturn" reportType="SUPPLIER"
              description="Supplier Return (A4)" path="Supplier/Return/A4/Supplier Return.jrxml"
              mimeType="text/xml" docType="document.other"/>

    <!-- Supplier Documents - Orders -->
    <template name="Order" archetype="act.supplierOrder" reportType="SUPPLIER" description="Order (A4)"
              path="Supplier/Order/A4/Order.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Order Items" archetype="SUBREPORT" reportType="SUPPLIER" description="Order Items (A4)"
              path="Supplier/Order/A4/Order Items.jrxml" mimeType="text/xml" docType="document.other"/>


    <!-- Print Stock Adjustments & Transfers, Messages, Appointment & Task -->
    <template name="Stock Adjustment" archetype="act.stockAdjust" reportType="PRODUCT"
              description="Stock Adjustment (A4)"
              path="Product/Stock Adjustment/A4/Stock Adjustment.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Stock Adjustment Items" archetype="SUBREPORT" reportType="PRODUCT"
              description="Stock Adjustment Items (A4)" path="Product/Stock Adjustment/A4/Stock Adjustment Items.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Stock Transfer" archetype="act.stockTransfer" reportType="PRODUCT" description="Stock Transfer (A4)"
              path="Product/Stock Transfer/A4/Stock Transfer.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Stock Transfer Items" archetype="SUBREPORT" reportType="PRODUCT"
              description="Stock Transfer Items (A4)"
              path="Product/Stock Transfer/A4/Stock Transfer Items.jrxml" mimeType="text/xml" docType="document.other"/>

    <template name="Message" archetype="act.userMessage" reportType="WORKFLOW" description="Message (A4)"
              path="Workflow/Message/A4/Message.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Audit Message" archetype="act.auditMessage" reportType="WORKFLOW" description="Audit Message (A4)"
              path="Workflow/Message/A4/Audit Message.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="System Message" archetype="act.systemMessage" reportType="WORKFLOW"
              description="System Message (A4)"
              path="Workflow/Message/A4/System Message.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Appointment" archetype="act.customerAppointment" reportType="WORKFLOW"
              description="Appointment (A4)" path="Workflow/Appointment/A4/Appointment.jrxml" mimeType="text/xml"
              docType="document.other"/>
    <template name="Task" archetype="act.customerTask" reportType="WORKFLOW" description="Task (A4)"
              path="Workflow/Task/A4/Task.jrxml" mimeType="text/xml" docType="document.other"/>


    <!-- Reports got using Print buttons -->
    <template name="Customer Account Balance Report" archetype="CUSTOMER_BALANCE" reportType="FINANCIAL"
              description="Used by Reporting|Debtors|Report button (A4)"
              path="Reporting/Debtors/A4/Customer Account Balance.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Bank Deposit" archetype="act.bankDeposit" reportType="FINANCIAL"
              description="Used by Reporting|Deposits|Print button (A4)"
              path="Reporting/Deposits/A4/Bank Deposit.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Till Balance" archetype="act.tillBalance" reportType="FINANCIAL"
              description="Used by Reporting|Till Balancing|Print button (A4)"
              path="Reporting/Till Balancing/A4/Till Balance.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Work In Progress Report" archetype="WORK_IN_PROGRESS_CHARGES" reportType="FINANCIAL"
              description="Used by Reporting|Charges|Work In Progress|Report button (A4)"
              path="Reporting/Charges/Work In Progress/A4/Work In Progress.jrxml"
              mimeType="text/xml" docType="document.other"/>
    <template name="Charges Report" archetype="CHARGES" reportType="FINANCIAL"
              description="Used by Reporting|Charges|Search|Report button (A4)"
              path="Reporting/Charges/Search/A4/Charges.jrxml"
              mimeType="text/xml" docType="document.other"/>

    <!-- Letterhead items -->
    <template name="Letterhead" archetype="SUBREPORT" reportType="WORKFLOW" description="Letterhead (A4)"
              path="Letterhead/A4/Letterhead.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Letterhead-A5" archetype="SUBREPORT" reportType="WORKFLOW" description="Letterhead (A5)"
              path="Letterhead/A5/Letterhead-A5.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Letterhead AddressBlock" archetype="SUBREPORT" reportType="WORKFLOW"
              description="Letterhead AddressBlock (A4)"
              path="Letterhead/A4/Letterhead AddressBlock.jrxml" mimeType="text/xml" docType="document.other"/>
    <template name="Letterhead AddressBlock-A5" archetype="SUBREPORT" reportType="WORKFLOW"
              description="Letterhead AddressBlock (A5)"
              path="Letterhead/A5/Letterhead AddressBlock-A5.jrxml" mimeType="text/xml" docType="document.other"/>

</templates>
