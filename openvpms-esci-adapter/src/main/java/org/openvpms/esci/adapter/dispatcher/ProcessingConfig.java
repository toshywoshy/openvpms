/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.esci.adapter.map.invoice.OrderResolver;

/**
 * ESCI processing configuration.
 *
 * @author Tim Anderson
 */
public class ProcessingConfig {

    /**
     * The order resolver. May be {@code null}
     */
    private final OrderResolver orderResolver;

    /**
     * Constructs a {@link ProcessingConfig}.
     *
     * @param resolver the order resolver, used when processing invoices. May be {@code null}
     */
    public ProcessingConfig(OrderResolver resolver) {
        this.orderResolver = resolver;
    }

    /**
     * Returns the order resolver to use when processing invoices.
     *
     * @return the order resolver. May be {@code null}
     */
    public OrderResolver getOrderResolver() {
        return orderResolver;
    }
}
