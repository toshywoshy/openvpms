/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.List;

/**
 * ESCI configuration.
 *
 * @author Tim Anderson
 */
public class ESCIConfig {

    /**
     * The configuration bean.
     */
    private final IMObjectBean bean;

    /**
     * The supplier.
     */
    private final Party supplier;

    /**
     * The stock location.
     */
    private final Party stockLocation;

    /**
     * Constructs an {@link ESCIConfig}.
     *
     * @param config        the configuration
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param service       the archetype service
     */
    public ESCIConfig(EntityRelationship config, Party supplier, Party stockLocation, ArchetypeService service) {
        bean = service.getBean(config);
        this.supplier = supplier;
        this.stockLocation = stockLocation;
    }

    /**
     * Returns the account identifier.
     *
     * @return the account identifier. May be {@code null}
     */
    public String getAccountId() {
        return bean.getString("accountId");
    }

    /**
     * Returns the user name.
     *
     * @return the user name
     */
    public String getUsername() {
        return bean.getString("username");
    }

    /**
     * Returns the password.
     *
     * @return the encrypted password
     */
    public String getPassword() {
        return bean.getString("password");
    }

    /**
     * Returns the service URL.
     *
     * @return the the service URL
     */
    public String getServiceURL() {
        return bean.getString("serviceURL");
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier
     */
    public Party getSupplier() {
        return supplier;
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location
     */
    public Party getStockLocation() {
        return stockLocation;
    }

    /**
     * Creates an ESCI configuration for the given supplier and stock location.
     *
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param service       the archetype service
     * @return the ESCI configuration, or {@code null} if there is no active relationship
     */
    public static ESCIConfig create(Party supplier, Party stockLocation, ArchetypeService service) {
        IMObjectBean bean = service.getBean(supplier);
        List<EntityRelationship> relationships = bean.getValues(
                "stockLocations", EntityRelationship.class,
                Predicates.<EntityRelationship>isA(SupplierArchetypes.SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI)
                        .and(Predicates.<EntityRelationship>activeNow()
                                     .and(Predicates.targetEquals(stockLocation))));
        return !relationships.isEmpty() ? new ESCIConfig(relationships.get(0), supplier, stockLocation, service) : null;
    }

}
