/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.map.invoice;

import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.esci.adapter.util.ESCIAdapterException;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link OrderResolver} that doesn't fail if referenced orders and order items don't exist.
 * <p/>
 * This can be used to process ESCI invoices with incorrect order references from broken implementations.
 *
 * @author Tim Anderson
 */
public class LenientOrderResolver extends AbstractOrderResolver {

    /**
     * The errors encountered when resolving orders and order items.
     */
    private List<ESCIAdapterException> errors = new ArrayList<>();

    /**
     * Constructs a {@link LenientOrderResolver}.
     *
     * @param service the archetype service
     */
    public LenientOrderResolver(ArchetypeService service) {
        super(service);
    }

    /**
     * Returns an order associated with an invoice.
     *
     * @param invoice       the invoice
     * @param supplier      the expected supplier of the order
     * @param stockLocation the expected stock location of the order
     * @return the corresponding order. May be {@code null} if it doesn't exist or is invalid
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public FinancialAct getOrder(UBLInvoice invoice, Party supplier, Party stockLocation) {
        FinancialAct order = null;
        try {
            order = super.getOrder(invoice, supplier, stockLocation);
        } catch (ESCIAdapterException exception) {
            errors.add(exception);
        }
        return order;
    }

    /**
     * Returns the order item associated with an invoice line.
     * <p/>
     * If there is no order item specified, but there is a document-level order, the result will contain it.
     * <p/>
     * This can be used when matching unreferenced invoice lines.
     *
     * @param line    the invoice line
     * @param context the mapping context
     * @return the corresponding order item, or {@code null} if the line isn't associated with an order item
     * @throws ESCIAdapterException      if the order item reference is malformed or references an invalid order
     * @throws ArchetypeServiceException for any archetype service error
     */
    @Override
    public OrderItem getOrderItem(UBLInvoiceLine line, MappingContext context) {
        OrderItem result;
        try {
            result = super.getOrderItem(line, context);
        } catch (ESCIAdapterException exception) {
            errors.add(exception);
            result = null;
            // NOTE: if enabled, the following code will enable unlinked items from the document-level order to
            // be matched. Not sure if this is desirable or not. TODO
            // FinancialAct docOrder = context.getDocumentOrder();
            // result = (docOrder != null) ? new OrderItem(docOrder) : null;
        }
        return result;
    }

    /**
     * Returns the errors encountered when processing the order.
     *
     * @return the errors
     */
    public List<ESCIAdapterException> getErrors() {
        return errors;
    }
}
