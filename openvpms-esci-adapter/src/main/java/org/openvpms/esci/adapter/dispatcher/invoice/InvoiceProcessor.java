/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.dispatcher.invoice;

import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.i18n.Message;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.esci.adapter.dispatcher.DocumentProcessor;
import org.openvpms.esci.adapter.dispatcher.InboxDocument;
import org.openvpms.esci.adapter.dispatcher.ProcessingConfig;
import org.openvpms.esci.adapter.i18n.ESCIAdapterMessages;
import org.openvpms.esci.adapter.map.invoice.DefaultOrderResolver;
import org.openvpms.esci.adapter.map.invoice.Delivery;
import org.openvpms.esci.adapter.map.invoice.InvoiceMapper;
import org.openvpms.esci.adapter.map.invoice.OrderResolver;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.ubl.invoice.InvoiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Maps invoices to <em>act.supplierDelivery</em> acts using {@link InvoiceMapper}.
 * <p>
 * UBL invoices are mapped to deliveries rather than <em>act.supplierAccountChargesInvoice</em> to reflect the fact
 * that practices may not use supplier invoices. An invoice can be created from the delivery if required.
 *
 * @author Tim Anderson
 */
public class InvoiceProcessor implements DocumentProcessor {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The invoice mapper.
     */
    private final InvoiceMapper mapper;

    /**
     * The listener to notify when an invoice is received. May be {@code null}
     */
    private final InvoiceListener listener;

    /**
     * The authentication context.
     */
    private final AuthenticationContext context;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(InvoiceProcessor.class);

    /**
     * Constructs an {@link InvoiceProcessor}.
     *
     * @param service  the archetype service
     * @param mapper   the invoice mapper
     * @param listener the invoice listener
     * @param context  the authentication context
     */
    public InvoiceProcessor(IArchetypeService service, InvoiceMapper mapper, InvoiceListener listener,
                            AuthenticationContext context) {
        this.service = service;
        this.mapper = mapper;
        this.listener = listener;
        this.context = context;
    }

    /**
     * Determines if this processor can handle the supplied document.
     *
     * @param document the document
     * @return {@code true} if the processor can handle the document, otherwise {@code false}
     */
    public boolean canHandle(InboxDocument document) {
        return document.getContent() instanceof InvoiceType;
    }

    /**
     * Processes an invoice.
     *
     * @param document      the document to process
     * @param supplier      the supplier submitting the invoice
     * @param stockLocation the stock location
     * @param accountId     the supplier account identifier
     * @param config        configures the document processing behaviour. May be {@code null}
     */
    public void process(InboxDocument document, Party supplier, Party stockLocation, String accountId,
                        ProcessingConfig config) {
        InvoiceType invoice = (InvoiceType) document.getContent();
        User user = context.getUser();
        try {
            if (user == null) {
                // no current user (being run as a background job), so try and set one from the stock location
                setAuthenticationContext(stockLocation);
            }
            OrderResolver resolver = (config != null) ? config.getOrderResolver() : null;
            if (resolver == null) {
                resolver = new DefaultOrderResolver(service);
            }
            Delivery delivery = mapper.map(invoice, supplier, stockLocation, accountId, resolver);
            service.save(delivery.getActs());
            notifyListener(delivery.getDelivery());
        } catch (Exception exception) {
            String invoiceId = (invoice.getID() != null) ? invoice.getID().getValue() : null;
            Message message = ESCIAdapterMessages.failedToProcessInvoice(
                    invoiceId, supplier, stockLocation, exception.getMessage());
            throw new ESCIAdapterException(message, exception);
        } finally {
            if (user == null) {
                // reset the context
                context.setUser(null);
            }
        }
    }

    /**
     * Notifies the registered listener that an invoice has been received and processed.
     *
     * @param delivery the delivery that the invoice was mapped to
     */
    protected void notifyListener(FinancialAct delivery) {
        InvoiceListener l = listener;
        if (l != null) {
            try {
                l.receivedInvoice(delivery);
            } catch (Exception exception) {
                log.error("InvoiceListener threw exception", exception);
            }
        }
    }

    /**
     * Sets the current user to the defaultAuthor from the stock location, if one is present.
     *
     * @param stockLocation the stock location
     */
    private void setAuthenticationContext(Party stockLocation) {
        // if there is no current user, run the processing in the context of the stockLocation's defaultAuthor
        // if one is present.
        IMObjectBean bean = service.getBean(stockLocation);
        User defaultAuthor = bean.getTarget("defaultAuthor", User.class, Policies.active());
        if (defaultAuthor != null) {
            context.setUser(defaultAuthor);
        }
    }

}
