/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.supplier.AbstractSupplierTest;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.IPage;
import org.openvpms.esci.adapter.map.UBLHelper;
import org.openvpms.esci.ubl.common.aggregate.CustomerPartyType;
import org.openvpms.esci.ubl.common.aggregate.SupplierPartyType;
import org.openvpms.esci.ubl.common.basic.CustomerAssignedAccountIDType;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;


/**
 * Base class for ESCI test cases.
 *
 * @author Tim Anderson
 */
public abstract class AbstractESCITest extends AbstractSupplierTest {

    /**
     * Helper to create a POSTED order with a single item.
     *
     * @return a new order
     */
    protected FinancialAct createOrder() {
        return createOrder(getSupplier());
    }

    /**
     * Helper to create a POSTED order with a single item.
     *
     * @param supplier the supplier
     * @return a new order
     */
    protected FinancialAct createOrder(Party supplier) {
        FinancialAct orderItem = createOrderItem(getProduct(), BigDecimal.ONE, 1, BigDecimal.ONE);
        IMObjectBean itemBean = getBean(orderItem);
        itemBean.setValue("reorderCode", "AREORDERCODE");
        itemBean.setValue("reorderDescription", "A reorder description");
        FinancialAct order = createOrder(supplier, orderItem);
        order.setStatus(ActStatus.POSTED);
        save(order, orderItem);
        return order;
    }

    /**
     * Helper to create a <tt>CustomerPartyType</tt>.
     *
     * @param stockLocation the stock location
     * @return a new <tt>CustomerPartyType</tt>
     */
    protected CustomerPartyType createCustomer(Party stockLocation) {
        CustomerPartyType result = new CustomerPartyType();
        result.setCustomerAssignedAccountID(UBLHelper.initID(new CustomerAssignedAccountIDType(),
                                                             stockLocation.getId()));
        return result;
    }

    /**
     * Helper to create a <tt>SupplierPartyType</tt>.
     *
     * @param supplier the supplier
     * @return a new <tt>SupplierPartyType</tt>
     */
    protected SupplierPartyType createSupplier(Party supplier) {
        SupplierPartyType result = new SupplierPartyType();
        result.setCustomerAssignedAccountID(UBLHelper.initID(new CustomerAssignedAccountIDType(), supplier.getId()));
        return result;
    }

    /**
     * Helper to return the URL of a WSDL file, given its resource path.
     *
     * @param resourcePath the path to the WSDL resource
     * @return the URL of the WSDL resource
     * @throws RuntimeException if the URL is invalid
     */
    protected String getWSDL(String resourcePath) {
        ClassPathResource wsdl = new ClassPathResource(resourcePath);
        try {
            return wsdl.getURL().toString();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * Adds an <em>entityRelationship.supplierStockLocationESCI</em> relationship between the supplier and stock
     * location.
     *
     * @param supplier the supplier
     * @param location the stock location
     * @param url      the order service URL
     * @return the relationship
     */
    protected EntityRelationship addESCIConfiguration(Party supplier, Party location, String url) {
        IMObjectBean bean = getBean(supplier);
        EntityRelationship relationship = (EntityRelationship) bean.addTarget(
                "stockLocations", SupplierArchetypes.SUPPLIER_STOCK_LOCATION_RELATIONSHIP_ESCI, location);
        IMObjectBean relBean = getBean(relationship);
        relBean.setValue("serviceURL", url);
        location.addEntityRelationship(relationship);
        save(supplier, location);
        return relationship;
    }

    /**
     * Verifies that an <em>act.systemMessage</em> has been created when an invoice is received.
     *
     * @param author   the author associated with the message
     * @param delivery the delivery associated with the message
     * @param reason   the expected reason
     */
    protected void checkSystemMessage(User author, FinancialAct delivery, String reason) {
        ArchetypeQuery query = new ArchetypeQuery(Constraints.shortName("act.systemMessage"));
        query.add(Constraints.join("to").add(Constraints.eq("entity", author.getObjectReference())));
        query.add(Constraints.join("item").add(Constraints.eq("target", delivery.getObjectReference())));
        IPage page = getArchetypeService().get(query);
        assertEquals(1, page.getResults().size());
        Act message = (Act) page.getResults().get(0);
        assertEquals(reason, message.getReason());
    }

    /**
     * Creates and associates an user with the stock location as the default author of invoices.
     *
     * @return the user
     */
    protected User initDefaultAuthor() {
        User author = TestHelper.createUser();
        IMObjectBean locBean = getBean(getStockLocation());
        Relationship relationship = locBean.setTarget("defaultAuthor", author);
        author.addEntityRelationship((EntityRelationship) relationship);
        locBean.save();
        return author;
    }
}
