/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.esci.adapter.dispatcher;

import org.junit.Test;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.esci.adapter.AbstractESCITest;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Tests the {@link ESCISuppliers} class.
 *
 * @author Tim Anderson
 */
public class ESCISuppliersTestCase extends AbstractESCITest {

    /**
     * Tests the {@link ESCISuppliers#getESCIConfigs()} method.
     */
    @Test
    public void testGetESCIConfigs() {
        ESCISuppliers suppliers = new ESCISuppliers(getArchetypeService());

        Party supplier1 = getSupplier();
        Party supplier2 = TestHelper.createSupplier();
        Party supplier3 = TestHelper.createSupplier();

        List<ESCIConfig> list1 = suppliers.getESCIConfigs();

        Party stockLocation1 = createStockLocation();
        Party stockLocation2 = createStockLocation();

        addESCIConfiguration(supplier1, stockLocation1, "https://localhost:8443/foo/bar1");
        addESCIConfiguration(supplier1, stockLocation2, "https://localhost:8443/foo/bar2");
        addESCIConfiguration(supplier2, stockLocation1, "https://localhost:8443/foo/bar1");
        addESCIConfiguration(supplier3, stockLocation2, "https://localhost:8443/foo/bar2");

        List<ESCIConfig> list2 = suppliers.getESCIConfigs();
        assertEquals(list1.size() + 4, list2.size());
        checkConfig(list2, supplier1, stockLocation1, "https://localhost:8443/foo/bar1", true);
        checkConfig(list2, supplier1, stockLocation2, "https://localhost:8443/foo/bar2", true);
        checkConfig(list2, supplier2, stockLocation1, "https://localhost:8443/foo/bar1", true);
        checkConfig(list2, supplier3, stockLocation2, "https://localhost:8443/foo/bar2", true);

        // deactivate supplier1 and verify it is no longer returned
        supplier1.setActive(false);
        save(supplier1);

        List<ESCIConfig> list3 = suppliers.getESCIConfigs();
        assertEquals(list1.size() + 2, list3.size());
        checkConfig(list3, supplier1, stockLocation1, "https://localhost:8443/foo/bar1", false);
        checkConfig(list3, supplier1, stockLocation2, "https://localhost:8443/foo/bar2", false);
        checkConfig(list3, supplier2, stockLocation1, "https://localhost:8443/foo/bar1", true);
        checkConfig(list3, supplier3, stockLocation2, "https://localhost:8443/foo/bar2", true);

        // deactivate stockLocation1 and verify suppliers linking to it are no longer returned
        stockLocation1.setActive(false);
        save(stockLocation1);

        List<ESCIConfig> list4 = suppliers.getESCIConfigs();
        assertEquals(list1.size() + 1, list4.size());
        checkConfig(list4, supplier1, stockLocation1, "https://localhost:8443/foo/bar1", false);
        checkConfig(list4, supplier1, stockLocation2, "https://localhost:8443/foo/bar2", false);
        checkConfig(list4, supplier2, stockLocation1, "https://localhost:8443/foo/bar1", false);
        checkConfig(list4, supplier3, stockLocation2, "https://localhost:8443/foo/bar2", true);
    }

    /**
     * Tests the {@link ESCISuppliers#getESCIConfigs()} when a relationship is deactivated.
     */
    @Test
    public void testGetESCIRelationships() {
        ESCISuppliers suppliers = new ESCISuppliers(getArchetypeService());

        Party supplier1 = getSupplier();
        Party supplier2 = TestHelper.createSupplier();

        List<ESCIConfig> list1 = suppliers.getESCIConfigs();

        Party stockLocation1 = createStockLocation();
        Party stockLocation2 = createStockLocation();

        EntityRelationship config1 = addESCIConfiguration(supplier1, stockLocation1, "https://localhost:8443/foo/bar1");
        addESCIConfiguration(supplier1, stockLocation2, "https://localhost:8443/foo/bar2");
        addESCIConfiguration(supplier2, stockLocation1, "https://localhost:8443/foo/bar1");

        List<ESCIConfig> list2 = suppliers.getESCIConfigs();
        assertEquals(list1.size() + 3, list2.size());

        checkConfig(list2, supplier1, stockLocation1, "https://localhost:8443/foo/bar1", true);
        checkConfig(list2, supplier1, stockLocation2, "https://localhost:8443/foo/bar2", true);
        checkConfig(list2, supplier2, stockLocation1, "https://localhost:8443/foo/bar1", true);

        // deactivate the first relationship, and verify it is no longer returned
        config1.setActiveEndTime(new Date(System.currentTimeMillis() - 1000));
        save(supplier1, stockLocation1);

        List<ESCIConfig> list3 = suppliers.getESCIConfigs();
        assertEquals(list1.size() + 2, list3.size());

        checkConfig(list3, supplier1, stockLocation1, "https://localhost:8443/foo/bar1", false);
        checkConfig(list3, supplier1, stockLocation2, "https://localhost:8443/foo/bar2", true);
        checkConfig(list3, supplier2, stockLocation1, "https://localhost:8443/foo/bar1", true);
    }

    /**
     * Checks if a config exists or not.
     *
     * @param configs       the configurations
     * @param supplier      the expected supplier
     * @param stockLocation the expected stock location
     * @param url           the expected URL
     * @param exists        if {@code true}, the config must exist, else it mustn't
     */
    private void checkConfig(List<ESCIConfig> configs, Party supplier, Party stockLocation, String url,
                             boolean exists) {
        boolean found = false;
        for (ESCIConfig config : configs) {
            if (config.getSupplier().equals(supplier) && config.getStockLocation().equals(stockLocation)
                && url.equals(config.getServiceURL())) {
                found = true;
                break;
            }
        }
        assertEquals(exists, found);
    }

}
