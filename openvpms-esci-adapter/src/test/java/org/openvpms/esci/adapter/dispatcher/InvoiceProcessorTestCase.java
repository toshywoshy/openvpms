/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.adapter.dispatcher;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.SystemMessageReason;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.user.User;
import org.openvpms.esci.FutureValue;
import org.openvpms.esci.adapter.dispatcher.invoice.InvoiceProcessor;
import org.openvpms.esci.adapter.dispatcher.invoice.SystemMessageInvoiceListener;
import org.openvpms.esci.adapter.map.UBLHelper;
import org.openvpms.esci.adapter.map.invoice.AbstractInvoiceTest;
import org.openvpms.esci.adapter.map.invoice.LenientOrderResolver;
import org.openvpms.esci.adapter.util.ESCIAdapterException;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.invoice.Invoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;


/**
 * Tests the {@link InvoiceProcessor} class.
 *
 * @author Tim Anderson
 */
public class InvoiceProcessorTestCase extends AbstractInvoiceTest {

    /**
     * Tests processing an invoice.
     *
     * @throws Exception for any error
     */
    @Test
    public void testProcess() throws Exception {
        // create an author for invoices
        User author = initDefaultAuthor();

        // set up the invoice listener
        FutureValue<FinancialAct> future = new FutureValue<>();
        SystemMessageInvoiceListener listener = new SystemMessageInvoiceListener(getArchetypeService()) {
            public void receivedInvoice(FinancialAct delivery) {
                super.receivedInvoice(delivery);
                future.set(delivery);
            }
        };
        AuthenticationContextImpl context = new AuthenticationContextImpl();
        context.setUser(null);
        InvoiceProcessor processor = new InvoiceProcessor(getArchetypeService(), createMapper(), listener, context);

        // submit an invoice
        InboxDocument invoice = createInvoiceDocument();
        processor.process(invoice, getSupplier(), getStockLocation(), null, null);

        // verify the delivery was created
        FinancialAct delivery = future.get(1000);
        assertNotNull(delivery);

        // verify the defaultAuthor was propagated to the delivery
        assertEquals(author.getObjectReference(), delivery.getCreatedBy());

        // verify a system message was sent to the author
        checkSystemMessage(author, delivery, SystemMessageReason.ORDER_INVOICED);

        assertNull(context.getUser()); // verify the context has been reset
    }

    /**
     * Verifies that when there is a logged-in user, that user appears on the delivery, and receives the notification.
     *
     * @throws Exception for any error
     */
    @Test
    public void testProcessCanInheritCurrentUser() throws Exception {
        User user = TestHelper.createUser();
        AuthenticationContextImpl context = new AuthenticationContextImpl();
        context.setUser(user);

        // set up the invoice listener
        FutureValue<FinancialAct> future = new FutureValue<>();
        SystemMessageInvoiceListener listener = new SystemMessageInvoiceListener(getArchetypeService()) {
            public void receivedInvoice(FinancialAct delivery) {
                super.receivedInvoice(delivery);
                future.set(delivery);
            }
        };
        InvoiceProcessor processor = new InvoiceProcessor(getArchetypeService(), createMapper(), listener, context);

        // submit an invoice
        InboxDocument invoice = createInvoiceDocument();
        processor.process(invoice, getSupplier(), getStockLocation(), null, null);

        // verify the delivery was created
        FinancialAct delivery = future.get(1000);
        assertNotNull(delivery);

        // verify the user was propagated to the delivery
        assertEquals(user.getObjectReference(), delivery.getCreatedBy());

        // verify a system message was sent to the user
        checkSystemMessage(user, delivery, SystemMessageReason.ORDER_INVOICED);

        assertEquals(user, context.getUser()); // verify the context is unchanged
    }

    /**
     * Verifies that an {@link ESCIAdapterException} is raised if {@link DocumentProcessor#process} encounters
     * an unexpected exception.
     */
    @Test
    public void testFailedToProcessInvoice() {
        getStockLocation().setName("Main Stock");
        getSupplier().setName("Vetshare");
        InboxDocument invoice = createInvoiceDocument();

        InvoiceProcessor processor = new InvoiceProcessor(getArchetypeService(), createMapper(), null,
                                                          new AuthenticationContextImpl()) {
            @Override
            protected void notifyListener(FinancialAct delivery) {
                throw new RuntimeException("Foo");
            }
        };

        checkSubmitException(invoice, processor, "ESCIA-0700: Failed to process Invoice 12345 for supplier Vetshare ("
                                                 + getSupplier().getId() + ") and stock location Main Stock ("
                                                 + getStockLocation().getId() + "): Foo");
    }

    /**
     * Verifies that invalid order references can be ignored using an {@link LenientOrderResolver}.
     *
     * @throws Exception for any error
     */
    @Test
    public void testIgnoreInvalidOrderReferences() throws Exception {
        // set up the invoice listener
        FutureValue<FinancialAct> future = new FutureValue<>();
        SystemMessageInvoiceListener listener = new SystemMessageInvoiceListener(getArchetypeService()) {
            public void receivedInvoice(FinancialAct delivery) {
                super.receivedInvoice(delivery);
                future.set(delivery);
            }
        };
        AuthenticationContextImpl context = new AuthenticationContextImpl();
        context.setUser(null);
        InvoiceProcessor processor = new InvoiceProcessor(getArchetypeService(), createMapper(), listener, context);

        // submit an invoice with an invalid order reference
        Invoice invoice = createInvoice();
        invoice.setOrderReference(UBLHelper.createOrderReference(0));

        InboxDocument document = new InboxDocument(new DocumentReferenceType(), invoice);
        try {
            processor.process(document, getSupplier(), getStockLocation(), null, null);
            fail("Expected processing to fail");
        } catch (ESCIAdapterException expected) {
            // do nothing
        }

        // reprocess the invoice, this time suppressing invalid orders
        LenientOrderResolver resolver = new LenientOrderResolver(getArchetypeService());
        ProcessingConfig config = new ProcessingConfig(resolver);
        processor.process(document, getSupplier(), getStockLocation(), null, config);
        assertEquals(1, resolver.getErrors().size());
        assertEquals("ESCIA-0108: Invalid Order: 0 referenced by Invoice: 12345",
                     resolver.getErrors().get(0).getMessage());

        // verify the delivery was created
        FinancialAct delivery = future.get(1000);
        assertNotNull(delivery);
    }

    /**
     * Verifies that {@link DocumentProcessor#process} fails with the expected message.
     *
     * @param invoice   the invoice
     * @param processor the processor
     * @param expected  the expected message
     */
    private void checkSubmitException(InboxDocument invoice, InvoiceProcessor processor, String expected) {
        try {
            processor.process(invoice, getSupplier(), getStockLocation(), null, null);
            fail("Expected submitInvoice() to fail");
        } catch (ESCIAdapterException exception) {
            assertEquals(expected, exception.getMessage());
        }
    }


    private InboxDocument createInvoiceDocument() {
        return new InboxDocument(new DocumentReferenceType(), createInvoice());
    }

}
