package org.openvpms.web.webdav.resource;

import io.milton.http.LockManager;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.webdav.session.Session;

import java.util.Date;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DocumentActResource} class.
 *
 * @author Tim Anderson
 */
public class DocumentActResourceTestCase extends ArchetypeServiceTest {

    /**
     * Verifies that file names with single quotes are encoded as a workaround for OVPMS-2220.
     *
     * @throws Exception for any error
     */
    @Test
    public void testFileNameEncoding() throws Exception {
        Session session = Mockito.mock(Session.class);
        DocumentAct act = PatientTestHelper.createDocumentLetter(new Date(), TestHelper.createPatient());
        String fileName = "Referral Letter for Muffet O'Brien.txt";
        String encodedName = "Referral Letter for Muffet O Brien.txt";
        act.setFileName(fileName);

        LockManager lockManager = Mockito.mock(LockManager.class);
        DocumentHandlers handlers = new DocumentHandlers(getArchetypeService());
        DocumentActResource documentActResource = new DocumentActResource(act, session, getArchetypeService(),
                                                                          handlers, lockManager, new Date());
        assertEquals(Long.toString(act.getId()), documentActResource.getName());
        assertTrue(documentActResource.getChildren().isEmpty());

        // add some content
        String mimeType = "text/plain";
        DocumentHandler handler = handlers.get(fileName, mimeType);
        Document document = handler.create(fileName, IOUtils.toInputStream("some plain text", UTF_8), mimeType, -1);
        act.setDocument(document.getObjectReference());
        save(act, document);

        assertEquals(1, documentActResource.getChildren().size());

        assertNull(documentActResource.child(fileName)); // need to use the encoded name

        DocumentResource documentResource = (DocumentResource) documentActResource.child(encodedName);
        assertNotNull(documentResource);
        assertEquals(encodedName, documentResource.getName());
    }
}
