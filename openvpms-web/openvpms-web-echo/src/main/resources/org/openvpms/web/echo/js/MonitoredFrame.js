/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

MonitoredFrame = function (elementId) {
    this.elementId = elementId;
    EP.ObjectMap.put(elementId, this);


};

/**
 * MonitoredFrame has a ServerMessage processor.
 */
MonitoredFrame.MessageProcessor = function () {
};

MonitoredFrame.MessageProcessor.process = function (messagePartElement) {
    for (var i = 0; i < messagePartElement.childNodes.length; ++i) {
        if (messagePartElement.childNodes[i].nodeType == 1) {
            switch (messagePartElement.childNodes[i].tagName) {
                case "init":
                    MonitoredFrame.MessageProcessor.processInit(messagePartElement.childNodes[i]);
                    break;
                case "dispose":
                    MonitoredFrame.MessageProcessor.processDispose(messagePartElement.childNodes[i]);
                    break;
                case "scroll-horizontal":
                    MonitoredFrame.MessageProcessor.processScroll(messagePartElement.childNodes[i]);
                    break;
                case "scroll-vertical":
                    MonitoredFrame.MessageProcessor.processScroll(messagePartElement.childNodes[i]);
                    break;
            }
        }
    }
};

MonitoredFrame.MessageProcessor.processDispose = function (disposeMessageElement) {
    for (var item = disposeMessageElement.firstChild; item; item = item.nextSibling) {
        var elementId = item.getAttribute("eid");
        EP.ObjectMap.destroy(elementId);
    }
};


MonitoredFrame.MessageProcessor.processInit = function (initMessageElement) {
    for (var item = initMessageElement.firstChild; item; item = item.nextSibling) {
        var elementId = item.getAttribute("eid");
        EP.ObjectMap.destroy(elementId);
        var frame = new MonitoredFrame(elementId);
        frame.init(item);
    }
};

MonitoredFrame.prototype.destroy = function () {
    var divE = document.getElementById(this.elementId);
    if (divE) {
        EP.Event.removeHandler('scroll', divE);
    }
    EchoDomUtil.removeEventListener(window, 'message', this.handler, false);

    if (this.stretcher) {
        this.stretcher.destroy();
    }
};

/*
 * ---------------------------
 */
MonitoredFrame.prototype.init = function (itemXML) {
    this.divE = document.getElementById(this.elementId);
    var horizontalScroll = itemXML.getAttribute("horizontal-scroll");
    var verticalScroll = itemXML.getAttribute("vertical-scroll");
    var frame = document.getElementById(this.elementId + "_frame");
    this.url = frame.src;

    if (horizontalScroll) {
        this.divE.scrollLeft = parseInt(horizontalScroll, 10);
    }
    if (verticalScroll) {
        this.divE.scrollTop = parseInt(verticalScroll, 10);
    }
    EP.Event.addHandler('scroll', this.divE, this);

    var self = this;
    this.handler = function (e) {
        self.processMessage(e);
    };
    EchoDomUtil.addEventListener(window, 'message', this.handler, false);

    // Only run the virtual position checking code
    // if virtual positioning is enabled.
    if (EchoVirtualPosition.enabled) {

        // See if our own element needs VP support
        if (MonitoredFrame.isVPRequired(this.divE)) {
            EchoVirtualPosition.register(this.elementId);
        }

        // Since all children of our div are purely our own
        // wrapper divs, see if the styles defined in their
        // LayoutData require VP support as well
        for (var count = 0; count < this.divE.childNodes.length; count++) {

            var node = this.divE.childNodes[count];

            if (MonitoredFrame.isVPRequired(node)) {
                EchoVirtualPosition.register(node.id);
            }
        }
    }

    // do they want us to stretch ourselves
    var heightStretched = EP.DOM.getBooleanAttr(itemXML, "heightStretched", false);
    if (heightStretched) {
        var minHeight = EP.DOM.getIntAttr(itemXML, "minimumStretchedHeight", null);
        var maxHeight = EP.DOM.getIntAttr(itemXML, "maximumStretchedHeight", null);
        this.stretcher = EP.Stretch.verticalStretcher.getInstance(this.elementId, minHeight, maxHeight);
    }
};

/**
 * Checks if virtual positioning is required on a given element.
 *
 * @param The DOM element to check.
 * @return True if virtual positioning is need on the element. false otherwise.
 */
MonitoredFrame.isVPRequired = function (element) {

    var isPx = EchoVirtualPosition.verifyPixelValue;
    var isPxUndef = EchoVirtualPosition.verifyPixelOrUndefinedValue;
    var s = element.style;

    // Check vertical positioning properties
    var verticalPos = (isPx(s.top) && isPx(s.bottom)
                       && isPxUndef(s.paddingTop) && isPxUndef(s.paddingBottom)
                       && isPxUndef(s.marginTop) && isPxUndef(s.marginBottom)
                       && isPxUndef(s.borderTopWidth) && isPxUndef(s.borderBottomWidth));

    if (verticalPos) {
        return true;
    }

    // Check horizontal positioning properties
    return (isPx(s.left) && isPx(s.right)
            && isPxUndef(s.paddingLeft) && isPxUndef(s.paddingRight)
            && isPxUndef(s.marginLeft) && isPxUndef(s.marginRight)
            && isPxUndef(s.borderLeftWidth) && isPxUndef(s.borderRightWidth));
};

/*
 * -----------------------------------  
 */
MonitoredFrame.MessageProcessor.processScroll = function (scrollMessageElement) {
    var elementId = scrollMessageElement.getAttribute("eid");
    var position = parseInt(scrollMessageElement.getAttribute("position"), 10);
    var divElement = document.getElementById(elementId);
    //
    // handle -1 as down to the bottom/right
    if (scrollMessageElement.nodeName == "scroll-horizontal") {
        if (position < 0) {
            position = 1000000;
        }
        divElement.scrollLeft = position;
    } else if (scrollMessageElement.nodeName == "scroll-vertical") {
        if (position < 0) {
            position = 1000000;
        }
        divElement.scrollTop = position;
    }
};

/*
 * -----------------------------------  
 */
MonitoredFrame.prototype.eventHandler = function (echoEvent) {
    if (!EchoClientEngine.verifyInput(this.elementId)) {
        return;
    }
    EchoClientMessage.setPropertyValue(this.elementId, "horizontalScroll", this.divE.scrollLeft + "px");
    EchoClientMessage.setPropertyValue(this.elementId, "verticalScroll", this.divE.scrollTop + "px");
};

MonitoredFrame.prototype.processMessage = function (e) {
    if (!EchoClientEngine.verifyInput(this.elementId)) {
        return;
    }
    echoEvent = e ? e : window.event;
    if (echoEvent.origin && this.url.startsWith(echoEvent.origin)) {
        // only notify the server if the event comes from the expected origin
        EchoClientMessage.setActionValue(this.elementId, "message", echoEvent.data);
        EchoServerTransaction.connect();
    }
};

