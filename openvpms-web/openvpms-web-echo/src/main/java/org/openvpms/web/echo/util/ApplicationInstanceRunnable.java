/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.util;

import nextapp.echo2.app.WindowPane;

/**
 * A {@link Runnable} that runs an underlying instance via a {@link TaskQueue}.<br/>
 * This allows the {@link Runnable} to update the user interface.
 * <p/>
 * Note that instances created prior to a {@link WindowPane} being displayed will have their execution suspended until
 * the dialog is closed, to avoid unwanted UI synchronisation events being generated<br/>
 * By default, any invocations to {@link #run} during this time will be discarded, unless a
 * {@link TaskQueues.QueueMode} is specified at construction.
 *
 * @author Tim Anderson
 * @see TaskQueue
 * @see TaskQueues
 */
public class ApplicationInstanceRunnable extends ApplicationInstanceFunctor implements Runnable {

    /**
     * The runnable to delegate to.
     */
    private final Runnable runnable;

    /**
     * Constructs an {@link ApplicationInstanceRunnable}.
     *
     * @param runnable the runnable
     */
    public ApplicationInstanceRunnable(Runnable runnable) {
        this(0, TaskQueues.QueueMode.DISCARD, runnable);
    }

    /**
     * Constructs an {@link ApplicationInstanceRunnable}.
     *
     * @param interval  the interval in seconds between asynchronous callbacks from the client to check
     *                  for queued tasks produced by this, or {@code 0} to use the default
     * @param queueMode determines how tasks are queued while the queue is stopped
     * @param runnable  the runnable
     */
    public ApplicationInstanceRunnable(int interval, TaskQueues.QueueMode queueMode, Runnable runnable) {
        super(interval, queueMode);
        this.runnable = runnable;
    }

    /**
     * Schedules the runnable.
     */
    @Override
    public void run() {
        enqueueTask(runnable);
    }

}
