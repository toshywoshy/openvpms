/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.factory;

import echopointng.LabelEx;
import echopointng.xhtml.XhtmlFragment;
import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.ImageReference;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.layout.CellLayoutData;
import org.apache.commons.text.StringEscapeUtils;
import org.openvpms.web.echo.text.TextHelper;
import org.openvpms.web.resource.i18n.format.NumberFormatter;


/**
 * Factory for {@link Label}s. Labels returned by this factory filter
 * invalid characters, using {@link TextHelper}.
 *
 * @author Tim Anderson
 */
public final class LabelFactory extends ComponentFactory {

    /**
     * Component type.
     */
    private static final String TYPE = "label";


    /**
     * Create a new label, with the default style.
     *
     * @return a new label
     */
    public static Label create() {
        return create(false);
    }

    /**
     * Creates a new label with the specified text and default style.
     * <p/>
     * This may contain multiple lines.
     *
     * @param text the label text
     * @return a new label
     */
    public static Label text(String text) {
        Label label = create(true);
        label.setText(text);
        return label;
    }

    /**
     * Creates a new label with the specified text.
     * <p/>
     * This may contain multiple lines.
     *
     * @param text  the label text
     * @param style the style name
     * @return a new label
     */
    public static Label text(String text, String style) {
        return text(text, style, false);
    }

    /**
     * Creates a new label with the specified text.
     * <p/>
     * This may contain multiple lines.
     *
     * @param text  the label text
     * @param style the style name
     * @param wrap  if {@code true}, wrap long lines
     * @return a new label
     */
    public static Label text(String text, String style, boolean wrap) {
        Label label = text(text, wrap);
        label.setStyleName(style);
        label.setLineWrap(wrap);
        return label;
    }

    /**
     * Creates a new label with the specified text.
     * <p/>
     * This may contain multiple lines.
     *
     * @param text the label text
     * @param wrap if {@code true}, wrap long lines
     * @return a new label
     */
    public static Label text(String text, boolean wrap) {
        Label label = text(text);
        label.setLineWrap(wrap);
        return label;
    }

    /**
     * Creates a label from an HTML fragment.
     *
     * @param html the html fragment
     * @return a new label
     */
    public static Label html(String html) {
        return html(html, false);
    }

    /**
     * Creates a label from an HTML fragment.
     *
     * @param html the html fragment
     * @param wrap if {@code true}, long lines will be wrapped
     * @return a new label
     */
    public static Label html(String html, boolean wrap) {
        html = "<span xmlns='http://www.w3.org/1999/xhtml'>" + replaceControlChars(html) + "</span>";
        LabelEx label = new LabelEx(new XhtmlFragment(html));
        label.setLineWrap(wrap);
        return label;
    }

    /**
     * Creates a new label that may support multiple lines.
     *
     * @param multiline if {@code true}, interprets new lines in the text
     * @return a new label
     */
    public static Label create(boolean multiline) {
        return create(multiline, false);
    }

    /**
     * Creates a new label that may support multiple lines, and wrap long lines.
     *
     * @param multiline if {@code true}, interprets new lines in the text
     * @param wrap      if {@code true}, long lines will be wrapped
     * @return a new label
     */
    public static Label create(boolean multiline, boolean wrap) {
        Label result;
        if (multiline) {
            LabelEx label = new ProtectedLabelEx();
            label.setIntepretNewlines(true);
            result = label;
        } else {
            result = new Label() {
                @Override
                public void setText(String newValue) {
                    newValue = replaceControlChars(newValue);
                    super.setText(newValue);
                }
            };
        }
        result.setLineWrap(wrap);
        setDefaultStyle(result);
        return result;
    }

    /**
     * Create a new label with an image.
     *
     * @param image the image
     * @return a new label.
     */
    public static Label create(ImageReference image) {
        Label label = create();
        label.setIcon(image);
        return label;
    }

    /**
     * Create a new label with localised text, and default style.
     *
     * @param key the resource bundle key. May be {@code null}
     * @return a new label
     */
    public static Label create(String key) {
        return create(key, false);
    }

    /**
     * Create a new label with localised text, and default style.
     *
     * @param key       the resource bundle key. May be {@code null}
     * @param multiline if {@code true}, iterprets new lines in the text
     * @return a new label
     */
    public static Label create(String key, boolean multiline) {
        return create(key, multiline, false);
    }

    /**
     * Create a new label with localised text, and default style.
     *
     * @param key       the resource bundle key. May be {@code null}
     * @param multiline if {@code true}, interprets new lines in the text
     * @param wrap      if {@code true}, long lines will be wrapped
     * @return a new label
     */
    public static Label create(String key, boolean multiline, boolean wrap) {
        Label label = create(multiline, wrap);
        if (key != null) {
            label.setText(getString(TYPE, key, false));
        }
        return label;
    }

    /**
     * Create a new label with localised text, and specific style.
     *
     * @param key   the resource bundle key. May be {@code null}
     * @param style the label style
     * @return a new label
     */
    public static Label create(String key, String style) {
        Label label = create(key);
        setStyle(label, style);
        return label;
    }

    /**
     * Creates a new label for a numeric value, to be right aligned in a cell.
     *
     * @param value  the value
     * @param layout the layout to assign the label
     * @return a new label
     */
    public static Label create(Number value, CellLayoutData layout) {
        Label label = create();
        label.setText(NumberFormatter.format(value));
        return rightAlign(label, layout);
    }

    /**
     * Creates a new label for a numeric value, to be right aligned in a cell.
     *
     * @param value  the value
     * @param layout the layout to assign the label
     * @return a new label
     */
    public static Label create(long value, CellLayoutData layout) {
        Label label = create();
        label.setText(NumberFormatter.format(value));
        return rightAlign(label, layout);
    }

    /**
     * Creates a preformatted label.
     *
     * @param text the text. May be {@code null}
     * @return the preformatted label
     */
    public static Label preformatted(String text) {
        return preformatted(text, false);
    }

    /**
     * Creates a preformatted label.
     *
     * @param text the text. May be {@code null}
     * @param wrap if {@code true}, wrap long lines
     * @return the preformatted label
     */
    public static Label preformatted(String text, boolean wrap) {
        PreformattedLabelEx label = new PreformattedLabelEx(wrap);
        label.setText(text);
        setDefaultStyle(label);
        return label;
    }

    /**
     * Helper to right-align a label.
     *
     * @param label  the label to align
     * @param layout the layout to assign the label
     * @return the label
     */
    private static Label rightAlign(Label label, CellLayoutData layout) {
        layout.setAlignment(new Alignment(Alignment.RIGHT, Alignment.DEFAULT));
        label.setLayoutData(layout);
        return label;
    }

    /**
     * Replaces control characters in text.
     *
     * @param text the text
     * @return the updated text
     */
    private static String replaceControlChars(String text) {
        if (TextHelper.hasControlChars(text)) {
            // replace any control chars with spaces.
            text = TextHelper.replaceControlChars(text, " ");
        }
        return text;
    }

    /**
     * A {@code LabelEx} that replaces invalid characters with spaces.
     */
    private static class ProtectedLabelEx extends LabelEx {
        @Override
        public void setText(String newValue) {
            super.setText(replaceControlChars(newValue));
        }
    }

    /**
     * A {@code LabelEx} that preserves whitespace.
     */
    private static class PreformattedLabelEx extends LabelEx {

        private final boolean wrap;

        public PreformattedLabelEx(boolean wrap) {
            this.wrap = wrap;
        }

        @Override
        public void setText(String newValue) {
            if (newValue != null) {
                newValue = replaceControlChars(newValue);
                StringBuilder builder = new StringBuilder();
                builder.append("<div xmlns='http://www.w3.org/1999/xhtml' ");
                builder.append(wrap ? "style='white-space:pre-wrap'>" : "style='white-space:pre'>");
                builder.append(StringEscapeUtils.escapeXml11(newValue));
                builder.append("</div>");
                setText(new XhtmlFragment(builder.toString()));
            } else {
                super.setText(newValue);
            }
        }
    }

}
