/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.style;

import java.util.Map;

/**
 * Manages {@link Theme themes}.
 *
 * @author Tim Anderson
 */
public interface Themes {

    /**
     * Returns the theme with the given identifier.
     *
     * @param id the theme identifier
     * @return the corresponding theme, or {@code null} if none is found
     */
    Theme getTheme(String id);

    /**
     * Returns the themes, keyed on identifier.
     *
     * @return the themes
     */
    Map<String, Theme> getThemes();

}