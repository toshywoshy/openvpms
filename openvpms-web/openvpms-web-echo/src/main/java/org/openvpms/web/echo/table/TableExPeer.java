/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.table;

import echopointng.EPNG;
import echopointng.layout.TableLayoutDataEx;
import echopointng.ui.resource.Resources;
import echopointng.ui.util.CssStyleEx;
import echopointng.ui.util.Render;
import echopointng.ui.util.RenderingContext;
import echopointng.xhtml.XhtmlFragment;
import nextapp.echo2.app.Border;
import nextapp.echo2.app.Color;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Extent;
import nextapp.echo2.app.Insets;
import nextapp.echo2.app.LayoutData;
import nextapp.echo2.app.Style;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.layout.TableLayoutData;
import nextapp.echo2.app.update.ServerComponentUpdate;
import nextapp.echo2.webcontainer.ComponentSynchronizePeer;
import nextapp.echo2.webcontainer.ContainerInstance;
import nextapp.echo2.webcontainer.DomUpdateSupport;
import nextapp.echo2.webcontainer.RenderContext;
import nextapp.echo2.webcontainer.SynchronizePeerFactory;
import nextapp.echo2.webcontainer.propertyrender.BorderRender;
import nextapp.echo2.webcontainer.propertyrender.CellLayoutDataRender;
import nextapp.echo2.webcontainer.propertyrender.ColorRender;
import nextapp.echo2.webcontainer.propertyrender.ExtentRender;
import nextapp.echo2.webcontainer.propertyrender.FontRender;
import nextapp.echo2.webcontainer.propertyrender.InsetsRender;
import nextapp.echo2.webrender.ServerMessage;
import nextapp.echo2.webrender.Service;
import nextapp.echo2.webrender.WebRenderServlet;
import nextapp.echo2.webrender.output.CssStyle;
import nextapp.echo2.webrender.service.JavaScriptService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Peer for {@link TableEx}.
 * <p/>
 * This duplicates large slabs of EPNG TableExPeer in order to support fixed headers.
 *
 * @author Tim Anderson
 */
public class TableExPeer extends echopointng.ui.syncpeer.TableExPeer {

    /**
     * The table javascript.
     */
    private static final Service TABLEEX_SERVICE = JavaScriptService.forResource(
            "EPNG.TableEx", "/org/openvpms/web/echo/js/TableEx.js");

    static {
        WebRenderServlet.getServiceRegistry().remove(TABLEEX_SERVICE); // remove the existing tableex.js
        WebRenderServlet.getServiceRegistry().add(TABLEEX_SERVICE);
    }

    /**
     * Constructs  a {@link TableExPeer}.
     */
    public TableExPeer() {
        super();
    }

    /**
     * Renders the component in its entirety as a child of the provided
     * parent <code>Element</code>.  The implementation should additionally
     * render any child components, either by invoking their
     * <code>renderHtml()</code> methods if their peers also implement
     * <code>DomUpdateSupport</code> or by invoking their
     * <code>ComponentSynchronizePeer.renderAdd()</code> methods if they do
     * not.
     * <p>
     * The implementation must also perform any non-HTML-rendering operations
     * which are performed in the <code>ComponentSynchronizePeer.renderAdd()</code>
     * method, e.g., adding message parts that registering event listeners on
     * the client.
     *
     * @param rcOrig     the relevant <code>RenderContext</code>
     * @param update     the <code>ServerComponentUpdate</code> for which this
     *                   rendering is being performed
     * @param parentNode the parent DOM node to which this child should
     *                   add HTML code
     * @param component  the <code>Component</code> to be rendered
     */
    public void renderHtml(RenderContext rcOrig, ServerComponentUpdate update, Node parentNode, Component component) {
        RenderingContext rc = new RenderingContext(rcOrig, update, component);
        Style fallbackStyle = EPNG.getFallBackStyle(component);
        ServerMessage serverMessage = rc.getServerMessage();
        serverMessage.addLibrary(Resources.EP_SCRIPT_SERVICE.getId());
        serverMessage.addLibrary(Resources.EP_DRAG_SERVICE.getId());
        serverMessage.addLibrary(TABLEEX_SERVICE.getId());
        TableEx table = (TableEx) component;
        String elementId = ContainerInstance.getElementId(table);
        Element itemXML = this.renderInitDirective(rc, table, fallbackStyle);
        Document document = parentNode.getOwnerDocument();
        Border border = (Border) rc.getRP("border", fallbackStyle);
        Extent borderSize = border == null ? null : border.getSize();
        Extent width = (Extent) rc.getRP("width", fallbackStyle);
        Extent height = (Extent) rc.getRP("height", fallbackStyle);
        boolean selectionEnabled = rc.getRP("selectionEnabled", fallbackStyle, false);
        boolean headerVisible = table.isHeaderVisible();
        boolean footerVisible = table.isFooterVisible();
        int rowCount = table.getModel().getRowCount();
        Insets tableInsets = (Insets) rc.getRP("insets", fallbackStyle);
        String defaultInsetsAttributeValue = tableInsets == null ? "0px"
                                                                 : InsetsRender.renderCssAttributeValue(tableInsets);
        Element contentDivE;
        Element tbodyElement;
        if (rc.getRP("scrollable", fallbackStyle, false)) {
            // NOTE: not supported
            Element outerDivE = document.createElement("div");
            outerDivE.setAttribute("id", elementId);
            outerDivE.setAttribute("style", "padding:0px;margin:0px");
            rc.addStandardWebSupport(component, outerDivE);
            if (headerVisible) {
                contentDivE = renderResizeableHeaderFooterAreas(rc, table, fallbackStyle, border,
                                                                defaultInsetsAttributeValue, itemXML, -1,
                                                                headerVisible, "header", rowCount);
                outerDivE.appendChild(contentDivE);
            }

            contentDivE = document.createElement("div");
            CssStyleEx style = new CssStyleEx(component, fallbackStyle);
            style.setAttribute("overflow", "auto");
            style.setAttribute("padding", "0px");
            style.setAttribute("margin", "0px");
            ExtentRender.renderToStyle(style, "width", width);
            if (rowCount > 0) {
                ExtentRender.renderToStyle(style, "height", height);
                Render.asBorder(style, border);
                if (headerVisible) {
                    style.setAttribute("border-top-width", "0px");
                }
            }

            contentDivE.setAttribute("style", style.renderInline());
            contentDivE.setAttribute("id", elementId + "_contentDiv");
            tbodyElement = document.createElement("table");
            style = new CssStyleEx();
            style.setAttribute("table-layout", "fixed");
            style.setAttribute("width", "100%");
            if (selectionEnabled && rowCount > 0) {
                style.setAttribute("cursor", "pointer");
            }

            tbodyElement.setAttribute("style", style.renderInline());
            tbodyElement.setAttribute("id", elementId + "_contentTable");
            tbodyElement.setAttribute("cellpadding", "0");
            tbodyElement.setAttribute("cellspacing", "0");
            tbodyElement.setAttribute("border", "0");
            Element colgroupE = this.renderColGroup(table, document);
            if (colgroupE != null) {
                tbodyElement.appendChild(colgroupE);
            }

            Element contentTbodyElement = document.createElement("tbody");

            for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
                this.renderRow(rc, contentTbodyElement, table, rowIndex, defaultInsetsAttributeValue, itemXML,
                               fallbackStyle);
            }

            tbodyElement.appendChild(contentTbodyElement);
            contentDivE.appendChild(tbodyElement);
            outerDivE.appendChild(contentDivE);
            if (footerVisible) {
                Element footerDivE = this.renderResizeableHeaderFooterAreas(rc, table, fallbackStyle, border,
                                                                            defaultInsetsAttributeValue, itemXML, -2, footerVisible, "footer", rowCount);
                outerDivE.appendChild(footerDivE);
            }

            parentNode.appendChild(outerDivE);
        } else {
            Element tableElement = document.createElement("table");
            tableElement.setAttribute("id", elementId);
            rc.addStandardWebSupport(component, tableElement);
            CssStyleEx tableCssStyle = new CssStyleEx(component, fallbackStyle);
            tableCssStyle.setAttribute("border-collapse", "collapse");
            if (selectionEnabled) {
                tableCssStyle.setAttribute("cursor", "pointer");
            }

            ColorRender.renderToStyle(tableCssStyle, component);
            FontRender.renderToStyle(tableCssStyle, component);
            BorderRender.renderToStyle(tableCssStyle, border);
            if (borderSize != null && !rc.getContainerInstance().getClientProperties().getBoolean("quirkCssBorderCollapseInside")) {
                tableCssStyle.setAttribute("margin", ExtentRender.renderCssAttributeValueHalf(borderSize));
            }

            if (rc.getContainerInstance().getClientProperties().getBoolean("quirkIETablePercentWidthScrollbarError") && width != null && width.getUnits() == 2 && width.getValue() > 95) {
                width = new Extent(95, 2);
            }

            ExtentRender.renderToStyle(tableCssStyle, "width", width);
            tableElement.setAttribute("style", tableCssStyle.renderInline());
            parentNode.appendChild(tableElement);
            contentDivE = this.renderColGroup(table, document);
            if (contentDivE != null) {
                tableElement.appendChild(contentDivE);
            }


            if (table.isHeaderVisible()) {
                Element theadElement = document.createElement("thead");
                theadElement.setAttribute("id", elementId + "_thead");
                tableElement.appendChild(theadElement);
                this.renderRow(rc, theadElement, table, -1, defaultInsetsAttributeValue, itemXML, fallbackStyle);
            }

            tbodyElement = document.createElement("tbody");
            tbodyElement.setAttribute("id", elementId + "_tbody");
            tableElement.appendChild(tbodyElement);

            int rows = table.getModel().getRowCount();

            for (int rowIndex = 0; rowIndex < rows; ++rowIndex) {
                this.renderRow(rc, tbodyElement, table, rowIndex, defaultInsetsAttributeValue, itemXML, fallbackStyle);
            }

            if (table.isFooterVisible()) {
                this.renderRow(rc, tbodyElement, table, -2, defaultInsetsAttributeValue, itemXML, fallbackStyle);
            }
        }
    }

    /**
     * Renders the init directive.
     *
     * @param rc            the rendering context
     * @param table         the table
     * @param fallbackStyle the fallback style
     * @return the initial element
     */
    @Override
    protected Element renderInitDirective(RenderingContext rc, echopointng.TableEx table, Style fallbackStyle) {
        Element element = super.renderInitDirective(rc, table, fallbackStyle);
        Integer row = ((TableEx) table).getScrollToRow();
        if (row != null) {
            element.setAttribute("scrollToRow", Integer.toString(row));
            ((TableEx) table).resetScrollToRow(); // once off
        }
        return element;
    }

    /**
     * Renders a row.
     *
     * @param rc                          the rendering context
     * @param parent                      the parent element
     * @param table                       the table
     * @param rowIndex                    the row index
     * @param defaultInsetsAttributeValue the default insets attribute
     * @param itemXML                     the item
     * @param fallbackStyle               the fallback style
     */
    protected void renderRow(RenderingContext rc, Element parent, TableEx table, int rowIndex,
                             String defaultInsetsAttributeValue, Element itemXML, Style fallbackStyle) {
        Document document = parent.getOwnerDocument();
        String elementId = ContainerInstance.getElementId(table);
        boolean isScrollable = rc.getRP("scrollable", fallbackStyle, false);
        Element trElement = document.createElement("tr");
        if (rowIndex == -1) {
            trElement.setAttribute("id", elementId + "_tr_header");
        } else if (rowIndex == -2) {
            trElement.setAttribute("id", elementId + "_tr_footer");
        } else {
            trElement.setAttribute("id", elementId + "_tr_" + rowIndex);
        }

        parent.appendChild(trElement);
        int columns = table.getColumnModel().getColumnCount();
        int rowCount = table.getModel().getRowCount();

        for (int columnIndex = 0; columnIndex < columns; ++columnIndex) {
            Object cellContent = table.getCellContent(columnIndex, rowIndex);
            if (cellContent != null && cellContent != TableEx.CELL_SPANNER) {
                Element tdCellE = (rowIndex == Table.HEADER_ROW) ? document.createElement("th")
                                                                 : document.createElement("td");

                Border border = (Border) rc.getRP("border", fallbackStyle);

                tdCellE.setAttribute("c", String.valueOf(columnIndex));
                trElement.appendChild(tdCellE);
                Element cellContentE = tdCellE;
                CssStyle style = new CssStyle();
                if (rowIndex == Table.HEADER_ROW && table.isHeaderFixed()) {
                    style.setAttribute("position", "sticky");
                    if (border != null) {
                        style.setAttribute("top", "-" + border.getSize()); // avoids a gap at the top when scrolling
                    }
                    style.setAttribute("background-clip", "padding-box");
                    // required for Firefox. Without it, the header borders are removed
                }

                if (!isScrollable) {
                    Render.asBorder(style, border);
                } else {
                    Element cellDivE = document.createElement("div");
                    tdCellE.appendChild(cellDivE);
                    cellContentE = cellDivE;
                }

                String cellId = null;
                Component renderComponent = table;
                Component childComponent = null;
                TableLayoutData tableLayoutData = null;
                TableLayoutDataEx tableLayoutDataEx = null;
                if (cellContent instanceof XhtmlFragment) {
                    XhtmlFragment fragment = (XhtmlFragment) cellContent;
                    cellId = elementId + "_cell_col" + columnIndex + "row" + rowIndex;
                    tableLayoutData = this.getLayoutData(fragment);
                    if (tableLayoutData instanceof TableLayoutDataEx) {
                        tableLayoutDataEx = (TableLayoutDataEx) tableLayoutData;
                    }

                    this.renderXhtmlFragment(rc, cellContentE, itemXML, cellId, fragment, rowCount);
                }

                if (cellContent instanceof Component) {
                    childComponent = (Component) cellContent;
                    renderComponent = childComponent;
                    cellId = elementId + "_cell_" + childComponent.getRenderId();
                    tableLayoutData = this.getLayoutData(childComponent);
                    if (tableLayoutData instanceof TableLayoutDataEx) {
                        tableLayoutDataEx = (TableLayoutDataEx) tableLayoutData;
                    }
                }

                if (childComponent != null) {
                    this.renderAddChild(rc, cellContentE, childComponent);
                }

                if (rowIndex == -1) {
                    Render.asColor(style, (Color) rc.getRP("headerBackground", fallbackStyle), "background");
                }

                if (rowIndex == -2) {
                    Render.asColor(style, (Color) rc.getRP("footerBackground", fallbackStyle), "background");
                }

                CellLayoutDataRender.renderToElementAndStyle(cellContentE, style, (Component) renderComponent,
                                                             tableLayoutData, defaultInsetsAttributeValue);
                if (tableLayoutData != null && tableLayoutData.getBackgroundImage() != null) {
                    Render.asBackgroundImage(rc, style, tableLayoutData.getBackgroundImage());
                }

                if (isScrollable) {
                    style.setAttribute("width", "100%");
                    style.setAttribute("overflow", "hidden");
                    style.setAttribute("white-space", "nowrap");
                }

                String inlineStyle = style.renderInline();
                if (rowIndex == Table.HEADER_ROW && table.isHeaderFixed()) {
                    // workaround for Safari
                    inlineStyle = "position:-webkit-sticky;" + inlineStyle;
                }
                cellContentE.setAttribute("style", inlineStyle);
                cellContentE.setAttribute("id", cellId);
                if (tableLayoutDataEx != null) {
                    if (tableLayoutDataEx.getToolTipText() != null) {
                        cellContentE.setAttribute("title", tableLayoutDataEx.getToolTipText());
                    }

                    int colSpan = tableLayoutDataEx.getColSpan();
                    int rowSpan = tableLayoutDataEx.getRowSpan();
                    if (colSpan > 1) {
                        tdCellE.setAttribute("colspan", String.valueOf(colSpan));
                    }

                    if (rowSpan > 1) {
                        tdCellE.setAttribute("rowspan", String.valueOf(rowSpan));
                    }
                }
            }
        }

        boolean statusQuo = true;

        for (int columnIndex = 0; columnIndex < columns; ++columnIndex) {
            if (!table.isActionCausingCell(columnIndex, rowIndex)
                || !table.isSelectionCausingCell(columnIndex, rowIndex)) {
                statusQuo = false;
                break;
            }
        }

        if (!statusQuo) {
            StringBuilder actionCausingStr = new StringBuilder();
            StringBuilder selectionCausingStr = new StringBuilder();

            for (int columnIndex = 0; columnIndex < columns; ++columnIndex) {
                if (!table.isActionCausingCell(columnIndex, rowIndex)) {
                    actionCausingStr.append("c:");
                    actionCausingStr.append(columnIndex);
                    actionCausingStr.append(";");
                }

                if (!table.isSelectionCausingCell(columnIndex, rowIndex)) {
                    selectionCausingStr.append("c:");
                    selectionCausingStr.append(columnIndex);
                    selectionCausingStr.append(";");
                }
            }

            Document documentXML = itemXML.getOwnerDocument();
            Element selectionElement;
            if (actionCausingStr.length() > 0) {
                selectionElement = documentXML.createElement("actionCausingCell");
                itemXML.appendChild(selectionElement);
                selectionElement.setAttribute("row", String.valueOf(rowIndex));
                selectionElement.setAttribute("cells", actionCausingStr.toString());
            }

            if (selectionCausingStr.length() > 0) {
                selectionElement = documentXML.createElement("selectionCausingCell");
                itemXML.appendChild(selectionElement);
                selectionElement.setAttribute("row", String.valueOf(rowIndex));
                selectionElement.setAttribute("cells", selectionCausingStr.toString());
            }
        }

    }

    /**
     * Adds a child component.
     *
     * @param rc            the rendering context
     * @param parentElement the parent element
     * @param child         the child element
     */
    private void renderAddChild(RenderingContext rc, Element parentElement, Component child) {
        if (child.isVisible()) {
            ServerComponentUpdate update = rc.getServerComponentUpdate();
            ComponentSynchronizePeer syncPeer = SynchronizePeerFactory.getPeerForComponent(child.getClass());
            if (syncPeer instanceof DomUpdateSupport) {
                ((DomUpdateSupport) syncPeer).renderHtml(rc, update, parentElement, child);
            } else {
                syncPeer.renderAdd(rc, update, this.getContainerId(child), child);
            }

        }
    }

    /**
     * Returns the layout data for a child component.
     *
     * @param child the child
     * @return the layout data. May be {@code null}
     */
    private TableLayoutData getLayoutData(Component child) {
        LayoutData layoutData = (LayoutData) child.getRenderProperty("layoutData");
        if (layoutData == null) {
            return null;
        } else if (layoutData instanceof TableLayoutData) {
            return (TableLayoutData) layoutData;
        } else {
            throw new RuntimeException("Invalid LayoutData for Table Child: " + layoutData.getClass().getName());
        }
    }

    /**
     * Returns the layout data for a fragment.
     *
     * @param fragment the fragment
     * @return the layout data. May be {@code null}
     */
    private TableLayoutData getLayoutData(XhtmlFragment fragment) {
        LayoutData layoutData = fragment.getLayoutData();
        if (layoutData == null) {
            return null;
        } else if (layoutData instanceof TableLayoutData) {
            return (TableLayoutData) layoutData;
        } else {
            throw new RuntimeException("Invalid LayoutData for Table: " + layoutData.getClass().getName());
        }
    }

    private Element renderResizeableHeaderFooterAreas(RenderingContext rc, TableEx table, Style fallbackStyle,
                                                      Border border, String defaultInsetsAttributeValue,
                                                      Element itemXML, int rowIndex, boolean isVisible,
                                                      String prefixName, int rowCount) {
        Document document = rc.getDocument();
        Element headerFooterDivE = document.createElement("div");
        CssStyleEx style = new CssStyleEx(table, fallbackStyle);
        style.setAttribute("width", "100%");
        style.setAttribute("overflow", "hidden");
        style.setAttribute("position", "relative");
        style.setAttribute("padding", "0px");
        style.setAttribute("margin", "0px");
        if (isVisible) {
            if (rowIndex == -1) {
                Render.asColor(style, (Color) rc.getRP("headerBackground", fallbackStyle), "background");
            }

            if (rowIndex == -2) {
                Render.asColor(style, (Color) rc.getRP("footerBackground", fallbackStyle), "background");
            }

            if (isVisible && rowIndex == -1) {
                Render.asBorder(style, border);
                if (rowCount > 0) {
                    style.setAttribute("border-bottom-width", style.getAttribute("border-top-width"));
                    style.setAttribute("border-bottom-color", style.getAttribute("border-top-color"));
                    style.setAttribute("border-bottom-style", style.getAttribute("border-top-style"));
                }
            }

            if (isVisible && rowIndex == -2) {
                Render.asBorder(style, border);
                if (rowCount > 0) {
                    style.setAttribute("border-top-width", "0px");
                }
            }
        }

        headerFooterDivE.setAttribute("style", style.renderInline());
        headerFooterDivE.setAttribute("id", rc.getElementId() + "_" + prefixName + "Div");
        Element headerFooterScrollerDivE = document.createElement("div");
        style = new CssStyleEx();
        style.setAttribute("width", "100%");
        style.setAttribute("position", "relative");
        style.setAttribute("padding", "0px");
        style.setAttribute("margin", "0px");
        headerFooterScrollerDivE.setAttribute("style", style.renderInline());
        headerFooterScrollerDivE.setAttribute("id", rc.getElementId() + "_" + prefixName + "ScrollerDiv");
        Element headerFooterTableE = document.createElement("table");
        style = new CssStyleEx();
        style.setAttribute("table-layout", "fixed");
        style.setAttribute("width", "100%");
        headerFooterTableE.setAttribute("style", style.renderInline());
        headerFooterTableE.setAttribute("id", rc.getElementId() + "_" + prefixName + "Table");
        headerFooterTableE.setAttribute("cellpadding", "0");
        headerFooterTableE.setAttribute("cellspacing", "0");
        headerFooterTableE.setAttribute("border", "0");
        Element headerFooterTbodyElement = document.createElement("tbody");
        headerFooterTableE.appendChild(headerFooterTbodyElement);
        if (isVisible) {
            this.renderRow(rc, headerFooterTbodyElement, table, rowIndex, defaultInsetsAttributeValue, itemXML, fallbackStyle);
        }

        headerFooterTableE.appendChild(headerFooterTbodyElement);
        headerFooterScrollerDivE.appendChild(headerFooterTableE);
        headerFooterDivE.appendChild(headerFooterScrollerDivE);
        return headerFooterDivE;
    }

}
