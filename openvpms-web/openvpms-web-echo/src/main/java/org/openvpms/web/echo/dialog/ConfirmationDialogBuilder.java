/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.dialog;

/**
 * Builds a  {@link ConfirmationDialog}.
 *
 * @author Tim Anderson
 */
public class ConfirmationDialogBuilder
        extends MessageDialogBuilder<ConfirmationDialog, ConfirmationDialogBuilder> {

    /**
     * Constructs a {@link ConfirmationDialogBuilder}.
     */
    ConfirmationDialogBuilder() {
        style(ConfirmationDialog.STYLE);
    }

    /**
     * Builds the dialog.
     *
     * @return the dialog
     */
    @Override
    public ConfirmationDialog build() {
        return new ConfirmationDialog(this);
    }

    /**
     * Returns this.
     *
     * @return this
     */
    @Override
    protected ConfirmationDialogBuilder getThis() {
        return this;
    }
}
