/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.servlet.file;

import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import org.openvpms.plugin.manager.PluginManager;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * File server servlet, used by plugins to serve static resources.
 *
 * @author Tim Anderson
 */
public class FileServerServlet extends AbstractFileServerServlet {

    /**
     * The plugin manager.
     */
    private PluginManager manager;

    /**
     * The download strategies.
     */
    private List<DownloadStrategy> strategies = null;


    /**
     * Constructs a {@link FileServerServlet}.
     */
    public FileServerServlet() {
        super();
    }

    /**
     * Initialises the servlet.
     */
    @Override
    public void init() {
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        manager = context.getBean(PluginManager.class);
    }

    /**
     * Returns a list of {@link DownloadStrategy} objects in the order that they will be matched against.
     * The list returned should be cached as this method is called for every request.
     */
    @Override
    protected synchronized List<DownloadStrategy> getDownloadStrategies() {
        if (strategies == null) {
            strategies = new ArrayList<>();
            PluginResourceLocator pluginResourceLocator = manager.getService(PluginResourceLocator.class);
            strategies.add(new PluginResourceDownload(pluginResourceLocator, new ContentTypeResolverImpl(), "UTF-8"));
        }
        return strategies;
    }
}
