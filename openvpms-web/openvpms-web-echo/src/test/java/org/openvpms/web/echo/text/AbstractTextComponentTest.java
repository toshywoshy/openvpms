/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.text;

import nextapp.echo2.app.ApplicationInstance;
import nextapp.echo2.app.Window;
import org.junit.Before;
import org.junit.Test;

import java.beans.PropertyChangeListener;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

/**
 * Base class for {@link TextComponent} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTextComponentTest {

    /**
     * The component to test.
     */
    private TextComponent component;

    /**
     * Constructs a {@link AbstractTextComponentTest}.
     *
     * @param component the text component to test
     */
    AbstractTextComponentTest(TextComponent component) {
        this.component = component;
    }

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        ApplicationInstance instance = new ApplicationInstance() {
            @Override
            public Window init() {
                return new Window();
            }
        };
        ApplicationInstance.setActive(instance);
        Window window = instance.doInit();
        window.getContent().add(component);
    }

    /**
     * Verifies that a {@link PropertyChangeListener} is invoked when the text updates from the
     * {@link TextComponent#processInput(String, Object)} method.
     */
    @Test
    public void testTextChangedListenerFiredForProcessInput() {
        AtomicInteger count = new AtomicInteger();
        component.addPropertyChangeListener(TextField.TEXT_CHANGED_PROPERTY, evt -> count.incrementAndGet());

        processInput("abc");
        assertEquals(1, count.get());

        processInput("cde");
        assertEquals(2, count.get());

        processInput("cde");    // shouldn't trigger a change
        assertEquals(2, count.get());

        processInput(null);
        assertEquals(3, count.get());
    }

    /**
     * Verifies that a {@link PropertyChangeListener} is invoked when the text updates from the
     * {@link TextComponent#processInput(String, Object)} method.
     */
    @Test
    public void testTextChangedListenerFiredForSetText() {
        AtomicInteger count = new AtomicInteger();
        component.addPropertyChangeListener(TextField.TEXT_CHANGED_PROPERTY, evt -> count.incrementAndGet());

        assertEquals(0, count.get());

        component.setText("foo");
        assertEquals(1, count.get());

        component.setText("foo");  // won't fire event
        assertEquals(1, count.get());

        component.setText("foo bar");
        assertEquals(2, count.get());

        component.setText(null);
        assertEquals(3, count.get());
    }

    /**
     * Verifies that text can be updated with the {@link PropertyChangeListener}.
     */
    @Test
    public void testUpdateWithinChangedListener() {
        AtomicInteger count = new AtomicInteger();
        component.addPropertyChangeListener(TextField.TEXT_CHANGED_PROPERTY, evt -> {
            String text = component.getText();
            if (text == null) {
                component.setText("abc");
            } else if (!text.endsWith("abc")) {
                component.setText(text + " abc");
            }
            count.incrementAndGet();
        });

        assertEquals(0, count.get());

        processInput("xyz");
        assertEquals("xyz abc", component.getText());
        assertEquals(2, count.get());  // fires once for xyz and again for xyz abc

        // event shouldn't fire
        processInput("xyz abc");
        assertEquals(2, count.get());
    }

    /**
     * Invokes {@link TextField#processInput(String, Object)} for the {@link TextField#TEXT_CHANGED_PROPERTY}.
     * <p/>
     * This sets the cursor position first, as both properties are required to update text for those components
     * that support cursor positioning.
     *
     * @param text the new text. May be {@code null}
     */
    private void processInput(String text) {
        component.processInput(TextField.PROPERTY_CURSOR_POSITION, 0);
        component.processInput(TextField.TEXT_CHANGED_PROPERTY, text);
    }
}
