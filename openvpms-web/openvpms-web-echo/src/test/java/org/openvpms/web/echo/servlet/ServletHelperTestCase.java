/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.echo.servlet;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link ServletHelper} class.
 *
 * @author Tim Anderson
 */
public class ServletHelperTestCase {

    /**
     * Tests the {@link ServletHelper#getRemoteAddress(HttpServletRequest)} method.
     */
    @Test
    public void testGetRemoteAddress() {
        checkRemoteAddress("192.168.0.100", null);
        checkRemoteAddress("194.223.4.181", "194.223.4.181");
        checkRemoteAddress("194.223.4.181", "194.223.4.181, proxy1");
        checkRemoteAddress("192.168.0.100", "");  // malformed
        checkRemoteAddress("192.168.0.100", ",");  // malformed
    }

    /**
     * Verifies that {@link ServletHelper#getRemoteAddress(HttpServletRequest)} returns the expected address.
     * <p/>
     * This uses "192.168.0.100" as the remote address.
     *
     * @param expected      the expected address
     * @param forwardedFor if non-null, specifies the value of X-Forwarded-For
     */
    private void checkRemoteAddress(String expected, String forwardedFor) {
        MockServletContext context = new MockServletContext();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/");
        if (forwardedFor != null) {
            builder.header(ServletHelper.X_FORWARDED_FOR, forwardedFor);
        }
        MockHttpServletRequest request = builder.buildRequest(context);
        request.setRemoteAddr("192.168.0.100");
        assertEquals(expected, ServletHelper.getRemoteAddress(request));
    }
}
