package org.openvpms.web.workspace.workflow.scheduling;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvent;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.util.PropertySet;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.getDatetime;

/**
 * Tests the {@link Schedule} class.
 *
 * @author Tim Anderson
 */
public class ScheduleTestCase extends ArchetypeServiceTest {

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules appointmentRules;

    private static final int MINUTES_PER_DAY = 24 * 60;

    /**
     * Verifies that {@link Schedule#getIntersectingEvent(Date, Date, int)} works for boarding events that fall during the
     * start and end of daylight savings.
     */
    @Test
    public void testGetIntersectingBoardingEventDuringDaylightSavings() {
        Entity boardingSchedule = ScheduleTestHelper.createSchedule(TestHelper.createLocation());
        Entity cageType = ScheduleTestHelper.createCageType(TestHelper.randomName("Z-Cage"));
        // create a schedule with a 24 hour slot size
        Schedule schedule = new Schedule(boardingSchedule, cageType, 0, MINUTES_PER_DAY, MINUTES_PER_DAY,
                                         Collections.emptyList(), Collections.emptyList(), appointmentRules);

        // first day of daylight saving 2019 (AEST).
        Act appointment1 = ScheduleTestHelper.createAppointment(getDatetime("2019-10-06 00:00:00"),
                                                                getDatetime("2019-10-06 01:00:00"),
                                                                boardingSchedule);
        // last day of daylight saving 2020 (AEST).
        Act appointment2 = ScheduleTestHelper.createAppointment(getDatetime("2020-04-05 00:00:00"),
                                                                getDatetime("2020-04-05 02:00:00"),
                                                                boardingSchedule);

        PropertySet event1 = createEvent(appointment1);
        PropertySet event2 = createEvent(appointment2);
        schedule.addEvent(event1);
        schedule.addEvent(event2);

        assertNull(schedule.getIntersectingEvent(getDatetime("2019-10-05 00:00:00"),
                                                 getDatetime("2019-10-05 00:00:00"), MINUTES_PER_DAY));
        assertEquals(event1, schedule.getIntersectingEvent(getDatetime("2019-10-06 00:00:00"),
                                                           getDatetime("2019-10-06 00:00:00"), MINUTES_PER_DAY));
        assertNull(schedule.getIntersectingEvent(getDatetime("2019-10-07 00:00:00"),
                                                 getDatetime("2019-10-07 00:00:00"), MINUTES_PER_DAY));

        assertNull(schedule.getIntersectingEvent(getDatetime("2020-04-04 00:00:00"),
                                                 getDatetime("2020-04-04 00:00:00"), MINUTES_PER_DAY));
        assertEquals(event2, schedule.getIntersectingEvent(getDatetime("2020-04-05 00:00:00"),
                                                           getDatetime("2020-04-05 00:00:00"), MINUTES_PER_DAY));
        assertNull(schedule.getIntersectingEvent(getDatetime("2020-04-06 00:00:00"),
                                                 getDatetime("2020-04-06 00:00:00"), MINUTES_PER_DAY));
    }

    /**
     * Creates an event representing an appointment.
     *
     * @param appointment the appointment
     * @return the event
     */
    private PropertySet createEvent(Act appointment) {
        PropertySet result = new ObjectSet();
        result.set(ScheduleEvent.ACT_START_TIME, appointment.getActivityStartTime());
        result.set(ScheduleEvent.ACT_END_TIME, appointment.getActivityEndTime());
        result.set(ScheduleEvent.ACT_REFERENCE, appointment.getObjectReference());
        return result;
    }

}
