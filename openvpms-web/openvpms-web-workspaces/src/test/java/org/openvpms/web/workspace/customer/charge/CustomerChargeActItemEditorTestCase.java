/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.customer.charge;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.archetype.rules.finance.discount.DiscountRules;
import org.openvpms.archetype.rules.finance.discount.DiscountTestHelper;
import org.openvpms.archetype.rules.laboratory.LaboratoryTestHelper;
import org.openvpms.archetype.rules.math.Currencies;
import org.openvpms.archetype.rules.math.MathRules;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.PatientHistoryChanges;
import org.openvpms.archetype.rules.patient.PatientTestHelper;
import org.openvpms.archetype.rules.patient.reminder.ReminderArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderTestHelper;
import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.rules.product.ProductPriceTestHelper;
import org.openvpms.archetype.rules.product.ProductTestHelper;
import org.openvpms.archetype.rules.stock.StockRules;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.business.service.security.AuthenticationContext;
import org.openvpms.component.business.service.security.AuthenticationContextImpl;
import org.openvpms.component.math.WeightUnits;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.EditDialog;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.edit.act.ActRelationshipCollectionEditor;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.dialog.PopupDialog;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.test.EchoTestHelper;
import org.openvpms.web.workspace.patient.mr.PatientMedicationActEditor;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE_ITEM;
import static org.openvpms.archetype.rules.product.ProductArchetypes.MEDICATION;
import static org.openvpms.web.workspace.customer.charge.CustomerChargeTestHelper.checkSavePopup;

/**
 * Tests the {@link CustomerChargeActItemEditor} class.
 *
 * @author Tim Anderson
 */
public class CustomerChargeActItemEditorTestCase extends AbstractCustomerChargeActEditorTest {

    /**
     * Tracks errors logged.
     */
    private final List<String> errors = new ArrayList<>();

    /**
     * The customer.
     */
    private Party customer;

    /**
     * The context.
     */
    private Context context;

    /**
     * The context used for createdBy, updatedBy nodes.
     */
    private AuthenticationContext authenticationContext;

    /**
     * Sets up the test case.
     */
    @Before
    @Override
    public void setUp() {
        super.setUp();
        customer = TestHelper.createCustomer();

        // register an ErrorHandler to collect errors
        initErrorHandler(errors);
        context = new LocalContext();
        context.setPractice(getPractice());
        Party location = TestHelper.createLocation(true); // enable stock control
        context.setLocation(location);
        context.setStockLocation(ProductTestHelper.createStockLocation(location));

        // set a minimum price for calculated prices.
        Lookup currency = TestHelper.getLookup(Currencies.LOOKUP, "AUD");
        IMObjectBean bean = getBean(currency);
        bean.setValue("minPrice", new BigDecimal("0.20"));
        bean.save();

        authenticationContext = new AuthenticationContextImpl();
    }

    /**
     * Tests populating an invoice item with a medication.
     */
    @Test
    public void testInvoiceItemMedication() {
        checkInvoiceItem(MEDICATION);
    }

    /**
     * Tests populating an invoice item with a merchandise product.
     */
    @Test
    public void testInvoiceItemMerchandise() {
        checkInvoiceItem(ProductArchetypes.MERCHANDISE);
    }

    /**
     * Tests populating an invoice item with a service product.
     */
    @Test
    public void testInvoiceItemService() {
        checkInvoiceItem(ProductArchetypes.SERVICE);
    }

    /**
     * Tests populating an invoice item with a template product.
     */
    @Test
    public void testInvoiceItemTemplate() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);
        checkItemWithTemplate(invoice, item);
    }

    /**
     * Tests populating a counter sale item with a medication product.
     */
    @Test
    public void testCounterSaleItemMedication() {
        checkCounterSaleItem(MEDICATION);
    }

    /**
     * Tests populating a counter sale item with a merchandise product.
     */
    @Test
    public void testCounterSaleItemMerchandise() {
        checkCounterSaleItem(ProductArchetypes.MERCHANDISE);
    }

    /**
     * Tests populating a counter sale item with a service product.
     */
    @Test
    public void testCounterSaleItemService() {
        checkCounterSaleItem(ProductArchetypes.SERVICE);
    }

    /**
     * Tests populating a counter sale item with a template product.
     */
    @Test
    public void testCounterSaleItemTemplate() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct counterSale = acts.get(0);
        FinancialAct item = acts.get(1);
        checkItemWithTemplate(counterSale, item);
    }

    /**
     * Tests populating a credit item with a medication product.
     */
    @Test
    public void testCreditItemMedication() {
        checkCreditItem(MEDICATION);
    }

    /**
     * Tests populating a credit item with a merchandise product.
     */
    @Test
    public void testCreditItemMerchandise() {
        checkCreditItem(ProductArchetypes.MERCHANDISE);
    }

    /**
     * Tests populating a credit item with a service product.
     */
    @Test
    public void testCreditItemService() {
        checkCreditItem(ProductArchetypes.SERVICE);
    }

    /**
     * Tests populating a credit item with a template product.
     */
    @Test
    public void testCreditItemTemplate() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer, null, null,
                                                                          ActStatus.IN_PROGRESS);
        FinancialAct credit = acts.get(0);
        FinancialAct item = acts.get(1);
        checkItemWithTemplate(credit, item);
    }

    /**
     * Verifies that the clinician can be cleared, as a test for OVPMS-1104.
     */
    @Test
    public void testClearClinician() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = TestHelper.createPatient();
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);
        User clinician = TestHelper.createClinician();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal discount = ZERO;
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = new BigDecimal("22");

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = createProduct(ProductArchetypes.MERCHANDISE, fixedCost, fixedPrice, unitCost, unitPrice);
        addTemplate(product, PatientArchetypes.DOCUMENT_FORM);
        addTemplate(product, PatientArchetypes.DOCUMENT_LETTER);

        // set up the context
        CustomerChargeEditContext editContext = createEditContext(layout);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, clinician.
        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setClinician(clinician);

        // set the product
        editor.setProduct(product);

        // editor should now be valid
        assertTrue(editor.isValid());
        editor.setClinician(null);

        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, author, null, ZERO, quantity,
                                      unitCost, new BigDecimal(10), fixedCost, new BigDecimal(2), discount, tax, total,
                                      true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies that prices and totals are correct when the customer has tax exemptions.
     */
    @Test
    public void testTaxExemption() {
        addTaxExemption(customer);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = TestHelper.createPatient();
        User author = TestHelper.createUser();
        User clinician = TestHelper.createClinician();

        authenticationContext.setUser(author);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal discount = ZERO;

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = createProduct(ProductArchetypes.MERCHANDISE, fixedCost, fixedPrice, unitCost, unitPrice);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, clinician.
        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setClinician(clinician);

        // set the product
        editor.setProduct(product);

        // editor should now be valid
        assertTrue(editor.isValid());
        editor.setClinician(null);

        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        BigDecimal unitPriceExTax = new BigDecimal("9.00");  // rounded due to minPrice
        BigDecimal fixedPriceExTax = new BigDecimal("1.80"); // rounded due to minPrice
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, author, null, ZERO, quantity,
                                      unitCost, unitPriceExTax, fixedCost, fixedPriceExTax, discount, ZERO,
                                      new BigDecimal("19.80"), true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies that prices and totals are correct when the customer has tax exemptions and a service ratio is in place.
     */
    @Test
    public void testTaxExemptionWithServiceRatio() {
        addTaxExemption(customer);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = TestHelper.createPatient();
        User author = TestHelper.createUser();
        User clinician = TestHelper.createClinician();

        authenticationContext.setUser(author);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal discount = ZERO;
        BigDecimal ratio = BigDecimal.valueOf(2); // double the fixed and unit prices

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = createProduct(ProductArchetypes.MERCHANDISE, fixedCost, fixedPrice, unitCost, unitPrice);
        Entity productType = ProductTestHelper.createProductType("Z Product Type");
        ProductTestHelper.addProductType(product, productType);
        ProductTestHelper.addServiceRatio(context.getLocation(), productType, ratio);

        authenticationContext.setUser(author);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, clinician and product
        editor.setQuantity(quantity);
        editor.setPatient(patient);
        editor.setClinician(clinician);
        editor.setProduct(product);

        // editor should now be valid
        assertTrue(editor.isValid());
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        BigDecimal fixedPriceExTax = new BigDecimal("3.60"); // rounded due to minPrice
        BigDecimal unitPriceExTax = new BigDecimal("18.20"); // rounded due to minPrice
        BigDecimal totalExTax = unitPriceExTax.multiply(quantity).add(fixedPriceExTax);
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, author, clinician, ZERO, quantity,
                                      unitCost, unitPriceExTax, fixedCost, fixedPriceExTax, discount, ZERO, totalExTax,
                                      true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests a product with a 10% discount on an invoice item.
     */
    @Test
    public void testInvoiceItemDiscounts() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);
        checkDiscounts(invoice, item, false);
    }

    /**
     * Tests a product with a 10% discount on an invoice item with a negative quantity.
     */
    @Test
    public void testNegativeInvoiceItemDiscounts() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);
        checkDiscounts(invoice, item, true);
    }

    /**
     * Tests a product with a 10% discount on a counter sale item.
     */
    @Test
    public void testCounterSaleItemDiscounts() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct counterSale = acts.get(0);
        FinancialAct item = acts.get(1);
        checkDiscounts(counterSale, item, false);
    }

    /**
     * Tests a product with a 10% discount on a counter sale item with a negative quantity.
     */
    @Test
    public void testNegativeCounterSaleItemDiscounts() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct counterSale = acts.get(0);
        FinancialAct item = acts.get(1);
        checkDiscounts(counterSale, item, true);
    }

    /**
     * Tests a product with a 10% discount on a credit item with a negative quantity.
     */
    @Test
    public void testCreditItemDiscounts() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer, null, null,
                                                                          ActStatus.IN_PROGRESS);
        FinancialAct credit = acts.get(0);
        FinancialAct item = acts.get(1);
        checkDiscounts(credit, item, false);
    }

    /**
     * Tests a product with a 10% discount on a credit item with a negative quantity.
     */
    @Test
    public void testNegativeCreditDiscounts() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer, null, null,
                                                                          ActStatus.IN_PROGRESS);
        FinancialAct credit = acts.get(0);
        FinancialAct item = acts.get(1);
        checkDiscounts(credit, item, true);
    }

    /**
     * Tests a product with a 10% discount where discounts are disabled at the practice location.
     * <p/>
     * The calculated discount should be zero.
     */
    @Test
    public void testDisableDiscounts() {
        IMObjectBean bean = getBean(context.getLocation());
        bean.setValue("disableDiscounts", true);

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(2);
        User clinician = TestHelper.createClinician();
        User author = TestHelper.createUser();

        authenticationContext.setUser(author);

        Entity discount = DiscountTestHelper.createDiscount(BigDecimal.TEN, true, DiscountRules.PERCENTAGE);
        Product product = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);
        Party patient = TestHelper.createPatient();
        addDiscount(customer, discount);
        addDiscount(product, discount);

        context.setClinician(clinician);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext editContext = createEditContext(layout);
        editContext.setEditorQueue(null); // disable popups

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        if (!TypeHelper.isA(item, CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient);
        }
        editor.setProduct(product);
        editor.setQuantity(quantity);

        // editor should now be valid
        assertTrue(editor.isValid());

        checkSave(charge, editor);

        item = get(item);
        // should be no discount
        BigDecimal discount1 = ZERO;
        BigDecimal tax1 = new BigDecimal("2.00");
        BigDecimal total1 = new BigDecimal("22.00");
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient, product, null, -1, author,
                                      clinician, ZERO, quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax,
                                      discount1, tax1, total1, true);
    }

    /**
     * Verifies that when a product with a dose is selected, the quantity is determined by the patient weight.
     */
    @Test
    public void testInvoiceProductDose() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        checkProductDose(acts.get(0), acts.get(1));
    }

    /**
     * Verifies that when a product with a dose is selected, the quantity is determined by the patient weight.
     */
    @Test
    public void testCreditProductDose() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer, null, null,
                                                                          ActStatus.IN_PROGRESS);
        checkProductDose(acts.get(0), acts.get(1));
    }

    /**
     * Verifies that when a product with a dose is selected during a counter sale, the quantity remains unchanged
     * as there is no patient.
     */
    @Test
    public void testCounterProductDose() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = TestHelper.createPatient();
        PatientTestHelper.createWeight(patient, new Date(), new BigDecimal("4.2"), WeightUnits.KILOGRAMS);
        User author = TestHelper.createUser();

        authenticationContext.setUser(author);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");

        Product product = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);
        Entity dose = ProductTestHelper.createDose(null, ZERO, BigDecimal.TEN, ONE, ONE);
        ProductTestHelper.addDose(product, dose);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, layout);
        assertFalse(editor.isValid());

        assertFalse((editor.isDefaultQuantity()));

        // populate quantity, patient, clinician and product
        editor.setQuantity(quantity);
        assertFalse((editor.isDefaultQuantity()));
        editor.setProduct(product);
        checkEquals(quantity, item.getQuantity());
        assertFalse((editor.isDefaultQuantity()));
    }

    /**
     * Verifies that a product with a microchip Patient Identity displays a prompt to add a microchip.
     */
    @Test
    public void testMicrochip() {
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = TestHelper.createPatient();

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = createProduct(ProductArchetypes.MERCHANDISE);
        IMObjectBean bean = getBean(product);
        bean.setValue("patientIdentity", PatientArchetypes.MICROCHIP);
        bean.save();

        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setQuantity(ONE);
        editor.setPatient(patient);
        editor.setProduct(product);

        // editor should be invalid
        assertFalse(editor.isValid());
        assertTrue(editContext.getEditorQueue().getCurrent() instanceof EditDialog);
        EditDialog dialog = (EditDialog) editContext.getEditorQueue().getCurrent();
        IMObjectEditor microchip = dialog.getEditor();
        microchip.getProperty("identity").setValue("123456789");
        checkSavePopup(editContext.getEditorQueue(), PatientArchetypes.MICROCHIP, false);
        checkSave(charge, editor);

        patient = get(patient);
        assertEquals(1, patient.getIdentities().size());
        EntityIdentity identity = patient.getIdentities().iterator().next();
        assertEquals("123456789", identity.getIdentity());
        assertEquals("  (Microchip: 123456789)", patient.getDescription());

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies that the editor is invalid if a quantity is less than a minimum quantity.
     * <p/>
     * Note that in practice, the minimum quantity is set by expanding a template or invoicing an estimate.
     */
    @Test
    public void testMinimumQuantities() {
        BigDecimal two = BigDecimal.valueOf(2);
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));

        Party patient = TestHelper.createPatient();

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = ProductTestHelper.createService();
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);

        editor.setMinimumQuantity(two);
        editor.setQuantity(two);
        assertTrue(editor.isValid());

        // editor should be invalid when quantity set below minimum
        editor.setQuantity(ONE);
        assertFalse(editor.isValid());

        // now set above
        editor.setQuantity(two);
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that a user with the appropriate user type can override minimum quantities.
     * <p/>
     * Note that in practice, the minimum quantity is set by expanding a template or invoicing an estimate.
     */
    @Test
    public void testMinimumQuantitiesOverride() {
        BigDecimal two = BigDecimal.valueOf(2);

        // set up a user that can override minimum quantities
        Lookup userType = TestHelper.getLookup("lookup.userType", "MINIMUM_QTY_OVERRIDE");
        Party practice = getPractice();
        IMObjectBean bean = getBean(practice);
        bean.setValue("minimumQuantitiesOverride", userType.getCode());
        bean.save();
        User author = TestHelper.createUser();
        author.addClassification(userType);
        authenticationContext.setUser(author);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        layout.getContext().setUser(author); // to propagate to acts

        Party patient = TestHelper.createPatient();

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = ProductTestHelper.createService();
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        editor.setPatient(patient);
        editor.setProduct(product);

        editor.setMinimumQuantity(two);
        editor.setQuantity(two);
        assertTrue(editor.isValid());

        // set the quantity above the minimum. The minimum quantity shouldn't change
        editor.setQuantity(BigDecimal.TEN);
        checkEquals(two, editor.getMinimumQuantity());

        // now set the quantity below the minimum. As the user has the override type, the minimum quantity should update
        editor.setQuantity(ONE);
        checkEquals(ONE, editor.getMinimumQuantity());
        assertTrue(editor.isValid());

        // now set a negative quantity. This is supported for for invoice level discounts
        BigDecimal minusOne = BigDecimal.valueOf(-1);
        editor.setQuantity(minusOne);
        checkEquals(minusOne, editor.getMinimumQuantity());
        assertTrue(editor.isValid());

        // set the quantity to zero. This should disable the minimum quantity
        editor.setQuantity(ZERO);
        checkEquals(ZERO, editor.getMinimumQuantity());
        assertTrue(editor.isValid());

        // verify the minimum is disabled
        editor.setQuantity(ONE);
        checkEquals(ZERO, editor.getMinimumQuantity());
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that when a product has two fixed prices, the default is selected.
     */
    @Test
    public void testDefaultFixedPrice() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);

        Party patient = TestHelper.createPatient();

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        Product product = ProductTestHelper.createService();
        ProductPrice fixed1 = ProductPriceTestHelper.createFixedPrice(
                ZERO, ZERO, ZERO, ZERO, (Date) null, null, false);
        ProductPrice fixed2 = ProductPriceTestHelper.createFixedPrice(
                new BigDecimal("0.909"), ZERO, ZERO, ZERO, (Date) null, null, true);
        product.addProductPrice(fixed1);
        product.addProductPrice(fixed2);
        save(product);

        TestCustomerChargeActItemEditor editor1 = createEditor(charge, item, createEditContext(layout), layout);
        editor1.setPatient(patient);
        editor1.setProduct(product);
        editor1.setQuantity(ONE);
        assertTrue(editor1.isValid());
        save(charge, editor1);

        checkEquals(ONE, editor1.getFixedPrice());
        checkEquals(ONE, editor1.getTotal());

        // reload and verify the price doesn't change
        charge = get(charge);
        item = get(item);
        TestCustomerChargeActItemEditor editor2 = createEditor(charge, item, createEditContext(layout), layout);
        checkEquals(ONE, editor2.getFixedPrice());
        checkEquals(ONE, editor2.getTotal());
    }

    /**
     * Verifies that stock counts update.
     */
    @Test
    public void testUpdateStock() {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);

        Party patient = TestHelper.createPatient();
        Product product = createProduct(ProductArchetypes.MERCHANDISE, BigDecimal.TEN);
        checkStock(product, ZERO);

        // create an item with an initial quantity of zero.
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(
                customer, patient, product, ZERO, ZERO, ONE, ZERO, ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);
        getBean(item).setTarget("stockLocation", context.getStockLocation());

        TestCustomerChargeActItemEditor editor1 = createEditor(charge, item, createEditContext(layout), layout);
        assertTrue(editor1.isValid());
        save(charge, editor1);
        checkStock(product, ZERO);

        charge = get(charge);
        item = get(item);

        // set to 10, and verify stock goes down to -10
        TestCustomerChargeActItemEditor editor2 = createEditor(charge, item, createEditContext(layout), layout);
        editor2.setQuantity(BigDecimal.TEN);
        save(charge, editor2);
        checkStock(product, BigDecimal.TEN.negate());

        // set to 0 and verify stock goes back to zero
        editor2.setQuantity(ZERO);
        save(charge, editor2);
        checkStock(product, ZERO);

        // set to 1, and verify stock goes down to -1
        editor2.setQuantity(ONE);
        save(charge, editor2);
        checkStock(product, ONE.negate());
    }

    /**
     * Verifies that discounts are limited to those specified on the fixed price.
     */
    @Test
    public void testFixedPriceMaximumDiscount() {
        ProductPrice fixedPrice1 = ProductPriceTestHelper.createFixedPrice("20", "10", "100", "75", (Date) null, null,
                                                                           true);
        ProductPrice fixedPrice2 = ProductPriceTestHelper.createFixedPrice("40", "20", "100", "50", (Date) null, null,
                                                                           false);
        Party patient = TestHelper.createPatient();
        Product product = ProductTestHelper.createMerchandise();

        // create a 100% discount, and associate it with the product nad patient.
        Entity discount = DiscountTestHelper.createDiscount(MathRules.ONE_HUNDRED, true, DiscountRules.PERCENTAGE);
        IMObjectBean patientBean = getBean(patient);
        patientBean.addTarget("discounts", discount);

        product.addProductPrice(fixedPrice1);
        product.addProductPrice(fixedPrice2);
        IMObjectBean productBean = getBean(product);
        productBean.addTarget("discounts", discount);
        save(patient, product);

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);
        TestCustomerChargeActItemEditor editor = createEditor(invoice, item, createEditContext(layout), layout);

        editor.setPatient(patient);
        editor.setProduct(product);

        // fixed price should be calculated from the default $20. This has a 75% maximum discount
        checkEquals(new BigDecimal("22.0"), editor.getFixedPrice());
        checkEquals(new BigDecimal("5.50"), editor.getTotal());

        // now change the price to the $44, which calculated from the tax-inc $40 price. This has a 50% maximum discount
        editor.setFixedPrice(new BigDecimal("44.0"));
        checkEquals(new BigDecimal("22.00"), editor.getTotal());

        // now change the price to one not linked to the product. The maximum disccount should default to 100%
        editor.setFixedPrice(new BigDecimal("11.0"));
        checkEquals(ZERO, editor.getTotal());
    }

    /**
     * Verifies that if a reminder type is for a different species than the patient, no reminder is generated.
     */
    @Test
    public void testReminderForDifferentSpecies() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient1 = TestHelper.createPatient(); // CANINE
        User author1 = TestHelper.createUser();
        User clinician1 = TestHelper.createClinician();
        authenticationContext.setUser(author1);
        context.setClinician(clinician1);

        BigDecimal quantity1 = BigDecimal.valueOf(2);
        BigDecimal unitCost1 = BigDecimal.valueOf(5);
        BigDecimal unitPrice1 = new BigDecimal("9.09");
        BigDecimal unitPrice1IncTax = BigDecimal.TEN;
        BigDecimal fixedCost1 = ONE;
        BigDecimal fixedPrice1 = new BigDecimal("1.82");
        BigDecimal fixedPrice1IncTax = BigDecimal.valueOf(2);
        BigDecimal discount1 = ZERO;
        BigDecimal tax1 = BigDecimal.valueOf(2);
        BigDecimal total1 = BigDecimal.valueOf(22);
        Product product = createProduct(ProductArchetypes.MERCHANDISE, fixedCost1, fixedPrice1, unitCost1, unitPrice1);
        Entity reminderType1 = addReminderType("ZReminderType1", product);
        Entity reminderType2 = addReminderType("ZReminderType2", product, "FELINE");
        Entity reminderType3 = addReminderType("ZReminderType3", product, "CANINE");
        Entity reminderType4 = addReminderType("ZReminderType4", product, "CANINE", "FELINE");

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(invoice, item, editContext, layout);
        assertFalse(editor.isValid());

        editor.setQuantity(quantity1);
        editor.setPatient(patient1);
        editor.setProduct(product);

        // editor should now be valid
        assertTrue(editor.isValid());

        // save it
        checkSave(invoice, editor);

        invoice = get(invoice);
        item = get(item);
        assertNotNull(invoice);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient1, product, null, -1, author1, clinician1, ZERO,
                                      quantity1, unitCost1, unitPrice1IncTax, fixedCost1, fixedPrice1IncTax, discount1,
                                      tax1, total1, true);
        IMObjectBean itemBean = getBean(item);
        assertTrue(itemBean.getTargets("dispensing").isEmpty());
        assertEquals(3, itemBean.getTargets("reminders").size());

        checkReminder(item, patient1, product, reminderType1, author1, clinician1);
        checkReminder(item, patient1, product, reminderType3, author1, clinician1);
        checkReminder(item, patient1, product, reminderType4, author1, clinician1);

        // now change the patient
        Party patient2 = TestHelper.createPatient();
        IMObjectBean bean = getBean(patient2);
        bean.setValue("species", "FELINE");
        bean.save();
        editor.setPatient(patient2);

        // save it
        checkSave(invoice, editor);
        item = get(item);

        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient2, product, null, -1, author1, clinician1, ZERO,
                                      quantity1, unitCost1, unitPrice1IncTax, fixedCost1, fixedPrice1IncTax, discount1,
                                      tax1, total1, true);

        itemBean = getBean(item);
        assertEquals(3, itemBean.getTargets("reminders").size());

        checkReminder(item, patient2, product, reminderType1, author1, clinician1);
        checkReminder(item, patient2, product, reminderType2, author1, clinician1);
        checkReminder(item, patient2, product, reminderType4, author1, clinician1);
    }

    /**
     * Verify that batch changes on an invoice item propagate to the medication and vice versa.
     */
    @Test
    public void testBatch() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient1 = TestHelper.createPatient(); // CANINE
        User author1 = TestHelper.createUser();
        authenticationContext.setUser(author1);
        User clinician1 = TestHelper.createClinician();
        context.setClinician(clinician1);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = ONE;
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(2);
        BigDecimal discount = ZERO;
        BigDecimal tax = BigDecimal.valueOf(2);
        BigDecimal total = BigDecimal.valueOf(22);
        Product product1 = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);
        Date batchExpiry1 = DateRules.getTomorrow();
        Date batchExpiry2 = DateRules.getNextDate(batchExpiry1);
        Entity batch1 = ProductTestHelper.createBatch("Z-923456789", product1, batchExpiry1);
        Entity batch2 = ProductTestHelper.createBatch("Z-123456789", product1, batchExpiry2);

        Product product2 = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor1 = createEditor(invoice, item, editContext, layout);
        assertFalse(editor1.isValid());

        editor1.setQuantity(quantity);
        editor1.setPatient(patient1);
        editor1.setProduct(product1);

        assertEquals(batch1, editor1.getBatch());

        assertFalse(editor1.isValid()); // not valid while popup is displayed
        EditDialog medicationEditDialog = EchoTestHelper.findEditDialog();
        assertTrue(medicationEditDialog.getEditor() instanceof PatientMedicationActEditor);
        PatientMedicationActEditor medicationEditor1 = (PatientMedicationActEditor) medicationEditDialog.getEditor();
        assertEquals(batch1, medicationEditor1.getBatch());
        assertEquals(batchExpiry1, medicationEditor1.getExpiryDate());

        // change the batch on the medication, and verify it updates on the invoice item after OK is pressed
        medicationEditor1.setBatch(batch2);
        assertEquals(batch2, medicationEditor1.getBatch());
        assertEquals(batchExpiry2, medicationEditor1.getExpiryDate());

        assertEquals(batch1, editor1.getBatch());  // doesn't update until medication popup closes

        EchoTestHelper.fireDialogButton(medicationEditDialog, PopupDialog.OK_ID);
        assertEquals(batch2, editor1.getBatch());

        assertTrue(editor1.isValid());

        // save it
        checkSave(invoice, editor1);

        invoice = get(invoice);
        item = get(item);
        assertNotNull(invoice);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, INVOICE_ITEM, patient1, product1, null, -1, author1, clinician1, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount, tax,
                                      total, true);
        checkBatch(item, batch2);
        checkMedication(item, patient1, product1, author1, clinician1, batch2, batchExpiry2);

        // re-edit the item and change the product to one without a batch
        TestCustomerChargeActItemEditor editor2 = createEditor(invoice, item, editContext, layout);

        editor2.setProduct(product2);
        assertNull(editor2.getBatch());
        assertEquals(batchExpiry2, medicationEditor1.getExpiryDate());
        List<Act> medication = editor2.getDispensingEditor().getCurrentActs();
        assertEquals(1, medication.size());
        PatientMedicationActEditor medicationEditor2
                = (PatientMedicationActEditor) editor2.getDispensingEditor().getEditor(medication.get(0));
        assertNotNull(medicationEditor2);

        assertNull(medicationEditor2.getBatch());
        assertNull(medicationEditor2.getExpiryDate());

        // now change the product back
        editor2.setProduct(product1);

        // the batch doesn't default for non-new acts
        assertNull(editor2.getBatch());

        editor2.setBatch(batch1);
        assertEquals(editor2.getBatch(), batch1);
        assertEquals(batch1, medicationEditor2.getBatch());
        assertEquals(batchExpiry1, medicationEditor2.getExpiryDate());
    }

    /**
     * Verifies that changing the product from one with document templates to one without deletes the documents
     * on the invoice item.
     */
    @Test
    public void testDeleteDocumentOnProductChange() {
        // product with document templates
        Product product1 = createProduct(ProductArchetypes.SERVICE);
        Entity template1 = addTemplate(product1, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addTemplate(product1, PatientArchetypes.DOCUMENT_LETTER);

        // product with no templates
        Product product2 = createProduct(ProductArchetypes.SERVICE);

        // product with single template
        Product product3 = createProduct(ProductArchetypes.SERVICE);
        Entity template3 = addTemplate(product3, PatientArchetypes.DOCUMENT_LETTER);

        User author = TestHelper.createUser();
        User clinician = TestHelper.createClinician();

        authenticationContext.setUser(author);
        context.setClinician(clinician);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = TestHelper.createPatient();

        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        CustomerChargeEditContext editContext = createEditContext(layout);

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);

        editor.setPatient(patient);
        editor.setProduct(product1);
        editor.setQuantity(ONE);

        save(charge, editor);

        // verify the item has 2 documents
        IMObjectBean bean = getBean(item);
        assertEquals(2, bean.getTargets("documents").size());

        DocumentAct document1 = checkDocument(item, patient, product1, template1, author, clinician, false);
        DocumentAct document2 = checkDocument(item, patient, product1, template2, author, clinician, false);

        // change the product to one without document templates, and verify the documents are removed
        editor.setProduct(product2);

        save(charge, editor);
        assertTrue(bean.getTargets("documents").isEmpty());
        assertNull(get(document1));
        assertNull(get(document2));

        // change to another document, this time with a single template
        editor.setProduct(product3);
        save(charge, editor);
        assertEquals(1, bean.getTargets("documents").size());
        checkDocument(item, patient, product3, template3, author, clinician, false);
    }

    /**
     * Verify that when the quantity is zero, the total, tax and discount is zero.
     */
    @Test
    public void testZeroQuantity() {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = new BigDecimal(2);
        User clinician = TestHelper.createClinician();
        User author = TestHelper.createUser();
        authenticationContext.setUser(author);
        Entity discount = DiscountTestHelper.createDiscount(BigDecimal.TEN, true, DiscountRules.PERCENTAGE);
        Product product = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);
        Party patient = TestHelper.createPatient();
        addDiscount(customer, discount);
        addDiscount(product, discount);

        context.setClinician(clinician);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context = createEditContext(layout);
        context.setEditorQueue(null); // disable popups

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context, layout);
        editor.setPatient(patient);
        editor.setProduct(product);
        editor.setQuantity(quantity);
        checkSave(charge, editor);

        item = get(item);
        BigDecimal discount1 = new BigDecimal("2.20");
        BigDecimal tax1 = new BigDecimal("1.80");
        BigDecimal total1 = new BigDecimal("19.80");
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, author, clinician, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount1, tax1,
                                      total1, true);

        // now set the quantity to zero
        editor.setQuantity(ZERO);
        checkSave(charge, editor);

        item = get(item);
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, author, clinician, ZERO,
                                      ZERO, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, ZERO, ZERO, ZERO,
                                      true);
    }

    /**
     * Verify there are no limits on invoicing restricted drugs.
     */
    @Test
    public void testInvoiceRestrictedDrug() {
        Party patient = TestHelper.createPatient();
        checkPreventDispenseRestrictedDrugsOTC(FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer,
                                                                                        patient, null,
                                                                                        ActStatus.IN_PROGRESS), false);
        checkDispenseRestrictedDrugs(FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer,
                                                                              patient, null,
                                                                              ActStatus.IN_PROGRESS));
    }

    /**
     * Verify there are no limits on crediting restricted drugs.
     */
    @Test
    public void testCreditRestrictedDrug() {
        checkPreventDispenseRestrictedDrugsOTC(FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer,
                                                                                       TestHelper.createPatient(), null,
                                                                                       ActStatus.IN_PROGRESS), false);
        checkDispenseRestrictedDrugs(FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer,
                                                                             TestHelper.createPatient(), null,
                                                                             ActStatus.IN_PROGRESS));
    }

    /**
     * Verifies that restricted drugs may not be dispensed when <em>sellRestrictedDrugsOTC</em> is
     * {@code true} for counter sales.
     */
    @Test
    public void testSellRestrictedDrugOverTheCounter() {
        checkPreventDispenseRestrictedDrugsOTC(FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer,
                                                                                        null, ActStatus.IN_PROGRESS),
                                               true);
        checkDispenseRestrictedDrugs(FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer,
                                                                              null, ActStatus.IN_PROGRESS));
    }

    /**
     * Verifies that restricted drugs may not be dispensed when <em>sellRestrictedDrugsOTC</em> is {@code true},
     * and {@code expectRestricted == true} is specified.
     *
     * @param acts         the charge act and item
     * @param expectRestricted if {@code true}, dispensing a restricted product should fail, else it should succeed
     */
    private void checkPreventDispenseRestrictedDrugsOTC(List<FinancialAct> acts, boolean expectRestricted) {
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);
        IMObjectBean practiceBean = getBean(getPractice());
        practiceBean.setValue("sellRestrictedDrugsOTC", false);
        Product restricted = ProductTestHelper.createMedication(true);
        Product unrestricted = ProductTestHelper.createMedication(false);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context1 = createEditContext(layout);
        context1.setEditorQueue(null); // disable popups

        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context1, layout);
        editor.setProduct(restricted);

        if (expectRestricted) {
            Validator validator = new DefaultValidator();
            assertFalse(editor.validate(validator));
            List<ValidatorError> errors = validator.getErrors(editor);
            assertEquals(1, errors.size());

            assertEquals(restricted.getName() + " cannot be sold over the counter.\n" +
                         "It must be invoiced instead.", errors.get(0).getMessage());
        } else {
            assertTrue(editor.isValid());
        }

        // verify unrestricted products can be sold
        editor.setProduct(unrestricted);
        assertTrue(editor.isValid());
    }

    /**
     * Verifies that there are no limits on dispensing restricted drugs when <em>sellRestrictedDrugsOTC</em> is
     * {@code true}.
     *
     * @param acts the charge act and item
     */
    private void checkDispenseRestrictedDrugs(List<FinancialAct> acts) {
        FinancialAct charge = acts.get(0);
        FinancialAct item = acts.get(1);
        IMObjectBean practiceBean = getBean(getPractice());
        practiceBean.setValue("sellRestrictedDrugsOTC", true);
        Product restricted = ProductTestHelper.createMedication(true);
        Product unrestricted = ProductTestHelper.createMedication(false);

        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context = createEditContext(layout);
        context.setEditorQueue(null); // disable popups

        // should be able to dispense both restricted and unrestricted products
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context, layout);
        editor.setProduct(restricted);
        assertTrue(editor.isValid());
        editor.setProduct(unrestricted);
        assertTrue(editor.isValid());
    }

    /**
     * Adds a reminder type to a product.
     *
     * @param name    the reminder type name
     * @param product the product
     * @param species the species the reminder type applies to
     * @return the reminder type
     */
    private Entity addReminderType(String name, Product product, String... species) {
        Entity reminderType = ReminderTestHelper.createReminderType();
        reminderType.setName(name);
        for (String code : species) {
            Lookup lookup = TestHelper.getLookup(PatientArchetypes.SPECIES, code);
            reminderType.addClassification(lookup);
        }
        save(reminderType);
        IMObjectBean bean = getBean(product);
        bean.addTarget("reminders", reminderType);
        save(product);
        return reminderType;
    }

    /**
     * Verifies stock matches that expected.
     *
     * @param product  the product
     * @param expected the expected stock
     */
    private void checkStock(Product product, BigDecimal expected) {
        StockRules rules = new StockRules(getArchetypeService());
        Party stockLocation = context.getStockLocation();
        assertNotNull(stockLocation);
        BigDecimal stock = rules.getStock(product.getObjectReference(), stockLocation.getObjectReference());
        checkEquals(expected, stock);
    }

    /**
     * Creates an edit context.
     *
     * @param layout the layout context
     * @return a new edit context
     */
    private CustomerChargeEditContext createEditContext(LayoutContext layout) {
        return new CustomerChargeEditContext(customer, layout.getContext().getLocation(), layout);
    }

    /**
     * Creates a charge item editor.
     *
     * @param charge  the charge
     * @param item    the charge item
     * @param context the layout context
     * @return a new editor
     */
    private TestCustomerChargeActItemEditor createEditor(FinancialAct charge, FinancialAct item,
                                                         LayoutContext context) {
        return createEditor(charge, item, createEditContext(context), context);
    }

    /**
     * Creates a charge item editor.
     *
     * @param charge        the charge
     * @param item          the charge item
     * @param context       the edit context
     * @param layoutContext the layout context
     * @return a new editor
     */
    private TestCustomerChargeActItemEditor createEditor(FinancialAct charge, FinancialAct item,
                                                         CustomerChargeEditContext context,
                                                         LayoutContext layoutContext) {
        TestCustomerChargeActItemEditor editor = new TestCustomerChargeActItemEditor(item, charge, context,
                                                                                     layoutContext);
        editor.getComponent();
        return editor;
    }

    /**
     * Checks populating an invoice item with a product.
     *
     * @param productShortName the product archetype short name
     */
    private void checkInvoiceItem(String productShortName) {
        List<FinancialAct> acts = FinancialTestHelper.createChargesInvoice(new BigDecimal(100), customer, null, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct invoice = acts.get(0);
        FinancialAct item = acts.get(1);
        checkItem(invoice, item, productShortName);
    }

    /**
     * Checks populating a counter sale item with a product.
     *
     * @param productShortName the product archetype short name
     */
    private void checkCounterSaleItem(String productShortName) {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCounter(new BigDecimal(100), customer, null,
                                                                           ActStatus.IN_PROGRESS);
        FinancialAct counterSale = acts.get(0);
        FinancialAct item = acts.get(1);
        checkItem(counterSale, item, productShortName);
    }

    /**
     * Checks populating a credit item with a product.
     *
     * @param productShortName the product archetype short name
     */
    private void checkCreditItem(String productShortName) {
        List<FinancialAct> acts = FinancialTestHelper.createChargesCredit(new BigDecimal(100), customer, null, null,
                                                                          ActStatus.IN_PROGRESS);
        FinancialAct credit = acts.get(0);
        FinancialAct item = acts.get(1);
        checkItem(credit, item, productShortName);
    }

    /**
     * Checks populating a charge item with a product.
     *
     * @param charge           the charge
     * @param item             the charge item
     * @param productShortName the product archetype short name
     */
    private void checkItem(FinancialAct charge, FinancialAct item, String productShortName) {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient1 = TestHelper.createPatient();
        Party patient2 = TestHelper.createPatient();
        User author1 = TestHelper.createUser();
        User author2 = TestHelper.createUser();
        User clinician1 = TestHelper.createClinician();
        User clinician2 = TestHelper.createClinician();

        // create product1 with reminder, investigation type and alert type
        BigDecimal quantity1 = BigDecimal.valueOf(2);
        BigDecimal unitCost1 = BigDecimal.valueOf(5);
        BigDecimal unitPrice1 = new BigDecimal("9.09");
        BigDecimal unitPrice1IncTax = BigDecimal.TEN;
        BigDecimal fixedCost1 = ONE;
        BigDecimal fixedPrice1 = new BigDecimal("1.82");
        BigDecimal fixedPrice1IncTax = BigDecimal.valueOf(2);
        BigDecimal discount1 = ZERO;
        BigDecimal tax1 = BigDecimal.valueOf(2);
        BigDecimal total1 = BigDecimal.valueOf(22);
        Entity batch1 = null;
        Date batchExpiry1 = DateRules.getTomorrow();
        Date batchExpiry2 = DateRules.getNextDate(batchExpiry1);
        Product product1 = createProduct(productShortName, fixedCost1, fixedPrice1, unitCost1, unitPrice1);
        if (product1.isA(MEDICATION, ProductArchetypes.MERCHANDISE)) {
            // add 2 batches to product. The first should be selected due to its nearer expiry date
            batch1 = ProductTestHelper.createBatch("Z-923456789", product1, batchExpiry1);
            ProductTestHelper.createBatch("Z-123456789", product1, batchExpiry2);
        }
        Entity reminderType = addReminder(product1);
        Entity investigationType = LaboratoryTestHelper.createInvestigationType();
        ProductTestHelper.addTest(product1, LaboratoryTestHelper.createTest(investigationType));
        Entity template1 = addTemplate(product1, PatientArchetypes.DOCUMENT_FORM);
        Entity template2 = addTemplate(product1, PatientArchetypes.DOCUMENT_LETTER);
        Entity alertType = addAlertType(product1);

        // create  product2 with no reminder no investigation type, and a service ratio that doubles the unit and
        // fixed prices
        BigDecimal quantity2 = ONE;
        BigDecimal unitCost2 = BigDecimal.valueOf(5);
        BigDecimal unitPrice2 = BigDecimal.valueOf(5.05);
        BigDecimal fixedCost2 = BigDecimal.valueOf(0.5);
        BigDecimal fixedPrice2 = BigDecimal.valueOf(5.05);
        BigDecimal discount2 = ZERO;
        BigDecimal ratio = BigDecimal.valueOf(2);
        BigDecimal tax2 = BigDecimal.valueOf(2.036);

        // when the service ratio is applied, the unit and and price will be calculated as 11.10, then rounded
        // according to minPrice
        BigDecimal roundedPrice = BigDecimal.valueOf(11.20);
        BigDecimal total2 = BigDecimal.valueOf(22.40);

        Product product2 = createProduct(productShortName, fixedCost2, fixedPrice2, unitCost2, unitPrice2);
        Entity productType = ProductTestHelper.createProductType("Z Product Type");
        ProductTestHelper.addProductType(product2, productType);
        ProductTestHelper.addServiceRatio(context.getLocation(), productType, ratio);

        // set up the context
        authenticationContext.setUser(author1);
        layout.getContext().setClinician(clinician1);

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        // populate quantity, patient, product. If product1 is a medication, it should trigger a patient medication
        // editor popup
        editor.setQuantity(quantity1);

        if (!TypeHelper.isA(item, CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient1);
        }
        editor.setProduct(product1);

        EditorQueue queue = editContext.getEditorQueue();
        if (item.isA(INVOICE_ITEM)) {
            if (product1.isA(MEDICATION)) {
                // invoice items have a dispensing node
                assertFalse(editor.isValid()); // not valid while popup is displayed
                checkSavePopup(queue, PatientArchetypes.PATIENT_MEDICATION, false);
                // save the popup editor - should be a medication
            }

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, InvestigationArchetypes.PATIENT_INVESTIGATION, false);

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, ReminderArchetypes.REMINDER, false);

            assertFalse(editor.isValid()); // not valid while popup is displayed
            checkSavePopup(queue, PatientArchetypes.ALERT, false);
        }

        // editor should now be valid
        assertTrue(editor.isValid());

        // save it
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient1, product1, null, -1, author1, clinician1,
                                      ZERO, quantity1, unitCost1, unitPrice1IncTax, fixedCost1, fixedPrice1IncTax,
                                      discount1, tax1, total1, true);
        IMObjectBean itemBean = getBean(item);
        if (item.isA(INVOICE_ITEM)) {
            checkBatch(item, batch1);
            if (product1.isA(MEDICATION)) {
                // verify there is a medication act
                checkMedication(item, patient1, product1, author1, clinician1, batch1, batchExpiry1);
            } else {
                assertTrue(itemBean.getTargets("dispensing").isEmpty());
            }

            assertEquals(1, itemBean.getTargets("investigations").size());
            assertEquals(1, itemBean.getTargets("reminders").size());
            assertEquals(2, itemBean.getTargets("documents").size());
            assertEquals(1, itemBean.getTargets("alerts").size());

            assertNotNull(investigationType);
            checkInvestigation(item, patient1, investigationType, author1, clinician1);
            checkReminder(item, patient1, product1, reminderType, author1, clinician1);
            checkDocument(item, patient1, product1, template1, author1, clinician1, false);
            checkDocument(item, patient1, product1, template2, author1, clinician1, false);
            checkAlert(item, patient1, product1, alertType, author1, clinician1);
        } else {
            // act shouldn't have investigations, reminders, documents, alerts nodes
            assertFalse(itemBean.hasNode("investigations"));
            assertFalse(itemBean.hasNode("reminders"));
            assertFalse(itemBean.hasNode("documents"));
            assertFalse(itemBean.hasNode("alerts"));
        }

        // now change the patient, product, and clinician
        authenticationContext.setUser(author2);
        if (itemBean.hasNode("patient")) {
            editor.setPatient(patient2);
        }
        if (item.isA(INVOICE_ITEM)) {
            checkSavePopup(queue, InvestigationArchetypes.PATIENT_INVESTIGATION, false);
            checkSavePopup(queue, ReminderArchetypes.REMINDER, false);
        }

        editor.setProduct(product2);
        editor.setQuantity(quantity2);
        editor.setDiscount(discount2);
        if (itemBean.hasNode("clinician")) {
            editor.setClinician(clinician2);
        }

        // should be no more popups. For medication products, the
        assertNull(queue.getCurrent());  // no new popup - existing medication should update
        assertTrue(editor.isValid());

        // save it
        checkSave(charge, editor);

        item = get(item);
        assertNotNull(item);

        // fixedPrice2 and unitPrice2 are calculated as 11.10, then rounded to minPrice
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient2, product2, null, -1, author1, clinician2,
                                      ZERO, quantity2, unitCost2, roundedPrice, fixedCost2, roundedPrice, discount2,
                                      tax2, total2, true);
        assertEquals(author2.getObjectReference(), item.getUpdatedBy());

        itemBean = getBean(item);
        if (itemBean.isA(INVOICE_ITEM)) {
            if (product2.isA(MEDICATION)) {
                // verify there is a medication act. Note that it retains the original author
                Act medication = checkMedication(item, patient2, product2, author1, clinician2, null, null);
                assertEquals(author2.getObjectReference(), medication.getUpdatedBy());
            } else {
                // verify there is no medication act
                assertTrue(itemBean.getTargets("dispensing").isEmpty());
            }

            assertTrue(itemBean.getTargets("investigations").isEmpty());
            assertTrue(itemBean.getTargets("reminders").isEmpty());
            assertTrue(itemBean.getTargets("documents").isEmpty());
            assertTrue(itemBean.getTargets("alerts").isEmpty());
        } else {
            assertFalse(itemBean.hasNode("dispensing"));
            assertFalse(itemBean.hasNode("investigations"));
            assertFalse(itemBean.hasNode("reminders"));
            assertFalse(itemBean.hasNode("documents"));
            assertFalse(itemBean.hasNode("alerts"));
        }

        // make sure that clinicians can be set to null, as a test for OVPMS-1104
        if (itemBean.hasNode("clinician")) {
            editor.setClinician(null);
            assertTrue(editor.isValid());
            checkSave(charge, editor);

            item = get(item);
            assertNotNull(item);

            // createdBy = author1 doesn't change
            FinancialTestHelper.checkItem(item, item.getArchetype(), patient2, product2, null,
                                          -1, author1, null, ZERO, quantity2, unitCost2, roundedPrice, fixedCost2,
                                          roundedPrice, discount2, tax2, total2, true);
            assertEquals(author2.getObjectReference(), item.getUpdatedBy());
        }

        editor.setProduct(null);       // make sure nulls are handled
        assertFalse(editor.isValid());

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Verifies an invoice item batch matches that expected.
     *
     * @param item  the invoice item
     * @param batch the expected batch. May be {@code null}
     */
    private void checkBatch(FinancialAct item, Entity batch) {
        IMObjectBean bean = getBean(item);
        if (batch == null) {
            assertNull(bean.getTargetRef("batch"));
        } else {
            assertEquals(batch.getObjectReference(), bean.getTargetRef("batch"));
        }
    }

    /**
     * Checks populating a charge item with a template product.
     * <p/>
     * NOTE: currently, charge items with template products validate correctly, but fail to save.
     * <p/>This is because the charge item relationship editor will only expand templates if the charge item itself
     * is valid - marking the item invalid for having a template would prevent this.
     * TODO - not ideal.
     *
     * @param charge the charge
     * @param item   the charge item
     */
    private void checkItemWithTemplate(FinancialAct charge, FinancialAct item) {
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        context.getContext().setPractice(getPractice());

        Party patient = TestHelper.createPatient();
        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        Product product = createProduct(ProductArchetypes.TEMPLATE, fixedCost, fixedPrice, unitCost, unitPrice);
        // costs and prices should be ignored
        User clinician = TestHelper.createClinician();

        User author = TestHelper.createUser();
        authenticationContext.setUser(author);
        context.getContext().setClinician(clinician);

        CustomerChargeActItemEditor editor = new DefaultCustomerChargeActItemEditor(
                item, charge, createEditContext(context), context);
        editor.getComponent();
        assertFalse(editor.isValid());

        // populate quantity, patient, product
        editor.setQuantity(quantity);
        if (!TypeHelper.isA(item, CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient);
        }
        editor.setProduct(product);

        // editor should now be valid, but won't save
        assertTrue(editor.isValid());

        try {
            save(charge, editor);
            fail("Expected save to fail");
        } catch (IllegalStateException expected) {
            assertEquals("Cannot save with product template: " + product.getName(), expected.getMessage());
        }

        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, null, clinician, ZERO,
                                      quantity, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, ZERO, true);
        if (item.isA(INVOICE_ITEM)) {
            IMObjectBean itemBean = getBean(item);
            // verify there are no medication acts
            assertTrue(itemBean.getTargets("dispensing").isEmpty());
        }

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Tests charging a product with a 10% discount.
     *
     * @param charge           the charge
     * @param item             the charge item
     * @param negativeQuantity if {@code true}, use a negative quantity
     */
    private void checkDiscounts(FinancialAct charge, FinancialAct item, boolean negativeQuantity) {
        BigDecimal quantity = (negativeQuantity) ? BigDecimal.valueOf(-2) : BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = new BigDecimal(2);
        User clinician = TestHelper.createClinician();
        User author = TestHelper.createUser();
        Entity discount = DiscountTestHelper.createDiscount(BigDecimal.TEN, true, DiscountRules.PERCENTAGE);
        Product product = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);
        Party patient = TestHelper.createPatient();
        addDiscount(customer, discount);
        addDiscount(product, discount);

        authenticationContext.setUser(author);
        context.setClinician(clinician);
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        CustomerChargeEditContext context = createEditContext(layout);
        context.setEditorQueue(null); // disable popups

        // create the editor
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, context, layout);
        assertFalse(editor.isValid());

        if (!item.isA(CustomerAccountArchetypes.COUNTER_ITEM)) {
            // counter sale items have no patient
            editor.setPatient(patient);
        }
        editor.setProduct(product);
        editor.setQuantity(quantity);

        // editor should now be valid
        assertTrue(editor.isValid());

        checkSave(charge, editor);

        item = get(item);
        BigDecimal discount1 = new BigDecimal("2.20");
        BigDecimal tax1 = new BigDecimal("1.80");
        BigDecimal total1 = new BigDecimal("19.80");
        if (negativeQuantity) {
            tax1 = tax1.negate();
            total1 = total1.negate();
        }
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, author, clinician, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount1, tax1,
                                      total1, true);

        // now remove the discounts
        editor.setDiscount(ZERO);
        checkSave(charge, editor);

        item = get(item);
        BigDecimal discount2 = ZERO;
        BigDecimal tax2 = new BigDecimal("2.00");
        BigDecimal total2 = new BigDecimal("22.00");
        if (negativeQuantity) {
            tax2 = tax2.negate();
            total2 = total2.negate();
        }
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, author, clinician, ZERO,
                                      quantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount2, tax2,
                                      total2, true);
    }

    /**
     * Tests charging a product with a dose.
     *
     * @param charge the charge
     * @param item   the charge item
     */
    private void checkProductDose(FinancialAct charge, FinancialAct item) {
        LayoutContext layout = new DefaultLayoutContext(context, new HelpContext("foo", null));
        Party patient = TestHelper.createPatient();
        PatientTestHelper.createWeight(patient, new Date(), new BigDecimal("4.2"), WeightUnits.KILOGRAMS);
        User author = TestHelper.createUser();
        User clinician = TestHelper.createClinician();

        BigDecimal quantity = BigDecimal.valueOf(2);
        BigDecimal unitCost = BigDecimal.valueOf(5);
        BigDecimal unitPrice = new BigDecimal("9.09");
        BigDecimal unitPriceIncTax = BigDecimal.TEN;
        BigDecimal fixedCost = BigDecimal.valueOf(1);
        BigDecimal fixedPrice = new BigDecimal("1.82");
        BigDecimal fixedPriceIncTax = BigDecimal.valueOf(2);
        BigDecimal discount = ZERO;
        BigDecimal doseQuantity = new BigDecimal("4.2");

        Product product = createProduct(MEDICATION, fixedCost, fixedPrice, unitCost, unitPrice);
        IMObjectBean bean = getBean(product);
        bean.setValue("concentration", ONE);
        Entity dose = ProductTestHelper.createDose(null, ZERO, BigDecimal.TEN, ONE, ONE);
        ProductTestHelper.addDose(product, dose);

        authenticationContext.setUser(author);

        // create the editor
        CustomerChargeEditContext editContext = createEditContext(layout);
        editContext.setEditorQueue(null);  // disable popups
        TestCustomerChargeActItemEditor editor = createEditor(charge, item, editContext, layout);
        assertFalse(editor.isValid());

        assertFalse((editor.isDefaultQuantity()));

        // populate quantity, patient, clinician and product
        editor.setQuantity(quantity);
        assertFalse((editor.isDefaultQuantity()));

        editor.setPatient(patient);
        editor.setClinician(clinician);
        editor.setProduct(product);
        checkEquals(doseQuantity, editor.getQuantity());
        assertTrue(editor.isDefaultQuantity());

        // editor should now be valid
        assertTrue(editor.isValid());
        checkSave(charge, editor);

        charge = get(charge);
        item = get(item);
        assertNotNull(charge);
        assertNotNull(item);

        // verify the item matches that expected
        BigDecimal tax = new BigDecimal("4.0");
        BigDecimal total = new BigDecimal("44.00");
        FinancialTestHelper.checkItem(item, item.getArchetype(), patient, product, null, -1, author, clinician, ZERO,
                                      doseQuantity, unitCost, unitPriceIncTax, fixedCost, fixedPriceIncTax, discount,
                                      tax, total, true);

        // verify no errors were logged
        assertTrue(errors.isEmpty());
    }

    /**
     * Saves a charge and charge item editor in a single transaction, verifying the save was successful.
     *
     * @param charge the charge
     * @param editor the charge item editor
     */
    private void checkSave(final FinancialAct charge, final CustomerChargeActItemEditor editor) {
        boolean result = save(charge, editor);
        assertTrue(result);
    }

    /**
     * Saves a charge and charge item editor in a single transaction.
     *
     * @param charge the charge
     * @param editor the charge item editor
     * @return {@code true} if the save was successful, otherwise {@code false}
     */
    private boolean save(final FinancialAct charge, final CustomerChargeActItemEditor editor) {
        TransactionTemplate template = new TransactionTemplate(ServiceHelper.getTransactionManager());
        return template.execute(status -> {
            PatientHistoryChanges changes = new PatientHistoryChanges(null, getArchetypeService());
            ChargeSaveContext context1 = editor.getSaveContext();
            context1.setHistoryChanges(changes);
            editor.getEditContext().getInvestigations().save();
            boolean saved = SaveHelper.save(charge);
            editor.save();
            if (saved) {
                context1.save();
            }
            context1.setHistoryChanges(null);
            return saved;
        });
    }

    private static class TestCustomerChargeActItemEditor extends CustomerChargeActItemEditor {

        /**
         * Constructs a {@link TestCustomerChargeActItemEditor}.
         * <p/>
         * This recalculates the tax amount.
         *
         * @param act           the act to edit
         * @param parent        the parent act
         * @param context       the edit context
         * @param layoutContext the layout context
         */
        TestCustomerChargeActItemEditor(FinancialAct act, Act parent, CustomerChargeEditContext context,
                                        LayoutContext layoutContext) {
            super(act, parent, context, layoutContext);
        }

        @Override
        public ActRelationshipCollectionEditor getDispensingEditor() {
            return super.getDispensingEditor();
        }
    }
}
