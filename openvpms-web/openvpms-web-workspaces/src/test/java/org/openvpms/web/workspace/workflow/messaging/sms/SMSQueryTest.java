/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.junit.Test;
import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.query.AbstractQueryTest;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.echo.help.HelpContext;

/**
 * Tests the {@link SMSQuery}.
 *
 * @author Tim Anderson
 */
public class SMSQueryTest extends AbstractQueryTest<Act> {

    /**
     * Tests querying SMSes by practice location.
     * <p/>
     * Only those SMSes for locations accessible by the user are returned.
     * <br/>
     * SMSes not associated with an practice location are always returned.
     */
    @Test
    public void testQueryByLocation() {
        Party location1 = TestHelper.createLocation();
        Party location2 = TestHelper.createLocation();
        Party location3 = TestHelper.createLocation();
        Party practice = create(PracticeArchetypes.PRACTICE, Party.class);
        IMObjectBean practiceBean = getBean(practice);
        practiceBean.addTarget("locations", location1);
        practiceBean.addTarget("locations", location2);
        practiceBean.addTarget("locations", location3);

        User user = TestHelper.createUser();
        IMObjectBean userBean = getBean(user);
        userBean.addTarget("locations", location1);
        userBean.addTarget("locations", location2);

        LocalContext context = new LocalContext();
        context.setPractice(practice);
        context.setUser(user);
        SMSQuery query = new SMSQuery(new DefaultLayoutContext(context, new HelpContext("foo", null)));

        Act sms1 = createSMS(location1);
        Act sms2 = createSMS(location2);
        Act sms3 = createSMS(location3);
        Act sms4 = createSMS(null);

        query.setLocation(null);
        checkSelects(true, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(false, query, sms3);
        checkSelects(true, query, sms4);

        query.setLocation(location1);
        checkSelects(true, query, sms1);
        checkSelects(false, query, sms2);
        checkSelects(false, query, sms3);
        checkSelects(true, query, sms4);

        query.setLocation(location2);
        checkSelects(false, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(false, query, sms3);
        checkSelects(true, query, sms4);
    }

    /**
     * Tests querying SMSes by location and recipient name.
     */
    @Test
    public void testQueryByLocationAndRecipient() {
        Party location1 = TestHelper.createLocation();
        Party location2 = TestHelper.createLocation();
        Party practice = create(PracticeArchetypes.PRACTICE, Party.class);
        IMObjectBean practiceBean = getBean(practice);
        practiceBean.addTarget("locations", location1);
        practiceBean.addTarget("locations", location2);

        String name1 = getUniqueValue();
        String name2 = getUniqueValue();

        Act sms1 = createSMS(name1, location1);
        Act sms2 = createSMS(name1, location2);
        Act sms3 = createSMS(name1, null);
        Act sms4 = createSMS(name2, location1);
        Act sms5 = createSMS(name2, location2);
        Act sms6 = createSMS(name2, null);

        LocalContext context = new LocalContext();
        context.setPractice(practice);
        SMSQuery query = new SMSQuery(new DefaultLayoutContext(context, new HelpContext("foo", null)));

        query.setLocation(null);
        query.setValue(name1);
        checkSelects(true, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(true, query, sms3);
        checkSelects(false, query, sms4);
        checkSelects(false, query, sms5);
        checkSelects(false, query, sms6);

        query.setLocation(location1);
        checkSelects(true, query, sms1);
        checkSelects(false, query, sms2);
        checkSelects(true, query, sms3);
        checkSelects(false, query, sms4);
        checkSelects(false, query, sms5);
        checkSelects(false, query, sms6);

        query.setLocation(location2);
        checkSelects(false, query, sms1);
        checkSelects(true, query, sms2);
        checkSelects(true, query, sms3);
        checkSelects(false, query, sms4);
        checkSelects(false, query, sms5);
        checkSelects(false, query, sms6);
    }

    /**
     * Creates a new query.
     *
     * @return a new query
     */
    @Override
    protected Query<Act> createQuery() {
        return new SMSQuery(new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null)));
    }

    /**
     * Creates a new object, selected by the query.
     *
     * @param value a value that can be used to uniquely identify the object
     * @param save  if {@code true} save the object, otherwise don't save it
     * @return the new object
     */
    @Override
    protected Act createObject(String value, boolean save) {
        Act sms = create(SMSArchetypes.MESSAGE, Act.class);
        IMObjectBean bean = getBean(sms);
        Party customer = TestHelper.createCustomer("foo", value, true);
        bean.setTarget("recipient", customer);
        bean.setValue("phone", "12345678");
        bean.setValue("message", "test message");
        if (save) {
            bean.save();
        }
        return sms;
    }

    /**
     * Generates a unique value which may be used for querying objects on.
     *
     * @return a unique value
     */
    @Override
    protected String getUniqueValue() {
        return getUniqueValue("ZCustomer");
    }

    private Act createSMS(Party location) {
        return createSMS(getUniqueValue(), location);
    }

    private Act createSMS(String recipientName, Party location) {
        Act sms = createObject(recipientName, false);
        if (location != null) {
            IMObjectBean bean = getBean(sms);
            bean.setTarget("location", location);
            bean.save();
        }
        save(sms);
        return sms;
    }

}
