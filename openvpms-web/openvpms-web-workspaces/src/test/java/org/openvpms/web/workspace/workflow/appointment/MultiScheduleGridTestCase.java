package org.openvpms.web.workspace.workflow.appointment;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.AppointmentRules;
import org.openvpms.archetype.rules.workflow.ScheduleEvents;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.workspace.workflow.scheduling.ScheduleEventGrid.Availability;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link MultiScheduleGrid}.
 *
 * @author Tim Anderson
 */
public class MultiScheduleGridTestCase extends AbstractAppointmentGridTest {

    /**
     * The appointment rules.
     */
    @Autowired
    private AppointmentRules rules;

    /**
     * The roster service.
     */
    @Autowired
    private RosterService rosterService;

    /**
     * Tests a grid with different schedule times and slot sizes.
     */
    @Test
    public void testGridWithDifferentScheduleTimesAndSlotSizes() {
        Entity schedule1 = createSchedule(15, "09:00:00", "17:00:00"); // 9am-5pm schedule, 15 min slots
        Entity schedule2 = createSchedule(60, "08:00:00", "16:00:00"); // 8am-4pm schedule, 60 min slots
        Date date = getDate("2019-10-07");

        Act appointment1a = createAppointment("2019-10-07 09:00", "2019-10-07 09:15", schedule1);
        Act appointment1b = createAppointment("2019-10-07 09:30", "2019-10-07 10:00", schedule1);
        Act appointment2a = createAppointment("2019-10-07 08:00", "2019-10-07 10:00", schedule2);
        Act appointment2b = createAppointment("2019-10-07 12:00", "2019-10-07 13:00", schedule2);

        Entity scheduleView = ScheduleTestHelper.createScheduleView(schedule1, schedule2);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule1, getScheduleEvents(appointment1a, appointment1b));
        map.put(schedule2, getScheduleEvents(appointment2a, appointment2b));
        MultiScheduleGrid grid1 = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        assertEquals(15, grid1.getSlotSize());  // uses smallest slot size of the schedules
        assertEquals(36, grid1.getSlots());

        // check schedule1
        checkSlot(grid1, schedule1, 0, "2019-10-07 08:00", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 1, "2019-10-07 08:15", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 2, "2019-10-07 08:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 3, "2019-10-07 08:45", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 4, "2019-10-07 09:00", appointment1a, 1, Availability.BUSY);
        checkSlot(grid1, schedule1, 5, "2019-10-07 09:15", null, 0, Availability.FREE);
        checkSlot(grid1, schedule1, 6, "2019-10-07 09:30", appointment1b, 2, Availability.BUSY);
        checkSlot(grid1, schedule1, 7, "2019-10-07 09:45", appointment1b, 1, Availability.BUSY);
        checkSlot(grid1, schedule1, 8, "2019-10-07 10:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule1, 35, "2019-10-07 16:45", null, 0, Availability.FREE);

        // check schedule2
        checkSlot(grid1, schedule2, 0, "2019-10-07 08:00", appointment2a, 8, Availability.BUSY);
        checkSlot(grid1, schedule2, 1, "2019-10-07 08:15", appointment2a, 7, Availability.BUSY);
        checkSlot(grid1, schedule2, 2, "2019-10-07 08:30", appointment2a, 6, Availability.BUSY);
        checkSlot(grid1, schedule2, 3, "2019-10-07 08:45", appointment2a, 5, Availability.BUSY);
        checkSlot(grid1, schedule2, 4, "2019-10-07 09:00", appointment2a, 4, Availability.BUSY);
        checkSlot(grid1, schedule2, 5, "2019-10-07 09:15", appointment2a, 3, Availability.BUSY);
        checkSlot(grid1, schedule2, 6, "2019-10-07 09:30", appointment2a, 2, Availability.BUSY);
        checkSlot(grid1, schedule2, 7, "2019-10-07 09:45", appointment2a, 1, Availability.BUSY);
        checkSlot(grid1, schedule2, 8, "2019-10-07 10:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule2, 15, "2019-10-07 11:45", null, 0, Availability.FREE);
        checkSlot(grid1, schedule2, 16, "2019-10-07 12:00", appointment2b, 4, Availability.BUSY);
        checkSlot(grid1, schedule2, 17, "2019-10-07 12:15", appointment2b, 3, Availability.BUSY);
        checkSlot(grid1, schedule2, 18, "2019-10-07 12:30", appointment2b, 2, Availability.BUSY);
        checkSlot(grid1, schedule2, 19, "2019-10-07 12:45", appointment2b, 1, Availability.BUSY);
        checkSlot(grid1, schedule2, 20, "2019-10-07 13:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule2, 35, "2019-10-07 16:45", null, 0, Availability.UNAVAILABLE);
    }

    /**
     * Verifies that if an appointment is outside the schedule times, slots are added.
     */
    @Test
    public void testAppointmentsOutsideOfScheduleTimes() {
        Entity schedule1 = createSchedule(30, "09:00:00", "17:00:00"); // 9am-5pm schedule, 30 min slots
        Entity schedule2 = createSchedule(60, "08:00:00", "16:00:00"); // 8am-4pm schedule, 60 min slots
        Date date = getDate("2019-10-07");

        // schedule1 appointments
        Act appointment1a = createAppointment("2019-10-07 08:00", "2019-10-07 08:30", schedule1);
        Act appointment1b = createAppointment("2019-10-07 16:30", "2019-10-07 17:30", schedule1);

        // schedule2 appointments
        Act appointment2a = createAppointment("2019-10-07 07:00", "2019-10-07 07:30", schedule2);
        // shorter than schedule2 slot size

        Act appointment2b = createAppointment("2019-10-07 16:00", "2019-10-07 17:00", schedule2);

        Entity scheduleView = ScheduleTestHelper.createScheduleView(schedule1, schedule2);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule1, getScheduleEvents(appointment1a, appointment1b));
        map.put(schedule2, getScheduleEvents(appointment2a, appointment2b));
        MultiScheduleGrid grid1 = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        assertEquals(30, grid1.getSlotSize());  // uses smallest slot size of the schedules
        assertEquals(21, grid1.getSlots());

        // check schedule1
        checkSlot(grid1, schedule1, 0, "2019-10-07 07:00", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 1, "2019-10-07 07:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 2, "2019-10-07 08:00", appointment1a, 1, Availability.BUSY);
        checkSlot(grid1, schedule1, 3, "2019-10-07 08:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule1, 4, "2019-10-07 09:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule1, 18, "2019-10-07 16:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule1, 19, "2019-10-07 16:30", appointment1b, 2, Availability.BUSY);
        checkSlot(grid1, schedule1, 20, "2019-10-07 17:00", appointment1b, 1, Availability.BUSY);

        // check schedule2
        checkSlot(grid1, schedule2, 0, "2019-10-07 07:00", appointment2a, 1, Availability.BUSY);
        checkSlot(grid1, schedule2, 1, "2019-10-07 07:30", null, 0, Availability.UNAVAILABLE);
        checkSlot(grid1, schedule2, 2, "2019-10-07 08:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule2, 3, "2019-10-07 08:30", null, 0, Availability.FREE);
        checkSlot(grid1, schedule2, 18, "2019-10-07 16:00", appointment2b, 2, Availability.BUSY);
        checkSlot(grid1, schedule2, 19, "2019-10-07 16:30", appointment2b, 1, Availability.BUSY);
        checkSlot(grid1, schedule2, 20, "2019-10-07 17:00", null, 0, Availability.UNAVAILABLE);
    }

    /**
     * Tests the behaviour of appointments that don't fall on slot boundaries.
     */
    @Test
    public void testAppointmentsNotOnSlotBoundaries() {
        Entity schedule = createSchedule(60, "09:00:00", "17:00:00"); // 9am-5pm schedule, 1 hour slots
        Date date = getDate("2019-10-07");
        Act appointment1 = createAppointment("2019-10-07 08:30", "2019-10-07 09:30", schedule);
        Act appointment2 = createAppointment("2019-10-07 11:00", "2019-10-07 11:15", schedule);
        Act appointment3 = createAppointment("2019-10-07 12:30", "2019-10-07 13:00", schedule);
        Act appointment4 = createAppointment("2019-10-07 16:45", "2019-10-07 17:15", schedule);

        Entity scheduleView = ScheduleTestHelper.createScheduleView(schedule);
        Map<Entity, ScheduleEvents> map = new HashMap<>();
        map.put(schedule, getScheduleEvents(appointment1, appointment2, appointment3, appointment4));
        MultiScheduleGrid grid1 = new MultiScheduleGrid(scheduleView, date, map, rules, rosterService);

        assertEquals(10, grid1.getSlots());
        checkSlot(grid1, schedule, 0, "2019-10-07T08:00:00+11:00", appointment1, 2, Availability.BUSY);
        checkSlot(grid1, schedule, 1, "2019-10-07T09:00:00+11:00", appointment1, 1, Availability.BUSY);
        checkSlot(grid1, schedule, 2, "2019-10-07T10:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule, 3, "2019-10-07T11:00:00+11:00",appointment2, 1, Availability.BUSY);
        checkSlot(grid1, schedule, 4, "2019-10-07T12:00:00+11:00", appointment3, 1, Availability.BUSY);
        checkSlot(grid1, schedule, 5, "2019-10-07T13:00:00+11:00", null, 0, Availability.FREE);
        checkSlot(grid1, schedule, 8, "2019-10-07T16:00:00+11:00", appointment4, 2, Availability.BUSY);
        checkSlot(grid1, schedule, 9, "2019-10-07T17:00:00+11:00", appointment4, 1, Availability.BUSY);
    }
}
