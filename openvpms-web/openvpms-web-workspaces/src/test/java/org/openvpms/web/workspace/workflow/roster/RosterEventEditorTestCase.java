/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.roster;

import org.junit.Test;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.rules.workflow.roster.RosterArchetypes;
import org.openvpms.archetype.rules.workflow.roster.RosterSyncStatus;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.domain.im.security.User;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.edit.SaveHelper;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link RosterEventEditor}.
 *
 * @author Tim Anderson
 */
public class RosterEventEditorTestCase extends AbstractAppTest {

    /**
     * Verifies that if a roster event has been synchronised, its sync statuses are set to PENDING on save.
     * <p/>
     * This enables roster synchronisation services to track and propagate changes.
     */
    @Test
    public void testSyncStatus() {
        Act event = (Act) create(RosterArchetypes.ROSTER_EVENT);
        IMObjectBean bean = getBean(event);
        User user = TestHelper.createUser();
        Party location = TestHelper.createLocation();
        Entity area = ScheduleTestHelper.createRosterArea(location);

        bean.setTarget("schedule", area);
        bean.setTarget("location", location);

        ActIdentity id1 = createSyncId();
        ActIdentity id2 = createSyncId();

        event.addIdentity(id1);
        event.addIdentity(id2);

        DefaultLayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        RosterEventEditor editor = new RosterEventEditor(event, null, context);

        assertTrue(SaveHelper.save(editor));

        checkStatus(id1, RosterSyncStatus.PENDING);
        checkStatus(id2, RosterSyncStatus.PENDING);

        setStatus(id1, RosterSyncStatus.SYNC);
        setStatus(id2, RosterSyncStatus.ERROR);

        assertTrue(SaveHelper.save(editor));

        checkStatus(id1, RosterSyncStatus.SYNC);
        checkStatus(id2, RosterSyncStatus.ERROR);

        editor.setUser(user);

        checkStatus(id1, RosterSyncStatus.PENDING);
        checkStatus(id2, RosterSyncStatus.PENDING);
    }

    private void checkStatus(ActIdentity identity, String status) {
        IMObjectBean bean = getBean(identity);
        assertEquals(status, bean.getString("status"));
    }

    private void setStatus(ActIdentity identity, String status) {
        IMObjectBean bean = getBean(identity);
        bean.setValue("status", status);
    }

    private ActIdentity createSyncId() {
        ActIdentity id = (ActIdentity) create("actIdentity.syncTest");
        id.setIdentity(UUID.randomUUID().toString());
        return id;
    }
}
