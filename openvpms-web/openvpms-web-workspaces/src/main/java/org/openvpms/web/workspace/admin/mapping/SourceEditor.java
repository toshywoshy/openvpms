/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import nextapp.echo2.app.table.TableColumn;
import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Source;
import org.openvpms.web.component.im.edit.AbstractSelectorPropertyEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.Browser;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.select.AbstractQuerySelector;
import org.openvpms.web.component.im.util.IMObjectHelper;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;

/**
 * Edit the source of a mapping.
 *
 * @author Tim Anderson
 */
class SourceEditor extends AbstractSelectorPropertyEditor<Source> implements MappingSource {

    /**
     * The mappings.
     */
    private final Mappings<?> mappings;

    /**
     * Constructs a {@link SourceEditor}.
     *
     * @param source   the source. May be {@code null}
     * @param mappings the mappings
     * @param context  the layout context
     */
    SourceEditor(Reference source, Mappings<?> mappings, LayoutContext context) {
        super(new SimpleProperty("source", Reference.class), context);
        getProperty().setValue(source);
        this.mappings = mappings;
        updateSelector();
    }

    /**
     * Returns the object corresponding to the property.
     *
     * @return the object. May be {@code null}
     */
    @Override
    public Source getObject() {
        Reference ref = getProperty().getReference();
        return ref != null ? new SourceImpl(ref, IMObjectHelper.getName(ref)) : null;
    }

    /**
     * Creates a new selector.
     *
     * @param property    the property
     * @param context     the layout context
     * @param allowCreate determines if objects may be created
     * @return a new selector
     */
    @Override
    protected AbstractQuerySelector<Source> createSelector(Property property, LayoutContext context,
                                                           boolean allowCreate) {
        return new AbstractQuerySelector<Source>(property.getDisplayName(), allowCreate, context) {
            @Override
            protected String getName(Source object) {
                return object.getName();
            }

            @Override
            protected String getDescription(Source object) {
                return null;
            }

            @Override
            protected boolean getActive(Source object) {
                return true;
            }

            /**
             * Creates a new browser.
             *
             * @param value    a value to filter results by. May be {@code null}
             * @param runQuery if {@code true} run the query
             * @return a return a new browser
             */
            @Override
            protected Browser<Source> createBrowser(String value, boolean runQuery) {
                return new SourceBrowser(value, getLayoutContext());
            }

            /**
             * Returns the results matching the specified value.
             *
             * @param value the value. May be {@code null}
             * @return the results matching the value
             */
            @Override
            protected ResultSet<Source> getMatches(String value) {
                return new SourceResultSet(value, MappingBrowser.MAX_RESULTS, mappings);
            }
        };
    }

    /**
     * Updates the underlying property with the specified value.
     *
     * @param property the property
     * @param value    the value to update with. May be {@code null}
     * @return {@code true} if the property was modified
     */
    @Override
    protected boolean updateProperty(Property property, Source value) {
        Reference reference = (value != null) ? value.getId() : null;
        return property.setValue(reference);
    }

    private class SourceBrowser extends MappingBrowser<Source> {

        /**
         * Constructs a {@link SourceBrowser}.
         *
         * @param value   the initial value
         * @param context the layout context
         */
        SourceBrowser(String value, LayoutContext context) {
            super(value, new SourceTableModel(), context);
        }

        /**
         * Creates a new result set.
         *
         * @param value      the value to filter on. May be {@code null}
         * @param maxResults the maximum number of results per page
         * @return the result set
         */
        @Override
        protected ResultSet<Source> createResultSet(String value, int maxResults) {
            return new SourceResultSet(value, maxResults, mappings);
        }
    }

    private static class SourceTableModel extends AbstractMappingTableModel<Source> {
        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(Source object, TableColumn column, int row) {
            if (column.getModelIndex() == ID_INDEX) {
                return object.getId().getId();
            }
            return object.getName();
        }
    }
}
