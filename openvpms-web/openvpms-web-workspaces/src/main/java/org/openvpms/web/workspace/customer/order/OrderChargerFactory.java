package org.openvpms.web.workspace.customer.order;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.rules.finance.order.OrderRules;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.echo.help.HelpContext;

/**
 * Factory for {@link OrderCharger} instances.
 *
 * @author Tim Anderson
 */
public class OrderChargerFactory {

    /**
     * The order rules.
     */
    private final OrderRules orderRules;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules customerAccountRules;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The rule-based archetype service.
     */
    private final IArchetypeRuleService ruleService;

    /**
     * Constructs an {@link OrderChargerFactory}.
     *
     * @param orderRules           the order rules
     * @param customerAccountRules the customer account rules
     * @param service              the archetype service
     * @param ruleService          the rule-based archetype service
     */
    public OrderChargerFactory(OrderRules orderRules, CustomerAccountRules customerAccountRules,
                               IArchetypeService service, IArchetypeRuleService ruleService) {
        this.orderRules = orderRules;
        this.customerAccountRules = customerAccountRules;
        this.service = service;
        this.ruleService = ruleService;
    }

    /**
     * Creates an {@link OrderCharger}.
     *
     * @param customer the customer
     * @param context  the context
     * @param help     the help context
     */
    public OrderCharger create(Party customer, Context context, HelpContext help) {
        return new OrderCharger(customer, orderRules, customerAccountRules, service, ruleService, context, help);
    }

    /**
     * Creates an {@link OrderCharger}.
     *
     * @param customer the customer
     * @param patient  the patient. May be {@code null}
     * @param context  the context
     * @param help     the help context
     */
    public OrderCharger create(Party customer, Party patient, Context context, HelpContext help) {
        return new OrderCharger(customer, patient, orderRules, customerAccountRules, service, ruleService, context,
                                help);
    }

}
