/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.resource.i18n.Messages;

/**
 * Editor for <em>entity.laboratoryTest</em>.
 * <p/>
 * This prevents investigation types managed by laboratories from being assigned to manually constructed tests.
 *
 * @author Tim Anderson
 */
public class TestEditor extends AbstractIMObjectEditor {

    /**
     * Determines if the test is a laboratory provided test.
     */
    private final boolean provided;

    /**
     * Test code node.
     */
    private static final String CODE = "code";

    /**
     * Investigation type node.
     */
    private static final String INVESTIGATION_TYPE = "investigationType";

    /**
     * Investigation type id node.
     */
    private static final String TYPE_ID = "typeId";

    /**
     * Constructs a {@link TestEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public TestEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        provided = !getCollectionProperty(CODE).isEmpty();
    }

    /**
     * Returns the investigation type.
     *
     * @return the investigation type. May be {@code null}
     */
    public Entity getInvestigationType() {
        return (Entity) getTarget(INVESTIGATION_TYPE);
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateInvestigationType(validator);
    }

    /**
     * Ensures that user-created tests cannot be submitted to laboratory services by preventing laboratory service
     * provided investigation types being assigned to them.
     *
     * @param validator the validator
     * @return {@code true} if the investigation type is valid
     */
    private boolean validateInvestigationType(Validator validator) {
        if (!provided) {
            Entity investigationType = getInvestigationType();
            if (investigationType != null) {
                IMObjectBean bean = getBean(investigationType);
                if (bean.getObject(TYPE_ID) != null) {
                    validator.add(this, Messages.format("test.invalidInvestigationType", investigationType.getName()));
                }
            }
        }
        return validator.isValid();
    }
}
