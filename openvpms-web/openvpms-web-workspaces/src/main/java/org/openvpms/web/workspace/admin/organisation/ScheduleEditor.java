/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2015 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Date;

/**
 * An editor for <em>party.organisationSchedule</em>.
 * <p>
 * This defaults the location to the current practice location for new schedules.
 *
 * @author Tim Anderson
 */
public class ScheduleEditor extends AbstractIMObjectEditor {

    /**
     * Constructs a {@link ScheduleEditor}.
     *
     * @param object        the object to edit
     * @param parent        the parent object. May be {@code null}
     * @param layoutContext the layout context
     */
    public ScheduleEditor(Entity object, IMObject parent, LayoutContext layoutContext) {
        super(object, parent, layoutContext);
        if (object.isNew()) {
            Party location = layoutContext.getContext().getLocation();
            if (location != null) {
                IMObjectBean bean = getBean(object);
                bean.setTarget("location", location);
            }
        }
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        return super.doValidation(validator) && validateTimes(validator);
    }

    /**
     * Verifies that the Start Time and End Time are set correctly.
     *
     * @param validator the validator
     * @return {@code true} if the times are valid
     */
    private boolean validateTimes(Validator validator) {
        boolean valid = false;
        Date startTime = getProperty("startTime").getDate();
        Date endTime = getProperty("endTime").getDate();
        if ((startTime == null && endTime != null) || (startTime != null && endTime == null)) {
            validator.add(this, new ValidatorError(Messages.get("schedule.time.bothRequired")));
        } else if (startTime != null && DateRules.compareTo(startTime, endTime) >= 0) {
            validator.add(this, new ValidatorError(Messages.get("schedule.time.less")));
        } else {
            valid = true;
        }
        return valid;
    }
}
