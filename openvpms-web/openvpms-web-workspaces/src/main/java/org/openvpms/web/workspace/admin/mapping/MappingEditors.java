/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.event.TableModelEvent;
import nextapp.echo2.app.event.TableModelListener;
import nextapp.echo2.app.table.TableColumn;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.component.property.AbstractSaveableEditor;
import org.openvpms.web.component.property.ModifiableListener;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.property.ValidatorError;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.table.DefaultTableCellRenderer;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

/**
 * Editor for {@link Mappings}.
 *
 * @author Tim Anderson
 */
public class MappingEditors<T extends IMObject> extends AbstractSaveableEditor {

    /**
     * The mappings.
     */
    private final Mappings<T> mappings;

    /**
     * The layout context.
     */
    private final LayoutContext context;

    /**
     * The mapping table.
     */
    private PagedIMTable<Mapping> table;

    /**
     * The component.
     */
    private Component component;

    /**
     * The mapping editors. These are recreated on each page display.
     */
    private MappingEditor<?>[] editors;

    /**
     * Constructs a {@link MappingEditors}.
     *
     * @param mappings the mappings to edit
     * @param context  the layout context
     */
    public MappingEditors(Mappings<T> mappings, LayoutContext context) {
        this.mappings = mappings;
        this.context = context;
    }

    /**
     * Returns the edit component.
     *
     * @return the edit component
     */
    @Override
    public Component getComponent() {
        if (component == null) {
            table = new PagedIMTable<>(new MappingEditorTableModel<>(mappings, context));
            style(table.getTable());
            refreshTable();
            component = ColumnFactory.create(Styles.INSET, table.getComponent());
            table.getTable().getModel().addTableModelListener((TableModelListener) event -> {
                if (event.getType() == TableModelEvent.UPDATE && event.getFirstRow() == event.getLastRow()) {
                    setModified();
                    refreshTable();
                }
            });
        }
        return component;
    }

    /**
     * Save any edits.
     */
    @Override
    protected void doSave() {
        mappings.save();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    protected boolean doValidation(Validator validator) {
        boolean valid = true;
        if (editors != null) {
            for (MappingEditor<?> editor : editors) {
                if (editor != null && !editor.validate(validator)) {
                    valid = false;
                    break;
                }
            }
            if (valid) {
                for (Mapping mapping : mappings.getMappings()) {
                    Target target = mapping.getTarget();
                    if (mapping.getSource() != null && target != null) {
                        if (mappings.getTarget(target.getId()) == null) {
                            String message = Messages.format("mapping.targetnotfound", target.getName(),
                                                             mappings.getTargetDisplayName());
                            validator.add(this, new ValidatorError(message));
                            valid = false;
                            break;
                        }
                    }
                }
            }
        }
        return valid;
    }

    private void style(Table table) {
        table.setStyleName("list");
        table.setDefaultRenderer(Object.class, DefaultTableCellRenderer.INSTANCE);
        table.setRolloverEnabled(false);
        table.setSelectionEnabled(false);
    }

    /**
     * Refreshes the table.
     */
    private void refreshTable() {
        int page = table.getModel().getPage();
        table.setResultSet(new MappingResultSet(null, 20, mappings));
        if (page != 0) {
            table.getNavigator().setPage(page);
        }
    }

    private class MappingEditorTableModel<O extends IMObject> extends MappingTableModel<O> {

        private final LayoutContext context;

        MappingEditorTableModel(Mappings<O> mappings, LayoutContext context) {
            super((mappings));
            this.context = context;
        }

        /**
         * Sets the objects to display.
         *
         * @param objects the objects to display
         */
        @Override
        public void setObjects(List<Mapping> objects) {
            super.setObjects(objects);
            editors = new MappingEditor<?>[objects.size()];
        }

        /**
         * Returns the sort criteria.
         *
         * @param column    the primary sort column
         * @param ascending if {@code true} sort in ascending order; otherwise sort in {@code descending} order
         * @return the sort criteria, or {@code null} if the column isn't sortable
         */
        @Override
        public SortConstraint[] getSortConstraints(int column, boolean ascending) {
            return null;
        }

        /**
         * Returns the value found at the given coordinate within the table.
         *
         * @param object the object
         * @param column the column
         * @param row    the row
         * @return the value at the given coordinate.
         */
        @Override
        protected Object getValue(Mapping object, TableColumn column, int row) {
            MappingEditor<?> editor = editors[row];
            if (editor == null) {
                MappingEditor<?> newEditor = new MappingEditor<>((MappingState) object, mappings, context);
                ModifiableListener listener = modifiable -> {
                    if (newEditor.isValid()) {
                        // only update the table if the input is valid, otherwise it will be discarded
                        // NOTE - this falls down if multiple rows are invalid
                        fireTableRowsUpdated(row, row);
                    }
                };
                newEditor.addModifiableListener(listener);
                editors[row] = newEditor;
                editor = newEditor;
            }
            if (column.getModelIndex() == SOURCE_INDEX) {
                return editor.getSource().getComponent();
            }
            return editor.getTarget().getComponent();
        }
    }
}
