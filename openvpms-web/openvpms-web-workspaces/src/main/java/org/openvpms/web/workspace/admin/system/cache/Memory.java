/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.cache;

import nextapp.echo2.app.Alignment;
import nextapp.echo2.app.Component;
import org.openvpms.web.component.bound.BoundTextComponentFactory;
import org.openvpms.web.component.im.layout.ComponentGrid;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;
import org.openvpms.web.echo.text.TextField;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.NumberFormatter;

/**
 * Helper to display memory statistics.
 *
 * @author Tim Anderson
 */
public class Memory {

    /**
     * Determines if the current allocation should be displayed.
     */
    private final boolean showAllocated;

    /**
     * The total amount of memory available to the JVM.
     */
    private final SimpleProperty totalMemory = new SimpleProperty("totalmemory", null, String.class,
                                                                  Messages.get("admin.system.cache.totalmemory"), true);

    /**
     * The free memory.
     */
    private final SimpleProperty freeMemory = new SimpleProperty("freememory", null, String.class,
                                                                 Messages.get("admin.system.cache.freememory"), true);

    /**
     * Memory use.
     */
    private final SimpleProperty memoryUse = new SimpleProperty("memoryuse", null, String.class,
                                                                Messages.get("admin.system.cache.memoryuse"), true);


    /**
     * Allocated total.
     */
    private final SimpleProperty allocatedTotalMemory
            = new SimpleProperty("allocatedtotal", null, String.class,
                                 Messages.get("admin.system.cache.allocatedtotal"), true);

    /**
     * Allocated free.
     */
    private final SimpleProperty allocatedFreeMemory
            = new SimpleProperty("allocatedtotal", null, String.class, Messages.get("admin.system.cache.allocatedfree"),
                                 true);

    /**
     * Constructs a {@link Memory}.
     */
    public Memory() {
        this(false);
    }

    /**
     * Constructs a {@link Memory}.
     *
     * @param showAllocated if {@code true}, display the current allocation
     */
    public Memory(boolean showAllocated) {
        this.showAllocated = showAllocated;
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        TextField total = createField(totalMemory);
        TextField free = createField(freeMemory);
        TextField use = createField(memoryUse);
        TextField allocatedTotal = createField(allocatedTotalMemory);
        TextField allocatedFree = createField(allocatedFreeMemory);
        refresh();

        ComponentGrid grid = new ComponentGrid();
        grid.add(new ComponentState(total, totalMemory));
        grid.add(new ComponentState(free, freeMemory));
        grid.add(new ComponentState(use, memoryUse));
        if (showAllocated) {
            grid.add(new ComponentState(allocatedTotal, allocatedTotalMemory));
            grid.add(new ComponentState(allocatedFree, allocatedFreeMemory));
        }
        return grid.createGrid();
    }

    /**
     * Returns the total memory property.
     *
     * @return the total memory property
     */
    public Property getTotalMemory() {
        return totalMemory;
    }

    /**
     * Returns the free memory property.
     *
     * @return the free memory property
     */
    public Property getFreeMemory() {
        return freeMemory;
    }

    /**
     * Returns the memory use property.
     *
     * @return the memory use property
     */
    public Property getMemoryUse() {
        return memoryUse;
    }

    /**
     * Returns the allocated total property.
     * <p/>
     * This represents the memory currently allocated to the process
     *
     * @return the allocated total property
     */
    public Property getAllocatedTotal() {
        return allocatedTotalMemory;
    }

    /**
     * Returns the allocated free property.
     * <p/>
     * This represents the amount of allocated memory that is currently free
     *
     * @return the allocated free property
     */
    public Property getAllocatedFree() {
        return allocatedFreeMemory;
    }

    /**
     * Refreshes the properties.
     */
    public void refresh() {
        Runtime runtime = Runtime.getRuntime();
        long total = runtime.maxMemory();
        long allocatedTotal = runtime.totalMemory();
        long allocatedFree = runtime.freeMemory();
        long used = allocatedTotal - allocatedFree;
        long free = total - used;
        int percent = (total != 0) ? (int) Math.round(100.0 * used / total) : 0;

        totalMemory.setValue(NumberFormatter.getSize(total));
        freeMemory.setValue(NumberFormatter.getSize(free));
        memoryUse.setValue(percent + "%");
        allocatedTotalMemory.setValue(NumberFormatter.getSize(allocatedTotal));
        allocatedFreeMemory.setValue(NumberFormatter.getSize(allocatedFree));
    }

    /**
     * Creates a right aligned read-only text field for a property.
     *
     * @param property the property
     * @return the field
     */
    private TextField createField(Property property) {
        TextField field = BoundTextComponentFactory.create(property, 10);
        field.setAlignment(Alignment.ALIGN_RIGHT);
        field.setEnabled(false);
        return field;
    }

}
