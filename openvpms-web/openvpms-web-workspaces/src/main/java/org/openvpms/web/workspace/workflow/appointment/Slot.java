package org.openvpms.web.workspace.workflow.appointment;

import java.time.ZonedDateTime;

/**
 * Represents an appointment slot.
 *
 * @author Tim Anderson
 */
public class Slot {

    /**
     * The start time of the slot.
     */
    private final ZonedDateTime start;

    /**
     * The end time of the slot.
     */
    private final ZonedDateTime end;

    /**
     * Constructs {@link Slot}.
     *
     * @param start the start time of the slot
     * @param end   the end time of the slot
     */
    public Slot(ZonedDateTime start, ZonedDateTime end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Returns the start time of the slot.
     *
     * @return the start time of the slot
     */
    public ZonedDateTime getStart() {
        return start;
    }

    /**
     * Returns the end time of the slot.
     *
     * @return the end time of the slot
     */
    public ZonedDateTime getEnd() {
        return end;
    }
}
