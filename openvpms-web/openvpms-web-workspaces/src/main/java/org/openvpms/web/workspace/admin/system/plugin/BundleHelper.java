/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.plugin;

import org.openvpms.web.resource.i18n.Messages;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

/**
 * OSGi bundle helper.
 *
 * @author Tim Anderson
 */
public class BundleHelper {

    /**
     * Returns a bundle name.
     *
     * @param bundle the bundle
     * @return the bundle name
     */
    public static String getName(Bundle bundle) {
        String result = bundle.getHeaders().get(Constants.BUNDLE_NAME);
        if (result == null) {
            result = bundle.getSymbolicName();
        }
        return result;
    }

    /**
     * Returns a display name for a bundle state.
     *
     * @param bundle the bundle
     * @return the state name
     */
    public static String getState(Bundle bundle) {
        String result;
        switch (bundle.getState()) {
            case Bundle.UNINSTALLED:
                result = Messages.get("admin.system.plugin.status.uninstalled");
                break;
            case Bundle.INSTALLED:
                result = Messages.get("admin.system.plugin.status.installed");
                break;
            case Bundle.RESOLVED:
                result = Messages.get("admin.system.plugin.status.resolved");
                break;
            case Bundle.STARTING:
                result = Messages.get("admin.system.plugin.status.starting");
                break;
            case Bundle.STOPPING:
                result = Messages.get("admin.system.plugin.status.stopping");
                break;
            case Bundle.ACTIVE:
                result = Messages.get("admin.system.plugin.status.active");
                break;
            default:
                result = Integer.toString(bundle.getState());
        }
        return result;
    }
}
