/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.organisation.practice;

import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.common.Participation;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.im.edit.AbstractIMObjectEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.util.IMObjectCreator;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.subscription.SubscriptionHelper;
import org.openvpms.web.echo.servlet.ServletHelper;

/**
 * Editor for <em>party.organisationPractice</em>.
 * <p>
 * This adds a tab to manage the subscription.
 *
 * @author Tim Anderson
 */
public class PracticeEditor extends AbstractIMObjectEditor {

    /**
     * The subscription participation editor.
     */
    private final SubscriptionParticipationEditor subscriptionEditor;

    /**
     * Constructs a {@link PracticeEditor}.
     *
     * @param practice the practice to edit
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context.
     */
    public PracticeEditor(Party practice, IMObject parent, LayoutContext context) {
        super(practice, parent, context);

        Participation participation = SubscriptionHelper.getSubscriptionParticipation(practice, getService());
        if (participation == null) {
            participation = (Participation) IMObjectCreator.create("participation.subscription");
        }
        subscriptionEditor = new SubscriptionParticipationEditor(participation, practice, context);
        subscriptionEditor.setDeleteAct(true);

        Property baseUrl = getProperty("baseUrl");
        if (baseUrl.getString() == null) {
            baseUrl.setValue(ServletHelper.getContextURL());
        }

        addEditor(subscriptionEditor);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        return new PracticeLayoutStrategy(subscriptionEditor.getComponent(), subscriptionEditor.getFocusGroup());
    }

}
