/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.esci;

import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.basic.IssueDateType;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Wraps a {@link DocumentReferenceType} to provide equality support.
 *
 * @author Tim Anderson
 */
class DocumentReference {

    /**
     * Invoice document.
     */
    public static final String INVOICE = "Invoice";

    /**
     * The underlying reference.
     */
    private final DocumentReferenceType reference;

    /**
     * Constructs a {@link DocumentReference}.
     *
     * @param reference the document reference
     */
    public DocumentReference(DocumentReferenceType reference) {
        this.reference = reference;
    }

    /**
     * Returns the document identifier.
     *
     * @return the document identifier. May be {@code null}
     */
    public String getId() {
        return reference.getID() != null ? reference.getID().getValue() : null;
    }

    /**
     * Returns the document type.
     *
     * @return the document type. May be {@code null}
     */
    public String getDocumentType() {
        return reference.getDocumentType() != null ? reference.getDocumentType().getValue() : null;
    }

    /**
     * Returns the issue date.
     *
     * @return the issue date. May be {@code null}
     */
    public OffsetDateTime getIssueDate() {
        IssueDateType issueDate = reference.getIssueDate();
        return (issueDate != null && issueDate.getValue() != null) ?
               issueDate.getValue().toGregorianCalendar().toZonedDateTime().toOffsetDateTime() : null;
    }

    /**
     * Returns the underlying reference.
     *
     * @return the underlying reference
     */
    public DocumentReferenceType getReference() {
        return reference;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj instanceof DocumentReference) {
            DocumentReference other = (DocumentReference) obj;
            return Objects.equals(getId(), other.getId()) && Objects.equals(getDocumentType(), other.getDocumentType());
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDocumentType());
    }
}
