/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.workspace.workflow.appointment.view;

import org.openvpms.archetype.rules.workflow.ScheduleArchetypes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.Hint;
import org.openvpms.web.workspace.workflow.scheduling.view.ParticipantScheduleEventTableModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Table for <em>act.customerAppointment</em> acts.
 *
 * @author Tim Anderson
 */
public class ParticipantAppointmentTableModel extends ParticipantScheduleEventTableModel {

    /**
     * The nodes to display.
     */
    private final String[] nodes;

    /**
     * Reason node.
     */
    private static final String REASON = "reason";

    /**
     * Schedule node.
     */
    private static final String SCHEDULE = "schedule";

    /**
     * The default nodes to display.
     */
    private static final String[] DEFAULT_NODES = {REASON, STATUS, SCHEDULE, NOTES};

    /**
     * The nodes to display with patient.
     */
    private static final String[] NODES_WITH_PATIENT;

    static {
        List<String> list = new ArrayList<>(Arrays.asList(DEFAULT_NODES));
        list.add(0, PATIENT);
        NODES_WITH_PATIENT = list.toArray(new String[0]);
    }

    /**
     * Constructs a {@link ParticipantAppointmentTableModel}.
     *
     * @param showPatient if {@code true}, display the patient column, else suppress it
     * @param context     the layout context
     */
    public ParticipantAppointmentTableModel(boolean showPatient, LayoutContext context) {
        super(context);
        nodes = (showPatient) ? NODES_WITH_PATIENT : DEFAULT_NODES;
        setTableColumnModel(createColumnModel(new String[]{ScheduleArchetypes.APPOINTMENT}, getLayoutContext()));
        getColumn(NOTES).setHint(Hint.multiline());
    }

    /**
     * Returns a list of node descriptor names to include in the table.
     *
     * @return the list of node descriptor names to include in the table
     */
    @Override
    protected String[] getNodeNames() {
        return nodes;
    }
}
