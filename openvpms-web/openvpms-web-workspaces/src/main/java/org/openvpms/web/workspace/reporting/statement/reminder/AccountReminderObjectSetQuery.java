/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.reporting.statement.reminder;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.reminder.ReminderItemStatus;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.SortConstraint;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.location.LocationSelectField;
import org.openvpms.web.component.im.lookup.LookupFilter;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.query.AbstractArchetypeServiceResultSet;
import org.openvpms.web.component.im.query.ActQuery;
import org.openvpms.web.component.im.query.ActStatuses;
import org.openvpms.web.component.im.query.DateRange;
import org.openvpms.web.component.im.query.ObjectSetQueryExecutor;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.resource.i18n.Messages;

import java.util.List;

import static org.openvpms.archetype.rules.finance.reminder.AccountReminderArchetypes.CHARGE_REMINDER_SMS;

/**
 * Queries <em>act.customerChargeReminder*</em> acts.
 *
 * @author Tim Anderson
 */
public class AccountReminderObjectSetQuery extends ActQuery<ObjectSet> {

    /**
     * The context.
     */
    private final Context context;

    /**
     * The date range.
     */
    private final DateRange dateRange;

    /**
     * The practice location.
     */
    private LocationSelectField location;

    /**
     * Dummy incomplete status. Finds all reminders that have PENDING or ERROR status.
     */
    private static final String INCOMPLETE = "INCOMPLETE";

    /**
     * Dummy incomplete status, used in the status selector.
     */
    private static final Lookup INCOMPLETE_STATUS
            = new org.openvpms.component.business.domain.im.lookup.Lookup(
            new ArchetypeId("lookup.local"), INCOMPLETE, Messages.get("reporting.statements.reminder.incomplete"));

    /**
     * The statuses to query.
     */
    private static final ActStatuses STATUSES = new ActStatuses(new StatusQuery());

    /**
     * Constructs an {@link AccountReminderObjectSetQuery}.
     *
     * @param context the context
     */
    public AccountReminderObjectSetQuery(Context context) {
        super(null, null, null, new String[]{CHARGE_REMINDER_SMS}, STATUSES, ObjectSet.class);
        this.context = context;
        dateRange = new DateRange();
        dateRange.getComponent();
        dateRange.setAllDates(true);
    }

    /**
     * Lays out the component in a container, and sets focus on the instance
     * name.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        addShortNameSelector(container);
        addStatusSelector(container);
        container.add(dateRange.getComponent());
        getFocusGroup().add(dateRange.getFocusGroup());
        addLocationSelector(container);
    }

    /**
     * Adds the location selector to a container.
     *
     * @param container the container
     */
    protected void addLocationSelector(Component container) {
        location = new LocationSelectField(context.getUser(), context.getPractice(), true);
        location.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                onQuery();
            }
        });
        Label label = LabelFactory.text(DescriptorHelper.getDisplayName(CustomerAccountArchetypes.INVOICE,
                                                                        "location", getService()));
        container.add(label);
        container.add(location);
        getFocusGroup().add(location);
    }


    /**
     * Creates the result set.
     *
     * @param sort the sort criteria. May be {@code null}
     * @return a new result set
     */
    @Override
    protected ResultSet<ObjectSet> createResultSet(SortConstraint[] sort) {
        ArchetypeQuery query = createQuery();
        return new AbstractArchetypeServiceResultSet<ObjectSet>(getMaxResults(), null, new ObjectSetQueryExecutor()) {
            @Override
            protected ArchetypeQuery createQuery() {
                return query;
            }
        };
    }

    /**
     * Returns the act statuses to query.
     *
     * @return the act statuses to query
     */
    @Override
    protected String[] getStatuses() {
        String[] result = super.getStatuses();
        Lookup selected = getStatusSelector().getSelected();
        if (selected == INCOMPLETE_STATUS) {
            result = new String[]{ReminderItemStatus.PENDING, ReminderItemStatus.ERROR};
        }
        return result;
    }


    /**
     * Creates the query.
     *
     * @return the query
     */
    private ArchetypeQuery createQuery() {
        AccountReminderQueryFactory factory = new AccountReminderQueryFactory();
        factory.setFrom(dateRange.getFrom());
        factory.setTo(dateRange.getTo());
        factory.setStatuses(getStatuses());
        factory.setLocation(location.getSelected());
        return factory.createQuery();
    }

    private static class StatusQuery extends LookupFilter {

        /**
         * Constructs a {@link StatusQuery}.
         */
        public StatusQuery() {
            super(new NodeLookupQuery(CHARGE_REMINDER_SMS, "status"), false);
        }

        /**
         * Returns the default lookup.
         *
         * @return {@link #INCOMPLETE_STATUS}
         */
        @Override
        public Lookup getDefault() {
            return INCOMPLETE_STATUS;
        }

        /**
         * Returns the lookups.
         *
         * @return the lookups
         */
        @Override
        public List<Lookup> getLookups() {
            List<Lookup> lookups = super.getLookups();
            lookups.add(0, INCOMPLETE_STATUS);
            return lookups;
        }
    }
}
