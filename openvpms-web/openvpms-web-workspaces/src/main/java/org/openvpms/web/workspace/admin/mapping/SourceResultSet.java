/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;


import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Source;

import java.util.List;

/**
 * A result set for sources of {@link Mappings}.
 *
 * @author Tim Anderson
 */
class SourceResultSet extends AbstractMappingResultSet<Source> {

    /**
     * The mappings.
     */
    private final Mappings<?> mappings;

    /**
     * Constructs an {@link SourceResultSet}.
     *
     * @param value    the value to filter on. May be {@code null}
     * @param pageSize the maximum no. of results per page
     * @param mappings the mappings
     */
    SourceResultSet(String value, int pageSize, Mappings<?> mappings) {
        super(value, pageSize);
        this.mappings = mappings;
    }

    /**
     * Returns the results matching the specified value.
     *
     * @param value       the value. May be {@code null}
     * @param firstResult the first result of the page to retrieve
     * @param maxResults  the maximum no. of results to retrieve
     * @return the matches
     */
    @Override
    protected List<Source> getMatches(String value, int firstResult, int maxResults) {
        return mappings.getSources(value, false, firstResult, maxResults);
    }

    /**
     * Counts the no. of results matching the query criteria.
     *
     * @param value the value to filter on. May be {@code null}
     * @return the total number of results
     */
    @Override
    protected int countResults(String value) {
        return (int) mappings.getSourceCount(value, false);
    }
}
