/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.vetcheck;

import org.openvpms.component.model.act.Act;

/**
 * VetCheck link.
 *
 * @author Tim Anderson
 */
public class VetCheckLink {

    /**
     * The link description.
     */
    private final String description;

    /**
     * The URL.
     */
    private final String url;

    /**
     * The link act.
     */
    private final Act act;

    /**
     * Constructs a {@link VetCheckLink}.
     *
     * @param description the link description
     * @param url         the URL
     * @param act         the link act
     */
    public VetCheckLink(String description, String url, Act act) {
        this.description = description;
        this.url = url;
        this.act = act;
    }

    /**
     * Returns the link description.
     *
     * @return the link description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the URL.
     *
     * @return the URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returns the link act.
     */
    public Act getAct() {
        return act;
    }
}