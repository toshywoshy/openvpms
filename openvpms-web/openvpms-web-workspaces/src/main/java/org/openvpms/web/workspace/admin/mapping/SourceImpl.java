/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.model.Source;

/**
 * Default implementation of {@link Source}.
 *
 * @author Tim Anderson
 */
class SourceImpl implements Source {

    /**
     * The reference.
     */
    private final Reference id;

    /**
     * The name.
     */
    private final String name;

    /**
     * Constructs a {@link SourceImpl}.
     *
     * @param id   the id
     * @param name the name
     */
    SourceImpl(Reference id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Returns the object identifier.
     *
     * @return the object identifier
     */
    @Override
    public Reference getId() {
        return id;
    }

    /**
     * Returns a display name for the object.
     *
     * @return a display name for the object
     */
    @Override
    public String getName() {
        return name;
    }
}
