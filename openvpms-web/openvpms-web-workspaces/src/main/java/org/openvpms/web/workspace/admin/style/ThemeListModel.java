/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.style;

import org.openvpms.web.component.im.list.ObjectListModel;
import org.openvpms.web.echo.style.Themes;

import java.util.ArrayList;

/**
 * List model for {@link Themes}.
 *
 * @author Tim Anderson
 */
public class ThemeListModel extends ObjectListModel<String> {

    /**
     * Constructs a {@link ThemeListModel}.
     *
     * @param themes the themes
     */
    public ThemeListModel(Themes themes) {
        super(new ArrayList<>(themes.getThemes().keySet()), false, false);
    }
}
