/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.laboratory;

import nextapp.echo2.app.event.ActionEvent;
import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.domain.internal.sync.Change;
import org.openvpms.domain.internal.sync.DefaultChanges;
import org.openvpms.laboratory.service.LaboratoryService;
import org.openvpms.laboratory.service.LaboratoryServices;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.im.archetype.Archetypes;
import org.openvpms.web.component.im.query.Query;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.workspace.ResultSetCRUDWindow;
import org.openvpms.web.echo.button.ButtonSet;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.openvpms.web.workspace.admin.sync.SynchronisationChanges;

import java.util.List;

/**
 * CRUD window for the Laboratories workspace.
 *
 * @author Tim Anderson
 */
public class LaboratoryCRUDWindow extends ResultSetCRUDWindow<Entity> {


    /**
     * Synchronise button identifier.
     */
    private static final String SYNC_DATA = "button.sync";

    /**
     * Constructs a {@link LaboratoryCRUDWindow}.
     *
     * @param archetypes the archetypes that this may create instances of
     * @param query      the query. May be {@code null}
     * @param set        the result set. May be {@code null}
     * @param context    the context
     * @param help       the help context
     */
    public LaboratoryCRUDWindow(Archetypes<Entity> archetypes, Query<Entity> query, ResultSet<Entity> set,
                                Context context, HelpContext help) {
        super(archetypes, query, set, context, help);
    }

    /**
     * Lays out the buttons.
     *
     * @param buttons the button row
     */
    @Override
    protected void layoutButtons(ButtonSet buttons) {
        super.layoutButtons(buttons);
        buttons.add(SYNC_DATA, new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                synchroniseData();
            }
        });
    }

    /**
     * Enables/disables the buttons that require an object to be selected.
     *
     * @param buttons the button set
     * @param enable  determines if buttons should be enabled
     */
    @Override
    protected void enableButtons(ButtonSet buttons, boolean enable) {
        super.enableButtons(buttons, enable);
        buttons.setEnabled(SYNC_DATA, enable && TypeHelper.isA(getObject(), LaboratoryArchetypes.LABORATORY_SERVICES));
    }

    /**
     * Synchronises service data.
     */
    private void synchroniseData() {
        Entity object = getObject();
        if (TypeHelper.isA(object, LaboratoryArchetypes.LABORATORY_SERVICES)) {
            LaboratoryServices services = ServiceHelper.getBean(LaboratoryServices.class);
            LaboratoryService service = services.getService(object);
            DefaultChanges<org.openvpms.component.model.entity.Entity> changes = new DefaultChanges<>();
            service.synchroniseData(changes);
            List<Change<org.openvpms.component.model.entity.Entity>> list = changes.getChanges();
            if (list.isEmpty()) {
                InformationDialog.show(Messages.get("admin.laboratory.sync.title"),
                                       Messages.format("admin.laboratory.sync.nochanges", object.getName()));
            } else {
                SynchronisationChanges<org.openvpms.component.model.entity.Entity> popup
                        = new SynchronisationChanges<>(Messages.get("admin.laboratory.sync.title"), list);
                popup.show();
            }
        }
    }

}
