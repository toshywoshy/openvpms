/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.Table;
import nextapp.echo2.app.table.DefaultTableModel;
import nextapp.echo2.app.table.TableModel;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.version.Version;
import org.openvpms.web.echo.factory.TableFactory;
import org.openvpms.web.echo.table.DefaultTableHeaderRenderer;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Displays system properties.
 *
 * @author Tim Anderson
 */
class PropertiesViewer extends AbstractDiagnosticTab {

    /**
     * A snapshot of the properties. Once collected, these are very unlikely to change.
     */
    private String[][] snapshot;

    /**
     * The columns names.
     */
    private static final String[] COLUMNS = new String[]{"Name", "Value"};

    /**
     * Constructs a {@link PropertiesViewer}.
     */
    PropertiesViewer() {
        super("admin.system.diagnostic.property");
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        String[][] data = getData(false);
        if (data != null) {
            result = toCSV("properties.csv", COLUMNS, data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component result = null;
        String[][] data = getData(true);
        if (data != null) {
            TableModel model = new DefaultTableModel(data, COLUMNS);
            Table table = TableFactory.create(model);
            table.setDefaultHeaderRenderer(DefaultTableHeaderRenderer.DEFAULT);
            result = table;
        }
        return result;
    }

    /**
     * Returns the properties.
     *
     * @param refresh if {@code true}, refresh the data if it has been collected previously
     * @return the property data
     */
    private String[][] getData(boolean refresh) {
        if (snapshot == null || refresh) {
            Properties properties = System.getProperties();
            Map<String, String> map = new TreeMap<>();
            for (String name : properties.stringPropertyNames()) {
                map.put(name, properties.getProperty(name));
            }
            snapshot = new String[map.size() + 1][2];
            snapshot[0][0] = "openvpms.version";
            snapshot[0][1] = Version.VERSION + " (" + Version.REVISION + ")";
            int i = 1;
            for (Map.Entry<String, String> next : map.entrySet()) {
                snapshot[i][0] = next.getKey();
                snapshot[i][1] = next.getValue();
                ++i;
            }
        }
        return snapshot;
    }
}
