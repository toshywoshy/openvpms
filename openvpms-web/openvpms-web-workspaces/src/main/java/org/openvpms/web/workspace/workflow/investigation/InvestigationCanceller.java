/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.investigation;

import org.openvpms.archetype.rules.patient.PatientRules;
import org.openvpms.archetype.rules.workflow.WorkflowStatus;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.component.system.common.cache.SoftRefIMObjectCache;
import org.openvpms.web.workspace.customer.charge.OrderPlacer;
import org.openvpms.web.workspace.customer.charge.OrderServices;

import java.util.Set;

/**
 * Cancels investigations.
 *
 * @author Tim Anderson
 */
public class InvestigationCanceller {

    /**
     * The order placer.
     */
    private final OrderPlacer placer;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The investigation bean.
     */
    private final IMObjectBean bean;

    /**
     * Constructs a {@link InvestigationCanceller}.
     *
     * @param investigation the investigation
     * @param user          the user responsible for the orders
     * @param practice      the practice
     * @param services      the order services
     * @param service       the archetype service
     */
    public InvestigationCanceller(Act investigation, User user, Party practice, OrderServices services,
                                  PatientRules rules, IArchetypeService service) {
        bean = service.getBean(investigation);

        IMObjectCache cache = new SoftRefIMObjectCache(service);
        Entity laboratory = (Entity) cache.get(bean.getTargetRef("laboratory"));
        if (laboratory != null) {
            Party patient = (Party) cache.get(bean.getTargetRef("patient"));
            Party location = (Party) cache.get(bean.getTargetRef("location"));
            if (patient == null) {
                throw new IllegalStateException("Cannot determine patient from investigation");
            }
            if (location == null) {
                throw new IllegalStateException("Cannot determine location from investigation");
            }
            Party customer = rules.getOwner(patient, investigation.getActivityStartTime(), false);
            if (customer == null) {
                throw new IllegalStateException("Cannot determine customer from investigation");
            }
            placer = new OrderPlacer(customer, location, user, practice, cache, services, service);
            placer.initialise(investigation);
        } else {
            // investigation not associated with a laboratory, so cancel by setting status
            placer = null;
        }
        this.service = service;
    }

    /**
     * Determines if the investigation has been charged.
     *
     * @return {@code true} if the investigation has been charged
     */
    public boolean isCharged() {
        return bean.getSourceRef("invoiceItems") != null;
    }

    /**
     * Cancels the investigation.
     *
     * @return {@code true} if the investigation was cancelled
     */
    public boolean cancel() {
        boolean result = false;
        if (placer != null) {
            // ordered via a laboratory
            Set<Act> updated = placer.cancel();
            if (!updated.isEmpty()) {
                service.save(updated);
                result = true;
            }
        } else {
            if (!WorkflowStatus.CANCELLED.equals(bean.getString("status"))) {
                bean.setValue("status", WorkflowStatus.CANCELLED);
                bean.save();
            }
        }
        return result;
    }
}