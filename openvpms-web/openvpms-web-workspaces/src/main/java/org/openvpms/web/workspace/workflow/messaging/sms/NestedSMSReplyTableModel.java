/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.messaging.sms;

import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;

/**
 * Table model for <em>act.smsReply</em> when the replies are nested in a parent <em>act.smsMessage</em>
 *
 * @author Tim Anderson
 */
public class NestedSMSReplyTableModel extends DefaultDescriptorTableModel<Act> {

    /**
     * Constructs an {@link NestedSMSReplyTableModel}.
     *
     * @param context the layout context
     */
    public NestedSMSReplyTableModel(LayoutContext context) {
        super(SMSArchetypes.REPLY, context, "startTime", "message");
    }

}
