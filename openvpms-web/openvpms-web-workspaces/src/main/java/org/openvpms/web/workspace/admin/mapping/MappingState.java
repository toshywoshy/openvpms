/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Target;

/**
 * Tracks the state of a {@link Mapping} being edited.
 *
 * @author Tim Anderson
 */
class MappingState implements Mapping {

    /**
     * The mapping source.
     */
    private final Reference source;

    /**
     * The mapping target.
     */
    private final Target target;

    /**
     * The mapping.
     */
    private Mapping mapping;

    /**
     * Constructs a {@link MappingState}.
     *
     * @param source  the mapping source
     * @param target  the mapping target
     * @param mapping the mapping
     */
    MappingState(Reference source, Target target, Mapping mapping) {
        this.source = source;
        this.target = target;
        this.mapping = mapping;
    }

    /**
     * Returns the identifier of the source object.
     *
     * @return the source object identifier
     */
    @Override
    public Reference getSource() {
        return source;
    }

    /**
     * Returns the target object.
     *
     * @return the target object
     */
    @Override
    public Target getTarget() {
        return target;
    }

    /**
     * Returns the mapping.
     *
     * @return the mapping
     */
    public Mapping getMapping() {
        return mapping;
    }

    /**
     * Sets the mapping.
     *
     * @param mapping the mapping. May be {@code null}
     */
    public void setMapping(Mapping mapping) {
        this.mapping = mapping;
    }
}
