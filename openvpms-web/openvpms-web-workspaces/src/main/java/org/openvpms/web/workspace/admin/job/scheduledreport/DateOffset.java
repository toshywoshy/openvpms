/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.scheduledreport;

import org.openvpms.archetype.function.date.RelativeDateParser;
import org.openvpms.archetype.rules.util.DateUnits;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Date offset used for relative date parameters.
 * This uses a subset of the {@link RelativeDateParser} tokens, i.e. {@code [-](0..9)+(d|m|w|y)}
 *
 * @author Tim Anderson
 */
class DateOffset {

    /**
     * The offset.
     */
    private final int offset;

    /**
     * The date units.
     */
    private final DateUnits units;

    /**
     * Constructs a {@link DateOffset}.
     *
     * @param offset the offset
     * @param units  the units
     */
    public DateOffset(int offset, DateUnits units) {
        this.offset = offset;
        this.units = units;
    }

    /**
     * Returns the offset.
     *
     * @return the offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Returns the date units.
     *
     * @return the date units
     */
    public DateUnits getUnits() {
        return units;
    }

    /**
     * Parses a date offset from a string.
     *
     * @param value the value to parse
     * @return the date offset, or {@code null} if the value is invalid
     */
    public static DateOffset parse(String value) {
        DateOffset result = null;
        int offset = 0;
        DateUnits units = null;
        if (value != null) {
            Pattern pattern = Pattern.compile("([+-]?\\d+)([dmwy])");
            Matcher matcher = pattern.matcher(value);
            if (matcher.matches()) {
                String valueGroup = matcher.group(1);
                offset = Integer.parseInt(valueGroup);
                String type = matcher.group(2);
                if ("d".equals(type)) {
                    units = DateUnits.DAYS;
                } else if ("w".equals(type)) {
                    units = DateUnits.WEEKS;
                } else if ("m".equals(type)) {
                    units = DateUnits.MONTHS;
                } else if ("y".equals(type)) {
                    units = DateUnits.YEARS;
                }
            }
            if (units != null) {
                result = new DateOffset(offset, units);
            }
        }
        return result;
    }
}
