/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import au.com.bytecode.opencsv.CSVWriter;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.SplitPane;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.TemporaryDocumentHandler;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.openvpms.archetype.csv.AbstractCSVReader.MIME_TYPE;
import static org.openvpms.report.DocFormats.TEXT_TYPE;

/**
 * Abstract implementation of {@link DiagnosticTab}.
 *
 * @author Tim Anderson
 */
abstract class AbstractDiagnosticTab implements DiagnosticTab {

    /**
     * The tab display name.
     */
    private final String name;

    /**
     * The tab component.
     */
    private Component component;

    /**
     * Constructs an {@link AbstractDiagnosticTab}.
     *
     * @param name the tab display name key
     */
    AbstractDiagnosticTab(String name) {
        this.name = Messages.get(name);
    }

    /**
     * Returns the tab display name.
     *
     * @return the tab display name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Returns the tab component
     *
     * @return the tab component
     */
    @Override
    public Component getComponent() {
        if (component == null) {
            refresh();
        }
        return component;
    }

    /**
     * Refreshes the display.
     */
    @Override
    public void refresh() {
        Component content = getContent();
        if (!(content instanceof SplitPane)) {
            component = ColumnFactory.create(Styles.LARGE_INSET, content);
        } else {
            component = content;
        }
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    protected abstract Component getContent();

    /**
     * Converts a byte array to a document.
     *
     * @param fileName the file name
     * @param content  the content
     * @param length   the uncompressed document size
     * @param mimeType the mime type
     * @return the document
     */
    protected Document toDocument(String fileName, byte[] content, int length, String mimeType) {
        DocumentHandler handler = new TemporaryDocumentHandler(ServiceHelper.getArchetypeService());
        return handler.create(fileName, content, mimeType, length);
    }

    /**
     * Creates a CSV document.
     *
     * @param fileName the file name
     * @param columns  the column names
     * @param data     the data
     * @return a new CSV document
     */
    Document toCSV(String fileName, String[] columns, String[][] data) {
        StringWriter writer = new StringWriter();
        CSVWriter csv = new CSVWriter(writer);
        csv.writeNext(columns);
        for (String[] entry : data) {
            csv.writeNext(entry);
        }
        return toCSV(fileName, writer);
    }

    /**
     * Creates a CSV document.
     *
     * @param fileName the file name
     * @param columns  the column names
     * @param data     the data
     * @return a new CSV document
     */
    Document toCSV(String fileName, String[] columns, List<String[]> data) {
        StringWriter writer = new StringWriter();
        CSVWriter csv = new CSVWriter(writer);
        csv.writeNext(columns);
        csv.writeAll(data);
        return toCSV(fileName, writer);
    }

    /**
     * Converts a string to a text document.
     *
     * @param fileName the file name
     * @param text     the text
     * @return the document
     */
    Document toText(String fileName, String text) {
        if (text == null) {
            text = "";
        }
        return toDocument(fileName, text, TEXT_TYPE);
    }

    /**
     * Converts a {@code StringWriter} to a CSV document.
     *
     * @param fileName the file name
     * @param writer   the writer
     * @return the document
     */
    private Document toCSV(String fileName, StringWriter writer) {
        String string = writer.getBuffer().toString();
        return toDocument(fileName, string, MIME_TYPE);
    }

    /**
     * Converts a string to a document.
     *
     * @param fileName the file name
     * @param text     the text
     * @param mimeType the mime type
     * @return the document
     */
    private Document toDocument(String fileName, String text, String mimeType) {
        byte[] buffer = text.getBytes(StandardCharsets.UTF_8);
        return toDocument(fileName, buffer, buffer.length, mimeType);
    }
}
