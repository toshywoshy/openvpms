/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.job.scheduledreport;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.report.ParameterType;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.PropertySet;
import org.openvpms.web.component.property.SimpleProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Report parameters.
 *
 * @author Tim Anderson
 */
public class Parameters {

    /**
     * Parameter name node prefix.
     */
    public static final String PARAM_NAME = "paramName";

    /**
     * Parameter display name node prefix.
     */
    public static final String PARAM_DISPLAY_NAME = "paramDisplayName";

    /**
     * Parameter type node prefix.
     */
    public static final String PARAM_TYPE = "paramType";

    /**
     * Parameter expression type node prefix.
     */
    public static final String PARAM_EXPR_TYPE = "paramExprType";

    /**
     * Parameter value node prefix.
     */
    public static final String PARAM_VALUE = "paramValue";

    /**
     * The properties.
     */
    private final PropertySet properties;

    /**
     * The maximum number of supported parameters.
     */
    private int maxParameters;

    /**
     * The parameters requested/in use.
     */
    private int parameterCount;


    /**
     * Constructs a {@link Parameters}.
     *
     * @param properties the properties
     */
    public Parameters(PropertySet properties) {
        this.properties = properties;
        // determine the number of report parameters in use
        int paramIndex = 0;
        parameterCount = 0;
        Property property;
        while ((property = getParamName(paramIndex)) != null) {
            paramIndex++;
            if (!StringUtils.isEmpty(property.getName())) {
                ++parameterCount;
            }
        }
        maxParameters = paramIndex;
    }


    /**
     * Returns a parameter name property, given its index.
     *
     * @param index the index
     * @return property, or {@code null} if none is found
     */
    public Property getParamName(int index) {
        return properties.get(PARAM_NAME + index);
    }

    /**
     * Returns a parameter display name property, given its index.
     *
     * @param index the index
     * @return property, or {@code null} if none is found
     */
    public Property getParamDisplayName(int index) {
        return properties.get(PARAM_DISPLAY_NAME + index);
    }

    /**
     * Returns a parameter type property, given its index.
     *
     * @param index the index
     * @return property, or {@code null} if none is found
     */
    public Property getParamType(int index) {
        return properties.get(PARAM_TYPE + index);
    }

    /**
     * Returns a parameter expression type property, given its index.
     *
     * @param index the index
     * @return property, or {@code null} if none is found
     */
    public Property getParamExpressionType(int index) {
        return properties.get(PARAM_EXPR_TYPE + index);
    }

    /**
     * Returns a parameter value property, given its index.
     *
     * @param index the index
     * @return property, or {@code null} if none is found
     */
    public Property getParamValue(int index) {
        return properties.get(PARAM_VALUE + index);
    }

    /**
     * Sets the parameter with the given index to that specified.
     *
     * @param index the parameter index
     * @param type  the parameter type
     */
    public void setParameter(int index, ParameterType type) {
        setParameter(index, type.getName(), type.getDescription(), type.getType().getName(), type.getDefaultValue());
    }

    /**
     * Sets the parameter with the given index to that specified.
     *
     * @param index       the parameter index
     * @param name        the parameter name. May be {@code null}
     * @param displayName the parameter display name. May be {@code null}
     * @param type        the parameter type. May be {@code null}
     * @param value       the parameter value. May be {@code null}
     */
    public void setParameter(int index, String name, String displayName, String type, Object value) {
        Property paramName = getParamName(index);
        Property paramDisplayName = getParamDisplayName(index);
        Property paramType = getParamType(index);
        Property paramExpressionType = getParamExpressionType(index);
        Property paramValue = getParamValue(index);
        paramName.setValue(name);
        paramType.setValue(type);
        paramDisplayName.setValue(displayName);
        paramExpressionType.setValue(null);
        paramValue.setValue(value);
    }

    /**
     * Returns the parameter count.
     *
     * @return the parameter count
     */
    public int getParameterCount() {
        return parameterCount;
    }

    /**
     * Returns the maximum number of parameters that can be supported.
     *
     * @return the maximum number of parameters that can be supported
     */
    public int getMaxParameters() {
        return maxParameters;
    }

    /**
     * Updates the parameters.
     *
     * @param parameterTypes the parameter types
     */
    public void update(Set<ParameterType> parameterTypes) {
        List<ParameterType> list = new ArrayList<>();
        for (ParameterType type : parameterTypes) {
            if (isSupportedParameter(type)) {
                list.add(type);
            }
        }
        parameterCount = list.size();
        int i = 0;
        for (; i < list.size() && i < maxParameters; ++i) {
            ParameterType type = list.get(i);
            setParameter(i, type);
        }
        for (; i < maxParameters; ++i) {
            setParameter(i, null, null, null, null);
        }
    }

    /**
     * Determines if a parameter is supported.
     *
     * @param parameterType the parameter type
     * @return {@code true} if the parameter type is supported
     */
    private boolean isSupportedParameter(ParameterType parameterType) {
        if (!parameterType.isSystem()) {
            SimpleProperty dummy = new SimpleProperty("dummy", parameterType.getType());
            return dummy.isString() || dummy.isBoolean() || dummy.isNumeric() || dummy.isDate();
        }
        return false;
    }

}
