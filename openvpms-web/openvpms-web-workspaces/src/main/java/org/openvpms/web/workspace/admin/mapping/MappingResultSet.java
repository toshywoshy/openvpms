/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import org.openvpms.component.model.object.Reference;
import org.openvpms.mapping.model.Cardinality;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Source;
import org.openvpms.mapping.model.Target;
import org.openvpms.web.component.im.query.ResultSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A {@link ResultSet} for {@link Mapping} instances.
 * <p/>
 * For mappings with {@link Cardinality#ONE_TO_ONE 1:1} cardinality, this includes all sources and targets, including
 * those that are unmapped.<br/>
 * For mappings with {@link Cardinality#MANY_TO_ONE N:1} cardinality, it includes all sources.
 *
 * @author Tim Anderson
 */
public class MappingResultSet extends AbstractMappingResultSet<Mapping> {

    /**
     * The mappings.
     */
    private final Mappings<?> mappings;

    /**
     * Cache of unmapped targets.
     */
    private List<Target> unmappedTargets;

    /**
     * Constructs a {@link MappingResultSet}.
     *
     * @param pageSize the maximum no. of results per page
     * @param mappings the mappings
     */
    MappingResultSet(String value, int pageSize, Mappings<?> mappings) {
        super(value, pageSize);
        this.mappings = mappings;
    }

    /**
     * Returns the results matching the specified value.
     *
     * @param value       the value. May be {@code null}
     * @param firstResult the first result of the page to retrieve
     * @param maxResults  the maximum no. of results to retrieve
     * @return the matches
     */
    @Override
    protected List<Mapping> getMatches(String value, int firstResult, int maxResults) {
        List<Mapping> result = new ArrayList<>();
        int size = 0;
        List<Target> unmapped = Collections.emptyList();
//        if (mappings.getCardinality() == Cardinality.ONE_TO_ONE) {
//            unmapped = getUnmapped(value);
//            size = unmapped.size();
//        } else {
//            unmapped = Collections.emptyList();
//        }
        if (firstResult < size) {
            for (int i = firstResult; i < size; ++i) {
                result.add(new MappingState(null, unmapped.get(i), null));
            }
            if (firstResult + maxResults > size) {
                int remainder = maxResults - size;
                getSources(value, 0, remainder, result);
            }
        } else {
            firstResult -= size;
            getSources(value, firstResult, maxResults, result);
        }
        return result;
    }

    /**
     * Counts the no. of results matching the query criteria.
     *
     * @param value the value to filter on. May be {@code null}
     * @return the total number of results
     */
    @Override
    protected int countResults(String value) {
        int count = (int) mappings.getSourceCount(value, false);
//        if (mappings.getCardinality() == Cardinality.ONE_TO_ONE) {
//            List<Target> unmapped = getUnmapped(value);
//            count += unmapped.size();
//        }
        return count;
    }

    /**
     * Returns the sources that match.
     *
     * @param value       the value to filter on. May be {@code null}
     * @param firstResult the first result
     * @param maxResults  the maximum no. of results to match
     * @param results     the collection to add results to
     */
    private void getSources(String value, int firstResult, int maxResults, List<Mapping> results) {
        for (Source source : mappings.getSources(value, false, firstResult, maxResults)) {
            Reference id = source.getId();
            Mapping mapping = mappings.getMapping(id);
            Target target = (mapping != null) ? mapping.getTarget() : null;
            results.add(new MappingState(id, target, mapping));
        }
    }

    /**
     * Collects the unmapped targets.
     *
     * @param value the value to filter on. May be {@code null}
     */
    private List<Target> getUnmapped(String value) {
        if (unmappedTargets == null) {
            unmappedTargets = mappings.getTargets(value, true, 0, -1);
        }
        return unmappedTargets;
    }

}
