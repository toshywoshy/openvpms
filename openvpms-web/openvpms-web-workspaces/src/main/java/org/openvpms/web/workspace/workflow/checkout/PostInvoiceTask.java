/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.checkout;

import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.edit.IMObjectEditor;
import org.openvpms.web.component.im.edit.IMObjectEditorSaver;
import org.openvpms.web.component.im.edit.act.ActEditor;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.workflow.EditIMObjectTask;
import org.openvpms.web.component.workflow.TaskContext;
import org.openvpms.web.echo.dialog.ErrorDialog;
import org.openvpms.web.resource.i18n.Messages;

import java.util.Date;

import static org.openvpms.web.workspace.workflow.checkout.CheckoutEditInvoiceTask.createProtectedLocationLayoutContext;

/**
 * Task to post an invoice.
 * <p>
 * This uses an editor to ensure that any HL7 Pharmacy Orders associated with the invoice are discontinued.
 * This is workaround for Cubex.
 *
 * @author Tim Anderson
 */
class PostInvoiceTask extends EditIMObjectTask {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Determines if this is the first time background editing. If {@code true}, save errors are suppressed.
     */
    private boolean first = true;

    /**
     * Constructs a {@link PostInvoiceTask}.
     *
     * @param service the archetype service
     */
    public PostInvoiceTask(ArchetypeService service) {
        super(CustomerAccountArchetypes.INVOICE, false, false);
        this.service = service;
    }

    /**
     * Edits an object in the background.
     *
     * @param editor  the editor
     * @param context the task context
     */
    @Override
    protected void edit(IMObjectEditor editor, TaskContext context) {
        ActEditor actEditor = (ActEditor) editor;
        actEditor.setStatus(ActStatus.POSTED);
        actEditor.setStartTime(new Date()); // for OVPMS-734 - TODO
    }

    /**
     * Shows the editor in an edit dialog.
     * <p/>
     * This is only invoked if the editor is invalid. For invoices, reload the invoice
     *
     * @param editor  the editor
     * @param context the task context
     */
    @Override
    protected void interactiveEdit(IMObjectEditor editor, TaskContext context) {
        super.interactiveEdit(editor, context);
    }

    /**
     * Saves an edit being performed in the background.
     * <p/>
     * This implementation suppresses any error dialogs if this is the first attempt to save.
     *
     * @param editor  the editor
     * @param context the task context
     */
    @Override
    protected void backgroundSave(IMObjectEditor editor, TaskContext context) {
        if (first) {
            IMObjectEditorSaver saver = new IMObjectEditorSaver();
            if (saver.save(editor, true)) {
                notifyCompleted();
            } else {
                backgroundEditFailed(editor, context);
            }
        } else {
            super.backgroundSave(editor, context);
        }
    }

    /**
     * Invoked when saving of the editor fails during a background edit.
     * <p/>
     * If this is the first time background editing has failed, this implementation reloads the invoice. If it:
     * <ul>
     *     <li>not present, it displays an error, and cancels the task</li>
     *     <li>present, and POSTED, it completes the task</li>
     *     <li>present, and not POSTED, it re-edits the invoice</li>
     * </ul>
     * If this is the second time, the workflow is cancelled.
     *
     * @param editor  the editor
     * @param context the task context
     */
    @Override
    protected void backgroundEditFailed(IMObjectEditor editor, TaskContext context) {
        if (first) {
            first = false;
            FinancialAct invoice = (FinancialAct) context.getObject(CustomerAccountArchetypes.INVOICE);
            if (invoice != null) {
                // get the latest instance
                invoice = service.get(invoice.getObjectReference(), FinancialAct.class);
            }
            if (invoice != null) {
                context.addObject(invoice);
                edit(context);
            } else {
                invoiceDeleted();
            }
        } else {
            notifyCancelled();
        }
    }

    /**
     * Creates an edit layout context.
     *
     * @param context the task context
     * @return a new edit layout context
     */
    @Override
    protected LayoutContext createLayoutContext(TaskContext context) {
        // make sure the invoice location doesn't change the current location
        return createProtectedLocationLayoutContext(context);
    }

    /**
     * Displays an error dialog if the invoice has been deleted, and invokes {@link #notifyCancelled()} when
     * the dialog is closed.
     */
    private void invoiceDeleted() {
        ErrorDialog.newDialog()
                .title(Messages.get("workflow.checkout.postinvoice.title"))
                .message(Messages.get("workflow.checkout.postinvoice.deleted"))
                .listener(this::notifyCancelled)
                .show();
    }
}
