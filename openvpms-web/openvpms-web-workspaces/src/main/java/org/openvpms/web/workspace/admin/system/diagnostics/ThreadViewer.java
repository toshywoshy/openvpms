/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.system.diagnostics;

import nextapp.echo2.app.Component;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.factory.LabelFactory;

import java.lang.management.LockInfo;
import java.lang.management.ManagementFactory;
import java.lang.management.MonitorInfo;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * Displays threads.
 *
 * @author Tim Anderson
 */
class ThreadViewer extends AbstractDiagnosticTab {

    /**
     * Snapshot of thread state.
     */
    private String snapshot;

    /**
     * Constructs a {@link ThreadViewer}.
     */
    ThreadViewer() {
        super("admin.system.diagnostic.thread");
    }

    /**
     * Returns a document containing the diagnostics.
     *
     * @return the document, or {@code null} if one cannot be created
     */
    @Override
    public Document getDocument() {
        Document result = null;
        String data = getData(false);
        if (data != null) {
            result = toText("threads.txt", data);
        }
        return result;
    }

    /**
     * Returns the diagnostic content.
     *
     * @return the diagnostic content, or {@code null} if it cannot be generated
     */
    @Override
    protected Component getContent() {
        Component result = null;
        String data = getData(true);
        if (data != null) {
            result = LabelFactory.preformatted(data);
        }
        return result;
    }

    /**
     * Returns the thread dump.
     *
     * @param refresh if {@code true}, refresh the dump if it has been collected previously
     * @return the thread dump
     */
    private String getData(boolean refresh) {
        if (snapshot == null || refresh) {
            try {
                ThreadMXBean bean = ManagementFactory.getThreadMXBean();
                ThreadInfo[] infos = bean.dumpAllThreads(true, true);
                StringBuilder builder = new StringBuilder();

                for (ThreadInfo info : infos) {
                    dump(info, builder);
                    builder.append(info);
                    builder.append('\n');
                }
                snapshot = builder.toString();
            } catch (Throwable exception) {
                snapshot = null;
                ErrorHelper.show(exception);
            }
        }
        return snapshot;
    }

    /**
     * Dumps the full stack trace of a {@code ThreadInfo}.
     * <p/>
     * This is largely copied from the {@code ThreadInfo#toString()} implementation without restricting the number
     * of frames.
     *
     * @param info the info to dump
     * @param sb   the string builder to populate
     */
    private void dump(ThreadInfo info, StringBuilder sb) {
        sb.append("\"").append(info.getThreadName()).append("\"").append(" Id=").append(info.getThreadId()).append(" ")
                .append(info.getThreadState());
        if (info.getLockName() != null) {
            sb.append(" on ").append(info.getLockName());
        }
        if (info.getLockOwnerName() != null) {
            sb.append(" owned by \"").append(info.getLockOwnerName()).append("\" Id=").append(info.getLockOwnerId());
        }
        if (info.isSuspended()) {
            sb.append(" (suspended)");
        }
        if (info.isInNative()) {
            sb.append(" (in native)");
        }
        sb.append('\n');
        int i = 0;
        for (; i < info.getStackTrace().length; i++) {
            StackTraceElement ste = info.getStackTrace()[i];
            sb.append("\tat ").append(ste.toString());
            sb.append('\n');
            if (i == 0 && info.getLockInfo() != null) {
                Thread.State ts = info.getThreadState();
                switch (ts) {
                    case BLOCKED:
                        sb.append("\t-  blocked on ").append(info.getLockInfo());
                        sb.append('\n');
                        break;
                    case WAITING:
                        sb.append("\t-  waiting on ").append(info.getLockInfo());
                        sb.append('\n');
                        break;
                    case TIMED_WAITING:
                        sb.append("\t-  waiting on ").append(info.getLockInfo());
                        sb.append('\n');
                        break;
                    default:
                }
            }

            for (MonitorInfo mi : info.getLockedMonitors()) {
                if (mi.getLockedStackDepth() == i) {
                    sb.append("\t-  locked ").append(mi);
                    sb.append('\n');
                }
            }
        }
        if (i < info.getStackTrace().length) {
            sb.append("\t...");
            sb.append('\n');
        }

        LockInfo[] locks = info.getLockedSynchronizers();
        if (locks.length > 0) {
            sb.append("\n\tNumber of locked synchronizers = ").append(locks.length);
            sb.append('\n');
            for (LockInfo li : locks) {
                sb.append("\t- ").append(li);
                sb.append('\n');
            }
        }
        sb.append('\n');
    }
}
