/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.patient.investigation;

import nextapp.echo2.app.Color;
import nextapp.echo2.app.Component;
import nextapp.echo2.app.Label;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.table.DefaultDescriptorTableModel;
import org.openvpms.web.component.im.table.DescriptorTableColumn;
import org.openvpms.web.component.property.DocumentBackedTextProperty;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.RowFactory;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.system.ServiceHelper;

/**
 * Table model for <em>act.patientInvestigationResultItem</em>.
 *
 * @author Tim Anderson
 */
public class ResultTableModel extends DefaultDescriptorTableModel<Act> {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Result node name.
     */
    private static final String RESULT = "result";

    /**
     * Long result relationship node name.
     */
    private static final String LONG_RESULT = "longResult";

    /**
     * Image relationship node name.
     */
    private static final String IMAGE = "image";

    /**
     * Out of range node name.
     */
    private static final String OUT_OF_RANGE = "outOfRange";

    /**
     * Constructs a {@link ResultTableModel}.
     *
     * @param shortNames the archetype short names. May contain wildcards
     * @param context    the layout context
     */
    public ResultTableModel(String[] shortNames, LayoutContext context) {
        super(shortNames, context, "analyteCode", "name", RESULT, "qualifier", "referenceRange");
        service = ServiceHelper.getArchetypeService();
    }

    /**
     * Returns a value for a given column.
     *
     * @param object the object to operate on
     * @param column the column
     * @param row    the row
     * @return the value for the column
     */
    @Override
    protected Object getValue(Act object, DescriptorTableColumn column, int row) {
        Object result;
        boolean outOfRange = service.getBean(object).getBoolean(OUT_OF_RANGE);
        if (column.getName().equals(RESULT)) {
            // if the result is multi-line or long, limit it to a single line of up to 50 characters
            result = getResult(object);
        } else {
            result = super.getValue(object, column, row);
        }
        if (outOfRange && result instanceof Component) {
            Component component = (Component) result;
            component.setForeground(Color.RED);
            component.setStyleName(Styles.BOLD);
        }
        return result;
    }

    /**
     * Returns a component representing the result, or {@code null} if there are none.
     * <p/>
     * This may include the result text and/or image if they are present.
     * <br/>
     * Any text will be truncated if it is too long.
     *
     * @param object the result act
     * @return the result component, or {@code null} if there are none
     */
    private Component getResult(Act object) {
        Component result = null;
        Property property = new DocumentBackedTextProperty(object, RESULT, LONG_RESULT);
        String text = StringUtils.abbreviate(property.getString(), 50);
        Label chart = null;
        IMObjectBean bean = getBean(object);
        if (bean.getTargetRef(IMAGE) != null) {
            chart = LabelFactory.create(null, "chart");
        }
        if (text != null && chart != null) {
            result = RowFactory.create(Styles.WIDE_CELL_SPACING, LabelFactory.text(text), chart);
        } else if (text != null) {
            result = LabelFactory.text(text);
        } else if (chart != null) {
            result = chart;
        }
        return result;
    }
}
