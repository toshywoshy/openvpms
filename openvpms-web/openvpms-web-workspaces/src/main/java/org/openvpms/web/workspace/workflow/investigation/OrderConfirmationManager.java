/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.workflow.investigation;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.component.model.act.Act;
import org.openvpms.laboratory.internal.dispatcher.Confirmation;
import org.openvpms.laboratory.internal.dispatcher.OrderDispatcher;
import org.openvpms.laboratory.order.WebOrderConfirmation;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.system.ServiceHelper;

/**
 * Manages user confirmation of laboratory orders associated with investigations.
 *
 * @author Tim Anderson
 */
public class OrderConfirmationManager {

    /**
     * The order dispatcher.
     */
    private final OrderDispatcher dispatcher;

    /**
     * Constructs an {@link OrderConfirmationManager}.
     */
    public OrderConfirmationManager() {
        dispatcher = ServiceHelper.getBean(OrderDispatcher.class);
    }

    /**
     * Confirms the order for an investigation if required.
     *
     * @param investigation the investigation
     * @param help          the help context
     * @param listener      a listener to invoke on completion. If no confirmation is required, this will be invoked
     *                      immediately. May be {@code null}
     */
    public void confirm(Act investigation, HelpContext help, Runnable listener) {
        OrderConfirmationDialog dialog = createConfirmationDialog(investigation, help);
        if (dialog != null) {
            if (listener != null) {
                dialog.addWindowPaneListener(new WindowPaneListener() {
                    @Override
                    public void onClose(WindowPaneEvent event) {
                        runProtected(listener);
                    }
                });
            }
            dialog.show();
        } else {
            if (listener != null) {
                runProtected(listener);
            }
        }
    }

    /**
     * Creates an order confirmation dialog for an investigation, if it needs to be confirmed.
     *
     * @param investigation the investigation
     * @param help          the help context
     * @return the order confirmation dialog, or {@code null} if the order cannot be confirmed
     */
    public OrderConfirmationDialog createConfirmationDialog(Act investigation, HelpContext help) {
        OrderConfirmationDialog result = null;
        Confirmation confirmation = dispatcher.order(investigation);
        if (confirmation != null) {
            if (confirmation.getConfirmation() instanceof WebOrderConfirmation) {
                result = new OrderConfirmationDialog(confirmation.getOrder(),
                                                     (WebOrderConfirmation) confirmation.getConfirmation(),
                                                     investigation, confirmation.getService(), help);
            }
        }
        return result;
    }

    /**
     * Runs a listener, displaying any exceptions.
     *
     * @param listener the listener
     */
    private void runProtected(Runnable listener) {
        try {
            listener.run();
        } catch (Throwable exception) {
            ErrorHelper.show(exception);
        }
    }

}
