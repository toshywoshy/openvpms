/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.mapping;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.event.ActionEvent;
import org.apache.commons.lang.StringUtils;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.query.AbstractTableBrowser;
import org.openvpms.web.component.im.query.ResultSet;
import org.openvpms.web.component.im.table.PagedIMTable;
import org.openvpms.web.echo.button.ButtonRow;
import org.openvpms.web.echo.event.ActionListener;
import org.openvpms.web.echo.factory.ColumnFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.focus.FocusGroup;
import org.openvpms.web.echo.style.Styles;
import org.openvpms.web.echo.text.TextComponent;

/**
 * Mapping browser.
 *
 * @author Tim Anderson
 */
abstract class MappingBrowser<T> extends AbstractTableBrowser<T> {

    /**
     * The value to filter on. May be {@code null}
     */
    private String value;

    /**
     * The search field.
     */
    private TextComponent search;

    /**
     * The no. of results to display per page.
     */
    static final int MAX_RESULTS = 20;

    /**
     * Constructs a {@link MappingBrowser}.
     *
     * @param value   the initial value. May be {@code null}
     * @param model   the model
     * @param context the layout context
     */
    MappingBrowser(String value, AbstractMappingTableModel<T> model, LayoutContext context) {
        super(model, context);
        this.value = value;
    }

    /**
     * Query using the specified criteria, and populate the browser with
     * matches.
     */
    @Override
    public void query() {
        Component component = getComponent();
        value = StringUtils.trimToNull(search.getText());
        ResultSet<T> set = createResultSet(value, MAX_RESULTS);
        boolean hasResults = hasResults(set);
        doLayout(component, hasResults);

        PagedIMTable<T> table = getTable();
        table.setResultSet(set);
        setFocusOnResults();
    }

    /**
     * Creates a new result set.
     *
     * @param value      the value to filter on. May be {@code null}
     * @param maxResults the maximum number of results per page
     * @return the result set
     */
    protected abstract ResultSet<T> createResultSet(String value, int maxResults);

    /**
     * Lay out this component.
     */
    @Override
    protected void doLayout() {
        super.doLayout();
        query();
    }

    /**
     * Lays out this component.
     *
     * @param container the container
     */
    @Override
    protected void doLayout(Component container) {
        search = TextComponentFactory.create();
        search.setText(value);
        search.addActionListener(new ActionListener() {
            @Override
            public void onAction(ActionEvent event) {
                query();
            }
        });
        FocusGroup group = getFocusGroup();
        ButtonRow row = new ButtonRow(group);
        row.add(search);
        row.addButton("button.query", new ActionListener() {
            public void onAction(ActionEvent event) {
                query();
            }
        });

        container.add(ColumnFactory.create(Styles.INSET, row));
    }

}


