/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.workspace.admin.eftpos;

import echopointng.HttpPaneEx;
import org.openvpms.eftpos.internal.event.EFTPOSEventMonitor;
import org.openvpms.eftpos.service.WebTerminalRegistrar;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.web.echo.dialog.InformationDialog;
import org.openvpms.web.echo.dialog.ModalDialog;
import org.openvpms.web.echo.util.ApplicationInstanceConsumer;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

/**
 * EFTPOS terminal registration dialog for EFTPOS services that provide a {@link WebTerminalRegistrar}.
 *
 * @author Tim Anderson
 */
public class WebTerminalRegistrationDialog extends ModalDialog {

    /**
     * The terminal to register.
     */
    private final Terminal terminal;

    /**
     * The monitor for EFTPOS events.
     */
    private final EFTPOSEventMonitor monitor;

    /**
     * The listener.
     */
    private ApplicationInstanceConsumer<TerminalStatus> listener;

    /**
     * Constructs a {@link WebTerminalRegistrationDialog}.
     *
     * @param terminal  the terminal to register
     * @param registrar the terminal registrar
     */
    public WebTerminalRegistrationDialog(Terminal terminal, WebTerminalRegistrar registrar) {
        super(Messages.get("admin.eftpos.register.title"), CANCEL, null);
        this.terminal = terminal;
        setContentWidth(registrar.getWidth());
        setContentHeight(registrar.getHeight());
        monitor = ServiceHelper.getBean(EFTPOSEventMonitor.class);
        HttpPaneEx frame = new HttpPaneEx(registrar.getUrl());
        getLayout().add(frame);
    }

    /**
     * Show the window.
     */
    @Override
    public void show() {
        super.show();
        listener = new ApplicationInstanceConsumer<>(this::onCompleted);
        monitor.addTerminalListener(terminal, listener);
    }

    /**
     * Disposes of this dialog.
     */
    @Override
    public void dispose() {
        super.dispose();
        if (listener != null) {
            listener.dispose();
        }
    }

    /**
     * Invoked when terminal registration has completed.
     *
     * @param status the registration status
     */
    private void onCompleted(TerminalStatus status) {
        monitor.removeTerminalListener(terminal, listener);
        // only display the notification if the dialog is still popped up
        if (getParent() != null) {
            String title = Messages.get("admin.eftpos.register.title");
            if (status.isRegistered()) {
                InformationDialog.newDialog()
                        .title(title)
                        .message(Messages.get("admin.eftpos.register.registered"))
                        .ok(() -> WebTerminalRegistrationDialog.this.close(OK_ID))
                        .show();
            } else {
                InformationDialog.newDialog()
                        .title(title)
                        .message(Messages.format("admin.eftpos.register.error", status.getMessage()))
                        .ok(() -> WebTerminalRegistrationDialog.this.close(CANCEL_ID))
                        .show();
            }
        }
    }
}
