<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<Configuration>
    <Appenders>
        <Console name="stdout" target="SYSTEM_OUT">
            <PatternLayout pattern="[%p,%c{1},%t - %X{user}] %m%n"/>
        </Console>
        <RollingFile name="fullout" fileName="${sys:catalina.base}/logs/openvpms-full.log"
                     filePattern="${sys:catalina.base}/logs/openvpms-full.log.%i">
            <PatternLayout pattern="%d{DATE} %5p %c{1},%t:%L - %X{user} - %m%n" charset="UTF-8"/>
            <Policies>
                <SizeBasedTriggeringPolicy size="10240KB"/>
            </Policies>
            <DefaultRolloverStrategy max="5"/>
        </RollingFile>
        <RollingFile name="fileout" fileName="${sys:catalina.base}/logs/openvpms.log"
                     filePattern="${sys:catalina.base}/logs/openvpms.log.%i">
            <PatternLayout pattern="%d{DATE} %5p %c{1},%t:%L - %X{user} - %m%n" charset="UTF-8"/>
            <Policies>
                <SizeBasedTriggeringPolicy size="10240KB"/>
            </Policies>
            <DefaultRolloverStrategy max="1"/>
        </RollingFile>
    </Appenders>
    <Loggers>
        <Logger name="org.openvpms.archetype.tools.account.AccountBalanceTool" Level="INFO"/>

        <Logger name="org.quartz.plugins.history.LoggingJobHistoryPlugin" Level="INFO"/>
        <Logger name="org.openvpms.web.echo.style.StyleSheetCache" Level="INFO"/>

        <!-- Validation -->
        <!-- Logger name="org.openvpms.web.component.property.AbstractValidator" Level="DEBUG"/-->

        <!-- Order Generator -->
        <!-- Logger name="org.openvpms.archetype.rules.supplier.OrderGenerator" Level="DEBUG"/-->

        <!-- Reporting -->
        <!--Logger name="org.openvpms.web.component.im.report.ReportRunner" Level="DEBUG"/-->
        <!--Logger name="org.openvpms.report.jasper.AbstractDataSource" Level="WARN"/-->
        <!-- NOTE: turn on DEBUG logging to see use of non-existent nodes, bad expressions -->

        <!-- OpenOffice debugging -->
        <!--Logger name="org.openvpms.report.openoffice.OOBootstrapService" Level="DEBUG"/>
        <Logger name="org.openvpms.report.openoffice.AbstractOOConnectionPool" Level="DEBUG"/>
        <Logger name="org.openvpms.report.openoffice.OpenOfficeDocument" Level="DEBUG"/>
        <Logger name="org.openvpms.report.openoffice.OpenOfficeIMReport" Level="DEBUG"/>
        <Logger name="org.openvpms.report.openoffice.OpenOfficeHelper" Level="DEBUG"/>
        <Logger name="org.openvpms.report.openoffice.Converter" Level="DEBUG"/>
        <Logger name="org.openvpms.report.openoffice.PrintService" Level="DEBUG"/-->

        <!-- HL7 -->
        <!-- Logger name="org.openvpms.hl7.impl.MessageDispatcherImpl" Level="DEBUG"/>
        <Logger name="org.openvpms.hl7.impl.PharmacyDispenseServiceImpl" Level="DEBUG"/>
        <Logger name="ca.uhn.hl7v2.raw.inbound" Level="DEBUG"/>
        <Logger name="ca.uhn.hl7v2.raw.outbound" Level="DEBUG"/-->

        <!-- Jobs -->
        <Logger name="org.openvpms.component.business.service.scheduler.JobScheduler" Level="INFO"/>
        <Logger name="org.openvpms.component.business.service.scheduler.JobRunner" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.account.AccountReminder" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.account.AccountReminderJob" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.account.AccountReminderSender" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.appointment.AppointmentReminderJob" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.docload.DocumentLoaderJob" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.docload.email.EmailDocumentLoaderJob" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.recordlocking.MedicalRecordLockingScheduler" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.recordlocking.MedicalRecordLockerJob" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.pharmacy.PharmacyOrderDiscontinuationScheduler" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.pharmacy.PharmacyOrderDiscontinuationJob" Level="INFO"/>

        <!-- Hibernate -->
        <!-- Comment the following out to see: "An item was expired by the cache while it was locked (increase your cache timeout)" -->
        <Logger name="org.hibernate.cache.ReadWriteCache" Level="ERROR"/>

        <!-- Mail -->
        <!--Logger name="org.openvpms.web.component.mail.AbstractMailer" Level="DEBUG"/-->

        <!-- Spring transactions -->
        <!-- Logger name="org.springframework.transaction.support.TransactionTemplate" Level="ERROR"/>
        <Logger name="org.springframework.transaction.support.AbstractPlatformTransactionManager" Level="DEBUG"/>
        <Logger name="org.springframework.orm.hibernate3.HibernateTransactionManager" Level="DEBUG"/-->

        <!-- Reminders -->
        <Logger name="org.openvpms.archetype.rules.patient.reminder.ReminderProcessor" Level="DEBUG"/>
        <Logger name="org.openvpms.web.jobs.reminder.PatientReminderQueueJob" Level="INFO"/>
        <Logger name="org.openvpms.web.jobs.reminder.PatientReminderSenderJob" Level="INFO"/>

        <!-- Application management -->
        <!--Logger name="org.openvpms.web.echo.servlet.SessionMonitor" Level="DEBUG"/-->
        <!--Logger name="org.openvpms.web.echo.spring.SpringApplicationInstance" Level="DEBUG"/-->
        <!--Logger name="org.openvpms.web.echo.util.PeriodicTask" Level="DEBUG"/-->

        <!-- Smart Flow Sheet -->
        <Logger name="org.openvpms.smartflow.event.impl.DefaultEventDispatcher" Level="DEBUG"/>
        <Logger name="org.openvpms.smartflow.client.HospitalizationService" Level="DEBUG"/>
        <Logger name="org.openvpms.smartflow.client.InventoryService" Level="DEBUG"/>
        <Logger name="org.openvpms.smartflow.event.impl.QueueDispatcher" Level="DEBUG"/>
        <Logger name="org.openvpms.smartflow.event.impl.QueueDispatchers" Level="DEBUG"/>
        <Logger name="org.openvpms.smartflow.event.impl.NotesEventProcessor" Level="DEBUG"/>
        <Logger name="org.openvpms.smartflow.event.impl.TreatmentEventProcessor" Level="DEBUG"/>

        <!-- JasperReports -->
        <!-- Comment the following out to see deprecation warnings -->
        <Logger name="net.sf.jasperreports.engine.xml.JRBandFactory" Level="ERROR"/>
        <Logger name="net.sf.jasperreports.engine.xml.JRTextElementFactory" Level="ERROR"/>
        <Logger name="net.sf.jasperreports.engine.xml.JRTextFieldFactory" Level="ERROR"/>
        <!--Logger name="net.sf.jasperreports.engine.query.JRJdbcQueryExecuter" Level="TRACE"/-->

        <!-- WebDAV -->
        <!--Logger name="org.openvpms.web.webdav.milton.ResourceLockManagerImpl" Level="DEBUG"/-->
        <!--Logger name="org.openvpms.web.webdav.servlet.WebDAVServlet" Level="DEBUG"/-->
        <!--Logger name="io.milton.http.webdav.PropFindHandler" Level="TRACE"/-->
        <!--Logger name="io.milton.http.webdav.DefaultWebDavResponseHandler" Level="TRACE"/-->
        <!--Logger name="io.milton" Level="TRACE"/-->

        <!-- Charging -->
        <!--Logger name="org.openvpms.web.workspace.customer.charge.CustomerChargeActItemEditor" Level="DEBUG"/-->

        <!-- Laboratory -->
        <Logger name="org.openvpms.laboratory.internal.dispatcher.OrderDispatcherImpl" Level="DEBUG"/>

        <!-- Claims -->
        <Logger name="au.com.petsure.vethub.openvpms.internal.service.InsuranceServiceImpl" Level="DEBUG"/>
        <Logger name="au.com.petsure.vethub.openvpms.internal.client.VetHubClientImpl" Level="DEBUG"/>
        <Logger name="au.com.petsure.vethub.openvpms.internal.service.ConversationProcessor" Level="DEBUG"/>

        <!-- IDEXX -->
        <Logger name="com.idexx.vetconnect.openvpms.service.VetConnectLaboratoryService" Level="DEBUG"/>
        <Logger name="com.idexx.vetconnect.openvpms.internal.client.VetConnectClient" Level="DEBUG"/>

        <!-- Zoetis -->
        <Logger name="com.zoetis.vetscan.openvpms.service.VetScanLaboratoryService" Level="DEBUG"/>
        <Logger name="com.zoetis.vetscan.openvpms.internal.client.VetScanClientImpl" Level="DEBUG"/>
        <Logger name="com.zoetis.vetscan.openvpms.internal.sync.LaboratorySynchroniser" Level="DEBUG"/>

        <!-- Deputy -->
        <Logger name="org.openvpms.deputy.internal.service.DeputyServiceImpl" Level="DEBUG"/>
        <Logger name="org.openvpms.deputy.internal.service.SynchronisationManager" Level="DEBUG"/>
        <Logger name="org.openvpms.deputy.internal.service.RosterSynchroniser" Level="DEBUG"/>
        <Logger name="org.openvpms.deputy.internal.service.DeputyClient" Level="DEBUG"/>

        <!-- Clickatell -->
        <Logger name="org.openvpms.clickatell.service.MessageStatusMonitorService" Level="DEBUG"/>
        <Logger name="org.openvpms.clickatell.internal.client.ClickatellClient" Level="DEBUG"/>
        <Logger name="org.openvpms.clickatell.internal.service.MessageStatusUpdater" Level="DEBUG"/>

        <!-- Scheduling -->
        <Logger name="org.openvpms.archetype.rules.workflow.cache.AbstractEventCache" Level="DEBUG"/>
        <Logger name="org.openvpms.archetype.rules.workflow.cache.DayCache" Level="DEBUG"/>
        <Logger name="org.openvpms.archetype.rules.workflow.AppointmentService" Level="DEBUG"/>

        <!-- Windcave -->
        <Logger name="com.openvpms.windcave.internal.client.WindcaveClient" Level="DEBUG"/>
        <Logger name="com.openvpms.windcave.service.WindcaveEFTPOSService" Level="INFO"/>

        <!-- VetCheck -->
        <Logger name="org.openvpms.web.workspace.patient.vetcheck.VetCheckDialog" Level="DEBUG"/>

        <!-- MessageMedia -->
        <Logger name="com.openvpms.messagemedia.internal.client.MessageMediaClient" Level="DEBUG"/>
        <Logger name="com.openvpms.messagemedia.service.MessageStatusUpdater" Level="DEBUG"/>

        <!-- ODi -->
        <!--Logger name="com.codeseam.odi.plugin.Plugin" Level="INFO"/-->
        <!--Logger name="com.codeseam.odi.pacs.asteris.AsterisSession" Level="DEBUG"/-->
        <!--Logger name="com.codeseam.odi.pacs.vetrocket.VetRocket" Level="DEBUG"/-->

        <!-- Spring security -->
        <!--Logger name="org.springframework.security" Level="DEBUG"/-->

        <!-- Ehcache size -->
        <!-- Also requires the system property org.ehcache.sizeof.verboseDebugLogging=true -->
        <!-- Logger name="org.ehcache.sizeof.ObjectGraphWalker" Level="DEBUG"/-->

        <!-- Plugins -->
        <Logger name="com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager" Level="INFO"/>
        <Logger name="com.atlassian.plugin.manager.DefaultPluginManager" Level="INFO"/>
        <!-- Logger name="org.openvpms.plugin.internal.manager.ExportPackages" Level="DEBUG"/-->
        <!-- enable to see included/excluded jars -->

        <!--Logger name="com.atlassian.plugin.osgi.container.felix.ExportsBuilder" Level="DEBUG"/-->
        <!-- enable to see exported packages -->

        <Root level="WARN">
            <AppenderRef ref="stdout"/>
            <AppenderRef ref="fileout"/>
            <AppenderRef ref="fullout"/>
        </Root>
    </Loggers>

</Configuration>
