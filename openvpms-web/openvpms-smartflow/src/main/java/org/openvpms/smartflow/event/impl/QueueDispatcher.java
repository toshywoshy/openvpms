/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.event.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.servicebus.ExceptionPhase;
import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.IMessageHandler;
import com.microsoft.azure.servicebus.IQueueClient;
import com.microsoft.azure.servicebus.MessageHandlerOptions;
import com.microsoft.azure.servicebus.QueueClient;
import com.microsoft.azure.servicebus.ReceiveMode;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import org.openvpms.component.model.party.Party;
import org.openvpms.smartflow.client.FlowSheetException;
import org.openvpms.smartflow.client.FlowSheetServiceFactory;
import org.openvpms.smartflow.client.ReferenceDataService;
import org.openvpms.smartflow.event.EventDispatcher;
import org.openvpms.smartflow.event.EventStatus;
import org.openvpms.smartflow.i18n.FlowSheetMessages;
import org.openvpms.smartflow.model.ServiceBusConfig;
import org.openvpms.smartflow.model.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

/**
 * Dispatches messages from a {@link QueueClient} to an {@link EventDispatcher}.
 *
 * @author Tim Anderson
 */
class QueueDispatcher {

    /**
     * The practice location that the queue is associated with.
     */
    private final Party location;

    /**
     * The event dispatcher.
     */
    private final EventDispatcher dispatcher;

    /**
     * The object mapper.
     */
    private final ObjectMapper mapper;

    /**
     * The executor service for processing messages.
     */
    private final ExecutorService executorService;

    /**
     * The factory for Smart Flow Sheet services.
     */
    private final FlowSheetServiceFactory factory;

    /**
     * The queue.
     */
    private IQueueClient queue;

    /**
     * Determines if this dispatcher has been started.
     */
    private boolean started = false;

    /**
     * Determines if this dispatcher has been destroyed.
     */
    private volatile boolean destroyed = false;

    /**
     * The date when an event was last received.
     */
    private Date lastReceived;

    /**
     * The time of the last error.
     */
    private Date lastError;

    /**
     * The last error message, if the last event was not processed successfully.
     */
    private String errorMessage;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(QueueDispatcher.class);

    /**
     * Constructs a {@link QueueDispatcher}.
     *
     * @param location        the practice location
     * @param dispatcher      the event dispatcher
     * @param mapper          the object mapper
     * @param executorService the executor service for processing messages
     * @param factory         the factory for Smart Flow Sheet services
     */
    public QueueDispatcher(Party location, EventDispatcher dispatcher, ObjectMapper mapper,
                           ExecutorService executorService, FlowSheetServiceFactory factory) {
        this.location = location;
        this.dispatcher = dispatcher;
        this.mapper = mapper;
        this.executorService = executorService;
        this.factory = factory;
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    public Party getLocation() {
        return location;
    }

    /**
     * Starts dispatching messages on the queue.
     *
     * @return {@code true} if the dispatcher is running, {@code false} if it couldn't start due to error
     */
    public synchronized boolean start() {
        boolean result = false;
        if (destroyed) {
            return false;
        }
        try {
            if (!started) {
                clearError();
                queue = createQueue();
                // options are single threaded receipt, this is responsible for managing message ack,
                // and 5 minutes maxAutoRenewDuration (the default)
                MessageHandlerOptions options = new MessageHandlerOptions(1, false, Duration.ofMinutes(5));
                queue.registerMessageHandler(new MessageHandler(), options, executorService);
                started = true;
            }
            result = true;
        } catch (Throwable exception) {
            stopped(exception);
            log.error("Failed start dispatching SFS events for location=" + location.getName() + ": "
                      + exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Determines if the dispatcher has been started.
     *
     * @return {@code true} if the dispatcher has been started
     */
    public synchronized boolean isStarted() {
        return started;
    }

    /**
     * Stops dispatching.
     */
    public synchronized void stop() {
        if (started) {
            try {
                queue.close();
            } catch (Throwable exception) {
                log.error(exception.getMessage(), exception);
            } finally {
                queue = null;
                stopped(null);
            }
        }
    }

    /**
     * Determines if this dispatcher has been destroyed.
     *
     * @return {@code true} if this dispatcher has been destroyed
     */
    public boolean isDestroyed() {
        return destroyed;
    }

    /**
     * Destroys this dispatcher. Once destroyed it cannot be started.
     */
    public void destroy() {
        try {
            stop();
        } finally {
            destroyed = true;
        }
    }

    /**
     * Returns the status of event processing.
     *
     * @return the status
     */
    public synchronized EventStatus getStatus() {
        return new EventStatus(lastReceived, lastError, errorMessage);
    }

    /**
     * Dispatches a message.
     * <p/>
     * It will be removed from the queue on completion.
     *
     * @param message the message
     */
    protected void dispatch(IMessage message) {
        lastReceived = new Date();
        clearError();
        try {
            Event<?> event = getEvent(message);
            if (event != null) {
                dispatcher.dispatch(event);
            }
            queue.complete(message.getLockToken());
        } catch (Throwable exception) {
            // failure has occurred. Azure Service Bus should retry the message, up to expiry
            log.error("Failed to process message=" + message.getMessageId() + " for location=" + location.getName()
                      + ": " + exception.getMessage(), exception);
            setError(exception);
        }
    }

    /**
     * Returns an event from a message.
     *
     * @param message the message
     * @return the event, or {@code null} if it cannot be deserialized
     */
    protected Event<?> getEvent(IMessage message) {
        byte[] bytes = null;
        List<byte[]> binaryData = message.getMessageBody().getBinaryData();
        if (binaryData != null && binaryData.size() != 0) {
            bytes = binaryData.get(0);
        }
        if (log.isDebugEnabled()) {
            log.debug("location='" + location.getName() + "', messageID=" + message.getMessageId() + ", timeToLive="
                      + message.getTimeToLive() + ", sequence=" + message.getSequenceNumber() + ", contentType="
                      + message.getContentType() + ", content=" + getContent(bytes));
        }
        Event<?> event = null;
        if (bytes != null) {
            try {
                event = mapper.readValue(new ByteArrayInputStream(bytes), Event.class);
            } catch (Throwable exception) {
                log.error("Failed to deserialize message for location='" + location.getName()
                          + "', messageID=" + message.getMessageId() + ", sequence=" + message.getSequenceNumber()
                          + ", timeToLive=" + message.getTimeToLive() + ", contentType=" + message.getContentType()
                          + ", content=" + getContent(bytes), exception);
            }
        }
        return event;
    }

    /**
     * Creates a queue.
     *
     * @return a new queue
     * @throws FlowSheetException for any error
     */
    protected IQueueClient createQueue() {
        IQueueClient result;
        try {
            ReferenceDataService service = factory.getReferenceDataService(location);
            ServiceBusConfig config = service.getServiceBusConfig();
            result = new QueueClient(new ConnectionStringBuilder(config.getConnectionString(), config.getQueueName()),
                                     ReceiveMode.PEEKLOCK);
        } catch (FlowSheetException exception) {
            throw exception;
        } catch (Throwable exception) {
            throw new FlowSheetException(FlowSheetMessages.failedToCreateAzureServiceBusQueue(
                    location, exception.getMessage()), exception);
        }
        return result;
    }

    /**
     * Helper to return a byte array as a UTF-8 string, for debugging.
     *
     * @param bytes the byte array. May be {@code null}
     * @return the string. May  be {@code null}
     */
    private String getContent(byte[] bytes) {
        return (bytes != null) ? new String(bytes, StandardCharsets.UTF_8) : null;
    }

    /**
     * Invoked when the dispatcher is stopped.
     *
     * @param exception the exception, if the dispatcher was stopped due to an error, otherwise {@code null}
     */
    private synchronized void stopped(Throwable exception) {
        this.started = false;
        if (exception == null) {
            clearError();
        } else {
            setError(exception);
        }
    }

    /**
     * Clears the error status.
     */
    private void clearError() {
        lastError = null;
        errorMessage = null;
    }

    /**
     * Sets the error for status reporting.
     *
     * @param exception the exception
     */
    private void setError(Throwable exception) {
        lastError = new Date();
        errorMessage = exception.getMessage();
        if (errorMessage == null) {
            errorMessage = exception.getClass().getSimpleName();
        }
    }

    private class MessageHandler implements IMessageHandler {

        @Override
        public CompletableFuture<Void> onMessageAsync(IMessage message) {
            dispatch(message);
            return CompletableFuture.completedFuture(null);
        }

        @Override
        public void notifyException(Throwable throwable, ExceptionPhase exceptionPhase) {
            log.error("Exception on Azure Service Bus queue for location=" + location.getName()
                      + ", exceptionPhase=" + exceptionPhase + ": " + throwable.getMessage(), throwable);
        }
    }

}
