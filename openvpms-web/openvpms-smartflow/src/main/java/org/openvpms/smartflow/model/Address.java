/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Smart Flow Sheet client address.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    /**
     * Address line 1.
     */
    private String line1;

    /**
     * Address line 2.
     */
    private String line2;

    /**
     * The city.
     */
    private String city;

    /**
     * The state.
     */
    private String state;

    /**
     * The post code.
     */
    private String zip;

    /**
     * Returns the first address line.
     *
     * @return the first address line
     */
    public String getLine1() {
        return line1;
    }

    /**
     * Sets the first address line.
     *
     * @param line1 the first address line
     */
    public void setLine1(String line1) {
        this.line1 = line1;
    }

    /**
     * Returns the second address line.
     *
     * @return the second address line
     */
    public String getLine2() {
        return line2;
    }

    /**
     * Sets the second address line.
     *
     * @param line2 the second address line
     */
    public void setLine2(String line2) {
        this.line2 = line2;
    }

    /**
     * Returns the city.
     *
     * @return the city.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Returns the state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state.
     *
     * @param state the state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Returns the post code.
     *
     * @return the post code
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the post code.
     *
     * @param zip the post code
     */
    public void setZip(String zip) {
        this.zip = zip;
    }
}