/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.smartflow.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Smart Flow Sheet patient owner.
 *
 * @author Tim Anderson
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Client {

    /**
     * Describes the type of the object transferred with the SFS events. Should be assigned client value. Optional.
     */
    private String objectType = "client";

    /**
     * EMR internal ID of the client. Optional.
     */
    private String ownerId;

    /**
     * Pet owner’s last name. Optional.
     */
    private String nameLast;

    /**
     * Pet owner’s first name. Optional.
     */
    private String nameFirst;

    /**
     * Pet owner’s home phone. Optional.
     */
    private String homePhone;

    /**
     * Optional. Pet owner’s work phone.
     */
    private String workPhone;

    /**
     * Optional. Pet owner's cellphone.
     */
    private String cellPhone;

    /**
     * Optional. Pet owner's fax.
     */
    private String fax;

    /**
     * Optional. Pet owner's emergency contact.
     */
    private String emergencyContact;

    /**
     * Optional. Pet owner's title.
     */
    private String title;

    /**
     * Optional. Pet owner's email.
     */
    private String email;

    /**
     * The pet owner's address.
     */
    private Address address;

    /**
     * Returns the object type.
     *
     * @return the object type
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * Sets the object type.
     *
     * @param objectType the object type. Should be {@code "client"}
     */
    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    /**
     * Returns the owner identifier.
     *
     * @return the owner identifier
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the owner identifier.
     *
     * @param ownerId the owner identifier
     */
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Returns the pet owner's title.
     *
     * @return the pet owner's title. May be {@code null}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the pet owner's title.
     *
     * @param title the pet owner's title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the pet owner's last name.
     *
     * @return the pet owner’s last name. May be {@code null}
     */
    public String getNameLast() {
        return nameLast;
    }

    /**
     * Sets the pet owner's last name.
     *
     * @param nameLast the pet owner’s last name. May be {@code null}
     */
    public void setNameLast(String nameLast) {
        this.nameLast = nameLast;
    }

    /**
     * Returns the pet owner's first name.
     *
     * @return the pet owner’s first name. May be {@code null}
     */
    public String getNameFirst() {
        return nameFirst;
    }

    /**
     * Sets the pet owner's first name.
     *
     * @param nameFirst the pet owner’s first name. May be {@code null}
     */
    public void setNameFirst(String nameFirst) {
        this.nameFirst = nameFirst;
    }

    /**
     * Returns the pet owner’s home phone.
     *
     * @return the pet owner’s home phone. May be {@code null}
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * Sets the pet owner’s home phone.
     *
     * @param homePhone the pet owner’s home phone. May be {@code null}
     */
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    /**
     * Returns the pet owner’s work phone.
     *
     * @return the pet owner’s work phone. May be {@code null}
     */
    public String getWorkPhone() {
        return workPhone;
    }

    /**
     * Sets the pet owner’s work phone.
     *
     * @param workPhone the pet owner’s work phone. May be {@code null}
     */
    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    /**
     * Returns the pet owner's cell phone.
     *
     * @return the cell phone. May be {@code null}
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * Sets the pet owner's cell phone.
     *
     * @param cellPhone the cell phone
     */
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    /**
     * Returns the pet owner's fax.
     *
     * @return the fax. May be {@code null}
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the pet owner's fax.
     *
     * @param fax the fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * Returns the emergency contact.
     *
     * @return the emergency contact. May be {@code null}
     */
    public String getEmergencyContact() {
        return emergencyContact;
    }

    /**
     * Sets the emergency contact.
     *
     * @param emergencyContact the emergency contact
     */
    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    /**
     * Returns the email.
     *
     * @return the email. May be {@code null}
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the email}
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Returns the pet owner's address.
     *
     * @return the pet owner's address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets the pet owner's address.
     *
     * @param address the address
     */
    public void setAddress(Address address) {
        this.address = address;
    }
}
