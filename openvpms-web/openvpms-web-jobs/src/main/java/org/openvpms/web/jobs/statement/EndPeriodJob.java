/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.statement;

import org.apache.commons.lang3.time.StopWatch;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.finance.statement.StatementService;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.rules.workflow.SystemMessageReason;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.system.common.query.criteria.TypedQueryIterator;
import org.openvpms.web.jobs.JobCompletionNotifier;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.resource.i18n.format.DateFormatter;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

/**
 * Runs end-of-period for each customer.
 *
 * @author Tim Anderson
 */
public class EndPeriodJob implements InterruptableJob {

    /**
     * The configuration.
     */
    private final IMObjectBean config;

    /**
     * The statement service.
     */
    private final StatementService statementService;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Used to send messages to users on completion or failure.
     */
    private final JobCompletionNotifier notifier;

    /**
     * Determines if the job should stop.
     */
    private volatile boolean stop;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(EndPeriodJob.class);

    /**
     * Constructs an {@link EndPeriodJob}.
     *
     * @param config           the configuration
     * @param statementService the statement service
     * @param service          the archetype service
     */
    public EndPeriodJob(Entity config, StatementService statementService, IArchetypeRuleService service) {
        this.config = service.getBean(config);
        this.statementService = statementService;
        this.service = service;
        notifier = new JobCompletionNotifier(service);
    }

    /**
     * Called by the {@link Scheduler} when a user interrupts the job.
     */
    @Override
    public void interrupt() {
        stop = true;
    }

    /**
     * Called by the {@link Scheduler} when a {@link Trigger} fires that is associated with the job.
     *
     * @param context the job execution context
     */
    @Override
    public void execute(JobExecutionContext context) {
        Processor processor = createProcessor();
        try {
            process(processor);
            complete(processor, null);
        } catch (Throwable exception) {
            log.error(exception.getMessage(), exception);
            complete(processor, exception);
        }
    }

    /**
     * Processes end-of-period for all customers until complete or interrupted.
     *
     * @param processor the processor
     */
    protected void process(Processor processor) {
        while (!stop && processor.hasNext()) {
            processor.process();
        }
    }

    /**
     * Calculates the statement date.
     * <p/>
     * This is the last day of the month prior to today's date.
     *
     * @return the statement date
     */
    protected Date getStatementDate() {
        Date yesterday = DateRules.getPreviousDate(getNow());
        Date statementDate = DateRules.getMonthEnd(yesterday);
        if (statementDate.compareTo(yesterday) > 0) {
            statementDate = DateRules.getMonthEnd(DateRules.getDate(statementDate, -1, DateUnits.MONTHS));
        }
        return statementDate;
    }

    /**
     * Returns the current date/time.
     *
     * @return the current date/time
     */
    protected Date getNow() {
        return new Date();
    }

    /**
     * Returns the customers to process.
     *
     * @return the customers
     */
    protected Iterator<Party> getCustomers() {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        Root<Party> from = query.from(Party.class, CustomerArchetypes.PERSON);
        query.orderBy(builder.asc(from.get("id")));
        return new TypedQueryIterator<>(service.createQuery(query), 1000);
    }

    /**
     * Creates a new processor.
     *
     * @return a new processor
     */
    private Processor createProcessor() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Iterator<Party> iterator = getCustomers();
        boolean postCompletedCharges = config.getBoolean("postCompletedCharges");
        return new Processor(getStatementDate(), iterator, postCompletedCharges, stopWatch);
    }

    /**
     * Invoked on completion of a job. Sends a message notifying the registered users of completion or failure of the
     * job if required.
     *
     * @param processor the processor
     * @param exception the exception, if the job failed, otherwise {@code null}
     */
    private void complete(Processor processor, Throwable exception) {
        Set<User> users = notifier.getUsers(config.getObject(Entity.class));
        if (!users.isEmpty()) {
            notifyUsers(users, processor, exception);
        }
    }

    /**
     * Notifies users on job completion or failure.
     *
     * @param users     the users to notify
     * @param processor the processor
     * @param exception the exception, if an error was encountered, or {@code null} if not
     */
    private void notifyUsers(Set<User> users, Processor processor, Throwable exception) {
        String subject;
        String reason;
        String text;
        String date = DateFormatter.formatDate(processor.getStatementDate(), false);
        String elapsed = processor.getStopWatch().toString();
        int processed = processor.getProcessed();
        if (exception != null) {
            reason = SystemMessageReason.ERROR;
            subject = Messages.format("endperiod.exception.subject", date);
            text = Messages.format("endperiod.exception.message", exception.getMessage(), processed, elapsed);
        } else if (!processor.completed()) {
            reason = SystemMessageReason.ERROR;
            subject = Messages.format("endperiod.incomplete.subject", date);
            text = Messages.format("endperiod.incomplete.message", processed, elapsed);
        } else {
            reason = SystemMessageReason.COMPLETED;
            subject = Messages.format("endperiod.completed.subject", date);
            text = Messages.format("endperiod.completed.message", processed, elapsed);
        }
        notifier.send(users, subject, reason, text);
    }

    protected class Processor {

        /**
         * The no. of customers processed.
         */
        int processed;

        /**
         * The statement date.
         */
        private final Date date;

        /**
         * The customers.
         */
        private final Iterator<Party> customers;

        /**
         * Determines if completed charges should be finalised.
         */
        private final boolean postCompletedCharges;

        /**
         * Used to track elapsed time.
         */
        private final StopWatch stopWatch;

        /**
         * Constructs a {@link Processor}.
         *
         * @param date                 the statement date
         * @param customers            the customers to perform end-of-period on
         * @param postCompletedCharges if {@code true}, POST charges with COMPLETED status
         * @param stopWatch            the stop-watch to track elapsed time
         */
        public Processor(Date date, Iterator<Party> customers, boolean postCompletedCharges, StopWatch stopWatch) {
            this.date = date;
            this.customers = customers;
            this.postCompletedCharges = postCompletedCharges;
            this.stopWatch = stopWatch;
        }

        /**
         * Returns the statement date.
         *
         * @return the statement date
         */
        public Date getStatementDate() {
            return date;
        }

        /**
         * Returns the number of customers processed.
         *
         * @return the number of customers processed
         */
        public int getProcessed() {
            return processed;
        }

        /**
         * Determines if there are any customers to process.
         *
         * @return {@code true} if there are customers to process, otherwise {@code false}
         */
        public boolean hasNext() {
            return customers.hasNext();
        }

        /**
         * Processes the next customer.
         */
        public void process() {
            Party customer = customers.next();
            statementService.endPeriod(customer, date, postCompletedCharges);
            processed++;
        }

        /**
         * Determines if processing completed.
         *
         * @return {@code true} if processing completed, otherwise {@code false}
         */
        public boolean completed() {
            return !customers.hasNext();
        }

        /**
         * Returns the stop-watch.
         *
         * @return the stop-watch
         */
        public StopWatch getStopWatch() {
            return stopWatch;
        }
    }
}