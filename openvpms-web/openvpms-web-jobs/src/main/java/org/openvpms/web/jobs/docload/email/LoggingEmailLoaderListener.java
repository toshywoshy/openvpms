/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.docload.email;

import org.slf4j.Logger;

import java.util.Date;

/**
 * Implementation of {@link EmailLoaderListener} that logs events to a {@link Logger}.
 *
 * @author Tim Anderson
 */
class LoggingEmailLoaderListener extends EmailLoaderListener {

    /**
     * The logger.
     */
    private final Logger log;

    /**
     * Constructs a {@link LoggingEmailLoaderListener}.
     *
     * @param log      the logger
     */
    public LoggingEmailLoaderListener(Logger log) {
        this.log = log;
    }

    /**
     * Notifies when an attachment is loaded.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param id        the corresponding act identifier
     */
    @Override
    public void loaded(String messageId, String from, String subject, Date date, String name, long id) {
        super.loaded(messageId, from, subject, date, name, id);
        log.info("Loaded attachment from messageId={}, from={}, subject={}, date={}, name={}, to id={}",
                 messageId, from, subject, date, name, id);
    }

    /**
     * Notifies that an attachment couldn't be loaded as it or another attachment had already been processed.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param id        the corresponding act identifier
     */
    @Override
    public void alreadyLoaded(String messageId, String from, String subject, Date date, String name, long id) {
        super.alreadyLoaded(messageId, from, subject, date, name, id);
        log.info("Skipped attachment from messageId={}, from={}, subject={}, date={}, name={}, to id={}",
                 messageId, from, subject, date, name, id);
    }

    /**
     * Notifies that an attachment couldn't be loaded as no identifier could be parsed from the attachment filename.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     */
    @Override
    public void missingId(String messageId, String from, String subject, Date date, String name) {
        super.missingId(messageId, from, subject, date, name);
        log.info("Missing id for messageId={}, from={}, subject={}, date={}, name={}",
                 messageId, from, subject, date, name);
    }

    /**
     * Notifies that an attachment couldn't be loaded as there was no corresponding act.
     *  @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param id        the corresponding act identifier
     */
    @Override
    public void missingAct(String messageId, String from, String subject, Date date, String name, long id) {
        super.missingAct(messageId, from, subject, date, name, id);
        log.info("Missing act for messageId={}, from={}, subject={}, date={}, name={}, id={}",
                 messageId, from, subject, date, name, id);
    }

    /**
     * Notifies that an email has no attachments.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     */
    @Override
    public void noAttachment(String messageId, String from, String subject, Date date) {
        super.noAttachment(messageId, from, subject, date);
        log.info("Email has no attachments for messageId={}, from={}, subject={}, date={}", messageId, from, subject,
                 date);
    }

    /**
     * Notifies that an attachment couldn't be loaded due to error.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param exception the error
     */
    @Override
    public void error(String messageId, String from, String subject, Date date, String name, Throwable exception) {
        super.error(messageId, from, subject, date, name, exception);
        log.info("Error for messageId={}, from={}, subject={}, date={}, name={}, id={}: {}",
                 messageId, from, subject, date, name, exception.getMessage(), exception);
    }

    /**
     * Notifies that a file couldn't be loaded due to error.
     *
     * @param messageId the message id
     * @param from      the from address
     * @param subject   the subject
     * @param date      the message date
     * @param name      the attachment name
     * @param message   the error message
     */
    @Override
    public void error(String messageId, String from, String subject, Date date, String name, String message) {
        super.error(messageId, from, subject, date, name, message);
        log.info("Error for messageId={}, from={}, subject={}, date={}, name={}: {}",
                 messageId, from, subject, date, name, message);
    }
}