/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs;

import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.archetype.test.builder.doc.TestSMSTemplateBuilder;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.sms.internal.service.SMSLengthCalculator;
import org.openvpms.web.component.im.sms.SMSTemplateEvaluator;
import org.openvpms.web.component.service.SimpleSMSService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Base class for testing jobs that send SMSes.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public abstract class AbstractSMSJobTest extends ArchetypeServiceTest {

    /**
     * The SMS template evaluator.
     */
    @Autowired
    protected SMSTemplateEvaluator smsTemplateEvaluator;

    /**
     * The document factory.
     */
    @Autowired
    protected TestDocumentFactory documentFactory;

    /**
     * The practice factory.
     */
    @Autowired
    protected TestPracticeFactory practiceFactory;

    /**
     * Creates a mock SMS service that supports single part messages.
     *
     * @return a new service
     */
    protected SimpleSMSService createSMSService() {
        SimpleSMSService smsService = Mockito.mock(SimpleSMSService.class);
        Mockito.when(smsService.getMaxParts()).thenReturn(1);
        Mockito.when(smsService.getParts(Mockito.anyString())).thenAnswer((Answer<Integer>) invocationOnMock -> {
            String message = invocationOnMock.getArguments()[0].toString();
            return SMSLengthCalculator.getParts(message);
        });
        return smsService;
    }

    /**
     * Returns a builder for an SMS template.
     *
     * @param archetype the SMS template archetype
     * @return a new builder
     */
    protected TestSMSTemplateBuilder newSMSTemplate(String archetype) {
        return documentFactory.newSMSTemplate(archetype);
    }
}
