/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.jobs.account;

import org.joda.time.Period;
import org.junit.Test;
import org.openvpms.component.model.entity.Entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.openvpms.archetype.test.TestHelper.getDate;

/**
 * Tests the {@link AccountReminderConfig} class.
 *
 * @author Tim Anderson
 */
public class AccountReminderConfigTestCase extends AbstractAccountReminderJobTest {

    /**
     * Tests the {@link AccountReminderConfig}.
     */
    @Test
    public void testConfig() {
        Entity template = createXPathTemplate("'dummy'");
        Entity entity = createConfig(BigDecimal.TEN, template);
        Date from = getDate("2021-08-15");
        AccountReminderConfig config = new AccountReminderConfig(entity, from, getArchetypeService());
        List<AccountReminderConfig.ReminderCount> counts = config.getReminderCounts();
        assertEquals(3, counts.size());
        checkEquals(BigDecimal.TEN, config.getMinBalance());
        checkCount(counts.get(0), 0, Period.days(2), getDate("2021-08-01"), getDate("2021-08-13"), template);
        checkCount(counts.get(1), 1, Period.weeks(2), getDate("2021-07-18"), getDate("2021-08-01"), template);
        checkCount(counts.get(2), 2, Period.weeks(4), getDate("2020-08-15"), getDate("2021-07-18"), template);

        assertEquals(getDate("2021-08-29"), config.getNextReminder(from, 0));
        assertEquals(getDate("2021-09-12"), config.getNextReminder(from, 1));
        assertNull(config.getNextReminder(from, 2));
    }

    /**
     * Checks a reminder count configuration.
     *
     * @param config   the configuration
     * @param count    the expected count
     * @param interval the expected interval
     * @param from     the expected start of the date range
     * @param to       the expected end of the date range
     * @param template the expected template
     */
    private void checkCount(AccountReminderConfig.ReminderCount config, int count, Period interval, Date from,
                            Date to, Entity template) {
        assertEquals(count, config.getCount());
        assertEquals(interval, config.getInterval());
        assertEquals(from, config.getFrom());
        assertEquals(to, config.getTo());
        assertEquals(template, config.getSMSTemplate());
    }

}