/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import org.junit.Test;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.archetype.descriptor.ArchetypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the {@link DefaultIMObjectEditor}.
 *
 * @author Tim Anderson
 */
public class DefaultIMObjectEditorTestCase extends AbstractAppTest {

    /**
     * Tests the {@link DefaultIMObjectEditor#newInstance()} method.
     */
    @Test
    public void testNewInstance() {
        Party patient = TestHelper.createPatient();
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        DefaultIMObjectEditor editor = new DefaultIMObjectEditor(patient, null, context);

        IMObjectEditor newInstance = editor.newInstance();
        assertTrue(newInstance instanceof DefaultIMObjectEditor);
    }

    /**
     * Verifies that a singleton instance cannot be saved, if one already exists and is active.
     */
    @Test
    public void testSingleton() {
        ArchetypeDescriptor descriptor = new ArchetypeDescriptor();
        String archetype = TestHelper.randomName("entity.testSingleton");
        String name = archetype + ".1.0";
        descriptor.setName(name);
        descriptor.setClassName(Entity.class.getName());
        descriptor.setSingleton(true);
        descriptor.setDisplayName("Test Singleton");
        NodeDescriptor node = new NodeDescriptor();
        node.setName("id");
        node.setPath("/id");
        node.setType(Long.class.getName());
        descriptor.addNodeDescriptor(node);
        save(descriptor);

        IMObject object1 = create(archetype);
        save(object1);

        IMObject object2 = create(archetype);
        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        DefaultIMObjectEditor editor = new DefaultIMObjectEditor(object2, null, context);
        Validator validator = new DefaultValidator();
        assertFalse(editor.validate(validator));
        assertEquals("There is a limit of a single active Test Singleton", validator.getFirstError().getMessage());

        // now deactivate object1 and verify object2 can be saved
        object1.setActive(false);
        save(object1);

        assertTrue(editor.isValid());
        assertTrue(SaveHelper.save(editor));
    }
}
