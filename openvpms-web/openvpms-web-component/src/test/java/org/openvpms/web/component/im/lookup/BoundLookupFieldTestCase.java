/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.lookup;

import org.openvpms.web.component.property.Property;


/**
 * Tests the {@link BoundLookupField} class.
 *
 * @author Tim Anderson
 */
public class BoundLookupFieldTestCase extends AbstractBoundLookupFieldTest<BoundLookupField> {

    /**
     * Constructs a {@link BoundLookupFieldTestCase}.
     */
    public BoundLookupFieldTestCase() {
        super();
    }

    /**
     * Creates a new bound field.
     *
     * @param property the property to bind to
     * @return a new bound field
     */
    @Override
    protected BoundLookupField createField(Property property, LookupQuery lookups) {
        return new BoundLookupField(property, lookups, false);
    }

    /**
     * Returns the value of the field.
     *
     * @param field the field
     * @return the value of the field
     */
    @Override
    protected String getValue(BoundLookupField field) {
        return field.getSelectedCode();
    }

    /**
     * Sets the value of the field.
     *
     * @param field the field
     * @param value the value to set
     */
    @Override
    protected void setValue(BoundLookupField field, String value) {
        field.setSelected(value);
    }

}
