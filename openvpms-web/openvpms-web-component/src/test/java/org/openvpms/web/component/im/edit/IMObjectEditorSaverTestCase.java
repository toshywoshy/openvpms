/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import com.mysql.cj.jdbc.exceptions.MySQLTransactionRollbackException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.StaleStateException;
import org.hibernate.exception.LockAcquisitionException;
import org.junit.Test;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.workflow.AppointmentStatus;
import org.openvpms.archetype.rules.workflow.ScheduleTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.web.component.app.LocalContext;
import org.openvpms.web.component.im.layout.DefaultLayoutContext;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.test.AbstractAppTest;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link IMObjectEditorSaver}.
 *
 * @author Tim Anderson
 */
public class IMObjectEditorSaverTestCase extends AbstractAppTest {

    /**
     * Verifies that if an exception is thrown during save, and  reloading isn't supported, the error itself is
     * displayed.
     */
    @Test
    public void testErrorMessageWhenExceptionThrownForNonReloadingEditor() {
        checkErrorMessageForExceptionDuringSave(new IllegalStateException("Save error"), false, "Save error");
        checkErrorMessageForExceptionDuringSave(new ObjectNotFoundException("foo", "bar"), false,
                                                "[bar#foo] may have been deleted by another user");
    }

    /**
     * Verifies that if an exception is thrown during save, and reloading is supported, a '
     * Your changes have been reverted' message is displayed.
     */
    @Test
    public void testErrorMessageWhenExceptionThrownForReloadingEditor() {
        checkErrorMessageForExceptionDuringSave(new IllegalStateException("Save error"), true,
                                                "An error was encountered saving the Appointment.\n\n" +
                                                "Your changes have been reverted.");
        checkErrorMessageForExceptionDuringSave(
                new ObjectNotFoundException("foo", "bar"), true,
                "The Appointment could not be saved as it has been modified by another user.\n\n" +
                "Your changes have been reverted.");
    }

    /**
     * Verifies that validation errors are displayed when a rule triggered by the save fails due to a validation error,
     * and the editor doesn't support reloading.
     */
    @Test
    public void testErrorMessageWhenRuleThrowsValidationExceptionThrownForNonReloadingEditor() {
        checkErrorMessageForRuleValidationException(false,
                                                    "Failed to validate Customer of Task: must supply at least 1 item");
    }

    /**
     * Verifies that validation errors are displayed when a rule triggered by the save fails due to a validation error,
     * and the editor supports reloading.
     */
    @Test
    public void testErrorMessageWhenRuleThrowsValidationExceptionThrownForReloadingEditor() {
        String error = "A validation error was encountered saving the Appointment:\n" +
                       "    Failed to validate Customer of Task: must supply at least 1 item.\n\n" +
                       "This may need to be corrected before the Appointment can be saved.\n\n" +
                       "Your changes have been reverted.";
        checkErrorMessageForRuleValidationException(true, error);
    }

    /**
     * Tests the errors displayed for deadlocks.
     */
    @Test
    public void testErrorMessageForDeadlock() {
        // simulate a deadlock stacktrace
        CannotAcquireLockException exception = new CannotAcquireLockException(
                "could not execute batch",
                new LockAcquisitionException(
                        "could not execute batch",
                        new BatchUpdateException(
                                new MySQLTransactionRollbackException(
                                        "Deadlock found when trying to get lock; try restarting transaction"))));

        // check error when an editor supports reloading
        checkErrorMessageForExceptionDuringSave(exception, true, "An error was encountered saving the Appointment.\n" +
                                                                 "\n" +
                                                                 "Your changes have been reverted.");

        // check error when reloading is not supported
        checkErrorMessageForExceptionDuringSave(exception, false, "Failed to save Appointment due to deadlock. " +
                                                                  "Try restarting the operation.");
    }

    /**
     * Tests the error displayed for Hibernate StaleStateExceptions.
     */
    @Test
    public void testErrorMessageForStaleStateException() {
        HibernateOptimisticLockingFailureException exception = new HibernateOptimisticLockingFailureException(
                new StaleStateException("Batch update returned unexpected row count from update [0]; actual row count: " +
                                        "0; expected: 1"));
        checkErrorMessageForExceptionDuringSave(
                exception, true,
                "The Appointment could not be saved as it has been modified by another user.\n\n" +
                "Your changes have been reverted.");
    }

    /**
     * Verify that the expected error message is displayed if an exception is thrown when the editor is saved.
     *
     * @param exception the exception to throw
     * @param reload    if {@code true}, support reloading
     * @param error     the expected error message
     */
    private void checkErrorMessageForExceptionDuringSave(RuntimeException exception, boolean reload, String error) {
        List<String> errors = new ArrayList<>();
        initErrorHandler(errors);
        Date start = DateRules.getToday();
        Date end = DateRules.getTomorrow();
        Party schedule = ScheduleTestHelper.createSchedule(TestHelper.createLocation());
        Act appointment = ScheduleTestHelper.createAppointment(start, end, schedule);

        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectEditor editor = new DefaultIMObjectEditor(appointment, context) {
            @Override
            public void save() {
                throw exception;
            }
        };
        IMObjectEditorSaver saver = new IMObjectEditorSaver() {
            protected boolean reload(IMObjectEditor editor) {
                return reload;
            }
        };
        saver.save(editor);
        assertEquals(1, errors.size());
        assertEquals(error, errors.get(0));
    }

    /**
     * Verify that the expected error message is displayed if an exception is thrown when the editor is saved.
     * <p/>
     * This saves an invalid act.customerTask, so when the archetypeService.save.act.customerAppointment.after rule
     * fires, a ValidationException will be thrown.
     *
     * @param reload if {@code true}, support reloading
     * @param error  the expected error message
     */
    private void checkErrorMessageForRuleValidationException(boolean reload, String error) {
        List<String> errors = new ArrayList<>();
        initErrorHandler(errors);
        Date start = DateRules.getToday();
        Date end = DateRules.getTomorrow();
        Party schedule = ScheduleTestHelper.createSchedule(TestHelper.createLocation());
        Act appointment = ScheduleTestHelper.createAppointment(start, end, schedule);
        Act task = ScheduleTestHelper.createTask(start, end, ScheduleTestHelper.createWorkList());

        IMObjectBean appointmentBean = getBean(appointment);      // link the appointment to a task, so the task is
        appointmentBean.addTarget("tasks", task, "appointments"); // updated by the rule when the appointment is saved.
        appointmentBean.save(task);
        IMObjectBean taskBean = getBean(task);
        taskBean.removeValues("customer");
        getArchetypeService().save(task, false);  // force saving an invalid object

        LayoutContext context = new DefaultLayoutContext(new LocalContext(), new HelpContext("foo", null));
        IMObjectEditor editor = new DefaultIMObjectEditor(appointment, context);
        editor.getProperty("status").setValue(AppointmentStatus.COMPLETED);
        IMObjectEditorSaver saver = new IMObjectEditorSaver() {
            protected boolean reload(IMObjectEditor editor) {
                return reload;
            }
        };
        saver.save(editor);
        assertEquals(1, errors.size());
        assertEquals(error, errors.get(0));
    }

}
