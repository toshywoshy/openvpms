/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.lookup;

import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.lookup.Lookup;
import org.openvpms.web.component.bound.AbstractBoundFieldTest;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.component.property.SimpleProperty;

import java.util.Arrays;

/**
 * Tests bound lookup field components.
 *
 * @author Tim Anderson
 */
public abstract class AbstractBoundLookupFieldTest<T> extends AbstractBoundFieldTest<T, String> {

    /**
     * The first lookup test value.
     */
    private static final Lookup lookup1 = new Lookup(new ArchetypeId("lookup.species"), "value1", "Species 1");

    /**
     * The second lookup test value.
     */
    private static final Lookup lookup2 = new Lookup(new ArchetypeId("lookup.species"), "value2", "Species 2");


    /**
     * Constructs an {@link AbstractBoundLookupFieldTest}.
     */
    public AbstractBoundLookupFieldTest() {
        super(lookup1.getCode(), lookup2.getCode());
    }

    /**
     * Creates a new property.
     *
     * @return a new property
     */
    protected Property createProperty() {
        return new SimpleProperty("lookup", String.class);
    }

    /**
     * Creates a new bound field.
     *
     * @param property the property to bind to
     * @return a new bound field
     */
    @Override
    protected T createField(Property property) {
        ListLookupQuery lookups = new ListLookupQuery(Arrays.asList(lookup1, lookup2));
        return createField(property, lookups);
    }

    /**
     * Creates a new bound field.
     *
     * @param property the property to bind to
     * @param lookups  the lookups
     * @return a new bound field
     */
    protected abstract T createField(Property property, LookupQuery lookups);

}
