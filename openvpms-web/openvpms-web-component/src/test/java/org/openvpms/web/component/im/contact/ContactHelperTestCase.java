/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.contact;

import org.junit.Test;
import org.openvpms.archetype.rules.party.CustomerRules;
import org.openvpms.archetype.rules.party.PartyRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.party.TestPhoneContactBuilder;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link ContactHelper} class.
 *
 * @author Tim Anderson
 */
public class ContactHelperTestCase extends ArchetypeServiceTest {

    /**
     * The customer rules.
     */
    @Autowired
    private CustomerRules rules;

    /**
     * Tests the {@link ContactHelper#abbreviatePhone(Contact, boolean, int, PartyRules, ArchetypeService)} method.
     */
    @Test
    public void testAbbreviatePhone() {
        Contact contact1 = new TestPhoneContactBuilder<>(getArchetypeService())
                .name("Joanne Smith")
                .areaCode("03")
                .phone("9444 5555")
                .build();
        Contact contact2 = new TestPhoneContactBuilder<>(getArchetypeService())
                .name("Joe")
                .areaCode("03")
                .phone("9444 5555")
                .build();
        Contact contact3 = new TestPhoneContactBuilder<>(getArchetypeService())
                .areaCode("03")
                .phone("9444 5555")
                .build();

        try {
            checkAbbreviatePhone(contact1, false, 3, null);
            fail();
        } catch (IllegalArgumentException exception) {
            assertEquals("Minimum abbreviation width is 4", exception.getMessage());
        }
        checkAbbreviatePhone(contact1, false, 4, "(...");
        checkAbbreviatePhone(contact1, false, 10, "(03) 94...");
        checkAbbreviatePhone(contact1, false, 13, "(03) 9444 ...");
        checkAbbreviatePhone(contact1, false, 14, "(03) 9444 5555");
        checkAbbreviatePhone(contact1, false, 16, "(03) 9444 5555");
        checkAbbreviatePhone(contact1, true, 4, "(...");
        checkAbbreviatePhone(contact1, true, 14, "(03) 9444 5...");
        checkAbbreviatePhone(contact1, true, 16, "(03) 9444 555...");
        checkAbbreviatePhone(contact1, true, 17, "(03) 9444 5555...");
        checkAbbreviatePhone(contact1, true, 18, "(03) 9444 5555 ...");
        checkAbbreviatePhone(contact1, true, 20, "(03) 9444 5555 (...)");
        checkAbbreviatePhone(contact1, true, 22, "(03) 9444 5555 (Jo...)");

        checkAbbreviatePhone(contact2, true, 19, "(03) 9444 5555 ...");
        checkAbbreviatePhone(contact2, true, 20, "(03) 9444 5555 (Joe)");
        checkAbbreviatePhone(contact2, true, 25, "(03) 9444 5555 (Joe)");

        // contact with a default value for the name
        checkAbbreviatePhone(contact3, true, 19, "(03) 9444 5555");

        // now with no name
        contact3.setName("");
        checkAbbreviatePhone(contact3, true, 19, "(03) 9444 5555");

        // now with null name
        contact3.setName(null);
        checkAbbreviatePhone(contact3, true, 19, "(03) 9444 5555");
    }

    /**
     * Tests the {@link ContactHelper#abbreviatePhone(Contact, boolean, int, PartyRules, ArchetypeService)} method.
     *
     * @param contact     the contact
     * @param includeName if {@code true}, include the contact name
     * @param maxLength   the maximum length of the resulting string
     * @param expected    the expected result
     */
    private void checkAbbreviatePhone(Contact contact, boolean includeName, int maxLength, String expected) {
        String actual = ContactHelper.abbreviatePhone(contact, includeName, maxLength, rules, getArchetypeService());
        assertTrue(actual.length() <= maxLength);
        assertEquals(expected, actual);
    }
}
