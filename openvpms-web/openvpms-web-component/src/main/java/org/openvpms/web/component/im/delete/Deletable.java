/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.delete;

/**
 * Determine if an object is deletable.
 *
 * @author Tim Anderson
 */
public class Deletable {

    /**
     * Determines if the object can be deleted.
     */
    private final boolean canDelete;

    /**
     * The reason the object isn't deletable.
     */
    private final String reason;

    /**
     * Constructs a {@link Deletable}.
     *
     * @param canDelete determines if the object can be deleted
     * @param reason    the reason it can't be deleted. May be {@code null}
     */
    private Deletable(boolean canDelete, String reason) {
        this.canDelete = canDelete;
        this.reason = reason;
    }

    /**
     * Determines if the object can be deleted.
     *
     * @return {@code true} if the object can be deleted, otherwise {@code false}
     */
    public boolean canDelete() {
        return canDelete;
    }

    /**
     * Returns the reason the object cannot be deleted.
     *
     * @return the reason. May be {@code null}
     */
    public String getReason() {
        return reason;
    }

    /**
     * Returns an instance indicating that an object is deletable.
     *
     * @return an instance indicating that an object can be deleted
     */
    public static Deletable yes() {
        return new Deletable(true, null);
    }

    /**
     * Returns an instance indicating that an object is not deletable.
     *
     * @return an instance indicating that an object cannot be deleted
     */
    public static Deletable no() {
        return new Deletable(false, null);
    }

    /**
     * Returns an instance indicating that an object is not deletable.
     *
     * @param reason the reason why the object can't be deleted
     * @return an instance indicating that an object cannot be deleted
     */
    public static Deletable no(String reason) {
        return new Deletable(false, reason);
    }
}
