/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.TemporaryDocumentHandler;
import org.openvpms.archetype.rules.patient.InvestigationArchetypes;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.DocumentAct;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.component.business.service.archetype.helper.DescriptorHelper;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.laboratory.resource.Content;
import org.openvpms.report.DocFormats;
import org.openvpms.report.openoffice.Converter;
import org.openvpms.web.component.app.Context;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.im.doc.DocumentGenerator;
import org.openvpms.web.component.im.doc.DocumentGeneratorFactory;
import org.openvpms.web.component.im.print.IMPrinter;
import org.openvpms.web.component.im.print.IMPrinterFactory;
import org.openvpms.web.component.im.report.ContextDocumentTemplateLocator;
import org.openvpms.web.echo.help.HelpContext;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;

import java.io.InputStream;
import java.util.function.Consumer;

/**
 * Generates attachments from {@link MailAttachment}s.
 *
 * @author Tim Anderson
 */
public class AttachmentGenerator {

    /**
     * The result of the attachment generation.
     */
    public static class Result {

        private final Document document;

        private final String error;

        private final Throwable cause;

        private Result(Document document, String error, Throwable cause) {
            this.document = document;
            this.error = error;
            this.cause = cause;
        }

        public Document getDocument() {
            return document;
        }

        public String getError() {
            return error;
        }

        public Throwable getCause() {
            return cause;
        }

        public static Result create(Document document) {
            return new Result(document, null, null);
        }

        public static Result error(String error) {
            return new Result(null, error, null);
        }

        public static Result error(Throwable cause) {
            String error = ErrorFormatter.format(cause);
            return new Result(null, error, cause);
        }
    }

    private final ArchetypeService service;

    private final DocumentGeneratorFactory documentGeneratorFactory;

    private final IMPrinterFactory printerFactory;

    private final Converter converter;

    private final Context context;

    private final HelpContext help;

    public AttachmentGenerator(ArchetypeService service, DocumentGeneratorFactory documentGeneratorFactory,
                               IMPrinterFactory printerFactory, Converter converter, Context context,
                               HelpContext help) {
        this.service = service;
        this.documentGeneratorFactory = documentGeneratorFactory;
        this.printerFactory = printerFactory;
        this.converter = converter;
        this.context = context;
        this.help = help;
    }

    /**
     * Generates an attachment.
     *
     * @param attachment the attachment
     * @param listener   the listener to invoke on completion or failure
     */
    public void generate(MailAttachment attachment, Consumer<Result> listener) {
        if (attachment instanceof ActAttachment) {
            generate(((ActAttachment) attachment).getContent(), listener);
        } else if (attachment instanceof ContentAttachment) {
            generate(((ContentAttachment) attachment).getContent(), listener);
        } else {
            listener.accept(Result.error("Unsupported attachment type"));
        }
    }

    /**
     * Generates an attachment from an act.
     *
     * @param act      the act
     * @param listener the listener to invoke on completion or failure
     */
    private void generate(Act act, Consumer<Result> listener) {
        try {
            if (act instanceof DocumentAct) {
                generateFromDocumentAct(((DocumentAct) act), listener);
            } else {
                generateFromTemplate(act, listener);
            }
        } catch (Throwable exception) {
            listener.accept(Result.error(exception));
        }
    }

    /**
     * Generates an attachment from a document act.
     *
     * @param act      the act
     * @param listener the listener to invoke on completion or failure
     */
    private void generateFromDocumentAct(DocumentAct act, Consumer<Result> listener) {
        Document document = null;
        if (act.getDocument() != null) {
            document = (Document) service.get(act.getDocument());
        }
        if (document != null) {
            String mimeType = document.getMimeType();
            if (!StringUtils.isEmpty(mimeType) && !DocFormats.PDF_TYPE.equals(mimeType)) {
                if (converter.canConvert(document, DocFormats.PDF_TYPE)) {
                    // convert it to PDF
                    document = converter.convert(document, DocFormats.PDF_TYPE, true);
                }
            }
            listener.accept(Result.create(document));
        } else if (act.isA(DocumentGenerator.LETTER, InvestigationArchetypes.PATIENT_INVESTIGATION)) {
            // Require:
            // . letters to be pre-generated as otherwise any parameter entry will be lost
            // . investigations to have report content. This is more likely to be the desired behaviour
            //   than attaching the investigation form
            String message = Messages.format("document.attachment.nocontent",
                                             DescriptorHelper.getDisplayName(act, service));
            listener.accept(Result.error(message));
        } else {
            DocumentGenerator.AbstractListener l = new DocumentGenerator.AbstractListener() {
                public void generated(Document document) {
                    listener.accept(Result.create(document));
                }
            };
            DocumentGenerator generator = documentGeneratorFactory.create(act, context, help, l);
            generator.generate();
        }
    }

    /**
     * Generates an attachment from a template associated with its archetype.
     *
     * @param act      the act
     * @param listener the listener to invoke on completion or failure
     */
    private void generateFromTemplate(Act act, Consumer<Result> listener) {
        ContextDocumentTemplateLocator locator = new ContextDocumentTemplateLocator(act, context);
        IMPrinter<Act> printer = printerFactory.create(act, locator, context);
        Document document = printer.getDocument(DocFormats.PDF_TYPE, true);
        listener.accept(Result.create(document));
    }

    /**
     * Generates an attachment from a content resource.
     *
     * @param content  the content resource
     * @param listener the listener to invoke on completion or failure
     */
    private void generate(Content content, Consumer<Result> listener) {
        try (InputStream stream = new CheckedCountingInputStream(content)) {
            TemporaryDocumentHandler handler = new TemporaryDocumentHandler(ServiceHelper.getArchetypeService());
            Document document = handler.create(content.getName(), stream, content.getMimeType(), -1);
            listener.accept(Result.create(document));
        } catch (Throwable exception) {
            listener.accept(Result.error(exception));
        }
    }

    /**
     * A stream that rejects content attachments greater than 15M. TODO - should be configurable
     */
    private static class CheckedCountingInputStream extends CountingInputStream {
        private final Content content;

        public CheckedCountingInputStream(Content content) {
            super(content.getContent());
            this.content = content;
        }

        @Override
        protected synchronized void afterRead(int n) {
            super.afterRead(n);
            if (getByteCount() > 15_000_000) {
                throw new MailException(MailException.ErrorCode.AttachmentToLarge, content.getName());
            }
        }
    }
}
