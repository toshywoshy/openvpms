/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.web.component.im.query;


/**
 * Represents the state of an {@link Browser}.
 * <p/>
 * This is used by {@link Browser} implementations to return lightweight representations of their state.
 * This may be used restore a browser to a prior state, or populate a compatible browser.
 *
 * @author Tim Anderson
 */
public interface BrowserState {

    /**
     * Determines if this state is supported by the specified browser.
     *
     * @param browser the browser
     * @return {@code true} if the state is supported by the browser; otherwise {@code false}
     */
    boolean supports(Browser<?> browser);

    /**
     * Determines if this state is supports the specified archetypes and type.
     * <p/>
     * Note that this is not as precise as the {@link #supports(Browser)} method. Two browser states may report that
     * they support the same archetypes and type but have been created by two incompatible browsers.
     *
     * @param shortNames the archetype short names
     * @param type       the type returned by the underlying query
     * @return {@code true} if the state supports the specified archetypes and type
     */
    boolean supports(String[] shortNames, Class<?> type);

    /**
     * Determines if the browser is empty.
     * <p/>
     * This occurs when no results have been queried. It does not mean that there are no results.
     *
     * @return {@code true} if the browser is empty, otherwise {@code false}
     */
    boolean isEmpty();
}
