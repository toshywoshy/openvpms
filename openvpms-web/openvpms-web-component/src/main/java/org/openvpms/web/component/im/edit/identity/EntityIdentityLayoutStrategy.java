/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit.identity;

import org.openvpms.web.component.im.layout.AbstractLayoutStrategy;
import org.openvpms.web.component.im.layout.ArchetypeNodes;

/**
 * A layout strategy for <em>entityIdentity.*</em> that suppresses the migrated node if it is empty.
 * <p/>
 * The migrated node holds migrated identities that don't conform to the identity node length requirements.
 *
 * @author Tim Anderson
 */
public class EntityIdentityLayoutStrategy extends AbstractLayoutStrategy {

    /**
     * The nodes to display. This excludes the migrated node if it is empty.
     */
    private static final ArchetypeNodes NODES = ArchetypeNodes.all().excludeIfEmpty("migrated");

    /**
     * Constructs an {@link EntityIdentityLayoutStrategy}.
     */
    public EntityIdentityLayoutStrategy() {
        super(NODES);
    }
}
