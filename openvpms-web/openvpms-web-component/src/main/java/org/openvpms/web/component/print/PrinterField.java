/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import nextapp.echo2.app.SelectField;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.web.echo.style.Styles;

import java.util.List;

/**
 * A select field for {@link PrinterReference}s.
 *
 * @author Tim Anderson
 */
public class PrinterField extends SelectField {

    /**
     * Constructs a {@link PrinterField} that shows all available printers.
     */
    public PrinterField() {
        this(new PrinterListModel());
    }

    /**
     * Constructs a {@link PrinterField}.
     *
     * @param printers the printers to select from
     */
    public PrinterField(List<PrinterReference> printers) {
        this(new PrinterListModel(printers));
    }

    /**
     * Constructs a {@link PrinterField}.
     *
     * @param model the model
     */
    public PrinterField(PrinterListModel model) {
        super(model);
        setCellRenderer(PrinterListCellRenderer.INSTANCE);
        setStyleName(Styles.DEFAULT);
    }
}


