/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.list;

import nextapp.echo2.app.Component;
import nextapp.echo2.app.list.ListCellRenderer;

/**
 * A {@link ListCellRenderer} that enables the underlying text of each cell to be returned.
 *
 * @author Tim Anderson
 */
public interface TextListCellRenderer extends ListCellRenderer {

    /**
     * Returns the text for the specified index.
     *
     * @param list  the list component
     * @param value the item value. May be {@code null}
     * @param index the item index
     * @return the text. May be {@code null}
     */
    String getText(Component list, Object value, int index);
}
