/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.list;

import nextapp.echo2.app.list.AbstractListModel;


/**
 * List model that optionally contains items for 'All', 'None', or 'Default', backed by a list.
 * TODO - replace this with a model that supports placeholders.
 *
 * @author Tim Anderson
 */
public abstract class AllNoneListModel extends AbstractListModel {


    /**
     * The index of 'All', or {@code -1} if it is not present.
     */
    private int allIndex = -1;

    /**
     * The index of 'None', or {@code -1} if it is not present.
     */
    private int noneIndex = -1;

    /**
     * The index of 'Default', or {@code -1} if it is not present.
     */
    private int defaultIndex = -1;

    /**
     * Returns the index of 'All' in the list.
     *
     * @return the index of 'All', or {@code -1} if it isn't present.
     */
    public int getAllIndex() {
        return allIndex;
    }

    /**
     * Returns the index of 'None' in the list.
     *
     * @return the index of 'None', or {@code -1} if it isn't present.
     */
    public int getNoneIndex() {
        return noneIndex;
    }

    /**
     * Returns the index of 'Default' in the list.
     *
     * @return the index of 'Default', or {@code -1} if it isn't present.
     */
    public int getDefaultIndex() {
        return defaultIndex;
    }

    /**
     * Determines if the specified index indicates 'All'.
     *
     * @param index the index
     * @return {@code true} if the index indicates 'All'
     */
    public boolean isAll(int index) {
        return index == allIndex;
    }

    /**
     * Determines if the specified index indicates 'None'.
     *
     * @param index the index
     * @return {@code true} if the index indicates 'None'
     */
    public boolean isNone(int index) {
        return index == noneIndex;
    }

    /**
     * Determines if the specified index indicates 'Default'.
     *
     * @param index the index
     * @return {@code true} if the index indicates 'Default'
     */
    public boolean isDefault(int index) {
        return index == defaultIndex;
    }

    /**
     * Sets the index of the 'All'.
     *
     * @param index the index of 'All', or {@code -1} if it is not present
     */
    protected void setAll(int index) {
        allIndex = index;
    }

    /**
     * Sets the index of the 'None'.
     *
     * @param index the index of 'None', or {@code -1} if it is not present
     */
    protected void setNone(int index) {
        noneIndex = index;
    }

    /**
     * Sets the index of the 'Default'.
     *
     * @param index the index of 'Default', or {@code -1} if it is not present
     */
    protected void setDefaultIndex(int index) {
        defaultIndex = index;
    }

}
