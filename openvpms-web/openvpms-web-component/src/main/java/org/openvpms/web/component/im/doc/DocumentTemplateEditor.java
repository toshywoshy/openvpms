/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.doc;

import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.report.DocFormats;
import org.openvpms.web.component.edit.PropertyComponentEditor;
import org.openvpms.web.component.im.layout.IMObjectLayoutStrategy;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.list.LookupListCellRenderer;
import org.openvpms.web.component.im.list.LookupListModel;
import org.openvpms.web.component.im.lookup.BoundLookupField;
import org.openvpms.web.component.im.lookup.LookupFieldFactory;
import org.openvpms.web.component.im.lookup.NodeLookupQuery;
import org.openvpms.web.component.im.view.ComponentState;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.system.ServiceHelper;


/**
 * Editor for <em>entity.documentTemplate</em>s entities.
 * <p/>
 * This supports associating a single mandatory <em>act.documentTemplate</em> with the template, representing the
 * template content.
 *
 * @author Tim Anderson
 */
public class DocumentTemplateEditor extends AbstractDocumentTemplateEditor {

    /**
     * The output format.
     */
    private final Property outputFormat;

    /**
     * The output format editor.
     */
    private final PropertyComponentEditor outputFormatEditor;

    /**
     * Determines if a letter archetype has been selected.
     */
    private boolean letter;

    /**
     * The type node name.
     */
    static final String TYPE = "type";

    /**
     * The output format node name.
     */
    static final String OUTPUT_FORMAT = "outputFormat";

    /**
     * Constructs a {@link DocumentTemplateEditor}.
     *
     * @param template the object to edit
     * @param parent   the parent object. May be {@code null}
     * @param context  the layout context. May be {@code null}
     * @throws ArchetypeServiceException for any archetype service error
     */
    public DocumentTemplateEditor(Entity template, IMObject parent, LayoutContext context) {
        super(template, parent, false, new DocumentTemplateHandler(), context);
        getProperty(TYPE).addModifiableListener(modifiable -> onTypeChanged());
        letter = isLetter();
        outputFormat = getProperty(OUTPUT_FORMAT);

        // create the output format editor. Only enable it if the template is a Letter
        BoundLookupField field = new BoundLookupField(outputFormat,
                                                      new LookupListModel(new NodeLookupQuery(template, outputFormat),
                                                                          false, false, true));
        LookupFieldFactory.setDefaultStyle(field);
        field.setCellRenderer(LookupListCellRenderer.INSTANCE);
        field.setEnabled(letter);
        outputFormatEditor = new PropertyComponentEditor(outputFormat, field);
    }

    /**
     * Creates the layout strategy.
     *
     * @return a new layout strategy
     */
    @Override
    protected IMObjectLayoutStrategy createLayoutStrategy() {
        DocumentTemplateLayoutStrategy strategy = new DocumentTemplateLayoutStrategy(getSelector());
        strategy.addComponent(new ComponentState(outputFormatEditor));
        return strategy;
    }

    /**
     * Determines if the template type is a letter
     *
     * @param template the template
     * @return {@code true} if the template type is a letter archetype, otherwise {@code false}
     */
    protected static boolean isLetter(IMObject template) {
        boolean result = false;
        Lookup type = ServiceHelper.getArchetypeService().getBean(template).getObject(TYPE, Lookup.class);
        if (type != null) {
            String code = type.getCode();
            if (PatientArchetypes.DOCUMENT_LETTER.equals(code) || code.equals(CustomerArchetypes.DOCUMENT_LETTER)
                || code.equals(SupplierArchetypes.DOCUMENT_LETTER)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Determines if a letter archetype has been selected.
     *
     * @return {@code true} if a letter archetype has been selected
     */
    private boolean isLetter() {
        return isLetter(getObject());
    }

    /**
     * Invoked when the type changes.
     * <p/>
     * Disables the outputFormatEditor if the template type isn't a letter.
     */
    private void onTypeChanged() {
        boolean newLetter = isLetter();
        if (newLetter != letter) {
            letter = newLetter;
            if (!letter) {
                // output format not supported for other template types
                outputFormat.setValue(null);
            }
            outputFormatEditor.getComponent().setEnabled(letter);
        }
    }

    private static class DocumentTemplateHandler extends SupportedContentDocumentHandler {

        private static final String[] SUPPORTED_EXTENSIONS = {DocFormats.ODT_EXT, DocFormats.DOC_EXT,
                                                              DocFormats.JRXML_EXT, DocFormats.RTF_EXT};

        private static final String[] SUPPORTED_MIME_TYPES = {DocFormats.ODT_TYPE, DocFormats.DOC_TYPE,
                                                              DocFormats.RTF_TYPE};

        public DocumentTemplateHandler() {
            super(SUPPORTED_EXTENSIONS, SUPPORTED_MIME_TYPES, ServiceHelper.getArchetypeService());
        }
    }

}