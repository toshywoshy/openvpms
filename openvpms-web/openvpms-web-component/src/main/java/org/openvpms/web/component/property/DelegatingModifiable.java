/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

/**
 * A {@link Modifiable} that delegates to another.
 *
 * @author Tim Anderson
 */
public class DelegatingModifiable implements Modifiable {

    /**
     * The modifiable instance to delegate to.
     */
    private Modifiable modifiable;

    /**
     * Constructs a {@link DelegatingModifiable}.
     *
     * @param modifiable the modifiable instance to delegate to
     */
    public DelegatingModifiable(Modifiable modifiable) {
        this.modifiable = modifiable;
    }

    /**
     * Default constructor provided for lazy initialisation.
     * <p/>
     * The {@link #setModifiable(Modifiable)} must be invoked before performing any other operation.
     */
    protected DelegatingModifiable() {
        super();
    }

    /**
     * Determines if the object has been modified.
     *
     * @return {@code true} if the object has been modified
     */
    @Override
    public boolean isModified() {
        return modifiable.isModified();
    }

    /**
     * Clears the modified status of the object.
     */
    @Override
    public void clearModified() {
        modifiable.clearModified();
    }

    /**
     * Adds a listener to be notified when this changes.
     * <p/>
     * Listeners will be notified in the order they were registered.
     * <p/>
     * Duplicate additions are ignored.
     *
     * @param listener the listener to add
     */
    @Override
    public void addModifiableListener(ModifiableListener listener) {
        modifiable.addModifiableListener(listener);
    }

    /**
     * Adds a listener to be notified when this changes, specifying the order of the listener.
     *
     * @param listener the listener to add
     * @param index    the index to add the listener at. The 0-index listener is notified first
     */
    @Override
    public void addModifiableListener(ModifiableListener listener, int index) {
        modifiable.addModifiableListener(listener, index);
    }

    /**
     * Removes a listener.
     *
     * @param listener the listener to remove
     */
    @Override
    public void removeModifiableListener(ModifiableListener listener) {
        modifiable.removeModifiableListener(listener);
    }

    /**
     * Sets a listener to be notified of errors.
     *
     * @param listener the listener to register. May be {@code null}
     */
    @Override
    public void setErrorListener(ErrorListener listener) {
        modifiable.setErrorListener(listener);
    }

    /**
     * Returns the listener to be notified of errors.
     *
     * @return the listener. May be {@code null}
     */
    @Override
    public ErrorListener getErrorListener() {
        return modifiable.getErrorListener();
    }

    /**
     * Determines if the object is valid.
     *
     * @return {@code true} if the object is valid; otherwise {@code false}
     */
    @Override
    public boolean isValid() {
        return modifiable.isValid();
    }

    /**
     * Validates the object.
     *
     * @param validator the validator
     * @return {@code true} if the object and its descendants are valid otherwise {@code false}
     */
    @Override
    public boolean validate(Validator validator) {
        return modifiable.validate(validator);
    }

    /**
     * Resets the cached validity state of the object, to force revalidation of the object and its descendants.
     */
    @Override
    public void resetValid() {
        modifiable.resetValid();
    }

    protected void setModifiable(Modifiable modifiable) {
        this.modifiable = modifiable;
    }

}
