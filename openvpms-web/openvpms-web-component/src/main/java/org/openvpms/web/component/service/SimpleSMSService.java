/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.service;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.customer.CustomerArchetypes;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.exception.SMSException;
import org.openvpms.sms.message.OutboundMessage;
import org.openvpms.sms.service.SMSService;
import org.openvpms.web.component.im.sms.SMSHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple SMS service.
 *
 * @author Tim Anderson
 */
public class SimpleSMSService {

    /**
     * The SMS service.
     */
    private final SMSService smsService;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SimpleSMSService.class);

    /**
     * Constructs a {@link SimpleSMSService}.
     *
     * @param smsService the SMS service to delegate to
     * @param service    the archetype service
     */
    public SimpleSMSService(SMSService smsService, IArchetypeService service) {
        this.smsService = smsService;
        this.service = service;
    }

    /**
     * Determines if SMS support is enabled.
     *
     * @return {@code true} if SMS support is enabled, otherwise {@code false}
     */
    public boolean isEnabled() {
        return smsService.isEnabled();
    }

    /**
     * Returns the number of parts that a message comprises.
     * <p/>
     * The number of parts is determined by the length of the message, and how the message will be encoded.
     * <p/>
     * A message that can be encoded using 7-bit GSM characters will use:
     * <ul>
     * <li>a single part for up to 160 characters</li>
     * <li>multiples of 153 characters for multi-part messages</li>
     * </ul>
     * A message encoded using UCS-2 will use:
     * <ul>
     * <li>a single part for up to 70 characters</li>
     * <li>multiples of 67 characters for multi-part messages</li>
     * </ul>
     *
     * @param message the message
     * @return the number of parts
     */
    public int getParts(String message) {
        return smsService.getParts(message);
    }

    /**
     * Returns the maximum number of message parts supported by the SMS provider.
     *
     * @return the maximum number of message parts
     */
    public int getMaxParts() {
        return smsService.getMaxParts();
    }

    /**
     * Sends an SMS.
     *
     * @param message   the SMS text
     * @param contact   the phone contact
     * @param recipient the party associated with the phone number. May be {@code null}
     * @param subject   the subject of the SMS, for communication logging purposes
     * @param reason    the reason of the SMS, for communication logging purposes
     * @param location  the practice location
     * @throws IllegalArgumentException if the phone contact is incomplete
     * @throws SMSException             if the send fails
     */
    public void send(String message, Contact contact, Party recipient, String subject, String reason, Party location) {
        send(message, contact, recipient, null, subject, reason, location);
    }

    /**
     * Sends an SMS.
     *
     * @param message   the SMS text
     * @param contact   the phone contact
     * @param recipient the party associated with the phone number. May be {@code null}
     * @param patient   the patient the SMS refers to. May be {@code null}
     * @param subject   the subject of the SMS, for communication logging purposes
     * @param reason    the reason of the SMS, for communication logging purposes
     * @param location  the practice location
     * @throws IllegalArgumentException if the phone contact is incomplete
     * @throws SMSException             if the send fails
     */
    public void send(String message, Contact contact, Party recipient, Party patient, String subject, String reason,
                     Party location) {
        IMObjectBean bean = service.getBean(contact);
        String telephoneNumber = bean.getString("telephoneNumber");
        if (StringUtils.isEmpty(telephoneNumber)) {
            throw new IllegalArgumentException("Argument 'contact' doesn't have a telephoneNumber");
        }
        String phone = SMSHelper.getPhone(contact);
        send(phone, message, recipient, patient, contact, subject, reason, location);
    }

    /**
     * Sends an SMS.
     *
     * @param phone     the phone number to send the SMS to
     * @param message   the SMS text
     * @param recipient the recipient associated with the phone number. May be {@code null}
     * @param patient   the patient the SMS refers to. May be {@code null}
     * @param contact   the phone contact. May be {@code null}
     * @param subject   the subject of the SMS, for communication logging purposes
     * @param reason    the reason of the SMS, for communication logging purposes
     * @param location  the practice location. May be {@code null}
     */
    public void send(String phone, String message, Party recipient, Party patient, Contact contact, String subject,
                     String reason, Party location) {
        if (log.isDebugEnabled()) {
            String p = (recipient != null) ? recipient.getName() + " (" + recipient.getId() + ")" : null;
            String c = (contact != null) ? contact.getDescription() + " (" + contact.getId() + ")" : null;
            String l = (location != null) ? location.getName() + " (" + location.getId() + ")" : null;
            log.debug("SMS: phone=" + phone + ", message='" + message + "', party=" + p + ", contact=" + c
                      + ", subject=" + subject + ", reason=" + reason + ", location=" + l);
        }
        if (TypeHelper.isA(recipient, CustomerArchetypes.PERSON)) {
            sendCustomerSMS(phone, message, recipient, patient, subject, reason, null, location);
        } else {
            sendGeneralSMS(phone, message, recipient, location);
        }
    }

    /**
     * Sends an SMS to a customer.
     *
     * @param phone    the phone number to send the SMS to
     * @param message  the SMS text
     * @param customer the customer associated with the phone number
     * @param patient  the patient the SMS refers to. May be {@code null}
     * @param contact  the phone contact. May be {@code null}
     * @param subject  the subject of the SMS, for communication logging purposes
     * @param reason   the reason of the SMS, for communication logging purposes
     * @param note     a note about the SMS, for communication logging purposes
     * @param location the practice location. May be {@code null}
     * @throws SMSException if the send fails
     */
    public void send(String phone, String message, Party customer, Party patient, Contact contact, String subject,
                     String reason, String note, Party location) {
        if (log.isDebugEnabled()) {
            String p = (patient != null) ? patient.getName() + " (" + patient.getId() + ")" : null;
            String c = (contact != null) ? contact.getDescription() + " (" + contact.getId() + ")" : null;
            String l = (location != null) ? location.getName() + " (" + location.getId() + ")" : null;
            log.debug("SMS: phone=" + phone + ", message='" + message + "', customer=" + customer.getName()
                      + "(" + customer.getId() + ")" + p + ", contact=" + c + ", subject=" + subject
                      + ", reason=" + reason + ", location=" + l);
        }
        sendCustomerSMS(phone, message, customer, patient, subject, reason, note, location);
    }

    /**
     * Sends an SMS.
     *
     * @param phone   the phone number
     * @param message the SMS text
     * @throws SMSException if the send fails
     */
    protected void send(String phone, String message, org.openvpms.component.model.party.Party location) {
        OutboundMessage sms = smsService.getOutboundMessageBuilder().phone(phone).message(message).location(location).build();
        smsService.send(sms);
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }

    /**
     * Sends a message to a non-customer recipient.
     *
     * @param phone     the phone number to send the SMS to
     * @param message   the SMS text
     * @param recipient the recipient associated with the phone number. May be {@code null}
     * @param location  the practice location. May be {@code null}
     */
    private void sendGeneralSMS(String phone, String message, Party recipient, Party location) {
        OutboundMessage sms = smsService.getOutboundMessageBuilder()
                .phone(phone)
                .message(message)
                .recipient(recipient)
                .location(location)
                .build();
        smsService.send(sms);
    }

    /**
     * Sends a customer SMS.
     *
     * @param phone    the phone
     * @param message  the message
     * @param customer the customer
     * @param patient  the patient
     * @param subject  the subject of the SMS, for communication logging purposes
     * @param reason   the reason of the SMS, for communication logging purposes
     * @param note     a note about the SMS, for communication logging purposes
     * @param location the practice location. May be {@code null}
     */
    private void sendCustomerSMS(String phone, String message, Party customer, Party patient, String subject,
                                 String reason, String note, Party location) {
        OutboundMessage sms = smsService.getOutboundMessageBuilder()
                .phone(phone)
                .message(message)
                .recipient(customer)
                .patient(patient)
                .subject(subject)
                .reason(reason)
                .note(note)
                .location(location)
                .build();
        smsService.send(sms);
    }
}
