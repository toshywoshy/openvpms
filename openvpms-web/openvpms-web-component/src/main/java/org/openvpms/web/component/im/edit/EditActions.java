/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.openvpms.web.echo.dialog.PopupDialog.APPLY_ID;
import static org.openvpms.web.echo.dialog.PopupDialog.CANCEL_ID;
import static org.openvpms.web.echo.dialog.PopupDialog.OK_ID;
import static org.openvpms.web.echo.dialog.PopupDialog.SKIP_ID;

/**
 * Dialog actions for edit dialogs.
 *
 * @author Tim Anderson
 */
public class EditActions {

    /**
     * The buttons to display before standard Apply/OK/Cancel/Skip. NOTE: may contain any of these.
     */
    private final Set<String> buttons = new LinkedHashSet<>();

    /**
     * The buttons to display after standard Apply/OK/Cancel/Skip. NOTE: may contain any of these.
     */
    private final Set<String> and = new LinkedHashSet<>();

    /**
     * Determines if the object being edited can be saved.
     */
    private boolean save;

    /**
     * Determines if the Apply/OK buttons save the object.
     */
    private boolean userSave;

    /**
     * Determines if an Apply button is displayed.
     */
    private boolean showApply;

    /**
     * Determines if an OK button is displayed.
     */
    private boolean showOK;

    /**
     * Determines if a Cancel button is displayed.
     */
    private boolean showCancel;

    /**
     * Determines if a Skip button is displayed.
     */
    private boolean showSkip;

    /**
     * Default constructor.
     */
    private EditActions() {
        this.save = true;
        this.userSave = true;
    }

    /**
     * Determines if the object being edited can be saved under any circumstances.
     *
     * @param save if {@code true}, the object can be saved, otherwise it will not be saved
     * @return this
     */
    public EditActions setSave(boolean save) {
        this.save = save;
        return this;
    }

    /**
     * Determines if the object being edited can be saved under any circumstances.
     *
     * @return {@code true} if the object can be saved, otherwise {@code false}. Defaults to {@code true}
     */
    public boolean save() {
        return save;
    }

    /**
     * Determines if the object is saved when the user clicks the OK/Apply buttons.
     * <p/>
     * This is subject to the {@link #save()} being {@code true}
     *
     * @param save if {@code true}, the Apply/OK buttons save the object
     * @return this
     */
    public EditActions setUserSave(boolean save) {
        this.userSave = save;
        return this;
    }

    /**
     * Determines if the object is saved when the user clicks the OK/Apply buttons.
     * <p/>
     * This is subject to the {@link #save()} being {@code true}
     *
     * @return {@code true} to allow the Apply/OK buttons to save the object, otherwise {@code false}. Defaults to
     * {@code true}
     */
    public boolean userSave() {
        return userSave;
    }

    /**
     * Determines if the Apply button should be displayed.
     *
     * @param showApply if {@code true}, display the Apply button, otherwise suppress it
     * @return this
     */
    public EditActions setShowApply(boolean showApply) {
        this.showApply = showApply;
        return this;
    }

    /**
     * Determines if the OK button should be displayed.
     *
     * @param showOK if {@code true}, display the OK button, otherwise suppress it
     * @return this
     */
    public EditActions setShowOK(boolean showOK) {
        this.showOK = showOK;
        return this;
    }

    /**
     * Determines if the Skip button should be displayed.
     *
     * @param showSkip if {@code true}, display the Skip button, otherwise suppress it
     * @return this
     */
    public EditActions setShowSkip(boolean showSkip) {
        this.showSkip = showSkip;
        return this;
    }

    /**
     * Determines if the Cancel button should be displayed.
     *
     * @param showCancel if {@code true}, display the Cancel button otherwise suppress it
     * @return this
     */
    public EditActions setShowCancel(boolean showCancel) {
        this.showCancel = showCancel;
        return this;
    }

    /**
     * Adds buttons that will be displayed before the standard Apply/OK/Cancel/Skip.
     * <p/>
     * If this list contains, the Apply, OK, Cancel or Skip button identifier, the corresponding flag will be set.
     *
     * @param buttons the buttons
     * @return this
     */
    public EditActions add(String... buttons) {
        this.buttons.addAll(Arrays.asList(buttons));
        updateFlags(this.buttons);
        return this;
    }

    /**
     * Adds buttons after the standard Apply/OK/Cancel/Skip.
     * <p/>
     * If this list contains, the Apply, OK, Cancel or Skip button identifier, the corresponding flag will be set.
     *
     * @param buttons the buttons to add
     * @return this
     */
    public EditActions and(String... buttons) {
        and.addAll(Arrays.asList(buttons));
        updateFlags(and);
        return this;
    }

    /**
     * Returns all of the buttons to display.
     *
     * @return the button identifiers
     */
    public String[] getButtons() {
        Set<String> result = new LinkedHashSet<>(buttons);
        List<String> after = new ArrayList<>(and);

        addOrRemove(result, after, APPLY_ID, showApply);
        addOrRemove(result, after, OK_ID, showOK);
        addOrRemove(result, after, SKIP_ID, showSkip);
        addOrRemove(result, after, CANCEL_ID, showCancel);
        result.addAll(after);
        return result.toArray(new String[0]);
    }

    /**
     * Creates a {link EditActions} with the buttons in the specified order.
     * <p/>
     * These will be displayed before Apply/OK/Cancel/Skip.
     *
     * @param buttons the buttons to display
     * @return a new {@link EditActions}
     */
    public static EditActions buttons(String... buttons) {
        EditActions actions = new EditActions();
        actions.add(buttons);
        return actions;
    }

    /**
     * Creates a {link EditActions} with an OK button.
     *
     * @return a new {@link EditActions}
     */
    public static EditActions ok() {
        return new EditActions().setShowOK(true);
    }

    /**
     * Creates a {link EditActions} with a Cancel button.
     *
     * @return a new {@link EditActions}
     */
    public static EditActions cancel() {
        return new EditActions().setShowCancel(true);
    }

    /**
     * Creates a {link EditActions} with an OK and Cancel button.
     *
     * @return a new {@link EditActions}
     */
    public static EditActions okCancel() {
        return ok().setShowCancel(true);
    }

    /**
     * Creates a {link EditActions} with an Apply, OK and Cancel button.
     *
     * @return a new {@link EditActions}
     */
    public static EditActions applyOKCancel() {
        return okCancel().setShowApply(true);
    }

    /**
     * Creates a {link EditActions} with an Apply, OK, Cancel and Skip button.
     *
     * @return a new {@link EditActions}
     */
    public static EditActions applyOKCancelSkip() {
        return applyOKCancel().setShowSkip(true);
    }

    /**
     * Adds or removes a button, based on its show flag.
     *
     * @param buttons the set of buttons to add to
     * @param after   the buttons that will be added after the set
     * @param button  the button identifier
     * @param show    if {@code true} show the button, else remove it.
     */
    private void addOrRemove(Set<String> buttons, List<String> after, String button, boolean show) {
        if (show) {
            if (!buttons.contains(button) && !after.contains(button)) {
                buttons.add(button);
            }
        } else {
            buttons.remove(button);
            after.remove(button);
        }
    }

    /**
     * Updates the various show flags based on the presence of button identifiers.
     */
    private void updateFlags(Set<String> buttons) {
        showApply = showApply || buttons.contains(APPLY_ID);
        showOK = showOK || buttons.contains(OK_ID);
        showCancel = showCancel || buttons.contains(CANCEL_ID);
        showSkip = showSkip || buttons.contains(SKIP_ID);
    }

}
