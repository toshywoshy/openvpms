/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.edit;

import nextapp.echo2.app.event.WindowPaneEvent;
import org.openvpms.component.business.service.archetype.ValidationException;
import org.openvpms.web.component.error.ErrorFormatter;
import org.openvpms.web.component.error.ExceptionHelper;
import org.openvpms.web.component.property.DefaultValidator;
import org.openvpms.web.component.property.ValidationHelper;
import org.openvpms.web.component.property.Validator;
import org.openvpms.web.component.util.ErrorHelper;
import org.openvpms.web.echo.error.ErrorHandler;
import org.openvpms.web.echo.event.WindowPaneListener;
import org.openvpms.web.resource.i18n.Messages;
import org.openvpms.web.system.ServiceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Helper to manage saving editors within transactions.
 *
 * @author Tim Anderson
 */
public class IMObjectEditorSaver {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(IMObjectEditorSaver.class);

    /**
     * Saves an editor.
     *
     * @param editor the editor to save
     * @return {@code true} if the editor was successfully saved
     */
    public boolean save(IMObjectEditor editor) {
        return save(editor, false);
    }

    /**
     * Saves an editor.
     *
     * @param editor          the editor to save
     * @param suppressDialogs if {@code true}, suppress any error dialogs
     * @return {@code true} if the editor was successfully saved
     */
    public boolean save(IMObjectEditor editor, boolean suppressDialogs) {
        return save(editor, suppressDialogs, null);
    }

    /**
     * Saves an editor.
     *
     * @param editor   the editor to save
     * @param listener the listener to notify when the error dialog closes, if the save was unsuccessful
     * @return {@code true} if the editor was successfully saved
     */
    public boolean save(IMObjectEditor editor, Runnable listener) {
        return save(editor, false, listener);
    }

    /**
     * Saves an editor.
     * <p/>
     * If the editor, fails to save, and error dialogs are not being suppressed, {@link #reload(IMObjectEditor)} will
     * be used to re-load
     *
     * @param editor          the editor to save
     * @param suppressDialogs if {@code true}, suppress any error dialogs
     * @param listener        the listener to notify when the error dialog closes, if the save was unsuccessful
     * @return {@code true} if the editor was successfully saved
     */
    protected boolean save(IMObjectEditor editor, boolean suppressDialogs, Runnable listener) {
        boolean result;
        TransactionTemplate template = new TransactionTemplate(ServiceHelper.getTransactionManager());
        try {
            // perform validation and save in the one transaction
            Boolean saved = template.execute(transactionStatus -> validateAndSave(editor, transactionStatus, listener,
                                                                                  suppressDialogs));
            result = saved != null && saved;
            if (result) {
                editor.committed();
            }
        } catch (Throwable exception) {
            result = false;
            log(editor, exception);
            if (!suppressDialogs) {
                reloadOnError(editor, exception, listener);
            }
        }
        return result;
    }

    /**
     * Creates a validator to validate an editor.
     *
     * @return a new validator
     */
    protected Validator createValidator() {
        return new DefaultValidator();
    }

    /**
     * Displays validation errors.
     *
     * @param validator the validator
     * @param listener  the listener to notify when the error dialog closes. May be {@code null}
     */
    protected void showError(Validator validator, Runnable listener) {
        WindowPaneListener closeListener = null;
        if (listener != null) {
            closeListener = new WindowPaneListener() {
                @Override
                public void onClose(WindowPaneEvent event) {
                    listener.run();
                }
            };
        }
        ValidationHelper.showError(null, validator, closeListener);
    }

    /**
     * Saves the editor.
     *
     * @param editor the editor to save
     * @param status the transaction status
     */
    protected void save(IMObjectEditor editor, TransactionStatus status) {
        editor.save();
    }

    /**
     * Invoked to reload the object if the save fails.
     *
     * @param editor the editor
     * @return {@code true} if the object was reloaded. This implementation always returns {@code false}
     */
    protected boolean reload(IMObjectEditor editor) {
        return false;
    }

    /**
     * Invoked to display a message that saving failed, and the editor has been reverted.
     *
     * @param title     the message title
     * @param message   the message
     * @param oldEditor the previous instance of the editor
     * @param listener  the listener to notify when the error dialog closes. May be {@code null}
     */
    protected void reloaded(String title, String message, IMObjectEditor oldEditor, Runnable listener) {
        WindowPaneListener closeListener = null;
        if (listener != null) {
            closeListener = new WindowPaneListener() {
                @Override
                public void onClose(WindowPaneEvent event) {
                    listener.run();
                }
            };
        }
        ErrorHandler.getInstance().error(title, message, null, closeListener);
    }

    /**
     * Invoked when saving fails.
     * <p/>
     * This implementation displays the error.
     *
     * @param editor    the editor
     * @param exception the cause of the failure
     * @param listener  the listener to notify when the error dialog closes. May be {@code null}
     */
    protected void failed(IMObjectEditor editor, Throwable exception, Runnable listener) {
        String displayName = editor.getDisplayName();
        String title = Messages.format("imobject.save.failed", displayName);
        ErrorHelper.show(title, displayName, editor.getObject(), exception, listener);
    }

    /**
     * Creates a new instance of the editor, with the latest instance of the object to edit.
     *
     * @return a new instance, or {@code null} if new instances are not supported
     */
    protected IMObjectEditor newInstance(IMObjectEditor editor) {
        IMObjectEditor newEditor = null;
        try {
            newEditor = editor.newInstance();
        } catch (Throwable exception) {
            log.error("Failed to create a new editor instance", exception);
        }
        return newEditor;
    }

    /**
     * Determines if an exception indicates that the object being edited (or a related object) was modified externally.
     *
     * @param exception the exception
     * @return {@code true} if the object was modified externally
     */
    protected boolean isModifiedExternally(Throwable exception) {
        return ExceptionHelper.isModifiedExternally(exception);
    }

    /**
     * Logs a save error.
     *
     * @param editor    the editor
     * @param exception the exception
     */
    protected void log(IMObjectEditor editor, Throwable exception) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String user = (authentication != null) ? authentication.getName() : null;

        String context = Messages.format("logging.error.editcontext", editor.getObject().getObjectReference(),
                                         editor.getClass().getName(), user);
        String message = ErrorFormatter.format(exception, editor.getDisplayName());
        log.error(Messages.format("logging.error.messageandcontext", message, context), exception);
    }

    /**
     * Invoked to attempt to reload the object when saving has failed.
     *
     * @param editor    the editor
     * @param exception the reason for the save failing
     * @param listener  the listener to notify when the error dialog closes. May be {@code null}
     */
    private void reloadOnError(IMObjectEditor editor, Throwable exception, Runnable listener) {
        // attempt to reload the editor
        if (reload(editor)) {
            // reloaded the editor to the last saved state
            String displayName = editor.getDisplayName();
            Throwable rootCause = ExceptionHelper.getRootCause(exception);
            String message;
            if (rootCause instanceof ValidationException) {
                message = Messages.format("imobject.save.reverted.validationerror", displayName,
                                          ErrorFormatter.format(rootCause));
            } else if (isModifiedExternally(rootCause)) {
                message = Messages.format("imobject.save.reverted.modified", displayName);
            } else {
                message = Messages.format("imobject.save.reverted.error", displayName);
            }
            String title = Messages.format("imobject.save.failed", displayName);
            reloaded(title, message, editor, listener);
        } else {
            failed(editor, exception, listener);
        }
    }

    /**
     * Saves the editor if it is valid. This is designed to be called from within a transaction.
     *
     * @param editor            the editor
     * @param transactionStatus the transaction status
     * @param listener          the listener to notify on completion, if an error dialog was displayed
     * @param suppressDialogs   if {@code true}, suppress any error dialogs
     * @return {@code true} if the editor was saved, otherwise {@code false}
     */
    private boolean validateAndSave(IMObjectEditor editor, TransactionStatus transactionStatus, Runnable listener,
                                    boolean suppressDialogs) {
        Validator validator = createValidator();
        boolean result = editor.validate(validator);
        if (result) {
            save(editor, transactionStatus);
        } else if (!suppressDialogs) {
            showError(validator, listener);
        }
        return result;
    }

}
