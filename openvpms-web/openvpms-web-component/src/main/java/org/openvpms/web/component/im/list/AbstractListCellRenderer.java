/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.list;

import nextapp.echo2.app.Component;
import org.openvpms.web.resource.i18n.Messages;


/**
 * List cell renderer that renders special 'All', 'None' and 'Default' objects in bold.
 * TODO - replace this with a model that supports placeholders.
 *
 * @author Tim Anderson
 */
public abstract class AbstractListCellRenderer<T> implements TextListCellRenderer {

    /**
     * The type being rendered.
     */
    private final Class<T> type;

    /**
     * Localised display name for "all".
     */
    private final String ALL = Messages.get("list.all");

    /**
     * Localised display name for "none".
     */
    private final String NONE = Messages.get("list.none");

    /**
     * Localised display name for "default".
     */
    private final String DEFAULT = Messages.get("list.default");

    /**
     * Constructs an {@link AbstractListCellRenderer}.
     *
     * @param type the type that this can render
     */
    public AbstractListCellRenderer(Class<T> type) {
        this.type = type;
    }

    /**
     * Renders an item in a list.
     *
     * @param list  the list component
     * @param value the item value. May be {@code null}
     * @param index the item index
     * @return the rendered form of the list cell
     */
    public Object getListCellRendererComponent(Component list, Object value, int index) {
        Object result = null;
        if (value == null || type.isAssignableFrom(value.getClass())) {
            T object = type.cast(value);
            if (isAll(list, object, index)) {
                result = new BoldListCell(getAll());
            } else if (isNone(list, object, index)) {
                result = new BoldListCell(getNone());
            } else if (isDefault(list, object, index)) {
                result = new BoldListCell(getDefault());
            } else {
                result = getComponent(list, object, index);
            }
        }
        if (result == null) {
            result = "";
        }
        return result;
    }

    /**
     * Returns the text for the specified index.
     *
     * @param list  the list component
     * @param value the item value. May be {@code null}
     * @param index the item index
     * @return the text. May be {@code null}
     */
    @Override
    public String getText(Component list, Object value, int index) {
        String result = null;
        if (value == null || type.isAssignableFrom(value.getClass())) {
            T object = type.cast(value);
            if (isAll(list, object, index)) {
                result = getAll();
            } else if (isNone(list, object, index)) {
                result = getNone();
            } else if (isDefault(list, object, index)) {
                result = getDefault();
            } else {
                result = toString(list, object, index);
            }
        }
        if (result == null) {
            result = "";
        }
        return result;
    }

    /**
     * Returns the text representing 'All'.
     *
     * @return the text
     */
    protected String getAll() {
        return ALL;
    }

    /**
     * Returns the text representing 'None'.
     *
     * @return the text
     */
    protected String getNone() {
        return NONE;
    }

    /**
     * Returns the text representing 'Default'.
     *
     * @return the text
     */
    protected String getDefault() {
        return DEFAULT;
    }

    /**
     * Renders an object.
     *
     * @param list   the list component
     * @param object the object to render. May be {@code null}
     * @param index  the object index
     * @return the rendered object
     */
    protected Object getComponent(Component list, T object, int index) {
        return toString(list, object, index);
    }

    /**
     * Returns the string form of the object at the specified cell.
     *
     * @param list   the list component
     * @param object the object to render. May be {@code null}
     * @param index  the object index
     * @return the text. May be {@code null}
     */
    protected abstract String toString(Component list, T object, int index);

    /**
     * Determines if an object represents 'All'.
     *
     * @param list   the list component
     * @param object the object. May be {@code null}
     * @param index  the object index
     * @return {@code true} if the object represents 'All'.
     */
    protected abstract boolean isAll(Component list, T object, int index);

    /**
     * Determines if an object represents 'None'.
     *
     * @param list   the list component
     * @param object the object. May be {@code null}
     * @param index  the object index
     * @return {@code true} if the object represents 'None'.
     */
    protected abstract boolean isNone(Component list, T object, int index);

    /**
     * Determines if an object represents 'Default'.
     *
     * @param list   the list component
     * @param object the object. May be {@code null}
     * @param index  the object index
     * @return {@code true} if the object represents 'Default'.
     */
    protected boolean isDefault(Component list, T object, int index) {
        return false;
    }
}
