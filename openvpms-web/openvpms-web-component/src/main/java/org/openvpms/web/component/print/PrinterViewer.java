/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.print;

import nextapp.echo2.app.Component;
import org.openvpms.archetype.rules.doc.PrinterReference;
import org.openvpms.web.component.im.layout.LayoutContext;
import org.openvpms.web.component.im.view.TableComponentFactory;
import org.openvpms.web.component.property.Property;
import org.openvpms.web.echo.factory.LabelFactory;
import org.openvpms.web.echo.factory.TextComponentFactory;
import org.openvpms.web.echo.text.TextField;

/**
 * Viewer for properties representing the stringified {@link PrinterReference}.
 *
 * @author Tim Anderson
 */
public class PrinterViewer {

    /**
     * The printer component.
     */
    private final Component component;

    /**
     * Constructs a {@link PrinterViewer}.
     *
     * @param property the property
     * @param context  the layout context
     */
    public PrinterViewer(Property property, LayoutContext context) {
        this(property.getString(), context);
    }

    /**
     * Constructs a {@link PrinterViewer}.
     *
     * @param printer the stringified printer reference. May be {@code null}
     * @param context the layout context
     */
    public PrinterViewer(String printer, LayoutContext context) {
        PrinterReference reference = PrinterReference.fromString(printer);
        StringBuilder builder = new StringBuilder();
        if (reference != null) {
            if (reference.getArchetype() != null && reference.getServiceName() != null) {
                builder.append(reference.getServiceName());
                builder.append(": ");
            }
            builder.append(reference.getName());
        }
        if (context.getComponentFactory() instanceof TableComponentFactory) {
            // when displayed in a table, display as a label
            component = LabelFactory.text(builder.toString());
        } else {
            TextField field = TextComponentFactory.create(50);
            field.setStyleName(context.getComponentFactory().getReadOnlyStyle());
            field.setText(builder.toString());
            component = field;
        }
    }

    /**
     * Returns the component.
     *
     * @return the component
     */
    public Component getComponent() {
        return component;
    }
}
