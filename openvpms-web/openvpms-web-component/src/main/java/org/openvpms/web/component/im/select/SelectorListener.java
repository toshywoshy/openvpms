/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.im.select;

import org.openvpms.web.component.im.query.Browser;


/**
 * Listener for {@link Selector} events.
 *
 * @author Tim Anderson
 */
public interface SelectorListener<T> extends java.util.EventListener {

    /**
     * Invoked when the selected object changes.
     *
     * @param object the object. May be {@code null}
     */
    void selected(T object);

    /**
     * Invoked when the selected object changes, selected from a browser.
     *
     * @param object  the object. May be {@code null}
     * @param browser the browser
     */
    void selected(T object, Browser<T> browser);

    /**
     * Invoked to create a new object.
     */
    void create();
}
