/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Abstract implementation of {@link PropertySet}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractPropertySet implements PropertySet {

    /**
     * Returns the editable properties.
     * <p/>
     * These are the non-hidden, modifiable properties.
     *
     * @return the editable properties
     */
    @Override
    public Collection<Property> getEditable() {
        List<Property> result = new ArrayList<>();
        for (Property property : getProperties()) {
            if (!property.isHidden() && !property.isReadOnly() && !property.isDerived()) {
                result.add(property);
            }
        }
        return result;
    }

    /**
     * Determines if any of the properties have been modified.
     *
     * @return {@code true} if at least one property has been modified
     */
    @Override
    public boolean isModified() {
        for (Property property : getProperties()) {
            if (property.isModified()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Clears the modified status of all properties.
     */
    @Override
    public void clearModified() {
        for (Property property : getProperties()) {
            property.clearModified();
        }
    }
}
