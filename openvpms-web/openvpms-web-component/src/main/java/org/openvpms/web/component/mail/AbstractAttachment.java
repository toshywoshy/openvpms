/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.web.component.mail;

/**
 * Abstract implementation of the {@link MailAttachment} interface.
 *
 * @author Tim Anderson
 */
public class AbstractAttachment<T> extends MailAttachment {

    /**
     * The content.
     */
    private final T content;

    /**
     * Constructs an {@link AbstractAttachment}.
     *
     * @param content the content
     */
    public AbstractAttachment(T content) {
        this.content = content;
    }

    /**
     * Returns the content.
     *
     * @return the content
     */
    public T getContent() {
        return content;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param object the reference object with which to compare
     * @return {@code true} if this object is the same as the obj argument; {@code false} otherwise.
     */
    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof AbstractAttachment) {
            return ((AbstractAttachment<?>) object).content.equals(content);
        }
        return false;
    }

    /**
     * Returns a hash code value for the object.
     *
     * @return a hash code value for this object
     */
    @Override
    public int hashCode() {
        return content.hashCode();
    }

}
