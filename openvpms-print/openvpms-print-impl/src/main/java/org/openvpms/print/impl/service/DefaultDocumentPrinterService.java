/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.impl.service;

import org.openvpms.domain.practice.Location;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;

import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.PrinterName;
import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link DocumentPrinterService}.
 * <p/>
 * This is implemented using the Java Print Service API.
 *
 * @author Tim Anderson
 */
public class DefaultDocumentPrinterService implements DocumentPrinterService {

    /**
     * Constructs a {@link DefaultDocumentPrinterService}.
     */
    public DefaultDocumentPrinterService() {
        super();
    }

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    @Override
    public String getName() {
        return "Default Printer Service";
    }

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter() {
        javax.print.PrintService result = PrintServiceLookup.lookupDefaultPrintService();
        return (result != null) ? new DefaultDocumentPrinter(result) : null;
    }

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    @Override
    public DocumentPrinter getDefaultPrinter(Location location) {
        return getDefaultPrinter();
    }

    /**
     * Returns a printer by identifier.
     * <p>
     * Returns a printer by identifier.
     *
     * @return the corresponding printer, or {@code null} if none is found
     */
    @Override
    public DocumentPrinter getPrinter(String id) {
        AttributeSet set = new HashAttributeSet();
        set.add(new PrinterName(id, null));
        javax.print.PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, set);
        return (printServices.length > 0) ? new DefaultDocumentPrinter(printServices[0]) : null;
    }

    /**
     * Returns the available printers.
     *
     * @return the available printers
     */
    @Override
    public List<DocumentPrinter> getPrinters() {
        List<DocumentPrinter> result = new ArrayList<>();
        for (javax.print.PrintService printer : PrintServiceLookup.lookupPrintServices(null, null)) {
            result.add(new DefaultDocumentPrinter(printer));
        }
        return result;
    }

    /**
     * Returns the available printers at a practice location.
     *
     * @param location the practice location
     * @return the available printers
     */
    @Override
    public List<DocumentPrinter> getPrinters(Location location) {
        return getPrinters();
    }

    /**
     * Returns the printer service archetype that this supports.
     *
     * @return an <em>entity.printerService*</em> archetype, or {@code null} for the default printer service
     * implementation
     */
    @Override
    public String getArchetype() {
        return null;
    }
}
