/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.service;

import org.openvpms.domain.practice.Location;
import org.openvpms.print.exception.PrinterException;

import java.util.List;

/**
 * Manages {@link DocumentPrinter}s.
 *
 * @author Tim Anderson
 */
public interface DocumentPrinterService {

    /**
     * Returns a display name for this service.
     *
     * @return a display name for this service
     */
    String getName();

    /**
     * Returns the default printer.
     *
     * @return the default printer, or {@code null} if there is no default printer
     */
    DocumentPrinter getDefaultPrinter();

    /**
     * Returns the default printer for a practice location.
     *
     * @param location the practice location
     * @return the default printer, or {@code null} if there is no default printer
     */
    DocumentPrinter getDefaultPrinter(Location location);

    /**
     * Returns a printer by identifier.
     *
     * @param id the printer identifier
     * @return the corresponding printer, or {@code null} if none is found
     */
    DocumentPrinter getPrinter(String id);

    /**
     * Returns the available printers at a practice location.
     *
     * @param location the practice location
     * @return the available printers
     */
    List<DocumentPrinter> getPrinters(Location location);

    /**
     * Returns the available printers.
     *
     * @return the available printers
     */
    List<DocumentPrinter> getPrinters();

    /**
     * Returns the printer service archetype that this supports.
     *
     * @return an <em>entity.printerService*</em> archetype, or {@code null} for the default printer service
     * implementation
     * @throws PrinterException for any error
     */
    String getArchetype();

}