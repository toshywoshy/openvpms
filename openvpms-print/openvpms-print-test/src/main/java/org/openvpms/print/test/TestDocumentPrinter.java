/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.print.test;

import org.openvpms.component.model.document.Document;
import org.openvpms.print.service.DocumentPrinter;
import org.openvpms.print.service.DocumentPrinterService;
import org.openvpms.print.service.PrintAttributes;

import javax.print.PrintService;
import java.util.ArrayList;
import java.util.List;

/**
 * Test implementation of {@link DocumentPrinter} that collects the documents printed.
 *
 * @author Tim Anderson
 */
public class TestDocumentPrinter implements DocumentPrinter {

    /**
     * The printer id.
     */
    private final String id;

    /**
     * The printed documents.
     */
    private final List<Document> printed = new ArrayList<>();

    /**
     * Const
     *
     * @param id
     */
    public TestDocumentPrinter(String id) {
        this.id = id;
    }

    /**
     * Returns the printer identifier.
     *
     * @return the printer identifier
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * Returns the printer name.
     *
     * @return the printer name
     */
    @Override
    public String getName() {
        return getId();
    }

    /**
     * Returns the archetype of the {@link DocumentPrinterService} that provides this printer.
     *
     * @return the printer service archetype, or {@code null} if the Java Print Service API provides the printer
     */
    @Override
    public String getArchetype() {
        return null;
    }

    /**
     * Determines if the printer can be printed to using the Java Print Service API.
     * <p/>
     * When supported, the {@link #getId() identifier} can be used to look up the corresponding {@link PrintService}
     * by name.
     *
     * @return {@code true} if the printer supports the Java Print Service API, otherwise {@code false}
     */
    @Override
    public boolean canUseJavaPrintServiceAPI() {
        return false;
    }

    /**
     * Determines if the printer can print the specified mime type.
     *
     * @param mimeType the mime type
     * @return {@code true} if the printer can print the specified mime type
     */
    @Override
    public boolean canPrint(String mimeType) {
        return true;
    }

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param attributes the print attributes
     */
    @Override
    public synchronized void print(Document document, PrintAttributes attributes) {
        printed.add(document);
    }

    /**
     * Returns the printed documents.
     *
     * @return the printed documents
     */
    public synchronized List<Document> getPrinted() {
        return printed;
    }

}