/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.report.openoffice;

import org.apache.commons.lang.time.StopWatch;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.business.domain.im.document.Document;
import org.openvpms.report.PrintProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * OpenOffice print service.
 *
 * @author Tim Anderson
 * @author Benjamin Charlton
 */
public class PrintService {

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The connection pool.
     */
    private OOConnectionPool pool;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PrintService.class);

    /**
     * Constructs a {@link PrintService}.
     *
     * @param pool     the connection pool
     * @param handlers the document handlers
     */
    public PrintService(OOConnectionPool pool, DocumentHandlers handlers) {
        this.pool = pool;
        this.handlers = handlers;
    }

    /**
     * Prints a document.
     *
     * @param document   the document to print
     * @param properties the print properties
     * @throws OpenOfficeException for any error
     */
    public void print(Document document, PrintProperties properties) {
        OpenOfficeDocument doc;
        StopWatch stopWatch = null;
        RuntimeException failure = null;
        if (log.isDebugEnabled()) {
            stopWatch = new StopWatch();
            stopWatch.start();
            log.debug("Printing document='" + document.getName() + "', id=" + document.getId());
        }
        try (OOConnection connection = pool.getConnection()) {
            doc = new OpenOfficeDocument(document, connection, handlers);
            try {
                doc.print(properties);
            } finally {
                doc.close();
            }
        } catch (RuntimeException exception) {
            failure = exception;
            throw exception;
        } finally {
            if (stopWatch != null) {
                if (failure != null) {
                    log.debug("Failed to print document='" + document.getName() + "', id=" + document.getId()
                              + ", elapsed=" + stopWatch, failure);
                } else {
                    log.debug("Printed document='" + document.getName() + "', id=" + document.getId() + ", elapsed="
                              + stopWatch);
                }

            }
        }
    }

}
