/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.report.openoffice;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToConnect;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToCreateDoc;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToExportDoc;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToGetField;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToGetInputFields;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToGetService;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToGetUserFields;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToPrint;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToSetField;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.FailedToStartService;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.InvalidURL;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.ServiceNotInit;
import static org.openvpms.report.openoffice.OpenOfficeException.ErrorCode.UnsupportedConversion;


/**
 * {@link OpenOfficeException} test case.
 *
 * @author Tim Anderson
 */
public class OpenOfficeExceptionTestCase {

    /**
     * Verifies that the messages are generated correctly.
     */
    @Test
    public void testMessages() {
        assertEquals("Need to update tests to incorporate new messages",
                     13, OpenOfficeException.ErrorCode.values().length);
        checkException(InvalidURL, "Invalid OpenOffice URL: foo", "foo");
        checkException(FailedToConnect, "Failed to connect to OpenOffice: foo", "foo");
        checkException(FailedToGetService, "Failed to get OpenOffice service foo", "foo");
        checkException(FailedToStartService, "Failed to start OpenOffice service: foo", "foo");
        checkException(ServiceNotInit,
                       "OpenOfficeServiceHelper has not been initialised");
        checkException(FailedToCreateDoc, "Failed to create document: foo", "foo");
        checkException(FailedToPrint, "Printing failed: foo", "foo");
        checkException(FailedToGetField, "Failed to get user field: foo",
                       "foo");
        checkException(FailedToSetField, "Failed to set user field: foo",
                       "foo");
        checkException(FailedToExportDoc, "Failed to export document: foo",
                       "foo");
        checkException(FailedToGetInputFields, "Failed to get input fields");
        checkException(FailedToGetUserFields, "Failed to get user fields");
        checkException(UnsupportedConversion, "Cannot convert 'foo' from text/html to application/pdf", "foo",
                       "text/html", "application/pdf");
    }

    /**
     * Creates an {@link OpenOfficeException} with the supplied code and
     * arguments and verifies that the generated message matches that expected.
     *
     * @param code     the error code
     * @param expected the expected message
     * @param args     exception arguments
     */
    private void checkException(OpenOfficeException.ErrorCode code,
                                String expected, Object... args) {
        OpenOfficeException exception = new OpenOfficeException(code, args);
        assertEquals(code, exception.getErrorCode());
        assertEquals(expected, exception.getMessage());
    }
}
