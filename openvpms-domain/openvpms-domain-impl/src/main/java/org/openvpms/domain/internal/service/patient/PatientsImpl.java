/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.service.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.service.patient.Patients;

/**
 * Default implementation of {@link Patients}.
 *
 * @author Tim Anderson
 */
public class PatientsImpl implements Patients {

    /**
     * The domain object service.
     */
    private final DomainService domainService;

    /**
     * Constructs a {@link PatientsImpl}.
     *
     * @param service the domain object service
     */
    public PatientsImpl(DomainService service) {
        this.domainService = service;
    }

    /**
     * Returns a patient given its identifier.
     *
     * @param id the patient identifier
     * @return the corresponding patient, or {@code null} if none is found
     */
    @Override
    public Patient getPatient(long id) {
        return domainService.get(PatientArchetypes.PATIENT, id, Patient.class);
    }

}
