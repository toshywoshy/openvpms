/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.factory;

import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.IMObjectBeanFactory;
import org.openvpms.component.model.bean.RelatedObjects;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.domain.internal.object.RelatedDomainObjects;
import org.openvpms.domain.service.object.DomainObjectService;

import java.util.List;

/**
 * Internal service for adapting model objects to domain objects.
 *
 * @author Tim Anderson
 */
public interface DomainService extends IMObjectBeanFactory, DomainObjectService {

    /**
     * Creates a domain object from a model object.
     *
     * @param object the model object
     * @param type   the domain object type
     * @return a new domain object
     */
    <R, T extends IMObject> R create(T object, Class<R> type);

    /**
     * Creates a domain object from a model object.
     *
     * @param object       the model object
     * @param type         the domain object type
     * @param fallbackType the fallback type, if there is no specific domain object type for the object.
     *                     May be {@code null}
     * @return a new domain object
     */
    <R, F extends R, T extends IMObject> R create(T object, Class<R> type, Class<F> fallbackType);

    /**
     * Creates a domain object from a model object.
     *
     * @param bean the model object wrapped in a bean
     * @param type the domain object type
     * @return a new domain object
     */
    <R> R create(IMObjectBean bean, Class<R> type);

    /**
     * Creates a builder of the specified type, that takes the parent object in the constructor.
     *
     * @param parent the parent object
     * @param type   the builder type
     * @return a new builder
     */
    <R, T extends IMObject> R createBuilder(T parent, Class<R> type);

    /**
     * Creates a {@link RelatedDomainObjects} instance that returns domain objects of the specified type for the targets
     * of the relationships.
     *
     * @param relationships the relationships
     * @param type          the domain object type
     * @return a new related objects instance
     */
    <T, R extends Relationship>
    RelatedDomainObjects<T, R> createRelatedObjects(List<R> relationships, Class<T> type);

    /**
     * Creates a {@link RelatedDomainObjects} instance that returns domain objects of the specified type for the targets
     * of the relationships.
     *
     * @param relationships the relationships
     * @param type          the domain object type
     * @param fallbackType  the fallback type, if there is no specific domain object type for an IMObject
     * @return a new related objects instance
     */
    <T, F extends T, R extends Relationship>
    RelatedDomainObjects<T, R> createRelatedObjects(List<R> relationships, Class<T> type, Class<F> fallbackType);

    /**
     * Creates a particular {@link RelatedObjects} implementation.
     * <p/>
     * This must provide a constructor taking a list of relationships.
     *
     * @param relationships the relationships
     * @param type          the domain object type
     * @return a new related objects instance
     */
    <T, R extends Relationship, P extends RelatedObjects<T, R, P>>
    P createRelated(List<R> relationships, Class<? extends P> type);

}
