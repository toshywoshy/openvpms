/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.DocumentVersion;

/**
 * Default implementation of {@link DocumentVersion}.
 *
 * @author Tim Anderson
 */
public class DocumentVersionImpl extends AbstractDocumentActRecordImpl implements DocumentVersion {

    /**
     * Constructs a {@link DocumentVersionImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    public DocumentVersionImpl(DocumentAct peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns the patient.
     * <p/>
     * This is not particularly efficient, as the only way the patient can be obtained is via the parent.
     *
     * @return the patient
     */
    @Override
    public Patient getPatient() {
        Act parent = getBean().getObject("parent", Act.class);
        if (parent == null) {
            throw new IllegalStateException(getObject() + " has no parent");
        }
        IMObject patient = getBean().getTarget("patient");
        if (patient == null) {
            throw new IllegalStateException("Record has no patient");
        }
        return getService().create(patient, Patient.class);
    }
}
