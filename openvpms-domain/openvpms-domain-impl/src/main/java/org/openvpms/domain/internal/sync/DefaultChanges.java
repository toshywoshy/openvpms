/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.sync;

import org.openvpms.domain.sync.Changes;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Default implementation of {@link Changes}.
 *
 * @author Tim Anderson
 */
public class DefaultChanges<T> implements Changes<T> {

    /**
     * The added objects.
     */
    private final Set<T> added = new LinkedHashSet<>();

    /**
     * The updated objects.
     */
    private final Set<T> updated = new LinkedHashSet<>();

    /**
     * The deactivated objects.
     */
    private final Set<T> deactivated = new LinkedHashSet<>();

    /**
     * Records an object as being added.
     *
     * @param object the object
     * @return this
     */
    @Override
    public Changes<T> added(T object) {
        added.add(object);
        return this;
    }

    /**
     * Records an object as being updated.
     *
     * @param object the object
     * @return this
     */
    @Override
    public Changes<T> updated(T object) {
        updated.add(object);
        return this;
    }

    /**
     * Records an object as being deactivated.
     *
     * @param object the object
     * @return this
     */
    @Override
    public Changes<T> deactivated(T object) {
        deactivated.add(object);
        return this;
    }

    /**
     * Returns the added objects.
     *
     * @return the added objects
     */
    public List<T> getAdded() {
        return new ArrayList<>(added);
    }

    /**
     * Returns the updated objects.
     *
     * @return the updated objects
     */
    public List<T> getUpdated() {
        return new ArrayList<>(updated);
    }

    /**
     * Returns the deactivated objects.
     *
     * @return the deactivated objects
     */
    public List<T> getDeactivated() {
        return new ArrayList<>(deactivated);
    }

    /**
     * Returns the changes, in order of added, updated and deactivated.
     *
     * @return the changes
     */
    public List<Change<T>> getChanges() {
        List<Change<T>> result = new ArrayList<>();
        for (T object : getAdded()) {
            result.add(new DefaultChange<>(object, Change.Type.ADDED));
        }
        for (T object : getUpdated()) {
            result.add(new DefaultChange<>(object, Change.Type.UPDATED));
        }
        for (T object : getDeactivated()) {
            result.add(new DefaultChange<>(object, Change.Type.DEACTIVATED));
        }
        return result;
    }

    private static class DefaultChange<T> implements Change<T> {

        private final T object;

        private final Type type;

        DefaultChange(T object, Type type) {
            this.object = object;
            this.type = type;
        }

        @Override
        public T getObject() {
            return object;
        }

        @Override
        public Type getType() {
            return type;
        }
    }

}
