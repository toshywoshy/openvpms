/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.alert;

import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.component.business.domain.im.act.BeanActDecorator;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.user.User;
import org.openvpms.domain.patient.alert.Allergy;

import java.time.OffsetDateTime;

/**
 * Default implementation of {@link Allergy}.
 *
 * @author Tim Anderson
 */
public class AllergyImpl implements Allergy {

    /**
     * The alert act.
     */
    private final BeanActDecorator act;

    /**
     * Constructs an {@link AllergyImpl}.
     *
     * @param act the allergy alert act
     */
    public AllergyImpl(IMObjectBean act) {
        this.act = new BeanActDecorator(act);
    }

    /**
     * Returns the date when the allergy was recorded.
     *
     * @return the date
     */
    @Override
    public OffsetDateTime getDate() {
        return DateRules.toOffsetDateTime(act.getActivityStartTime());
    }

    /**
     * Returns a summary of the allergy.
     *
     * @return a summary of the allergy
     */
    @Override
    public String getAllergy() {
        return act.getReason();
    }

    /**
     * Return notes about the allergy.
     *
     * @return the notes. May be {@code null}
     */
    @Override
    public String getNotes() {
        return act.getBean().getString("notes");
    }

    /**
     * Returns the clinician the recorded the allergy.
     *
     * @return the clinician. May be {@code null}
     */
    @Override
    public User getClinician() {
        return act.getBean().getTarget("clinician", User.class);
    }

}
