/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.party;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.component.business.domain.im.party.BeanContactDecorator;
import org.openvpms.component.model.party.Contact;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.party.Email;

/**
 * Default implementation of {@link Email}.
 *
 * @author Tim Anderson
 */
public class EmailImpl extends BeanContactDecorator implements Email {

    /**
     * Constructs an {@link EmailImpl}.
     *
     * @param peer    the peer to delegate to
     * @param service the domain object service
     */
    public EmailImpl(Contact peer, DomainService service) {
        super(peer, service);
    }

    /**
     * Returns the email address.
     *
     * @return the email address. May be {@code null}
     */
    @Override
    public String getEmailAddress() {
        return StringUtils.trimToNull(getBean().getString("emailAddress"));
    }
}
