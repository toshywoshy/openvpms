/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.patient.record;

import org.apache.commons.lang3.StringUtils;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.archetype.rules.doc.DocumentRules;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.document.DocumentBuilder;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Abstract implementation of {@link DocumentBuilder}.
 *
 * @author Tim Anderson
 */
public abstract class AbstractDocumentBuilder<T extends DocumentBuilder> implements DocumentBuilder {

    /**
     * The parent act.
     */
    private final DocumentAct act;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * The document rules.
     */
    private final DocumentRules rules;

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The mime type.
     */
    private String mimeType;

    /**
     * The document content.
     */
    private InputStream content;

    /**
     * Determines if any existing document should be versioned.
     */
    private boolean version = true;

    /**
     * Protected document node.
     */
    static final String PROTECTED_DOCUMENT = "protectedDocument";

    /**
     * The versions node name.
     */
    static final String VERSIONS = "versions";

    /**
     * The node holding the last modified time. This is different to the created/updated times in that these will
     * also be updated through record locking.
     */
    private static final String LAST_MODIFIED = "endTime";

    /**
     * Constructs an {@link AbstractDocumentBuilder}.
     *
     * @param act      the parent document act
     * @param service  the archetype service
     * @param handlers the document handlers
     * @param rules    the document rules
     */
    protected AbstractDocumentBuilder(DocumentAct act, ArchetypeService service, DocumentHandlers handlers,
                                      DocumentRules rules) {
        this.act = act;
        this.service = service;
        this.handlers = handlers;
        this.rules = rules;
    }

    /**
     * Sets the document file name.
     *
     * @param fileName the file name
     * @return this
     */
    @Override
    public T fileName(String fileName) {
        this.fileName = fileName;
        return getThis();
    }

    /**
     * Sets the document mime type.
     *
     * @param mimeType the mime type
     * @return this
     */
    @Override
    public T mimeType(String mimeType) {
        this.mimeType = mimeType;
        return getThis();
    }

    /**
     * Sets the document content.
     *
     * @param content the content stream
     * @return this
     */
    @Override
    public T content(InputStream content) {
        this.content = content;
        return getThis();
    }

    /**
     * Determines if any existing document should versioned.
     *
     * @param version if {@code true}, version any existing document, else replace it
     * @return this
     */
    @Override
    public T version(boolean version) {
        this.version = version;
        return getThis();
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected T getThis() {
        return (T) this;
    }

    /**
     * Performs the build.
     *
     * @param toSave   collects the objects that need to be saved
     * @param toRemove collects the objects that need to be deleted
     * @return the document
     */
    protected Document doBuild(List<IMObject> toSave, List<Reference> toRemove) {
        Document document = createDocument();
        doBuild(document, toSave, toRemove);
        return document;
    }

    /**
     * Performs the build.
     *
     * @param document the document to associate with the parent act
     * @param toSave   collects the objects that need to be saved
     * @param toRemove collects the objects that need to be deleted
     */
    protected void doBuild(Document document, List<IMObject> toSave, List<Reference> toRemove) {
        IMObjectBean bean = service.getBean(act);
        toSave.add(document);
        toSave.add(act);
        Reference existing = act.getDocument();
        boolean deleteExisting = false;
        boolean isProtected = isProtectedDocument(bean);
        if (existing != null && (isProtected || version) && supportsVersioning(bean)) {
            // only version if there is an existing document
            DocumentAct versioned = rules.createVersion(act);
            bean.addTarget(VERSIONS, versioned, "parent");
            toSave.add(versioned);
        } else if (existing != null) {
            deleteExisting = true;
        }
        act.setDocument(document.getObjectReference());
        act.setFileName(document.getName());       // duplicate fields onto the act
        act.setMimeType(document.getMimeType());
        setLastModified(bean);
        if (isProtected) {
            bean.setValue(PROTECTED_DOCUMENT, null); // latest version is not protected
        }
        if (deleteExisting) {
            toRemove.add(existing);
        }
    }

    /**
     * Creates the document.
     *
     * @return the document
     */
    protected Document createDocument() {
        fileName = StringUtils.trimToNull(fileName);
        mimeType = StringUtils.trimToNull(mimeType);
        if (fileName == null) {
            throw new IllegalStateException("'fileName' may not be null or empty");
        }
        if (mimeType == null) {
            throw new IllegalStateException("'mimeType' may not be null or empty");
        }
        if (content == null) {
            throw new IllegalStateException("'content' may not be null");
        }
        DocumentHandler handler = handlers.get(fileName, mimeType);
        return handler.create(fileName, content, mimeType, -1);
    }

    /**
     * Builds the document and commits the changes.
     * <p/>
     * NOTE: this must be invoked in a transaction to avoid orphan documents being created if there is a partial
     * failure.
     *
     * @return the document.
     */
    protected Document buildAndCommit() {
        List<IMObject> toSave = new ArrayList<>();
        List<Reference> toRemove = new ArrayList<>();
        Document document = doBuild(toSave, toRemove);
        service.save(toSave);
        for (Reference reference : toRemove) {
            IMObject object = service.get(reference);
            if (object != null) {
                service.remove(object);
            }
        }
        return document;
    }

    /**
     * Updates the last-modified date of the parent act. By default, this is mapped to the endTime node.
     *
     * @param bean the act bean
     */
    protected void setLastModified(IMObjectBean bean) {
        if (bean.hasNode(LAST_MODIFIED)) {
            bean.setValue(LAST_MODIFIED, new Date());
        }
    }

    /**
     * Determines the if act supports versioning.
     *
     * @param bean the act bean
     * @return {@code true} if the act supports versioning
     */
    protected boolean supportsVersioning(IMObjectBean bean) {
        return bean.hasNode(VERSIONS);
    }

    /**
     * Determines if the attached document is protected.
     * <p/>
     * Protected documents should be versioned rather than replaced.
     *
     * @param bean the act bean
     * @return {@code true} if the document is protected
     */
    protected boolean isProtectedDocument(IMObjectBean bean) {
        return bean.hasNode(PROTECTED_DOCUMENT) && bean.getBoolean(PROTECTED_DOCUMENT);
    }
}
