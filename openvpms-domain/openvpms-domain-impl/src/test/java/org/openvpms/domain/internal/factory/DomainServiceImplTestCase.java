/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.domain.internal.factory;

import org.junit.Test;
import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.document.DocumentBuilder;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.domain.customer.Customer;
import org.openvpms.domain.customer.CustomerPatients;
import org.openvpms.domain.internal.customer.CustomerImpl;
import org.openvpms.domain.internal.customer.CustomerPatientsImpl;
import org.openvpms.domain.internal.object.RelatedDomainObjects;
import org.openvpms.domain.internal.patient.record.DefaultRecordImpl;
import org.openvpms.domain.internal.patient.record.DocumentRecordDocumentBuilderImpl;
import org.openvpms.domain.patient.Patient;
import org.openvpms.domain.patient.record.Record;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link DomainServiceImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class DomainServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * The domain service.
     */
    @Autowired
    private DomainServiceImpl domainService;

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * Tests the {@link DomainServiceImpl#create(IMObject, Class)} and
     * {@link DomainServiceImpl#create(IMObjectBean, Class)} methods.
     */
    @Test
    public void testCreate() {
        Party party = customerFactory.newCustomer().build(false);
        Customer customer1 = domainService.create(party, Customer.class);
        assertTrue(customer1 instanceof CustomerImpl);

        IMObjectBean bean = getBean(party);
        Customer customer2 = domainService.create(bean, Customer.class);
        assertTrue(customer2 instanceof CustomerImpl);
    }

    /**
     * Tests the {@link DomainServiceImpl#create(IMObject, Class, Class)} method.
     */
    @Test
    public void testCreateWithFallback() {
        Act act = create(PatientArchetypes.CLINICAL_ADDENDUM, Act.class);
        try {
            // when there is no specific class registered this, should fail
            domainService.create(act, Record.class);
            fail("Expected create to fail");
        } catch (ClassCastException exception) {
            // no-op
        }

        // verify the fallback is created
        Record record = domainService.create(act, Record.class, DefaultRecordImpl.class);
        assertTrue(record instanceof DefaultRecordImpl);
    }

    /**
     * Tests the {@link DomainServiceImpl#getBean(IMObject)} method.
     */
    @Test
    public void testBean() {
        Party party = customerFactory.newCustomer().build(false);

        // verify a bean can be created and used to save the party.
        IMObjectBean bean = domainService.getBean(party);
        bean.save();
        assertNotNull(get(party));
    }

    /**
     * Tests the {@link DomainServiceImpl#createBuilder(IMObject, Class)} method.
     */
    @Test
    public void testCreateBuilder() {
        DocumentAct act = patientFactory.newInvestigation().build(false);
        DocumentBuilder builder = domainService.createBuilder(act, DocumentRecordDocumentBuilderImpl.class);
        assertNotNull(builder);
    }

    /**
     * Tests the {@link DomainServiceImpl#createRelated(List, Class)} method.
     */
    @Test
    public void testCreateRelated() {
        List<EntityRelationship> relationships = new ArrayList<>();
        CustomerPatients related = domainService.createRelated(relationships, CustomerPatientsImpl.class);
        assertNotNull(related);
    }

    /**
     * Tests the {@link DomainServiceImpl#createRelatedObjects(List, Class)} method.
     */
    @Test
    public void testCreateRelatedObjects() {
        List<EntityRelationship> relationships = new ArrayList<>();
        RelatedDomainObjects<Patient, EntityRelationship> related
                = domainService.createRelatedObjects(relationships, Patient.class);
        assertNotNull(related);
    }

    /**
     * Tests the {@link DomainServiceImpl#createRelatedObjects(List, Class, Class)} method.
     */
    @Test
    public void testCreateRelatedObjectsWithFallback() {
        List<ActRelationship> relationships = new ArrayList<>();
        RelatedDomainObjects<Record, ActRelationship> related
                = domainService.createRelatedObjects(relationships, Record.class, DefaultRecordImpl.class);
        assertNotNull(related);
    }

    /**
     * Tests the {@link DomainServiceImpl#get(Reference, Class)} method.
     */
    @Test
    public void testGet() {
        Party active = customerFactory.createCustomer();
        Party inactive = customerFactory.newCustomer().active(false).build();

        // test active retrieval
        assertTrue(domainService.get(active.getObjectReference(), Customer.class) instanceof CustomerImpl);
        assertTrue(domainService.get(active.getObjectReference(), Customer.class, true) instanceof CustomerImpl);
        assertNull(domainService.get(active.getObjectReference(), Customer.class, false));
        assertTrue(domainService.get(active.getArchetype(), active.getId(), Customer.class) instanceof CustomerImpl);
        assertTrue(domainService.get(active.getArchetype(), active.getId(), Customer.class, true)
                           instanceof CustomerImpl);
        assertNull(domainService.get(active.getArchetype(), active.getId(), Customer.class, false));

        // test inactive retrieval
        assertTrue(domainService.get(inactive.getObjectReference(), Customer.class) instanceof CustomerImpl);
        assertNull(domainService.get(inactive.getObjectReference(), Customer.class, true));
        assertTrue(domainService.get(inactive.getObjectReference(), Customer.class, false) instanceof CustomerImpl);
        assertTrue(domainService.get(inactive.getArchetype(), inactive.getId(), Customer.class)
                           instanceof CustomerImpl);
        assertNull(domainService.get(inactive.getArchetype(), inactive.getId(), Customer.class, true));
        assertTrue(domainService.get(inactive.getArchetype(), inactive.getId(), Customer.class, false)
                           instanceof CustomerImpl);
    }
}