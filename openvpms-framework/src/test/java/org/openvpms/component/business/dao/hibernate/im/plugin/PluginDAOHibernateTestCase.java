/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.plugin;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the {@link PluginDAOHibernate} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration("/plugindao-context.xml")
public class PluginDAOHibernateTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The plugin DAO.
     */
    @Autowired
    private PluginDAOHibernate dao;

    /**
     * Tests the DAO.
     */
    @Test
    public void testDAO() throws IOException {
        byte[] binary = RandomUtils.nextBytes(10);
        String key = UUID.randomUUID().toString();
        String name = key + ".jar";
        dao.save(key, name, new ByteArrayInputStream(binary));

        // verify the plugin can be retrieved by key
        Plugin plugin = dao.getPlugin(key);
        assertNotNull(plugin);
        assertEquals(key, plugin.getKey());
        assertEquals(name, plugin.getName());
        assertEquals("plugin.default", plugin.getArchetype());

        // verify the binary matches that expected
        InputStream stream = dao.getBinary(key);
        byte[] read = IOUtils.toByteArray(stream);
        assertArrayEquals(binary, read);

        // test iteration
        Iterator<Plugin> iterator = dao.getPlugins();
        int found = 0;
        while (iterator.hasNext()) {
            Plugin next = iterator.next();
            if (key.equals(next.getKey())) {
                assertEquals(name, next.getName());
                assertEquals("plugin.default", next.getArchetype());
                assertNull(next.getDescription());
                assertEquals(plugin.getId(), next.getId());
                found++;
            }
        }
        assertEquals(1, found);

        // verify the plugin can be removed
        assertTrue(dao.remove(key));
        assertNull(dao.getPlugin(key));
    }

    /**
     * Verifies that a plugin can't be saved with the same name as an existing plugin.
     */
    @Test
    public void testSaveWithDuplicateName() {
        byte[] binary = RandomUtils.nextBytes(10);
        String name = UUID.randomUUID().toString() + ".jar";
        dao.save(UUID.randomUUID().toString(), name, new ByteArrayInputStream(binary));
        try {
            dao.save(UUID.randomUUID().toString(), name, new ByteArrayInputStream(binary));
            fail("Expected ConstraintViolationException");
        } catch (ConstraintViolationException exception) {
            // do nothing
        }
    }
}
