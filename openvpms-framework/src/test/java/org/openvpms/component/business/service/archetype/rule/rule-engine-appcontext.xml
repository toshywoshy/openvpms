<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd">

    <import resource="classpath:/datasource-context.xml"/>

    <bean id="assertionBeanFactory"
          class="org.openvpms.component.business.service.archetype.assertion.AssertionBeanFactory">
        <constructor-arg>
            <list>
                <value>lookupService</value>
            </list>
        </constructor-arg>
    </bean>

    <bean id="assembler" class="org.openvpms.component.business.dao.hibernate.im.AssemblerImpl">
        <constructor-arg ref="archetypeDescriptorCache"/>
        <constructor-arg ref="assertionBeanFactory"/>
    </bean>

    <!--  The IMObject DAO bean -->
    <bean id="imObjectDao" class="org.openvpms.component.business.dao.hibernate.im.IMObjectDAOHibernate">
        <constructor-arg ref="sessionFactory"/>
        <property name="archetypeDescriptorCache" ref="archetypeDescriptorCache"/>
        <property name="assembler" ref="assembler"/>
    </bean>

    <!--  The Archetype Descriptor Cache -->
    <bean id="archetypeDescriptorCache"
          class="org.openvpms.component.business.service.archetype.descriptor.cache.ArchetypeDescriptorCacheFS">
        <constructor-arg value="org/openvpms/archetype"/>
        <constructor-arg index="1">
            <list>
                <value>adl</value>
            </list>
        </constructor-arg>
        <constructor-arg value="org/openvpms/archetype/assertionTypes.xml"/>
        <constructor-arg ref="assertionBeanFactory"/>
    </bean>


    <!--  The Archetype Service -->
    <bean id="archetypeService"
          class="org.openvpms.component.business.service.archetype.ArchetypeService">
        <constructor-arg ref="txnManager"/>
        <constructor-arg ref="archetypeDescriptorCache"/>
        <property name="dao" ref="imObjectDao"/>
        <property name="ruleEngine" ref="ruleEngine"/>
    </bean>

    <!--  The rule-based Archetype Service -->
    <bean id="archetypeRuleService"
          class="org.openvpms.component.business.service.archetype.rule.ArchetypeRuleService">
        <constructor-arg ref="archetypeService"/>
        <constructor-arg ref="txnManager"/>
        <constructor-arg ref="ruleEngine"/>
        <property name="facts">
            <list>
                <ref bean="ruleRecorder"/>
            </list>
        </property>
    </bean>

    <bean id="ruleRecorder" class="org.openvpms.component.business.service.archetype.rule.RemoveRuleRecorder"/>

    <!--  Instantiate the archetype service helper bean and inject their dependencies -->
    <bean id="archetypeServiceHelper"
          class="org.openvpms.component.business.service.archetype.ArchetypeServiceHelper">
        <constructor-arg ref="archetypeRuleService"/>
    </bean>

    <!-- The `local' ruleServiceProvider; we can have as many different ruleProviders as we want -->
    <bean id="ruleServiceProvider"
          class="org.springmodules.jsr94.factory.DefaultRuleServiceProviderFactoryBean">
        <property name="provider" value="http://drools.org"/>
        <property name="providerClass"
                  value="org.drools.jsr94.rules.RuleServiceProviderImpl"/>
    </bean>

    <!-- The local ruleRuntime. Usually, there is one ruleRuntime bean for each ruleServiceProvider bean. -->
    <bean id="ruleRuntime"
          class="org.springmodules.jsr94.factory.RuleRuntimeFactoryBean">
        <property name="serviceProvider" ref="ruleServiceProvider"/>
    </bean>

    <!-- the local ruleAdministrator. Usually, there is one ruleAdministrator bean for each ruleServiceProvider bean -->
    <bean id="ruleAdministrator"
          class="org.springmodules.jsr94.factory.RuleAdministratorFactoryBean">
        <property name="serviceProvider" ref="ruleServiceProvider"/>
    </bean>

    <!-- the ruleSource. There can be many ruleSource beans for each ruleAdministrator/ruleRuntime. -->
    <bean id="ruleSource"
          class="org.openvpms.component.business.service.ruleengine.RuleDescriptorRuleSource">
        <property name="ruleRuntime" ref="ruleRuntime"/>
        <property name="ruleAdministrator" ref="ruleAdministrator"/>
    </bean>

    <!--  The Rule Engine -->
    <bean id="ruleEngine"
          class="org.openvpms.component.business.service.ruleengine.RuleEngine">
        <constructor-arg ref="ruleSource"/>
    </bean>

    <!--  The Lookup Service -->
    <bean id="lookupService" class="org.openvpms.component.business.service.lookup.LookupService">
        <constructor-arg ref="archetypeRuleService"/>
        <constructor-arg ref="imObjectDao"/>
    </bean>

    <!--  Instantiate the lookup service helper bean and inject their dependencies -->
    <bean id="lookupServiceHelper" class="org.openvpms.component.business.service.lookup.LookupServiceHelper">
        <constructor-arg ref="lookupService"/>
    </bean>

</beans>
