/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype;

import org.junit.Test;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.service.AbstractArchetypeServiceTest;
import org.openvpms.component.model.user.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static org.junit.Assert.fail;

/**
 * Base class for {@link ReadOnlyArchetypeService} tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractReadOnlyAchetypeServiceTest extends AbstractArchetypeServiceTest {

    /**
     * Verifies that the mutating methods throw {@link UnsupportedOperationException}.
     */
    @Test
    public void testUnsupportedOperations() {
        ReadOnlyArchetypeService service = createService();
        User user = createUser();

        checkUnsupported(() -> service.create("security.user"));
        checkUnsupported(() -> service.create("security.user", User.class));
        checkUnsupported(() -> service.create(new ArchetypeId("security.user")));
        checkUnsupported(() -> service.validateObject(user));
        checkUnsupported(() -> service.deriveValues(user));
        checkUnsupported(() -> service.deriveValue(user, "description"));
        checkUnsupported(() -> service.save(user));
        checkUnsupported(() -> service.save(user, false));
        checkUnsupported(() -> service.save(Collections.singletonList(user)));
        checkUnsupported(() -> service.save(Collections.singletonList(user), false));
        checkUnsupported(() -> service.remove(user));
        checkUnsupported(() -> service.remove(user.getObjectReference()));
        checkUnsupported(() -> service.executeRule("foo", new HashMap<>(), new ArrayList<>()));
        checkUnsupported(() -> service.addListener("security.user", new AbstractArchetypeServiceListener() {
        }));
        checkUnsupported(() -> service.removeListener("security.user", new AbstractArchetypeServiceListener() {
        }));
    }

    /**
     * Creates a new {@link ReadOnlyArchetypeService}.
     *
     * @return a new service
     */
    protected abstract ReadOnlyArchetypeService createService();

    /**
     * Verifies an operation throws {@link UnsupportedOperationException}.
     *
     * @param operation the operation
     */
    private void checkUnsupported(Runnable operation) {
        try {
            operation.run();
            fail("Expected UnsupportedOperationException");
        } catch (UnsupportedOperationException expected) {
            // do nothing
        }
    }
}
