/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.rule;

import org.junit.Test;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.common.EntityRelationship;
import org.openvpms.component.business.domain.im.common.IMObject;
import org.openvpms.component.business.domain.im.party.Contact;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.object.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


/**
 * Tests the {@link ArchetypeRuleService} class.
 *
 * @author <a href="mailto:support@openvpms.org>OpenVPMS Team</a>
 */
@ContextConfiguration("rule-engine-appcontext.xml")
public class ArchetypeRuleServiceTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The archetype service.
     */
    @Autowired
    private IArchetypeRuleService service;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager txnManager;

    /**
     * Tracks rule invocation at removal.
     */
    @Autowired
    private RemoveRuleRecorder ruleRecorder;

    /**
     * Verifies that rule engine is called when an object is being saved.
     * If both the before and after rules execute, the object will have
     * 'before' and 'after' properties set to {@code true}.
     * <p/>
     * Requires the <em>archetypeService.save.party.person.before</em> and
     * <em>archetypeService.save.party.person.after</em> rules.
     */
    @Test
    public void testSave() {
        Party person = createPerson("MR", "Jim", "Alateras");
        service.save(person);
        checkPerson(person);
    }

    /**
     * Verifies that rule engine is called when objects are being saved.
     * If both the before and after rules execute, the object will have
     * 'before' and 'after' properties set to {@code true}.
     * <p/>
     * Requires the <em>archetypeService.save.party.person.before</em> and
     * <em>archetypeService.save.party.person.after</em> rules.
     */
    @Test
    public void testSaveCollection() {
        Party person1 = createPerson("MR", "Jim", "Alateras");
        Party person2 = createPerson("MR", "Tim", "Anderson");
        Collection<IMObject> list = Arrays.asList(person1, person2);
        service.save(list);
        checkPerson(person1);
        checkPerson(person2);
    }

    /**
     * Verifies that the rule engine is called when an object is removed by
     * {@link ArchetypeRuleService#remove(org.openvpms.component.model.object.IMObject)}
     */
    @Test
    public void testRemoveObject() {
        Party person = createPerson("MR", "Tim", "Anderson");
        service.save(person);
        assertFalse(ruleRecorder.removedBeforeInvoked(person));
        assertFalse(ruleRecorder.removedAfterInvoked(person));
        service.remove(person);

        // verify that the remove.before and after rules have been invoked
        checkRemoved(person);
    }

    /**
     * Verifies that the rule engine is called when an object is removed by
     * {@link ArchetypeRuleService#remove(Reference)}
     */
    @Test
    public void testRemoveByReference() {
        Party person = createPerson("MR", "Tim", "Anderson");
        service.save(person);
        service.remove(person.getObjectReference());

        // verify that the remove.before and after rules have been invoked
        checkRemoved(person);
    }

    /**
     * Test for OVPMS-127.
     */
    @Test
    public void testOVPMS127() {
        Party personA = createPerson("MR", "Jim", "Alateras");
        service.save(personA);
        Party personB = createPerson("MR", "Oscar", "Alateras");
        service.save(personB);
        Party pet = createAnimal("lucky");
        service.save(pet);

        // create the entity relationships
        EntityRelationship rel = createEntityRelationship(
                personA, pet, "entityRelationship.animalOwner");
        pet.addEntityRelationship(rel);
        service.save(pet);
        assertNull(rel.getActiveEndTime());

        EntityRelationship rel1 = createEntityRelationship(
                personB, pet, "entityRelationship.animalOwner");
        pet.addEntityRelationship(rel1);
        service.save(pet);

        assertNotNull(rel.getActiveEndTime());
        assertNull(rel1.getActiveEndTime());

        pet = createAnimal("billy");
        service.save(pet);
        rel = createEntityRelationship(personB, pet,
                                       "entityRelationship.animalOwner");
        personB.addEntityRelationship(rel);
        service.save(personB);
    }

    /**
     * Verifies that when a rule throws an exception, it is propagated, and
     * no changes are saved.
     * This requires that:
     * <ul>
     * <li><em>the archetypeService.save.act.simple.before</em> rule throws an
     * IllegalStateException when the act status is "EXCEPTION_BEFORE"
     * <li><em>the archetypeService.save.act.simple.after</em> rule throws an
     * IllegalStateException when the act status is "EXCEPTION_AFTER"
     * <li>both rules set the act reason to the act status when they throw.
     * </ul>
     */
    @Test
    public void testException() {
        Act act = service.create("act.simple", Act.class);
        checkException(act, null);
        checkException(act, "EXCEPTION_BEFORE");
        checkException(act, "EXCEPTION_AFTER");
    }

    /**
     * Verifies that if a rule throws an exception, all changes are rolled
     * back.
     */
    @Test
    public void testTransactionRollbackOnException() {
        Party person = createPerson("MR", "T", "Anderson");
        Act act = service.create("act.simple", Act.class);
        service.save(person);
        service.save(act);

        // start a new transaction
        TransactionStatus status = txnManager.getTransaction(
                new DefaultTransactionDefinition());

        // change some details
        person.getDetails().put("lastName", "Foo");

        service.save(person);

        try {
            // make the act.simple.after save rule throw an exception
            act.setStatus("EXCEPTION_AFTER");
            service.save(act);
            fail("Expected save to fail");
        } catch (Exception expected) {
            // expected
        }
        try {
            txnManager.commit(status);
            fail("Expected commit to fail");
        } catch (TransactionException expected) {
            // verify that no changes are made persistent
            person = reload(person);
            act = reload(act);
            assertEquals(person.getName(), "Anderson,T");
            assertNull(act.getStatus());
        }
    }

    /**
     * Verifies that a rule can establish its own isolated transaction.
     * This saves an act.simple which triggers a rule which creates and saves a
     * new act in a separate transaction, prior to throwing an exception. The
     * new act should save, but the original act shouldn't.
     *
     * @see ActSimpleRules#insertNewActInIsolation
     */
    @Test
    public void testTransactionIsolation() {
        Act act = service.create("act.simple", Act.class);
        try {
            act.setStatus("INSERT_NEW_AND_THROW");
            service.save(act);
            fail("Expected save to throw exception");
        } catch (Exception expected) {
            // expected
        }

        // ensure the act has not been saved as the exception should have rolled
        // back the transaction
        assertTrue(act.isNew());

        // verify that there is an associated act that did save
        Set<ActRelationship> relationships = act.getActRelationships();
        assertEquals(1, relationships.size());
        ActRelationship relationship = relationships.iterator().next();
        Act related = (Act) get(relationship.getSource());
        assertNotNull(related);
    }

    /**
     * Verifies that an object has been removed and that the before and after rules were fired.
     * <p/>
     * If both the before and after rules execute, the
     * {@link RemoveRuleRecorder#removedBeforeInvoked(org.openvpms.component.model.object.IMObject)}
     * and {@link RemoveRuleRecorder#removedAfterInvoked(org.openvpms.component.model.object.IMObject)} will both return
     * {@code true}.
     * <p/>
     * Requires the <em>archetypeService.remove.party.person.before</em> and
     * <em>archetypeService.remove.party.person.after</em> rules.
     *
     * @param object the object that is expected to be removed
     */
    private void checkRemoved(Party object) {
        // verify that the remove.before and after rules have been invoked
        assertTrue(ruleRecorder.removedBeforeInvoked(object));
        assertTrue(ruleRecorder.removedAfterInvoked(object));

        // verify that the person has been removed
        object = reload(object);
        assertNull(object);
    }

    /**
     * Verifies that the before and after rules have been invoked after a
     * <em>party.person</em> has been saved/removed.
     * If both rules execute, the object will have 'before' and 'after'
     * properties set to {@code true}.
     *
     * @param person the person
     */
    private void checkPerson(Party person) {
        assertEquals(Boolean.TRUE, person.getDetails().get("before"));
        assertEquals(Boolean.TRUE, person.getDetails().get("after"));

        person = reload(person);
        assertNotNull(person);

        // verify the persistent instance has the before property
        assertEquals(Boolean.TRUE, person.getDetails().get("before"));
    }

    /**
     * Tests the behaviour of rules throwing exceptions.
     *
     * @param act    the act
     * @param status the act status
     * @see #testException()
     */
    private void checkException(Act act, String status) {
        long version = act.getVersion();
        try {
            act.setStatus(status);
            service.save(act);
            if (status != null) {
                fail("Expected save of act.simple to fail");
            }
        } catch (Throwable exception) {
            if (status == null) {
                fail("Expected save of act.simple to succeed");
            } else {
                // verify that the correct rule threw the exception
                assertEquals(status, act.getReason());

                // verify that an IllegalStateException is the root cause
                while (exception.getCause() != null) {
                    exception = exception.getCause();
                }
                if (!(exception instanceof IllegalStateException)) {
                    fail("Expected rule to throw IllegalStateException");
                }

                if (!act.isNew()) {
                    // verify that the changes weren't saved
                    Act original = reload(act);
                    assertEquals(version, original.getVersion());
                }
            }
        }
    }

    /**
     * Helper to retrieve an object given its reference.
     *
     * @param ref the object reference
     * @return the corresponding object or {@code null}
     */
    private IMObject get(Reference ref) {
        return service.get(ref);
    }

    /**
     * Helper to reload an object.
     *
     * @param object the object to reload
     * @return the reloaded object
     */
    @SuppressWarnings("unchecked")
    private <T extends IMObject> T reload(T object) {
        return (T) get(object.getObjectReference());
    }

    /**
     * Creates a person.
     *
     * @param title     the person's title
     * @param firstName the person's first name
     * @param lastName  the person's last name
     * @return Person
     */
    private Party createPerson(String title, String firstName, String lastName) {
        Party person = service.create("party.person", Party.class);
        person.getDetails().put("lastName", lastName);
        person.getDetails().put("firstName", firstName);
        person.getDetails().put("title", title);
        person.addContact(createPhoneContact());

        return person;
    }

    /**
     * Creates an animal entity.
     *
     * @param name the name of the pet
     * @return Animal
     */
    private Party createAnimal(String name) {
        Party pet = service.create("party.animalpet", Party.class);
        pet.setName(name);
        pet.getDetails().put("breed", "dog");
        pet.getDetails().put("colour", "brown");
        pet.getDetails().put("sex", "UNSPECIFIED");
        pet.getDetails().put("species", "k9");
        pet.setDescription("A dog");
        pet.getDetails().put("dateOfBirth", new Date());

        return pet;
    }

    /**
     * Creates a phone contact.
     *
     * @return Contact
     */
    private Contact createPhoneContact() {
        Contact contact = service.create("contact.phoneNumber", Contact.class);
        contact.getDetails().put("areaCode", "03");
        contact.getDetails().put("telephoneNumber", "1234567");
        contact.getDetails().put("preferred", true);

        return contact;
    }

    /**
     * Create an entity relationship of the specified type between the
     * source and target entities.
     *
     * @param source    the source entity
     * @param target    the target entity
     * @param shortName the short name of the relationship to create
     * @return EntityRelationship
     */
    private EntityRelationship createEntityRelationship(Entity source, Entity target, String shortName) {
        EntityRelationship rel = service.create(shortName, EntityRelationship.class);
        rel.setActiveStartTime(new Date());
        rel.setSource(source.getObjectReference());
        rel.setTarget(target.getObjectReference());

        return rel;
    }
}
