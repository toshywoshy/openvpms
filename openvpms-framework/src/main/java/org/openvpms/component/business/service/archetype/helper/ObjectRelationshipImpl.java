/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.model.bean.ObjectRelationship;

/**
 * Default implementation of {@link ObjectRelationship}.
 *
 * @author Tim Anderson
 */
public class ObjectRelationshipImpl<T, R> implements ObjectRelationship<T, R> {

    /**
     * The object.
     */
    private final T object;

    /**
     * The relationship.
     */
    private final R relationship;

    /**
     * Constructs an {@link ObjectRelationshipImpl}.
     *
     * @param object       the object
     * @param relationship the relationship
     */
    public ObjectRelationshipImpl(T object, R relationship) {
        this.object = object;
        this.relationship = relationship;
    }

    /**
     * Returns the object.
     *
     * @return the object. May be {@code null}
     */
    @Override
    public T getObject() {
        return object;
    }

    /**
     * Returns the relationship.
     *
     * @return the relationship
     */
    @Override
    public R getRelationship() {
        return relationship;
    }
}
