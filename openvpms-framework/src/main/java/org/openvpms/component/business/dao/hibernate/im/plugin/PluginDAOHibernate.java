/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.plugin;

import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.jdbc.BlobProxy;
import org.hibernate.query.Query;
import org.openvpms.component.business.dao.hibernate.cache.ArchetypeIdCache;
import org.openvpms.component.business.dao.hibernate.im.common.CompoundAssembler;
import org.openvpms.component.business.dao.hibernate.im.common.Context;
import org.openvpms.component.business.dao.hibernate.im.entity.IMObjectResultCollector;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.component.business.domain.archetype.ArchetypeId;
import org.openvpms.component.business.domain.im.plugin.Plugin;
import org.openvpms.component.business.service.archetype.descriptor.cache.IArchetypeDescriptorCache;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * Hibernate implementation of {@link PluginDAO}.
 *
 * @author Tim Anderson
 */
public class PluginDAOHibernate implements PluginDAO {

    /**
     * The session factory.
     */
    private final SessionFactory factory;

    /**
     * The archetype id cache.
     */
    private final ArchetypeIdCache archetypeIds;

    /**
     * The binary assembler.
     */
    private final CompoundAssembler assembler;

    /**
     * Query plugins by key.
     */
    private static final String QUERY_BY_KEY =
            "from " + PluginDOImpl.class.getName() + " as plugin where plugin.key = :key";

    /**
     * Query active plugins.
     */
    private static final String QUERY_ACTIVE = "from " + PluginDOImpl.class.getName()
                                               + " as plugin where plugin.active = 1";

    /**
     * Constructs a {@link PluginDAOHibernate}.
     *
     * @param factory      the session factory
     * @param archetypes   the archetype descriptor cache
     * @param archetypeIds the archetype id cache
     */
    public PluginDAOHibernate(SessionFactory factory, IArchetypeDescriptorCache archetypes,
                              ArchetypeIdCache archetypeIds) {
        this.factory = factory;
        this.archetypeIds = archetypeIds;
        assembler = new Assembler(archetypes);
    }

    /**
     * Returns the plugin with the given key.
     *
     * @param key the plugin key
     * @return the corresponding plugin, or {@code null} if none is found
     */
    @Override
    @Transactional(readOnly = true)
    public Plugin getPlugin(String key) {
        Plugin result = null;
        Session session = factory.getCurrentSession();
        PluginDOImpl plugin = get(key, session);
        if (plugin != null) {
            Context context = Context.getContext(session, assembler, archetypeIds);
            IMObjectResultCollector collector = new IMObjectResultCollector();
            collector.setContext(context);
            result = (Plugin) assembler.assemble(plugin, context);
            context.resolveDeferredReferences();
        }
        return result;
    }

    /**
     * Returns all active plugins.
     *
     * @return the active plugins
     */
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public Iterator<Plugin> getPlugins() {
        Session session = factory.getCurrentSession();
        Query<PluginDOImpl> query = session.createQuery(QUERY_ACTIVE, PluginDOImpl.class);
        Context context = Context.getContext(session, assembler, archetypeIds);
        IMObjectResultCollector collector = new IMObjectResultCollector();
        collector.setContext(context);
        for (PluginDOImpl object : query.list()) {
            collector.collect(object);
        }
        context.resolveDeferredReferences();
        List<Plugin> results = (List<Plugin>) (List<?>) collector.getPage().getResults();
        return results.iterator();
    }

    /**
     * Returns a stream to the binary for a plugin.
     *
     * @param key the plugin key
     * @return the stream, or {@code null} if the plugin was not found
     */
    @Override
    @Transactional(readOnly = true)
    public InputStream getBinary(String key) {
        InputStream result = null;
        PluginDOImpl plugin = get(key, factory.getCurrentSession());
        Blob data = (plugin != null) ? plugin.getData() : null;
        if (data != null) {
            try {
                result = data.getBinaryStream();
            } catch (SQLException exception) {
                throw new RuntimeException("Failed to get plugin data for key=" + key + ": " + exception.getMessage(),
                                           exception);
            }
        }
        return result;
    }

    /**
     * Saves a plugin.
     *
     * @param key    the plugin key
     * @param name   the plugin name
     * @param stream stream to the plugin binary
     */
    @Override
    @Transactional
    public void save(String key, String name, InputStream stream) {
        Session session = factory.getCurrentSession();
        PluginDOImpl plugin = get(key, session);
        if (plugin == null) {
            plugin = new PluginDOImpl();
            plugin.setKey(key);
            plugin.setArchetypeId(new ArchetypeId("plugin.default", "1.0"));
            plugin.setActive(true);
        }
        plugin.setName(name);
        try {
            byte[] data = IOUtils.toByteArray(stream);
            // BlobProxy needs to know the data length, so can't use the stream directly
            plugin.setData(BlobProxy.generateProxy(data));
        } catch (IOException exception) {
            throw new RuntimeException("Failed to read " + name + ": " + exception.getMessage(), exception);
        }
        session.saveOrUpdate(plugin);
        session.flush();
    }

    /**
     * Removes the plugin with the given key.
     *
     * @param key the plugin key
     */
    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = false;
        Session session = factory.getCurrentSession();
        PluginDOImpl plugin = get(key, session);
        if (plugin != null) {
            session.remove(plugin);
            result = true;
        }
        return result;
    }

    /**
     * Returns a plugin given its key.
     *
     * @param key     the plugin key
     * @param session the session
     * @return the corresponding plugin or {@code null} if none is found
     */
    private PluginDOImpl get(String key, Session session) {
        Query<PluginDOImpl> query = session.createQuery(QUERY_BY_KEY, PluginDOImpl.class);
        query.setParameter("key", key);
        return get(query);
    }

    /**
     * Returns the first plugin matching a query.
     *
     * @param query the query
     * @return the first plugin, or {@code null} if none is found
     */
    private PluginDOImpl get(Query<PluginDOImpl> query) {
        List<PluginDOImpl> plugins = query.list();
        return !plugins.isEmpty() ? plugins.get(0) : null;
    }

    private static class Assembler extends CompoundAssembler {

        Assembler(IArchetypeDescriptorCache archetypes) {
            super(archetypes);
            addAssembler(new PluginAssembler());
        }
    }
}
