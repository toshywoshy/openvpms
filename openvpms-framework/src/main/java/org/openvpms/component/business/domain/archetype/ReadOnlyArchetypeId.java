/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.archetype;

/**
 * A read-only {@link ArchetypeId}.
 * <p/>
 * This exists to ensure that cached instances aren't updated via reflection.
 *
 * @author Tim Anderson
 */
public class ReadOnlyArchetypeId extends ArchetypeId {

    /**
     * Constructs a {@link ReadOnlyArchetypeId}.
     *
     * @param archetypeId the archetype id to copy
     */
    public ReadOnlyArchetypeId(ArchetypeId archetypeId) {
        super(archetypeId.getEntityName(), archetypeId.getConcept(), archetypeId.getVersion());
    }

    /**
     * Constructs a {@link ReadOnlyArchetypeId}.
     *
     * @param qname the qualified name. The version is optional
     * @throws ArchetypeIdException if an illegal archetype id has been specified
     */
    public ReadOnlyArchetypeId(String qname) {
        super(qname);
    }

    public ReadOnlyArchetypeId(String archetype, String version) {
        super(archetype, version);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return this;
    }

    @Override
    protected void setQualifiedName(String qname) {
        throw new IllegalStateException("ArchetypeId is read-only");
    }

    @Override
    protected void setShortName(String shortName) {
        throw new IllegalStateException("ArchetypeId is read-only");
    }

    @Override
    protected void setVersion(String version) {
        throw new IllegalStateException("ArchetypeId is read-only");
    }
}
