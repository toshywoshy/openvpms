/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.component.business.service.archetype;

import org.apache.commons.jxpath.AbstractFactory;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.Pointer;
import org.openvpms.component.business.domain.im.archetype.descriptor.NodeDescriptor;
import org.openvpms.component.business.domain.im.datatypes.quantity.Money;
import org.openvpms.component.system.common.util.ClassHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * This class is used to by JXPath during the object construction phase. It
 * uses reflection and the commons-beanutils library to create a node on an
 * object.
 * <p>
 * Objects created through this factory must have a default constructor.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class JXPathGenericObjectCreationFactory extends AbstractFactory {
    /**
     * Define a logger for this class
     */
    private static final Logger logger = LoggerFactory.getLogger(JXPathGenericObjectCreationFactory.class);

    /**
     * Default constructor.
     */
    public JXPathGenericObjectCreationFactory() {
        // no-op
    }

    /* (non-Javadoc)
     * @see org.apache.commons.jxpath.AbstractFactory#createObject(org.apache.commons.jxpath.JXPathContext, org.apache.commons.jxpath.Pointer, java.lang.Object, java.lang.String, int)
     */
    @Override
    public boolean createObject(JXPathContext context, Pointer ptr, Object parent, String name, int index) {
        try {
            NodeDescriptor node = (NodeDescriptor) context.getVariables().getVariable("node");

            if (logger.isDebugEnabled()) {
                logger.debug("root={}, parent={}, name={}, index={}, type={}", context.getContextBean(), parent, name,
                             index, node.getType());
            }

            Class<?> clazz = ClassHelper.getClass(node.getType());
            if (clazz == Boolean.class) {
                ptr.setValue(Boolean.FALSE);
            } else if (clazz == Integer.class) {
                ptr.setValue(0);
            } else if (clazz == Long.class) {
                ptr.setValue(0L);
            } else if (clazz == Double.class) {
                ptr.setValue(0.0);
            } else if (clazz == Float.class) {
                ptr.setValue(0.0F);
            } else if (clazz == Short.class) {
                ptr.setValue((short) 0);
            } else if (clazz == Byte.class) {
                ptr.setValue((byte) 0);
            } else if (clazz == Money.class) {
                ptr.setValue(Money.ZERO);
            } else if (clazz == BigDecimal.class) {
                ptr.setValue(BigDecimal.ZERO);
            } else {
                ptr.setValue(clazz.getDeclaredConstructor().newInstance());
            }
        } catch (Exception exception) {
            logger.error("root={}, parent={}, name={}, index={}", context.getContextBean(), parent, name, index,
                         exception);
            return false;
        }

        return true;
    }

}
