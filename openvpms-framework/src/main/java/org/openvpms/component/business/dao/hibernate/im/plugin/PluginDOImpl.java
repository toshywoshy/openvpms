/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.plugin;

import org.openvpms.component.business.dao.hibernate.im.common.AuditableIMObjectDOImpl;

import java.sql.Blob;

/**
 * Implementation of the {@link PluginDO} interface.
 *
 * @author Tim Anderson
 */
public class PluginDOImpl extends AuditableIMObjectDOImpl implements PluginDO {

    /**
     * The plugin key.
     */
    private String key;

    /**
     * The plugin binary.
     */
    private Blob data;

    /**
     * Returns the plugin key.
     *
     * @return the plugin key
     */
    @Override
    public String getKey() {
        return key;
    }

    /**
     * Sets the plugin key
     *
     * @param key the plugin key
     */
    @Override
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Returns the binary.
     *
     * @return the binary
     */
    @Override
    public Blob getData() {
        return data;
    }

    /**
     * Sets the binary.
     *
     * @param data the binary
     */
    @Override
    public void setData(Blob data) {
        this.data = data;
    }
}
