/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;


/**
 * Property file reader.
 *
 * @author Tim Anderson
 */
public abstract class PropertiesReader extends ConfigReader {

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(PropertiesReader.class);


    /**
     * Parse a property file entry.
     *
     * @param key   the property key
     * @param value the property value
     * @param path  the path the property came from
     */
    protected abstract void parse(String key, String value, String path);

    /**
     * Reads the configuration at the specified URL.
     *
     * @param url the URL to read
     */
    protected void read(URL url) {
        try {
            Properties properties = new Properties();
            try (InputStream stream = url.openStream()) {
                properties.load(stream);
            }
            Enumeration<?> keys = properties.propertyNames();
            String path = url.toString();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String value = properties.getProperty(key).trim();
                // trim required as Properties doesn't seem to remove
                // trailing whitespace
                parse(key, value, path);
            }
        } catch (IOException exception) {
            log.error("Failed to read properties: " + url, exception);
        }
    }

}
