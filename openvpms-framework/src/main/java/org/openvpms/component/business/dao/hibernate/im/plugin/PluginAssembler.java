/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.plugin;

import org.openvpms.component.business.dao.hibernate.im.common.AuditableIMObjectAssembler;
import org.openvpms.component.business.dao.hibernate.im.common.Context;
import org.openvpms.component.business.dao.hibernate.im.common.DOState;
import org.openvpms.component.business.domain.im.plugin.Plugin;

/**
 * Assembles {@link Plugin} instances from {@link PluginDO} instances and vice-versa.
 *
 * @author Tim Anderson
 */
public class PluginAssembler extends AuditableIMObjectAssembler<Plugin, PluginDO> {

    /**
     * Constructs a {@link PluginAssembler}.
     */
    public PluginAssembler() {
        super(null, Plugin.class, PluginDO.class, PluginDOImpl.class);
    }

    /**
     * Assembles an object from a data object.
     *
     * @param target  the object to assemble
     * @param source  the object to assemble from
     * @param context the assembly context
     */
    @Override
    protected void assembleObject(Plugin target, PluginDO source, Context context) {
        super.assembleObject(target, source, context);
        target.setKey(source.getKey());
    }

    /**
     * Assembles a data object from an object.
     *
     * @param target  the object to assemble
     * @param source  the object to assemble from
     * @param state   the data object state
     * @param context the assembly context
     */
    @Override
    protected void assembleDO(PluginDO target, Plugin source, DOState state, Context context) {
        super.assembleDO(target, source, state, context);
        target.setKey(source.getKey());
    }

    /**
     * Creates a new object.
     *
     * @param object the source data object
     * @return a new object corresponding to the supplied data object
     */
    @Override
    protected Plugin create(PluginDO object) {
        return new Plugin();
    }

    /**
     * Creates a new data object.
     *
     * @param object the source object
     * @return a new data object corresponding to the supplied object
     */
    @Override
    protected PluginDO create(Plugin object) {
        return new PluginDOImpl();
    }

}