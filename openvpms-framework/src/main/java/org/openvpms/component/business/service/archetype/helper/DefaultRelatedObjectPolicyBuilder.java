/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.PolicyBuilder;
import org.openvpms.component.model.bean.RelatedObjectPolicyBuilder;
import org.openvpms.component.model.bean.RelatedObjects;
import org.openvpms.component.model.object.Relationship;

import java.util.Comparator;
import java.util.Date;
import java.util.function.Predicate;

/**
 * Default implementation of {@link RelatedObjectPolicyBuilder}.
 *
 * @author Tim Anderson
 */
public class DefaultRelatedObjectPolicyBuilder<T, R extends Relationship, P extends RelatedObjects<T, R, P>>
        implements RelatedObjectPolicyBuilder<T, R, P> {

    /**
     * The parent {@link RelatedObjects}.
     */
    private final P parent;

    /**
     * The policy builder.
     */
    private final PolicyBuilder<R> builder;

    /**
     * Constructs an {@link DefaultRelatedObjectPolicyBuilder}.
     *
     * @param parent the parent
     * @param type   the relationship type
     */
    public DefaultRelatedObjectPolicyBuilder(P parent, Class<R> type) {
        this.parent = parent;
        builder = Policies.newPolicy(type);
    }

    /**
     * Selects active relationships, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> active() {
        builder.active();
        return this;
    }

    /**
     * Selects relationships active at the specified time, and returns active objects.
     * <p/>
     * This replaces any existing predicate.
     *
     * @param time the time
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> active(Date time) {
        builder.active(time);
        return this;
    }

    /**
     * Selects active objects.
     *
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> activeObjects() {
        builder.activeObjects();
        return this;
    }

    /**
     * Selects inactive objects.
     *
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> inactiveObjects() {
        builder.inactiveObjects();
        return this;
    }

    /**
     * Selects both active and inactive objects.
     *
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> anyObject() {
        builder.anyObject();
        return this;
    }

    /**
     * Sets the predicate for filtering relationships.
     *
     * @param predicate the predicate
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> predicate(Predicate<R> predicate) {
        builder.predicate(predicate);
        return this;
    }

    /**
     * Adds a predicate that is a logical AND of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> and(Predicate<R> predicate) {
        builder.and(predicate);
        return this;
    }

    /**
     * Adds a predicate that is a logical OR of any existing predicate.
     *
     * @param predicate the predicate
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> or(Predicate<R> predicate) {
        builder.or(predicate);
        return this;
    }

    /**
     * Sets the comparator for ordering relationships.
     *
     * @param comparator the comparator
     * @return this
     */
    @Override
    public RelatedObjectPolicyBuilder<T, R, P> comparator(Comparator<R> comparator) {
        builder.comparator(comparator);
        return this;
    }

    /**
     * Builds the {@link RelatedObjects} for the policy.
     *
     * @return an instance that selects objects according to the policy
     */
    @Override
    public P build() {
        return parent.policy(builder.build());
    }
}
