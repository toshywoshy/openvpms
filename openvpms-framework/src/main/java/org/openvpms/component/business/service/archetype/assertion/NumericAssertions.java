/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.assertion;

import org.openvpms.component.business.domain.im.archetype.descriptor.ActionContext;
import org.openvpms.component.business.domain.im.archetype.descriptor.ActionTypeDescriptor;
import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor;


/**
 * Assertions for numeric nodes.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @see ActionTypeDescriptor
 * @see AssertionTypeDescriptor
 */
public class NumericAssertions {

    /**
     * Default constructor.
     */
    private NumericAssertions() {
        // no-op
    }

    /**
     * Determines if a node value is positive ({@code > 0}).
     *
     * @param context the assertion context
     * @return {@code true} if value is positive, otherwise {@code false}
     */
    public static boolean positive(ActionContext context) {
        Number number = (Number) context.getValue();
        return number.doubleValue() > 0.0;
    }

    /**
     * Determines if a node value is negative ({@code < 0}).
     *
     * @param context the assertion context
     * @return {@code true} if value is negative, otherwise {@code false}
     */
    public static boolean negative(ActionContext context) {
        Number number = (Number) context.getValue();
        return number.doubleValue() < 0.0;
    }

    /**
     * Determines if a node value is non-negative ({@code >= 0}).
     *
     * @param context the assertion context
     * @return {@code true} if value is negative, otherwise {@code false}
     */
    public static boolean nonNegative(ActionContext context) {
        Number number = (Number) context.getValue();
        return number.doubleValue() >= 0.0;
    }
}
