package org.openvpms.component.business.service.security;


import org.springframework.security.access.AccessDeniedException;

/**
 * .
 *
 * @author Tim Anderson
 */
public class ArchetypeAccessDeniedException extends AccessDeniedException {

    private final String archetype;

    private final String operation;

    /**
     * Constructs an {@link ArchetypeAccessDeniedException}.
     *
     * @param archetype the archetype access is denied to
     * @param operation the operation being performed
     */
    public ArchetypeAccessDeniedException(String archetype, String operation) {
        super("Access denied to " + operation + " " + archetype);
        this.archetype = archetype;
        this.operation = operation;
    }

    public String getArchetype() {
        return archetype;
    }

    public String getOperation() {
        return operation;
    }
}
