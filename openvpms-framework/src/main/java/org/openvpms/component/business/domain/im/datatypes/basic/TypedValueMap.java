/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.domain.im.datatypes.basic;

import org.apache.commons.collections4.keyvalue.DefaultMapEntry;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


/**
 * A map that adapts an underlying map of name->TypedValue pairs to
 * a map of name->Object pairs.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 */
public class TypedValueMap implements Map<String, Object> {

    /**
     * The underlying map.
     */
    private final Map<String, TypedValue> map;


    /**
     * Constructs a {@link TypedValueMap}, backed by a {@code HashMap}.
     */
    public TypedValueMap() {
        this(new HashMap<>());
    }

    /**
     * Constructs a {@link TypedValueMap}.
     *
     * @param map the underlying map
     */
    public TypedValueMap(Map<String, TypedValue> map) {
        this.map = map;
    }

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map.
     */
    @Override
    public int size() {
        return map.size();
    }

    /**
     * Returns {@code true} if this map contains no key-value mappings.
     *
     * @return {@code true} if this map contains no key-value mappings.
     */
    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * Returns {@code true} if this map contains a mapping for the specified
     * key.
     *
     * @param key key whose presence in this map is to be tested.
     * @return {@code true} if this map contains a mapping for the specified
     * key.
     * @throws ClassCastException   if the key is of an inappropriate type for
     *                              this map (optional).
     * @throws NullPointerException if the key is {@code null} and this map
     *                              does not permit {@code null} keys
     *                              (optional).
     */
    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    /**
     * Returns {@code true} if this map maps one or more keys to the
     * specified value.
     *
     * @param value value whose presence in this map is to be tested.
     * @return {@code true} if this map maps one or more keys to the
     * specified value.
     * @throws ClassCastException   if the value is of an inappropriate type for
     *                              this map (optional).
     * @throws NullPointerException if the value is {@code null} and this map
     *                              does not permit {@code null} values
     *                              (optional).
     */
    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(new TypedValue(value));
    }

    /**
     * Returns the value to which this map maps the specified key.
     *
     * @param key key whose associated value is to be returned.
     * @return the value to which this map maps the specified key, or
     * {@code null} if the map contains no mapping for this key.
     * @throws ClassCastException   if the key is of an inappropriate type for
     *                              this map (optional).
     * @throws NullPointerException if the key is {@code null} and this map
     *                              does not permit {@code null} keys (optional).
     * @see #containsKey(Object)
     */
    @Override
    public Object get(Object key) {
        TypedValue value = map.get(key);
        return (value != null) ? value.getObject() : null;
    }

    /**
     * Associates the specified value with the specified key in this map
     * (optional operation).
     *
     * @param key   key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @return previous value associated with specified key, or {@code null}
     * if there was no mapping for key.  A {@code null} return can
     * also indicate that the map previously associated {@code null}
     * with the specified key, if the implementation supports
     * {@code null} values.
     * @throws UnsupportedOperationException if the {@code put} operation is
     *                                       not supported by this map.
     * @throws ClassCastException            if the class of the specified key or value
     *                                       prevents it from being stored in this map.
     * @throws IllegalArgumentException      if some aspect of this key or value
     *                                       prevents it from being stored in this map.
     * @throws NullPointerException          if this map does not permit {@code null}
     *                                       keys or values, and the specified key or value is
     *                                       {@code null}.
     */
    @Override
    public Object put(String key, Object value) {
        Object old = get(key);
        if (value == null) {
            map.remove(key);
        } else {
            map.put(key, new TypedValue(value));
        }
        return old;
    }

    /**
     * Removes the mapping for this key from this map if it is present
     * (optional operation).
     *
     * @param key key whose mapping is to be removed from the map.
     * @return previous value associated with specified key, or {@code null}
     * if there was no mapping for key.
     * @throws ClassCastException            if the key is of an inappropriate type for
     *                                       this map (optional).
     * @throws NullPointerException          if the key is {@code null} and this map
     *                                       does not permit {@code null} keys (optional).
     * @throws UnsupportedOperationException if the {@code remove} method is
     *                                       not supported by this map.
     */
    @Override
    public Object remove(Object key) {
        Object old = get(key);
        map.remove(key);
        return old;
    }

    /**
     * Copies all of the mappings from the specified map to this map
     * (optional operation).
     *
     * @param t Mappings to be stored in this map.
     * @throws UnsupportedOperationException if the {@code putAll} method is
     *                                       not supported by this map.
     * @throws ClassCastException            if the class of a key or value in the
     *                                       specified map prevents it from being stored in this map.
     * @throws IllegalArgumentException      some aspect of a key or value in the
     *                                       specified map prevents it from being stored in this map.
     * @throws NullPointerException          if the specified map is {@code null}, or if
     *                                       this map does not permit {@code null} keys or values, and the
     *                                       specified map contains {@code null} keys or values.
     */
    @Override
    public void putAll(Map<? extends String, ?> t) {
        for (Map.Entry<? extends String, ?> entry :
                t.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Removes all mappings from this map.
     */
    @Override
    public void clear() {
        map.clear();
    }

    /**
     * Returns a set view of the keys contained in this map.  The set is
     * backed by the map, so changes to the map are reflected in the set, and
     * vice-versa.
     *
     * @return a set view of the keys contained in this map.
     */
    @Override
    public Set<String> keySet() {
        return map.keySet();
    }

    /**
     * Returns a collection view of the values contained in this map.  The
     * collection is backed by the map, so changes to the map are reflected in
     * the collection, and vice-versa.
     *
     * @return a collection view of the values contained in this map.
     */
    @Override
    public Collection<Object> values() {
        return new ValueCollection();
    }

    /**
     * Returns a set view of the mappings contained in this map.  Each element
     * in the returned set is a {@link Entry}.  The set is backed by the
     * map, so changes to the map are reflected in the set, and vice-versa.
     *
     * @return a set view of the mappings contained in this map.
     */
    @Override
    public Set<Entry<String, Object>> entrySet() {
        return new EntrySet();
    }

    /**
     * Helper to create a new map of names to {@link TypedValue} instances
     * from a name->object map.
     *
     * @param map the map to convert. May be {@code null}
     * @return a new map, or {@code null}
     */
    public static Map<String, TypedValue> create(Map<String, Object> map) {
        if (map == null) {
            return null;
        }
        Map<String, TypedValue> result = new HashMap<>(map.size());
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() != null) {
                result.put(entry.getKey(), new TypedValue(entry.getValue()));
            }
        }
        return result;
    }

    /**
     * Adapts the underlying map's values collection.
     */
    private class ValueCollection extends AbstractCollection<Object> {

        /**
         * Returns an iterator over the elements contained in this collection.
         *
         * @return an iterator over the elements contained in this collection.
         */
        @Override
        public Iterator<Object> iterator() {
            return new ValueIterator(map.values().iterator());
        }

        /**
         * Returns the number of elements in this collection.  If the collection
         * contains more than {@code Integer.MAX_VALUE} elements, returns
         * {@code Integer.MAX_VALUE}.
         *
         * @return the number of elements in this collection.
         */
        @Override
        public int size() {
            return map.size();
        }

    }

    /**
     * Adapts the underlying map's values iterator.
     */
    private static class ValueIterator implements Iterator<Object> {

        private final Iterator<TypedValue> iterator;

        public ValueIterator(Iterator<TypedValue> iterator) {
            this.iterator = iterator;
        }

        /**
         * Returns {@code true} if the iteration has more elements. (In other
         * words, returns {@code true} if {@code next} would return an element
         * rather than throwing an exception.)
         *
         * @return {@code true} if the iterator has more elements.
         */
        public boolean hasNext() {
            return iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.  Calling this method
         * repeatedly until the {@link #hasNext()} method returns false will
         * return each element in the underlying collection exactly once.
         *
         * @return the next element in the iteration.
         * @throws NoSuchElementException iteration has no more elements.
         */
        public Object next() {
            return iterator.next().getObject();
        }

        /**
         * Removes from the underlying collection the last element returned by the
         * iterator (optional operation).
         */
        public void remove() {
            iterator.remove();
        }
    }

    /**
     * Adapts the underlying map's entry set.
     */
    private class EntrySet extends AbstractSet<Map.Entry<String, Object>> {

        /**
         * Returns an iterator over the elements contained in this collection.
         *
         * @return an iterator over the elements contained in this collection.
         */
        public Iterator<Entry<String, Object>> iterator() {
            return new EntrySetIterator(map.entrySet().iterator());
        }

        /**
         * Returns the number of elements in this collection.
         *
         * @return the number of elements in this collection.
         */
        public int size() {
            return map.entrySet().size();
        }
    }

    /**
     * Adapts the underlying map's entry set iterator.
     */
    private static class EntrySetIterator implements Iterator<Map.Entry<String, Object>> {

        private final Iterator<Map.Entry<String, TypedValue>> iterator;

        public EntrySetIterator(Iterator<Map.Entry<String, TypedValue>> iterator) {
            this.iterator = iterator;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         *
         * @return {@code true} if the iterator has more elements.
         */
        public boolean hasNext() {
            return iterator.hasNext();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration.
         * @throws NoSuchElementException iteration has no more elements.
         */
        public Map.Entry<String, Object> next() {
            Map.Entry<String, TypedValue> entry = iterator.next();
            return new DefaultMapEntry<>(entry.getKey(), entry.getValue().getObject());
        }

        /**
         * Removes from the underlying collection the last element returned by the
         * iterator.
         */
        public void remove() {
            iterator.remove();
        }
    }


}
