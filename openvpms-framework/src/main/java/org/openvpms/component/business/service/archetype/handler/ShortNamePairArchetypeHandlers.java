/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.handler;

import org.openvpms.component.service.archetype.ArchetypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * Loads properties resources containing a mapping of short names to the
 * implementation classes that can handle them. Each property file entry is
 * of the form:
 * {@code &lt;primaryShortName&gt;[,secondaryShortName] &lt;className&gt;}
 *
 * @author Tim Anderson
 */
public class ShortNamePairArchetypeHandlers<T> extends AbstractArchetypeHandlers<T> {

    /**
     * The class that each handler must implement/extend.
     */
    private final Class<T> type;

    /**
     * Map of primary short names to their corresponding handler classes.
     */
    private final Map<String, Handlers<T>> handlers = new HashMap<>();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ShortNamePairArchetypeHandlers.class);


    /**
     * Construct a new {@code ShortNamePairArchetypeHandlers}.
     *
     * @param name    the resource name
     * @param type    class the each handler must implement/extend
     * @param service the archetype service
     */
    public ShortNamePairArchetypeHandlers(String name, Class<T> type, ArchetypeService service) {
        super(service);
        this.type = type;
        load(name);
    }

    /**
     * Merges handlers from the specified resource.
     *
     * @param name the resource name
     */
    public void load(String name) {
        Reader parser = new Reader();
        parser.read(name);
    }

    /**
     * Returns a class that can handle an archetype.
     *
     * @param shortName the archetype short name
     * @return an implemenation that supports {@code shortName} or
     * {@code null} if there is no match
     */
    public ArchetypeHandler<T> getHandler(String shortName) {
        return getHandler(shortName, null);
    }

    /**
     * Returns a handler that can handle a pair of short names.
     *
     * @param primary   the primary archetype short name
     * @param secondary the secondary archetype short name.
     *                  May be {@code null}
     * @return an implemenation that supports the short names  or
     * {@code null} if there is no match
     */
    public ArchetypeHandler<T> getHandler(String primary, String secondary) {
        ArchetypeHandler<T> handler = null;
        String match = getShortName(primary, handlers.keySet());
        if (match != null) {
            Handlers<T> handlers = this.handlers.get(match);
            if (secondary != null) {
                match = getShortName(secondary, handlers.getShortNames());
                if (match != null) {
                    handler = handlers.get(match);
                }
            }
            if (handler == null) {
                // no secondary handler so fall back to the primary handler
                handler = handlers.getHandler();
            }
        }
        return handler;
    }

    /**
     * Property file parser.
     */
    private class Reader extends PropertiesReader {

        /**
         * Parse a property file entry.
         *
         * @param key   the property key
         * @param value the property value
         * @param path  the path the property came from
         */
        @SuppressWarnings("unchecked")
        protected void parse(String key, String value, String path) {
            Class<T> clazz = (Class<T>) getClass(value, type, path);
            if (clazz != null) {
                String[] pair = key.split(",");
                if (pair.length == 0 || pair.length > 2) {
                    log.error("Invalid short name pair={}, loaded from path={}", key, path);
                } else if (pair.length == 1) {
                    addHandler(pair[0], clazz, path);
                } else {
                    addHandler(pair[0], pair[1], clazz, path);
                }
            }
        }

        /**
         * Adds a handler for a primary short name.
         *
         * @param shortName the primary short name
         * @param type      the handler type
         * @param path      the path the handler came from
         */
        private void addHandler(String shortName, Class<T> type, String path) {
            String[] matches = getShortNames(shortName, path);
            if (matches.length != 0) {
                Handlers<T> handlers = getHandlers(shortName);
                if (handlers.getHandler() != null) {
                    log.warn("Duplicate short name={} from {}: ignoring", shortName, path);

                } else {
                    ArchetypeHandler<T> handler = new ArchetypeHandler<>(shortName, type);
                    handlers.setHandler(handler);
                }
            } else {
                log.warn("Invalid archetype for handler={}, short name={} from {}: ignoring", type, shortName, path);
            }
        }

        /**
         * Adds a handler for a short name pair.
         *
         * @param primary   the primary short name
         * @param secondary the secondary short name
         * @param type      the handler type
         * @param path      the path the handler came from
         */
        private void addHandler(String primary, String secondary, Class<T> type, String path) {
            String[] primaryMatches = getShortNames(primary, path);
            String[] secondaryMatches = getShortNames(secondary, path);
            if (primaryMatches.length != 0 && secondaryMatches.length != 0) {
                Handlers<T> handlers = getHandlers(primary);
                if (handlers.get(secondary) != null) {
                    log.warn("Duplicate short name={} for primary short name={} from {}: ignoring", secondary, primary,
                             path);
                } else {
                    ArchetypeHandler<T> handler = new ArchetypeHandler<>(secondary, type);
                    handlers.add(secondary, handler);
                }
            }
        }

        /**
         * Returns the archetype short names matching a short name.
         *
         * @param shortName the short name
         * @param path      the path the handler came from
         * @return a list of short names matching {@code shprtName}
         */
        private String[] getShortNames(String shortName, String path) {
            String[] matches = ShortNamePairArchetypeHandlers.this.getShortNames(shortName, false);
            if (matches.length == 0) {
                log.warn("No archetypes found matching short name={}, loaded from path={}", shortName, path);
            }
            return matches;
        }

        /**
         * Returns the handlers for a short name.
         *
         * @param shortName the short names
         * @return the handlers for {@code shortName}
         */
        private Handlers<T> getHandlers(String shortName) {
            return handlers.computeIfAbsent(shortName, (s) -> new Handlers<>());
        }
    }

    /**
     * Registers the handlers for a primary short name.
     */
    private static class Handlers<T> {

        /**
         * The handlers, keyed on short name.
         */
        private final Map<String, ArchetypeHandler<T>> handlers = new HashMap<>();

        /**
         * The global handler.
         */
        private ArchetypeHandler<T> handler;

        /**
         * Sets the primary handler.
         *
         * @param handler the handler
         */
        public void setHandler(ArchetypeHandler<T> handler) {
            this.handler = handler;
        }

        /**
         * Returns the primary handler.
         *
         * @return the handler. May be {@code null}
         */
        public ArchetypeHandler<T> getHandler() {
            return handler;
        }

        /**
         * Adds a handler for a secondary short name.
         *
         * @param shortName the secondary short name
         * @param handler   the handler handler
         */
        public void add(String shortName, ArchetypeHandler<T> handler) {
            handlers.put(shortName, handler);
        }

        /**
         * Returns the handler for a secondary short name.
         *
         * @param shortName the secondary short name
         * @return the handler type
         */
        public ArchetypeHandler<T> get(String shortName) {
            return handlers.get(shortName);
        }

        /**
         * Returns the secondary short names for which there are registered
         * handlers.
         *
         * @return the secondary short names
         */
        public Set<String> getShortNames() {
            return handlers.keySet();
        }

    }
}
