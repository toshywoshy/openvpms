/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.dao.hibernate.im.common;

import org.openvpms.component.business.dao.hibernate.im.security.UserDO;
import org.openvpms.component.model.object.AuditableIMObject;

import java.util.Date;

/**
 * Data object interface corresponding to the {@link AuditableIMObject} interface.
 *
 * @author Tim Anderson
 */
public interface AuditableIMObjectDO extends IMObjectDO {

    /**
     * Returns the time the object was created.
     *
     * @return the create time. May be {@code null}
     */
    Date getCreated();

    /**
     * Sets the time the object was created.
     *
     * @param created the create time. May be {@code null}
     */
    void setCreated(Date created);

    /**
     * Returns the user that created the object.
     *
     * @return the user. May be {@code null}
     */
    UserDO getCreatedBy();

    /**
     * Sets the user that created the object.
     *
     * @param createdBy the user. May be {@code null}
     */
    void setCreatedBy(UserDO createdBy);

    /**
     * Returns the time when the object was updated.
     *
     * @return the update time. May be {@code null}
     */
    Date getUpdated();

    /**
     * Sets the time when the object was updated.
     *
     * @param updated the update time. May be {@code null}
     */
    void setUpdated(Date updated);

    /**
     * Returns the user that updated the object.
     *
     * @return the user. May be {@code null}
     */
    UserDO getUpdatedBy();

    /**
     * Sets the user that updated the object.
     *
     * @param updatedBy the user. May be {@code null}
     */
    void setUpdatedBy(UserDO updatedBy);

}
