/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.business.service.archetype.helper;

import org.openvpms.component.model.bean.ObjectRelationship;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.bean.Policy.State;
import org.openvpms.component.model.bean.RelatedIMObjects;
import org.openvpms.component.model.bean.RelatedObjectPolicyBuilder;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Default implementation of {@link RelatedIMObjects}.
 *
 * @author Tim Anderson
 */
public class RelatedIMObjectsImpl<T extends IMObject, R extends Relationship> implements RelatedIMObjects<T, R> {

    /**
     * The relationships.
     */
    private final Collection<R> relationships;

    /**
     * Returns the reference from a relationship.
     */
    private final Function<R, Reference> accessor;

    /**
     * Used to resolve an object given its reference.
     */
    private final BiFunction<Reference, State, T> resolver;

    /**
     * The object retrieval policy.
     */
    private final Policy<R> policy;

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     * <p/>
     * The object determines if the source or target of the relationships are returned.
     *
     * @param object        the object
     * @param relationships the object's relationships
     * @param type          the type of the related objects
     * @param service       the archetype service
     */
    public RelatedIMObjectsImpl(IMObject object, Collection<R> relationships, Class<T> type, ArchetypeService service) {
        this(relationships, getAccessor(object.getObjectReference()), getResolver(service, type));
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     * <p/>
     * The object determines if the source or target of the relationships are returned.
     *
     * @param object        the object
     * @param relationships the object's relationships
     * @param resolver      used to resolve the related objects
     */
    public RelatedIMObjectsImpl(IMObject object, Collection<R> relationships,
                                BiFunction<Reference, State, T> resolver) {
        this(relationships, getAccessor(object.getObjectReference()), resolver);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param type          the type of the related objects
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param service       the archetype service
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Class<T> type, boolean source, ArchetypeService service) {
        this(relationships, getAccessor(source), getResolver(service, type));
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param type          the type of the related objects
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param service       the archetype service
     * @param policy        the policy for object retrieval, or {@code null} to retrieve all objects
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Class<T> type, boolean source, ArchetypeService service,
                                Policy<R> policy) {
        this(relationships, getAccessor(source), getResolver(service, type), policy);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param source        if {@code true}, return the source of the relationships, else return the target
     * @param resolver      used to resolve the related objects
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, boolean source, BiFunction<Reference, State, T> resolver) {
        this(relationships, getAccessor(source), resolver);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param accessor      returns the related object reference from a relationship.
     * @param resolver      used to resolve the related objects
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Function<R, Reference> accessor,
                                BiFunction<Reference, State, T> resolver) {
        this(relationships, accessor, resolver, null);
    }

    /**
     * Constructs a {@link RelatedIMObjectsImpl}.
     *
     * @param relationships the object's relationships
     * @param accessor      returns the related object reference from a relationship.
     * @param resolver      used to resolve the related objects
     * @param policy        the policy for object retrieval, or {@code null} to retrieve all objects
     */
    public RelatedIMObjectsImpl(Collection<R> relationships, Function<R, Reference> accessor,
                                BiFunction<Reference, State, T> resolver, Policy<R> policy) {
        this.relationships = relationships;
        this.accessor = accessor;
        this.resolver = resolver;
        this.policy = policy;
    }

    /**
     * Selects all related objects for retrieval.
     * <p/>
     * This is the default.
     *
     * @return an instance that selects all related objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedIMObjects<T, R> all() {
        return policy((Policy<R>) Policies.all());
    }

    /**
     * Selects active relationships and objects for retrieval.
     *
     * @return an instance that selects active relationships and objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedIMObjects<T, R> active() {
        return policy((Policy<R>) Policies.active());
    }

    /**
     * Selects relationships active at the specified time and returns active objects.
     *
     * @param time the time
     * @return an instance that selects active relationships at {@code time}, and active objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedIMObjects<T, R> active(Date time) {
        return policy((Policy<R>) Policies.active(time));
    }

    /**
     * Selects objects based on the specified policy.
     *
     * @param policy the policy
     * @return an instance that selects objects according to {@code policy}
     */
    @Override
    public RelatedIMObjects<T, R> policy(Policy<R> policy) {
        return !hasPolicy(policy) ? newInstance(relationships, accessor, resolver, policy) : this;
    }

    /**
     * Returns a policy builder to select objects.
     *
     * @return a new policy builder
     */
    @Override
    @SuppressWarnings("unchecked")
    public RelatedObjectPolicyBuilder<T, R, RelatedIMObjects<T, R>> newPolicy() {
        return new DefaultRelatedObjectPolicyBuilder<>(this, (Class<R>) Relationship.class);
    }

    /**
     * Returns the first object to match the criteria, after applying any comparator.
     * <br/>
     * If {@code state == State.ACTIVE} the object must be active in order to be returned, otherwise an active object
     * will be returned in preference to an inactive one.
     */
    @Override
    public T getObject() {
        ObjectRelationship<T, R> result = getObjectRelationship();
        return (result != null) ? result.getObject() : null;
    }

    /**
     * Returns the first object and relationship to match the {@link #getPolicy() policy}.
     * <ul>
     *     <li>If a {@link Policy#getComparator() comparator} is registered, this will be applied.</li>
     *     <li>If {@link Policy#getState() state} == State.ACTIVE} the object must be active in order to be returned,
     *     otherwise an active object will be returned in preference to an inactive one.</li>
     * </ul>
     *
     * @return the first object and relationship, or {@code null} if none is found
     */
    @Override
    public ObjectRelationship<T, R> getObjectRelationship() {
        ObjectRelationship<T, R> result = null;
        State state = getState();
        for (R relationship : getRelationships()) {
            Reference reference = accessor.apply(relationship);
            if (reference != null) {
                T object = resolver.apply(reference, state);
                if (object != null) {
                    if (object.isActive() || state == Policy.State.INACTIVE) {
                        // found a match, so return it
                        result = new ObjectRelationshipImpl<>(object, relationship);
                        break;
                    } else if (result == null) {
                        // can return inactive, but keep looking for an active match
                        result = new ObjectRelationshipImpl<>(object, relationship);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns the related objects.
     *
     * @return the related objects
     */
    @Override
    public Iterable<T> getObjects() {
        List<Reference> references = getReferences();
        return () -> new LazyObjectIterator(references.iterator());
    }

    /**
     * Returns the objects and their corresponding relationships matching the criteria.
     *
     * @return the objects and their corresponding relationships
     */
    @Override
    public Iterable<ObjectRelationship<T, R>> getObjectRelationships() {
        List<R> relationships = getRelationships();
        return () -> new LazyRelationshipIterator(relationships.iterator());
    }

    /**
     * Returns the relationships matching the criteria.
     *
     * @return the relationships matching the criteria
     */
    @Override
    public List<R> getRelationships() {
        List<R> result = new ArrayList<>();
        Predicate<R> predicate = (policy != null) ? policy.getPredicate() : null;
        if (predicate == null) {
            result = new ArrayList<>(relationships);
        } else {
            for (R relationship : relationships) {
                if (predicate.test(relationship)) {
                    result.add(relationship);
                }
            }
        }
        Comparator<R> comparator = (policy != null) ? policy.getComparator() : null;
        if (comparator != null && !result.isEmpty()) {
            result.sort(comparator);
        }
        return result;
    }

    /**
     * Returns the relationship references matching the criteria.
     * <p/>
     * NOTE: this does not determine if the objects are active or inactive
     *
     * @return the related object references
     */
    @Override
    public List<Reference> getReferences() {
        List<Reference> result = new ArrayList<>();
        for (R relationship : getRelationships()) {
            Reference reference = accessor.apply(relationship);
            if (reference != null) {
                result.add(reference);
            }
        }
        return result;
    }

    /**
     * Returns the policy being used to select objects.
     *
     * @return the policy. May be {@code null} to indicate that all objects are returned
     */
    @Override
    public Policy<R> getPolicy() {
        return policy;
    }

    /**
     * Determines if the policy specified is the same as that being used.
     *
     * @param policy the policy. May be {@code null}
     * @return {@code true} if the policies are the same, otherwise {@code false}
     */
    public boolean hasPolicy(Policy<R> policy) {
        return Objects.equals(this.policy, policy) || (isAll(this.policy) && isAll(policy));
    }

    /**
     * Creates a new instance.
     *
     * @param relationships the relationships.
     * @param accessor      returns the related object reference from a relationship.
     * @param resolver      used to resolve the related objects
     * @param policy        the policy for object retrieval, or {@code null} to retrieve all objects
     * @return a new instance
     */
    protected RelatedIMObjectsImpl<T, R> newInstance(Collection<R> relationships, Function<R, Reference> accessor,
                                                     BiFunction<Reference, State, T> resolver, Policy<R> policy) {
        return new RelatedIMObjectsImpl<>(relationships, accessor, resolver, policy);
    }

    /**
     * Determines if a policy selects all objects.
     *
     * @param policy the policy. A {@code null} policy indicates it selects all objects
     * @return {@code true} if the policy selects all objects, otherwise {@code false}
     */
    private boolean isAll(Policy<R> policy) {
        return (policy == null || Objects.equals(policy, Policies.all()));
    }

    /**
     * Returns a function to resolve objects by reference from an {@link ArchetypeService}.
     *
     * @param service the archetype service
     * @param type    the type of the objects
     * @return a function to resolve objects by reference
     */
    private static <T extends IMObject> BiFunction<Reference, State, T> getResolver(ArchetypeService service,
                                                                                    Class<T> type) {
        return (reference, state) -> type.cast(resolve(reference, state, service));
    }

    /**
     * Returns the active state of the objects to return.
     *
     * @return the active state
     */
    private State getState() {
        return (policy != null) ? policy.getState() : State.ANY;
    }

    /**
     * Resolves an object given its reference.
     *
     * @param reference the object reference
     * @param state     the active state
     * @param service   the archetype service
     * @return the corresponding object, or {@code null} if none is found or doesn't match the state
     */
    private static IMObject resolve(Reference reference, State state, ArchetypeService service) {
        IMObject result = null;
        if (reference != null) {
            if (state == Policy.State.ANY) {
                result = service.get(reference);
            } else {
                boolean active = state == Policy.State.ACTIVE;
                result = service.get(reference, active);
            }
        }
        return result;
    }

    /**
     * Returns a function that returns the source or target of a relationship.
     *
     * @param source if {@code true}, return the source of the relationships, else return the target
     * @return a new function
     */
    private static <R extends Relationship> Function<R, Reference> getAccessor(boolean source) {
        return (source) ? R::getSource : R::getTarget;
    }

    /**
     * Returns a function that returns the related reference from a relationship.
     *
     * @param reference the reference to the object
     * @return a function that returns the reference related to {@code reference}
     */
    private static <R extends Relationship> Function<R, Reference> getAccessor(Reference reference) {
        return relationship -> {
            Reference target = relationship.getTarget();
            Reference related = null;
            if (target != null && !target.equals(reference)) {
                related = target;
            } else {
                Reference source = relationship.getSource();
                if (source != null && !source.equals(reference)) {
                    related = source;
                }
            }
            return related;
        };
    }

    /**
     * An iterator over relationships that returns {@link ObjectRelationship} instances where the object is lazily
     * constructed.
     */
    private class LazyRelationshipIterator extends AdaptingIterator<R, ObjectRelationship<T, R>> {

        public LazyRelationshipIterator(Iterator<R> iterator) {
            super(iterator);
        }

        /**
         * Adapts an object.
         *
         * @param relationship the object to adapt
         * @return the object. May be {@code null}
         */
        @Override
        ObjectRelationship<T, R> adapt(R relationship) {
            Reference reference = accessor.apply(relationship);
            T object = (reference != null) ? resolver.apply(reference, getState()) : null;
            return object != null ? new ObjectRelationshipImpl<>(object, relationship) : null;
        }
    }

    /**
     * An iterator over references that returns {@link IMObject} instances where the object is lazily loaded.
     */
    private class LazyObjectIterator extends AdaptingIterator<Reference, T> {

        public LazyObjectIterator(Iterator<Reference> references) {
            super(references);
        }

        /**
         * Adapts an object.
         *
         * @param object the object to adapt
         * @return the object. May be {@code null}
         */
        @Override
        T adapt(Reference object) {
            return resolver.apply(object, getState());
        }
    }

    /**
     * An iterator that adapts the results of an iterator to a different type.
     */
    private abstract static class AdaptingIterator<A, B> implements Iterator<B> {

        /**
         * The iterator to adapt.
         */
        private final Iterator<A> iterator;

        /**
         * The next object to return.
         */
        private B next;

        /**
         * Constructs an {@link AdaptingIterator}.
         *
         * @param iterator the iterator to adapt
         */
        protected AdaptingIterator(Iterator<A> iterator) {
            this.iterator = iterator;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            if (next == null) {
                next = resolve();
            }
            return next != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public B next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            B result = next;
            next = null;
            return result;
        }

        /**
         * Adapts an object.
         * <p/>
         * NOTE: if the returned object is {@code null}, it will be skipped in the iteration. i.e. {@link #next}
         * will never return {@code null}.
         *
         * @param object the object to adapt
         * @return the object. May be {@code null}
         */
        abstract B adapt(A object);

        /**
         * Resolves the next object.
         *
         * @return the next object, or {@code null} if there is none
         */
        private B resolve() {
            while (next == null && iterator.hasNext()) {
                next = adapt(iterator.next());
            }
            return next;
        }
    }
}
