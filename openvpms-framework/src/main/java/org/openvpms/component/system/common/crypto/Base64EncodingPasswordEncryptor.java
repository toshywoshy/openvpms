package org.openvpms.component.system.common.crypto;

import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.security.crypto.encrypt.BytesEncryptor;

import java.util.Base64;

/**
 * Delegates to an {@link BytesEncryptor} to encrypt text strings. Raw text strings are
 * UTF-8 encoded before being passed to the encryptor. Encrypted strings are returned base64-encoded.
 *
 * @author Tim Anderson
 */
public class Base64EncodingPasswordEncryptor implements PasswordEncryptor {

    /**
     * The bytes encryptor.
     */
    private final BytesEncryptor bytesEncryptor;

    /**
     * Constructs a {@link Base64EncodingPasswordEncryptor}.
     *
     * @param bytesEncryptor the bytes encryptor to delegate to
     */
    public Base64EncodingPasswordEncryptor(BytesEncryptor bytesEncryptor) {
        this.bytesEncryptor = bytesEncryptor;
    }

    /**
     * Encrypts a password.
     *
     * @param password the password
     */
    @Override
    public String encrypt(String password) {
        return Base64.getEncoder().withoutPadding().encodeToString(bytesEncryptor.encrypt(Utf8.encode(password)));
    }

    /**
     * Decrypts a password.
     *
     * @param encryptedPassword the encrypted password
     */
    @Override
    public String decrypt(String encryptedPassword) {
        return Utf8.decode(bytesEncryptor.decrypt(Base64.getDecoder().decode(encryptedPassword)));
    }
}
