package org.openvpms.component.system.common.crypto;

import org.openvpms.component.security.crypto.PasswordEncryptor;
import org.springframework.security.crypto.encrypt.Encryptors;

/**
 * Default implementation of the {@link PasswordEncryptorFactory}.
 *
 * @author Tim Anderson
 */
public class DefaultPasswordEncryptorFactory implements PasswordEncryptorFactory {

    /**
     * The the salt and password separated by a colon and base64-encoded.
     */
    private final String key;

    /**
     * Constructs a {@link DefaultPasswordEncryptorFactory}.
     *
     * @param key the salt and password separated by a colon and base64-encoded
     */
    public DefaultPasswordEncryptorFactory(String key) {
        this.key = key;
    }

    /**
     * Creates a password encryptor.
     *
     * @return a new password encryptor
     */
    @Override
    public PasswordEncryptor create() {
        String[] pair = KeyGenerator.decode(key);
        String salt = pair[0];
        String password = pair[1];
        return new Base64EncodingPasswordEncryptor(Encryptors.stronger(password, salt));
    }

}
