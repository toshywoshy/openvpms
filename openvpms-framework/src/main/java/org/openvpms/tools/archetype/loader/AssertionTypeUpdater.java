/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.loader;

import org.openvpms.component.business.domain.im.archetype.descriptor.AssertionTypeDescriptor;
import org.openvpms.component.model.archetype.ActionTypeDescriptor;
import org.openvpms.tools.archetype.comparator.ActionTypeChange;
import org.openvpms.tools.archetype.comparator.AssertionTypeChange;
import org.openvpms.tools.archetype.comparator.AssertionTypeComparator;
import org.openvpms.tools.archetype.comparator.AssertionTypeFieldChange;

/**
 * Updates an {@link AssertionTypeDescriptor}.
 *
 * @author Tim Anderson
 */
public class AssertionTypeUpdater extends DescriptorUpdater<AssertionTypeDescriptor> {


    /**
     * Updates an assertion type descriptor.
     *
     * @param target the descriptor to update
     * @param source the descriptor to update from
     * @return {@code true} if the descriptor was updated, {@code false} if no changes were applied
     */
    public boolean update(AssertionTypeDescriptor target, AssertionTypeDescriptor source) {
        boolean updated = false;
        AssertionTypeComparator comparator = new AssertionTypeComparator();
        AssertionTypeChange change = comparator.compare(target, source);
        if (change != null) {
            for (AssertionTypeFieldChange fieldChange : change.getFieldChanges()) {
                switch (fieldChange.getField()) {
                    case NAME:
                        target.setName(toString(fieldChange));
                        break;
                    case PROPERTY_ARCHETYPE:
                        target.setPropertyArchetype(toString(fieldChange));
                        break;
                    default:
                        throw new IllegalStateException("Cannot update " + target.getName() +
                                                        ": unsupported field " + fieldChange.getField());
                }
                updated = true;
            }
            for (ActionTypeChange typeChange : change.getActionTypeChanges()) {
                if (typeChange.isAdd()) {
                    target.addActionType(typeChange.getNewVersion());
                } else if (typeChange.isUpdate()) {
                    ActionTypeDescriptor existingType = typeChange.getOldVersion();
                    ActionTypeDescriptor newType = typeChange.getNewVersion();
                    existingType.setName(newType.getName());
                    existingType.setClassName(newType.getClassName());
                    existingType.setMethodName(newType.getMethodName());
                } else {
                    target.removeActionType(typeChange.getOldVersion());
                }
                updated = true;
            }
        }
        return updated;
    }
}