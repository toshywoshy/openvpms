/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.tools.archetype.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.File;
import java.util.Collection;

/**
 * Archetype I/O helper methods.
 *
 * @author Tim Anderson
 */
public class ArchetypeIOHelper {

    /**
     * Returns all .adl files in a directory.
     *
     * @param dir     the directory to search
     * @param recurse if {@code true}, recurse subdirectories
     * @return the archetype files
     */
    public static Collection<File> getArchetypeFiles(File dir, boolean recurse) {
        IOFileFilter fileFilter = FileFilterUtils.suffixFileFilter("adl");
        IOFileFilter dirFilter = (recurse) ? TrueFileFilter.INSTANCE : null;
        return FileUtils.listFiles(dir, fileFilter, dirFilter);
    }
}
