/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */


package org.openvpms.ws.util.filter;

import javax.ws.rs.client.ClientResponseContext;

/**
 * Reads an error message from a {@link ClientResponseContext}.
 *
 * @author Tim Anderson
 */
public interface ErrorMessageReader {

    /**
     * Determines if this reader can read messages from a response.
     *
     * @param response the response
     * @return {@code true} if the response is supported, otherwise {@code false}
     */
    boolean canRead(ClientResponseContext response);

    /**
     * Reads an error message from a response.
     *
     * @param response the response
     * @return the error message. May be {@code null}
     */
    String read(ClientResponseContext response);
}
