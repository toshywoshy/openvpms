/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.ws.util.filter;

import org.junit.Test;

import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.core.MediaType;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link JAXBErrorMessageReader}.
 *
 * @author Tim Anderson
 */
public class JAXBErrorMessageReaderTestCase {

    /**
     * Tests the {@link JAXBErrorMessageReader#canRead(ClientResponseContext)} method.
     *
     * @throws Exception for any error
     */
    @Test
    public void testCanRead() throws Exception {
        checkCanRead(MediaType.APPLICATION_XML_TYPE, true);
        checkCanRead(MediaType.TEXT_XML_TYPE, true);
        checkCanRead(MediaType.TEXT_PLAIN_TYPE, false);
        checkCanRead(null, false);
    }

    /**
     * Tests that messages can be read.
     */
    @Test
    public void testReader() throws Exception {
        checkRead("test", "<error><message>test</message></error>");
        checkRead(null, "<error><message></message></error>");
        checkRead(null, "");
        checkRead(null, "<malformed>");
    }

    /**
     * Tests the {@link JAXBErrorMessageReader#canRead(ClientResponseContext)} method.
     *
     * @param type    the media type
     * @param canRead if {@code true}, the type should be readable, otherwise not
     * @throws Exception for any error
     */
    private void checkCanRead(MediaType type, boolean canRead) throws Exception {
        ClientResponseContext response = mock(ClientResponseContext.class);
        when(response.getMediaType()).thenReturn(type);
        assertEquals(canRead, new JAXBErrorMessageReader<>(ErrorMessage.class).canRead(response));
    }

    /**
     * Tests the {@link JAXBErrorMessageReader#read(ClientResponseContext)} method.
     *
     * @param expected the expected message
     * @param body     the response body
     * @throws Exception for any error
     */
    private void checkRead(String expected, String body) throws Exception {
        ClientResponseContext response = mock(ClientResponseContext.class);
        when(response.getMediaType()).thenReturn(MediaType.TEXT_XML_TYPE);
        when(response.getEntityStream()).thenReturn(new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8)));

        JAXBErrorMessageReader<ErrorMessage> reader = new JAXBErrorMessageReader<>(ErrorMessage.class);
        assertEquals(expected, reader.read(response));
    }

}
