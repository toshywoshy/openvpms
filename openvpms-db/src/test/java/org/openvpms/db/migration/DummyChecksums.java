/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration;

import org.openvpms.db.service.Checksums;

/**
 * An implementation of {@link Checksums} that returns {@code null}
 *
 * @author Tim Anderson
 */
public class DummyChecksums implements Checksums {

    /**
     * Returns the archetype checksum.
     *
     * @return the archetype checksum. May be {@code null}
     */
    @Override
    public Integer getArchetypeChecksum() {
        return null;
    }

    /**
     * Returns the plugin checksum.
     *
     * @return the plugin checksum. May be {@code null}
     */
    @Override
    public Integer getPluginChecksum() {
        return null;
    }
}
