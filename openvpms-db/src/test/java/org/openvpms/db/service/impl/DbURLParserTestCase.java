/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link DbURLParser} class.
 *
 * @author Tim Anderson
 */
public class DbURLParserTestCase {

    /**
     * Tests the parser.
     */
    @Test
    public void testParser() {
        DbURLParser parser = new DbURLParser("jdbc:mysql://localhost:3306/openvpms");
        assertEquals("jdbc:mysql://localhost:3306", parser.getRootUrl());
        assertEquals("openvpms", parser.getSchemaName());
    }

    /**
     * Tests the parser when parameters are passed.
     */
    @Test
    public void testParameters() {
        DbURLParser parser = new DbURLParser("jdbc:mysql://localhost:3306/openvpms_dev?useSSL=false");
        assertEquals("jdbc:mysql://localhost:3306?useSSL=false", parser.getRootUrl());
        assertEquals("openvpms_dev", parser.getSchemaName());
    }
}
