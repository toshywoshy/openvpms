/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import javax.sql.DataSource;
import java.security.InvalidKeyException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link DatabaseServiceImpl} class.
 *
 * @author Tim Anderson
 */
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class DatabaseServiceImplTestCase extends AbstractJUnit4SpringContextTests {

    /**
     * The data source.
     */
    @Autowired
    private DataSource dataSource;

    /**
     * The JDBC driver.
     */
    @Value("${jdbc.driverClassName}")
    private String driverClassName;

    /**
     * The JDBC url.
     */
    @Value("${jdbc.url}")
    private String url;

    /**
     * Verifies that if strong encryption is not available, database migration fails before execution.
     * <p/>
     * Strong encryption is required by the migratio for OVPMS-2233.
     */
    @Test
    public void testUpdateWithoutStrongEncryption() {
        DatabaseServiceImpl service = new DatabaseServiceImpl(driverClassName, url, dataSource) {
            @Override
            protected void checkPasswordEncryptionSupport() {
                super.checkPasswordEncryptionSupport();
                // simulate the JCE -> Spring exception chain
                throw new IllegalArgumentException("Unable to intialize due to invalid secret key",
                                                   new InvalidKeyException("Illegal key size"));
            }
        };
        try {
            service.update(null, null);
            fail("Expected update to fail");
        } catch (SQLException expected) {
            assertEquals("Unable to perform database migration. Strong password encryption is not supported.\n" +
                         "Please update to a newer version of Java.", expected.getMessage());
        }
    }
}
