#
# OBF-257 Add support for uni-directional lookup relationships
#

CREATE TABLE IF NOT EXISTS `lookup_links` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `linkId` varchar(36) NOT NULL,
  `arch_short_name` varchar(100) NOT NULL,
  `arch_version` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `source_id` bigint(20) DEFAULT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF970D8B4F5B96353` (`target_id`),
  KEY `FKF970D8B4C11A6889` (`source_id`),
  CONSTRAINT `FKF970D8B4C11A6889` FOREIGN KEY (`source_id`) REFERENCES `lookups` (`lookup_id`) ON DELETE CASCADE,
  CONSTRAINT `FKF970D8B4F5B96353` FOREIGN KEY (`target_id`) REFERENCES `lookups` (`lookup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lookup_link_details` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(5000) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`name`),
  KEY `FKD00125A2671B76FF` (`id`),
  CONSTRAINT `FKD00125A2671B76FF` FOREIGN KEY (`id`) REFERENCES `lookup_links` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

