#
# OVPMS-2525 Improve visibility of SMS replies.
#

#
# Change act.smsReply to an Act rather than DocumentAct.
#
DELETE d
FROM document_acts d
         JOIN acts a
              ON a.act_id = d.document_act_id
WHERE a.arch_short_name = 'act.smsReply';

#
# Rename participation.smsRecipient to participation.smsContact.
#
UPDATE participations p
SET p.arch_short_name = 'participation.smsContact'
WHERE p.arch_short_name = 'participation.smsRecipient';

#
# Rename the archetype descriptor.
#
UPDATE archetype_descriptors
SET name         = 'participation.smsContact.1.0',
    display_name = 'SMS Contact'
WHERE name = 'participation.smsRecipient.1.0';

#
# Duplicate contact, customer, patient and location from act.smsMessage to act.smsReply
#
INSERT INTO participations (version, linkId, arch_short_name, arch_version, active, act_arch_short_name, entity_id,
                            act_id, activity_start_time, activity_end_time)
SELECT 1,
       UUID(),
       participant.arch_short_name,
       '1.0',
       1,
       'act.smsReply',
       participant.entity_id,
       reply.act_id,
       reply.activity_start_time,
       reply.activity_end_time
FROM act_relationships r
         JOIN acts sms
              ON sms.act_id = r.source_id
         JOIN participations participant
              ON sms.act_id = participant.act_id
                  AND participant.arch_short_name IN
                      ('participation.smsContact', 'participation.customer', 'participation.patient',
                       'participation.location')
         JOIN acts reply
              ON reply.act_id = r.target_id
WHERE r.arch_short_name = 'actRelationship.smsMessageReply'
  AND NOT EXISTS(SELECT *
                 FROM participations preply
                 WHERE preply.act_id = reply.act_id
                   AND preply.arch_short_name = participant.arch_short_name);

DROP PROCEDURE IF EXISTS sp_add_openvpms_authority;
DROP PROCEDURE IF EXISTS sp_add_openvpms_role_authority;

DELIMITER $$

#
# Adds an authority, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_authority(IN auth_name VARCHAR(255), IN auth_description VARCHAR(255),
                                           IN auth_method VARCHAR(255), IN auth_archetype VARCHAR(255))
BEGIN
    INSERT INTO granted_authorities (version, linkId, arch_short_name, arch_version, name, description, active,
                                     service_name, method, archetype)
    SELECT 0,
           UUID(),
           'security.archetypeAuthority',
           '1.0',
           auth_name,
           auth_description,
           1,
           'archetypeService',
           auth_method,
           auth_archetype
    FROM dual
    WHERE NOT EXISTS(
            SELECT *
            FROM granted_authorities auth
            WHERE auth.method = auth_method
              AND auth.archetype = auth_archetype);
    IF ROW_COUNT() = 0 THEN
        # update the description
        UPDATE granted_authorities auth
        SET auth.description = auth_description
        WHERE auth.method = auth_method
          AND auth.archetype = auth_archetype;
    END IF;
END $$

#
# Adds a role authority, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_role_authority(IN role_name VARCHAR(255), IN auth_method VARCHAR(255),
                                                IN auth_archetype VARCHAR(255))
BEGIN
    INSERT INTO roles_authorities (security_role_id, authority_id)
    SELECT role.security_role_id, granted.granted_authority_id
    FROM security_roles role
             JOIN granted_authorities granted
                  ON granted.method = auth_method
                      AND granted.archetype = auth_archetype
    WHERE role.name = role_name
      AND role.arch_short_name = 'security.role'
      AND NOT EXISTS(SELECT *
                     FROM roles_authorities ra
                     WHERE ra.security_role_id = role.security_role_id
                       AND ra.authority_id = granted.granted_authority_id);
END $$

DELIMITER ;

#
# Add an SMS Plugin role.
#
INSERT INTO security_roles (version, linkId, arch_short_name, arch_version, name, description, active)
SELECT 0,
       UUID(),
       'security.role',
       '1.0',
       'SMS Plugin',
       'Restricted role for SMS plugins',
       1
FROM dual
WHERE NOT EXISTS(SELECT *
                 FROM security_roles
                 WHERE name = 'SMS Plugin');

CALL sp_add_openvpms_authority('SMS Reply Create', 'Authority to Create SMS Reply', 'create', 'act.smsReply');
CALL sp_add_openvpms_authority('SMS Reply Save', 'Authority to Save SMS Reply', 'save', 'act.smsReply');
CALL sp_add_openvpms_authority('SMS Reply Remove', 'Authority to Remove SMS Reply', 'remove', 'act.smsReply');

CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.smsReply');

DROP PROCEDURE sp_add_openvpms_authority;
DROP PROCEDURE sp_add_openvpms_role_authority;