#
# OBF-275 Add singleton flag to ArchetypeDescriptor
#

DROP PROCEDURE IF EXISTS sp_add_is_singleton_to_archetype_descriptors;

DELIMITER $$

CREATE PROCEDURE sp_add_is_singleton_to_archetype_descriptors()
BEGIN
    DECLARE colName TEXT;
    SELECT column_name
    INTO colName
    FROM information_schema.columns
    WHERE table_schema = DATABASE()
      AND table_name = 'archetype_descriptors'
      AND column_name = 'is_singleton';

    IF colName IS NULL THEN
        ALTER TABLE archetype_descriptors
            ADD COLUMN `is_singleton` bit(1) DEFAULT NULL AFTER `is_primary`;

        UPDATE archetype_descriptors SET is_singleton = 0;
    END IF;
END$$

DELIMITER ;

CALL sp_add_is_singleton_to_archetype_descriptors;

DROP PROCEDURE sp_add_is_singleton_to_archetype_descriptors;

