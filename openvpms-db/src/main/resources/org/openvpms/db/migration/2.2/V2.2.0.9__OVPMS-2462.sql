#
# OVPMS-2462 Provide option to suppress communications by reason in Medical Records - Summary
#

INSERT INTO lookup_details(lookup_id, type, value, name)
SELECT reason.lookup_id,
       'boolean',
       'true',
       'showInPatientHistory'
FROM lookups reason
         LEFT JOIN lookup_details show_in_history
                   ON reason.lookup_id = show_in_history.lookup_id
                       AND show_in_history.name = 'showInPatientHistory'
WHERE reason.arch_short_name = 'lookup.customerCommunicationReason'
  AND show_in_history.lookup_id IS NULL;