#
# OVPMS-2443 Print API.
#

#
# Update entity.printer to duplicate the name to the printer node, prefixed by ':'
# Any embedded ':' is escaped.
#
INSERT INTO entity_details (entity_id, type, value, name)
SELECT printer.entity_id,
       'string',
       CONCAT(':', REPLACE(printer.name, ':', '\\:')),
       'printer'
FROM entities printer
WHERE printer.arch_short_name = 'entity.printer'
  AND NOT EXISTS(SELECT *
                 FROM entity_details d
                 WHERE d.entity_id = printer.entity_id
                   AND d.name = 'printer');

#
# Update party.organisationLocation defaultPrinter to prefix with ':'.
# Any embedded ':' is escaped.
#
UPDATE entities location
    JOIN entity_details default_printer
    ON location.entity_id = default_printer.entity_id
        AND default_printer.name = 'defaultPrinter'
SET default_printer.value = CONCAT(':', REPLACE(default_printer.value, ':', '\\:'))
WHERE location.arch_short_name = 'party.organisationLocation'
  AND default_printer.value NOT LIKE ':%';

#
# Update entity.jobScheduledReport printer to prefix with ':'.
# Any embedded ':' is escaped.
#
UPDATE entities job
    JOIN entity_details printer
    ON job.entity_id = printer.entity_id
        AND printer.name = 'printer'
SET printer.value = CONCAT(':', REPLACE(printer.value, ':', '\\:'))
WHERE job.arch_short_name = 'entity.jobScheduledReport'
  AND printer.value NOT LIKE ':%';

#
# Rename 'printerName' node of entityRelationship.documentTemplatePrinter to 'printer', and prefix value with ':'.
# Any embedded ':' is escaped.
#
UPDATE entity_relationships dtp
    JOIN entity_relationship_details printer_name
    ON dtp.entity_relationship_id = printer_name.entity_relationship_id
        AND printer_name.name = 'printerName'
SET printer_name.name  = 'printer',
    printer_name.value = CONCAT(':', REPLACE(printer_name.value, ':', '\\:'))
WHERE dtp.arch_short_name = 'entityRelationship.documentTemplatePrinter';