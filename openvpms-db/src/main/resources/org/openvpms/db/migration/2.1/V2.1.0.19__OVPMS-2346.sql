#
# OVPMS-2346 Migrate entity.scheduledJob relative date parameters
#

delete param_value
from entities report
         join entity_details param_name
              on report.entity_id = param_name.entity_id
                  and param_name.name like 'paramName%'
         join entity_details param_type
              on report.entity_id = param_type.entity_id
                  and param_type.name like 'paramType%'
         join entity_details param_value
              on report.entity_id = param_value.entity_id
                  and param_value.name like 'paramValue%'
         join entity_details param_expr_type
              on report.entity_id = param_expr_type.entity_id
                  and param_expr_type.name like 'paramExprType%'
where report.arch_short_name = 'entity.jobScheduledReport'
  and param_type.value = 'java.util.Date'
  and param_value.type = 'sql-timestamp'
  and substring(param_name.name, 10) = substring(param_type.name, 10)
  and substring(param_type.name, 10) = substring(param_value.name, 11)
  and substring(param_type.name, 10) = substring(param_expr_type.name, 14)
  and param_expr_type.value <> 'VALUE';
