/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

#
# OVPMS-2538 SMS account reminders
#

INSERT INTO lookups (version, linkId, arch_short_name, active, arch_version, code, name, description, default_lookup)
SELECT 0,
       UUID(),
       'lookup.customerCommunicationReason',
       1,
       '1.0',
       'ACCOUNT_REMINDER',
       'Account Reminder',
       NULL,
       0
FROM dual
WHERE NOT EXISTS(SELECT *
                 FROM lookups e
                 WHERE e.arch_short_name = 'lookup.customerCommunicationReason'
                   AND e.code = 'ACCOUNT_REMINDER');

DROP PROCEDURE IF EXISTS sp_add_openvpms_authority;
DROP PROCEDURE IF EXISTS sp_add_openvpms_role_authority;

DELIMITER $$

#
# Adds an authority, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_authority(IN auth_name VARCHAR(255), IN auth_description VARCHAR(255),
                                           IN auth_method VARCHAR(255), IN auth_archetype VARCHAR(255))
BEGIN
    INSERT INTO granted_authorities (version, linkId, arch_short_name, arch_version, name, description, active,
                                     service_name, method, archetype)
    SELECT 0,
           UUID(),
           'security.archetypeAuthority',
           '1.0',
           auth_name,
           auth_description,
           1,
           'archetypeService',
           auth_method,
           auth_archetype
    FROM dual
    WHERE NOT EXISTS(
            SELECT *
            FROM granted_authorities auth
            WHERE auth.method = auth_method
              AND auth.archetype = auth_archetype);
    IF ROW_COUNT() = 0 THEN
        # update the description
        UPDATE granted_authorities auth
        SET auth.description = auth_description
        WHERE auth.method = auth_method
          AND auth.archetype = auth_archetype;
    END IF;
END $$

#
# Adds a role authority, if it does not exist.
#
CREATE PROCEDURE sp_add_openvpms_role_authority(IN role_name VARCHAR(255), IN auth_method VARCHAR(255),
                                                IN auth_archetype VARCHAR(255))
BEGIN
    INSERT INTO roles_authorities (security_role_id, authority_id)
    SELECT role.security_role_id, granted.granted_authority_id
    FROM security_roles role
             JOIN granted_authorities granted
                  ON granted.method = auth_method
                      AND granted.archetype = auth_archetype
    WHERE role.name = role_name
      AND role.arch_short_name = 'security.role'
      AND NOT exists(SELECT *
                     FROM roles_authorities ra
                     WHERE ra.security_role_id = role.security_role_id
                       AND ra.authority_id = granted.granted_authority_id);
END $$

DELIMITER ;

CALL sp_add_openvpms_authority('Customer Charge Reminder Create', 'Authority to Create Customer Charge Reminder',
                               'create', 'act.customerChargeReminder*');
CALL sp_add_openvpms_authority('Customer Charge Reminder Save', 'Authority to Save Customer Charge Reminder', 'save',
                               'act.customerChargeReminder*');
CALL sp_add_openvpms_authority('Customer Charge Reminder Remove', 'Authority to Remove Customer Charge Reminder',
                               'remove', 'act.customerChargeReminder*');

CALL sp_add_openvpms_role_authority('Base Role', 'create', 'act.customerChargeReminder*');
CALL sp_add_openvpms_role_authority('Base Role', 'save', 'act.customerChargeReminder*');
CALL sp_add_openvpms_role_authority('Base Role', 'remove', 'act.customerChargeReminder*');

DROP PROCEDURE sp_add_openvpms_authority;
DROP PROCEDURE sp_add_openvpms_role_authority;

