#
# OVPMS-2241 Create/update user information
#

#
# Add created, created_id, updated, updated_id columns.
#
DROP PROCEDURE IF EXISTS sp_add_created_updated_columns;

DELIMITER  $$

CREATE PROCEDURE sp_add_created_updated_columns(IN tableName VARCHAR(255), IN afterCol VARCHAR(255))
BEGIN
    DECLARE colName VARCHAR(255);
    SELECT COLUMN_NAME
    INTO colName
    FROM information_schema.COLUMNS
    WHERE TABLE_SCHEMA = database()
      AND TABLE_NAME = tableName
      AND COLUMN_NAME = 'created';

    IF colName IS NULL THEN
        SET @sql = CONCAT(
                'ALTER TABLE ', tableName,
                ' ADD COLUMN `created` DATETIME   DEFAULT NULL AFTER `', afterCol, '`,',
                ' ADD COLUMN `created_id` BIGINT(20) DEFAULT NULL AFTER `created`,',
                ' ADD COLUMN `updated` DATETIME DEFAULT NULL AFTER `created_id`,',
                ' ADD COLUMN `updated_id` BIGINT (20) DEFAULT NULL AFTER `updated`');
        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END $$

DELIMITER ;

CALL sp_add_created_updated_columns('acts', 'version');
CALL sp_add_created_updated_columns('archetype_descriptors', 'linkId');
CALL sp_add_created_updated_columns('entities', 'linkId');
CALL sp_add_created_updated_columns('lookups', 'linkId');
CALL sp_add_created_updated_columns('product_prices', 'linkId');
CALL sp_add_created_updated_columns('security_roles', 'linkId');

DROP PROCEDURE IF EXISTS sp_add_created_updated_columns;

#
# Remove the last_modified column from archetype_descriptors.
#

DROP PROCEDURE IF EXISTS sp_remove_last_modified_from_archetype_descriptors;

DELIMITER $$

CREATE PROCEDURE sp_remove_last_modified_from_archetype_descriptors()
BEGIN
    DECLARE colName TEXT;
    SELECT COLUMN_NAME
    INTO colName
    FROM information_schema.columns
    WHERE TABLE_SCHEMA = database()
      AND TABLE_NAME = 'archetype_descriptors'
      AND COLUMN_NAME = 'last_modified';

    IF colName IS NOT NULL THEN
        ALTER TABLE archetype_descriptors
            DROP COLUMN `last_modified`;
    END IF;
END$$

DELIMITER ;

CALL sp_remove_last_modified_from_archetype_descriptors;

DROP PROCEDURE sp_remove_last_modified_from_archetype_descriptors;

#
# NOTE: the following statements are wrapped in individual transactions to defeat Flyway from wrapping everything
# in a single large transaction, slowing the migration down.
#

#
# Migrate date details nodes to sql-timestamp.
# Originally done by OVPMS-1627 but not applied on all sites. See OVPMS-2450.
#
START TRANSACTION;
update act_details d
set d.value=substring(value, 1, 21),
    d.type='sql-timestamp'
where d.type = 'date';
COMMIT;

START TRANSACTION;
update entity_details d
set d.value=substring(value, 1, 21),
    d.type='sql-timestamp'
where d.type = 'date';
COMMIT;

#
# Copy activity_start_time to created for all acts dated in the past.
# i.e. don't base creation timestamp on future dated timestamps, such as appointments
#
START TRANSACTION;
UPDATE acts
SET created = activity_start_time
WHERE activity_start_time < NOW();
COMMIT;

#
# Copy act.patientReminder createdTime node to created, and rename it initialTime.
#
START TRANSACTION;
UPDATE acts a
    JOIN act_details created_time
    ON a.act_id = created_time.act_id
        AND created_time.name = 'createdTime'
SET created = created_time.value
WHERE a.arch_short_name = 'act.patientReminder';
COMMIT;

START TRANSACTION;
UPDATE act_details created_time
    JOIN acts a
    ON a.act_id = created_time.act_id
        AND created_time.name = 'createdTime'
SET created_time.name = 'initialTime'
WHERE a.arch_short_name = 'act.patientReminder';
COMMIT;

#
# Move author node to created_id.
#
START TRANSACTION;
UPDATE acts a
    JOIN participations p
    ON a.act_id = p.act_id
SET a.created_id = p.entity_id
WHERE p.arch_short_name = 'participation.author';
COMMIT;

START TRANSACTION;
DELETE p
FROM acts a
         JOIN participations p
              ON a.act_id = p.act_id
                  AND p.arch_short_name = 'participation.author';
COMMIT;

#
# Migrate party.customerperson and party.patientpet createdDate to created
#
START TRANSACTION;
UPDATE entities e
    JOIN entity_details created_date
    ON e.entity_id = created_date.entity_id
        AND created_date.name = 'createdDate'
SET e.created = created_date.value
WHERE e.arch_short_name IN ('party.customerperson', 'party.patientpet');
COMMIT;

START TRANSACTION;
DELETE created_date
FROM entities e
         JOIN entity_details created_date
              ON e.entity_id = created_date.entity_id
                  AND created_date.name = 'createdDate'
WHERE e.arch_short_name IN ('party.customerperson', 'party.patientpet');
COMMIT;

#
# Copy start_time to created for product prices acts dated in the past.
# i.e. don't base creation timestamp on future dated prices
#
START TRANSACTION;
UPDATE product_prices
SET created = start_time
WHERE start_time < NOW();
COMMIT;


#
# Add keys.
#
DROP PROCEDURE IF EXISTS sp_add_created_keys;

DELIMITER  $$

CREATE PROCEDURE sp_add_created_keys(IN tableName VARCHAR(255), IN createdKey varchar(255), IN updatedKey varchar(255))
BEGIN
    IF NOT EXISTS(
            SELECT 1
            FROM information_schema.STATISTICS
            WHERE table_schema = DATABASE()
              AND table_name = tableName
              AND index_name = createdKey
        )
    THEN
        SET @sql = CONCAT(
                'ALTER TABLE ', tableName,
                ' ADD KEY ', createdKey, ' (`created_id`),',
                ' ADD KEY ', updatedKey, ' (`updated_id`),',
                'ADD CONSTRAINT ', updatedKey, ' FOREIGN KEY (`updated_id`) REFERENCES `users`(`user_id`),',
                'ADD CONSTRAINT ', createdKey, ' FOREIGN KEY (`created_id`) REFERENCES `users`(`user_id`) ');
        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END $$

DELIMITER ;

CALL sp_add_created_keys('acts', 'FK2D9A21B96D33A7', 'FK2D9A21562D1CF4');
CALL sp_add_created_keys('archetype_descriptors', 'FK30DC3E2EB96D33A7', 'FK30DC3E2E562D1CF4');
CALL sp_add_created_keys('entities', 'FK82B447C1B96D33A7', 'FK82B447C1562D1CF4');
CALL sp_add_created_keys('lookups', 'FK14D98639B96D33A7', 'FK14D98639562D1CF4');
CALL sp_add_created_keys('product_prices', 'FKFBD40D9AB96D33A7', 'FKFBD40D9A562D1CF4');
CALL sp_add_created_keys('security_roles', 'FKEAC2F8FEB96D33A7', 'FKEAC2F8FE562D1CF4');

DROP PROCEDURE IF EXISTS sp_add_created_keys;

#
# Add indexes
#

DROP PROCEDURE IF EXISTS sp_add_created_index;

DELIMITER  $$

CREATE PROCEDURE sp_add_created_index(IN tableName VARCHAR(255), IN indexName varchar(255))
BEGIN
    IF NOT EXISTS(
            SELECT 1
            FROM information_schema.STATISTICS
            WHERE table_schema = DATABASE()
              AND table_name = tableName
              AND index_name = indexName
        )
    THEN
        SET @sql = CONCAT('ALTER TABLE ', tableName,
                          ' ADD KEY ', indexName, '(`created`, `arch_short_name`)');
        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
END $$

DELIMITER ;

#
# Add indexes on created, arch_short_name for the acts and entities tables.
#
CALL sp_add_created_index('acts', 'act_created_idx');
CALL sp_add_created_index('entities', 'entity_created_idx');

DROP PROCEDURE IF EXISTS sp_add_created_index;

