#
# OBF-266 Prevent addition of duplicate roles and authorities
#
DROP PROCEDURE IF EXISTS sp_add_unique_key_to_security_roles;
DROP PROCEDURE IF EXISTS sp_add_unique_key_to_granted_authorities;

DELIMITER $$

CREATE PROCEDURE sp_add_unique_key_to_security_roles()
BEGIN
    DECLARE keyName TEXT;
    select constraint_name into keyName
    from information_schema.table_constraints
    where table_name = 'security_roles'
      and table_schema = database()
      and constraint_name = 'arch_short_name';

    IF keyName is null THEN
        alter table security_roles add UNIQUE KEY `arch_short_name` (arch_short_name, name);
    END IF;
END$$

CREATE PROCEDURE sp_add_unique_key_to_granted_authorities()
BEGIN
    DECLARE keyName TEXT;
    select constraint_name into keyName
    from information_schema.table_constraints
    where table_name = 'granted_authorities'
      and table_schema = database()
      and constraint_name = 'arch_short_name';

    IF keyName is null THEN
        alter table granted_authorities add UNIQUE KEY `arch_short_name` (arch_short_name, name);
    END IF;
END$$

DELIMITER ;

update security_roles r1
join security_roles r2
on r1.security_role_id > r2.security_role_id
and r1.arch_short_name = r2.arch_short_name
and r1.name = r2.name
set r1.name = substring(concat(r1.security_role_id, ' ', r1.name), 1, 255);


update granted_authorities a1
join granted_authorities a2
on a1.granted_authority_id > a2.granted_authority_id
and a1.arch_short_name = a2.arch_short_name
and a1.name = a2.name
set a1.name = substring(concat(a1.granted_authority_id, ' ', a1.name), 1, 255);


CALL sp_add_unique_key_to_security_roles;
CALL sp_add_unique_key_to_granted_authorities;

DROP PROCEDURE sp_add_unique_key_to_security_roles;
DROP PROCEDURE sp_add_unique_key_to_granted_authorities;
