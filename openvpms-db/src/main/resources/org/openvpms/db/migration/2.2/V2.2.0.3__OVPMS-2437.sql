#
# OVPMS-2437 Migrated tests missing useDevice node
#

INSERT INTO entity_details (entity_id, type, value, name)
SELECT lab_test.entity_id,
       'string',
       'NO',
       'useDevice'
FROM entities lab_test
WHERE lab_test.arch_short_name = 'entity.laboratoryTest'
  AND NOT exists(SELECT *
                 FROM entity_details use_device
                 WHERE use_device.entity_id = lab_test.entity_id
                   AND use_device.name = 'useDevice');