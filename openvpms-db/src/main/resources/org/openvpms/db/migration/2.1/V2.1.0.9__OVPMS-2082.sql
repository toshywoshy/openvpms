#
# OVPMS-2082 Laboratory API
#

#
# Cache existing investigation types.
#

DROP TABLE IF EXISTS tmp_investigation_type;

CREATE TEMPORARY TABLE tmp_investigation_type
(
    entity_id  bigint(20) PRIMARY KEY,
    linkId     varchar(36) NOT NULL,
    service_id varchar(100),
    test_id    bigint(20),
    active     bit(1) DEFAULT NULL,
    INDEX tmp_investigation_type_test_id_idx (test_id)
);

INSERT INTO tmp_investigation_type (entity_id, linkId, service_id, active)
SELECT investigation_type.entity_id, investigation_type.linkId, SUBSTRING(service_id.value, 1, 100),
       investigation_type.active
FROM entities investigation_type
         LEFT JOIN entity_details service_id
                   ON investigation_type.entity_id = service_id.entity_id
                       AND service_id.name = 'universalServiceIdentifier'
WHERE investigation_type.arch_short_name = 'entity.investigationType';


#
# Create an entity.laboratoryTest for each investigation type that does not have a service_id.
#
INSERT INTO entities (version, linkId, arch_short_name, arch_version, name, active)
SELECT 1,
       tmp_investigation_type.linkId,
       'entity.laboratoryTest',
       '1.0',
       entities.name,
       tmp_investigation_type.active
FROM tmp_investigation_type
         JOIN entities
              ON tmp_investigation_type.entity_id = entities.entity_id
WHERE tmp_investigation_type.service_id IS NULL
  AND NOT exists(SELECT *
                 FROM entities
                 WHERE entities.linkId = tmp_investigation_type.linkId
                   AND entities.arch_short_name = 'entity.laboratoryTest');

#
# Create entity.laboratoryTestHL7 for each investigation type with a service_id.
#
INSERT INTO entities (version, linkId, arch_short_name, arch_version, name, active)
SELECT 1,
       tmp_investigation_type.linkId,
       'entity.laboratoryTestHL7',
       '1.0',
       SUBSTRING(concat('HL7 Laboratory Test - ', tmp_investigation_type.service_id), 1, 100),
       tmp_investigation_type.active
FROM tmp_investigation_type
WHERE tmp_investigation_type.service_id IS NOT NULL
  AND NOT exists(SELECT *
                 FROM entities
                 WHERE entities.linkId = tmp_investigation_type.linkId
                   AND entities.arch_short_name = 'entity.laboratoryTestHL7');

#
# Update the temporary table with the corresponding new test identifiers.
#
UPDATE tmp_investigation_type
    JOIN entities
    ON tmp_investigation_type.linkId = entities.linkId
SET tmp_investigation_type.test_id = entities.entity_id
WHERE entities.arch_short_name IN ('entity.laboratoryTest', 'entity.laboratoryTestHL7');


#
# Create an entityIdentity.laboratoryTest for each entity.laboratoryTestHL7
#
INSERT INTO entity_identities (version, linkId, entity_id, arch_short_name, arch_version, name, active, identity)
SELECT 0,
       tmp_investigation_type.linkId,
       entities.entity_id,
       'entityIdentity.laboratoryTest',
       '1.0',
       tmp_investigation_type.service_id,
       1,
       tmp_investigation_type.service_id
FROM tmp_investigation_type
         JOIN entities
              ON tmp_investigation_type.linkId = entities.linkId
                  AND entities.arch_short_name = 'entity.laboratoryTestHL7'
WHERE NOT exists(SELECT *
                 FROM entity_identities
                 WHERE entity_identities.linkId = tmp_investigation_type.linkId
                   AND entity_identities.arch_short_name = 'entityIdentity.laboratoryTest');


#
# Link each entity.laboratoryTest* to its corresponding entity.investigationType.
#

INSERT INTO entity_links (version, linkId, arch_short_name, arch_version, name, active_start_time,
                          active_end_time, sequence, source_id, target_id)
SELECT 1,
       UUID(),
       'entityLink.laboratoryTestInvestigationType',
       '1.0',
       NULL,
       NULL,
       NULL,
       0,
       tmp_investigation_type.test_id,
       tmp_investigation_type.entity_id
FROM tmp_investigation_type
         JOIN entities test
              ON tmp_investigation_type.test_id = test.entity_id
                  AND test.arch_short_name IN ('entity.laboratoryTest', 'entity.laboratoryTestHL7')
         JOIN entities investigation_type
              ON tmp_investigation_type.entity_id = investigation_type.entity_id
                  AND investigation_type.arch_short_name = 'entity.investigationType'
                  AND NOT exists(
                          SELECT *
                          FROM entity_links
                          WHERE entity_links.arch_short_name = 'entityLink.laboratoryTestInvestigationType'
                            AND entity_links.source_id = tmp_investigation_type.test_id
                            AND entity_links.target_id = tmp_investigation_type.entity_id);


#
# Remove the universalServiceIdentifier from entity.investigationType
#
DELETE service_id
FROM entities investigation_type
         JOIN entity_details service_id
              ON investigation_type.entity_id = service_id.entity_id
                  AND service_id.name = 'universalServiceIdentifier'
WHERE investigation_type.arch_short_name = 'entity.investigationType';

#
# Rename entityLink.investigationLaboratory to entityLink.investigationTypeLaboratory
#
UPDATE entity_links
SET arch_short_name = 'entityLink.investigationTypeLaboratory'
WHERE arch_short_name = 'entityLink.investigationLaboratory';

#
# Replace each entityLink.productInvestigationType with an entityLink.productLaboratoryTest
#
UPDATE entity_links
    JOIN tmp_investigation_type
    ON entity_links.target_id = tmp_investigation_type.entity_id
        AND entity_links.arch_short_name = 'entityLink.productInvestigationType'
SET entity_links.arch_short_name = 'entityLink.productLaboratoryTest',
    entity_links.name            = 'Product Laboratory Test',
    entity_links.target_id       = tmp_investigation_type.test_id;

#
# Add tests to each existing investigation.
#
INSERT INTO participations (version, linkId, arch_short_name, arch_version, active, act_arch_short_name, entity_id,
                            act_id, activity_start_time, activity_end_time)
SELECT 1,
       UUID(),
       'participation.laboratoryTest',
       '1.0',
       1,
       'act.patientInvestigation',
       tmp_investigation_type.test_id,
       investigation.act_id,
       investigation.activity_start_time,
       investigation.activity_end_time
FROM acts investigation
         JOIN participations pinvestigation_type
              ON investigation.act_id = pinvestigation_type.act_id
                  AND pinvestigation_type.arch_short_name = 'participation.investigationType'
         JOIN tmp_investigation_type
              ON tmp_investigation_type.entity_id = pinvestigation_type.entity_id
WHERE tmp_investigation_type.test_id IS NOT NULL
  AND NOT exists(SELECT *
                 FROM participations plaboratory_test
                 WHERE plaboratory_test.act_id = investigation.act_id
                   AND plaboratory_test.arch_short_name = 'participation.laboratoryTest');

DROP TABLE tmp_investigation_type;

#
# Create authorities, if they don't already exist.
#

DROP TABLE IF EXISTS new_authorities;
CREATE TEMPORARY TABLE new_authorities
(
    name        VARCHAR(255) PRIMARY KEY,
    description VARCHAR(255),
    method      VARCHAR(255),
    archetype   VARCHAR(255)
);

INSERT INTO new_authorities (name, description, method, archetype)
VALUES ('Laboratory Order Create', 'Authority to Create Laboratory Order', 'create', 'act.laboratoryOrder'),
       ('Laboratory Order Save', 'Authority to Save Laboratory Order', 'save', 'act.laboratoryOrder'),
       ('Laboratory Order Remove', 'Authority to Remove Laboratory Order', 'remove', 'act.laboratoryOrder'),
       ('Laboratory Service Create', 'Authority to Create Laboratory Service', 'create', 'entity.laboratoryService*'),
       ('Laboratory Service Save', 'Authority to Save Laboratory Service', 'save', 'entity.laboratoryService*'),
       ('Laboratory Service Remove', 'Authority to Remove Laboratory Service', 'remove', 'entity.laboratoryService*'),
       ('Laboratory Test Create', 'Authority to Create Laboratory Test', 'create', 'entity.laboratoryTest*'),
       ('Laboratory Test Save', 'Authority to Save Laboratory Test', 'save', 'entity.laboratoryTest*'),
       ('Laboratory Test Remove', 'Authority to Remove Laboratory Test', 'remove', 'entity.laboratoryTest*'),
       ('Entity Mappings Create', 'Authority to Create Entity Mappings', 'create', 'entity.entityMappings*'),
       ('Entity Mappings Save', 'Authority to Save Entity Mappings', 'save', 'entity.entityMappings*'),
       ('Entity Mappings Remove', 'Authority to Remove Entity Mappings', 'remove', 'entity.entityMappings*'),
       ('Lookup Mappings Create', 'Authority to Create Lookup Mappings', 'create', 'lookup.lookupMappings*'),
       ('Lookup Mappings Save', 'Authority to Save Lookup Mappings', 'save', 'lookup.lookupMappings*'),
       ('Lookup Mappings Remove', 'Authority to Remove Lookup Mappings', 'remove', 'lookup.lookupMappings*'),
       ('Plugin Configuration Create', 'Authority to Create Plugin Configuration', 'create', 'entity.plugin*'),
       ('Plugin Configuration Save', 'Authority to Save Plugin Configuration', 'save', 'entity.plugin*'),
       ('Plugin Configuration Remove', 'Authority to Remove Plugin Configuration', 'remove', 'entity.plugin*');


INSERT INTO granted_authorities (version, linkId, arch_short_name, arch_version, name, description, active,
                                 service_name, method, archetype)
SELECT 0,
       UUID(),
       'security.archetypeAuthority',
       '1.0',
       a.name,
       a.description,
       1,
       'archetypeService',
       a.method,
       a.archetype
FROM new_authorities a
WHERE NOT exists(
        SELECT *
        FROM granted_authorities g
        WHERE g.method = a.method
          AND g.archetype = a.archetype);

#
# Add authorities to Base Role.
#
INSERT INTO roles_authorities (security_role_id, authority_id)
SELECT r.security_role_id,
       g.granted_authority_id
FROM security_roles r
         JOIN granted_authorities g
WHERE r.name = 'Base Role'
  AND g.archetype IN ('act.laboratoryOrder')
  AND g.method IN ('create', 'save', 'remove')
  AND NOT exists(SELECT *
                 FROM roles_authorities x
                 WHERE x.security_role_id = r.security_role_id
                   AND x.authority_id = g.granted_authority_id);

DROP TABLE new_authorities;
