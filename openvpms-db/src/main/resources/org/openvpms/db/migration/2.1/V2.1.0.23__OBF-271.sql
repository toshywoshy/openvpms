#
# OBF-271 Add index on entity_identities.identity
#

#
# Copy identities > 100 characters into a 'migrated' node.
#
INSERT INTO entity_identity_details (entity_identity_id, type, value, name)
SELECT a.entity_identity_id,
       'string',
       a.identity,
       'migrated'
FROM entity_identities a
WHERE length(a.identity) > 100
  AND NOT EXISTS(SELECT *
                 FROM entity_identity_details b
                 WHERE b.entity_identity_id = a.entity_identity_id
                   AND b.name = 'migrated');

#
# Truncate the identity column to 100 characters.
#
UPDATE entity_identities
SET identity = SUBSTRING(identity, 1, 100)
WHERE length(identity) > 100;

DROP PROCEDURE IF EXISTS sp_alter_entity_identities;

DELIMITER  $$

CREATE PROCEDURE sp_alter_entity_identities()
BEGIN
    IF NOT EXISTS(
            SELECT 1
            FROM information_schema.STATISTICS
            WHERE table_schema = DATABASE()
              AND table_name = 'entity_identities'
              AND index_name = 'entity_identity_idx'
        )
    THEN
        ALTER TABLE entity_identities
            MODIFY COLUMN `identity` VARCHAR(100),
            ADD KEY entity_identity_idx (`identity`);
    END IF;
END $$

DELIMITER ;

CALL sp_alter_entity_identities();

DROP PROCEDURE sp_alter_entity_identities;