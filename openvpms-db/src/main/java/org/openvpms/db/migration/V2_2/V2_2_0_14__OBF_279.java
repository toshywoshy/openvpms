/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.migration.V2_2;

import org.flywaydb.core.api.migration.jdbc.BaseJdbcMigration;
import org.openvpms.db.util.ArchetypeDescriptorRemover;

import java.sql.Connection;

/**
 * Migration for OBF-279 Add support to deploy plugins from the database.
 * <p/>
 * This class is responsible for removing the entity.pluginConfiguration archetype descriptor.
 * <br>
 * Also see the <em>V2.2.0.13__OBF-279.sql</em> migration script.
 *
 * @author Tim Anderson
 */
public class V2_2_0_14__OBF_279 extends BaseJdbcMigration {

    /**
     * Executes this migration. The execution will automatically take place within a transaction, when the underlying
     * database supports it.
     *
     * @param connection The connection to use to execute statements.
     * @throws Exception when the migration failed.
     */
    @Override
    public void migrate(Connection connection) throws Exception {
        ArchetypeDescriptorRemover remover = new ArchetypeDescriptorRemover();
        remover.remove("entity.pluginConfiguration", connection);
    }
}
