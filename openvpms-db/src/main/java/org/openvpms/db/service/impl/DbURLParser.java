/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.db.service.impl;

/**
 * Helper to extract information from a MySQL JDBC url.
 *
 * @author Tim Anderson
 */
class DbURLParser {

    /**
     * The root URL.
     */
    private final String rootUrl;

    /**
     * The schema name.
     */
    private final String schemaName;

    /**
     * Constructs a {@link DbURLParser}.
     *
     * @param url the url
     * @throws IllegalArgumentException if the URL is invalid
     */
    public DbURLParser(String url) {
        if (!url.startsWith("jdbc:")) {
            throw new IllegalArgumentException("Invalid JDBC URL: " + url);
        }
        // strip any arguments, including ?
        int questionIndex = url.indexOf('?');
        String parameters = null;
        if (questionIndex != -1) {
            parameters = url.substring(questionIndex);
            url = url.substring(0, questionIndex);
        }
        int lastSlash = url.lastIndexOf('/');
        if (lastSlash == -1) {
            throw new IllegalArgumentException("Invalid JDBC URL: " + url);
        }
        schemaName = url.substring(lastSlash + 1);
        url = url.substring(0, lastSlash);
        if (parameters != null) {
            rootUrl = url + parameters;
        } else {
            rootUrl = url;
        }
    }

    /**
     * Returns the root URL. This is the JDBC URL minus any schema.
     *
     * @return the root URL
     */
    public String getRootUrl() {
        return rootUrl;
    }

    /**
     * Returns the schema name.
     *
     * @return the schema name
     */
    public String getSchemaName() {
        return schemaName;
    }
}
