/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.openvpms.mapping.model.AbstractTargets;
import org.openvpms.mapping.model.Target;
import org.openvpms.mapping.model.Targets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Test implementation of {@link Targets} that uses a provides {@link Target}s generated from a list of codes.
 *
 * @author Tim Anderson
 */
class SimpleTargets extends AbstractTargets {

    /**
     * The available targets.
     */
    private final List<Target> targets = new ArrayList<>();

    /**
     * Constructs a {@link SimpleTargets}.
     *
     * @param displayName the display name
     * @param codes       the available codes
     */
    SimpleTargets(String displayName, String... codes) {
        super(displayName);
        for (String code : codes) {
            targets.add(create(code, code, true));
        }
    }

    /**
     * Returns the available targets.
     *
     * @return the available targets
     */
    @Override
    protected Collection<Target> getTargets() {
        return targets;
    }

}
