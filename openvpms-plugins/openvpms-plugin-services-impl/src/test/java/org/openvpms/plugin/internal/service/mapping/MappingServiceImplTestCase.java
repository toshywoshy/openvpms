/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.mapping.exception.MappingException;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.service.MappingService;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests the {@link MappingServiceImpl} class.
 *
 * @author Tim Anderson
 */
public class MappingServiceImplTestCase extends ArchetypeServiceTest {

    /**
     * The archetype service.
     */
    @Autowired
    private IArchetypeRuleService service;

    /**
     * The practice service.
     */
    private PracticeService practiceService;

    /**
     * The mapping service.
     */
    private MappingService mappingService;

    /**
     * Sets up the test.
     */
    @Before
    public void setUp() {
        practiceService = Mockito.mock(PracticeService.class);
        when(practiceService.getServiceUser()).thenReturn(
                new org.openvpms.component.business.domain.im.security.User());
        PluginArchetypeService pluginArchetypeService = new PluginArchetypeService(service, getLookupService(),
                                                                                   practiceService);
        mappingService = new MappingServiceImpl(pluginArchetypeService);
    }

    /**
     * Verifies that entity mappings can be created.
     */
    @Test
    public void testCreateMappingConfigurationForEntity() {
        IMObject config1 = mappingService.createMappingConfiguration(Entity.class);
        IMObject config2 = mappingService.createMappingConfiguration(Party.class);
        IMObject config3 = mappingService.createMappingConfiguration(Product.class);
        IMObject config4 = mappingService.createMappingConfiguration(User.class);
        assertTrue(config1.isA(MappingArchetypes.ENTITY_MAPPINGS));
        assertTrue(config2.isA(MappingArchetypes.ENTITY_MAPPINGS));
        assertTrue(config3.isA(MappingArchetypes.ENTITY_MAPPINGS));
        assertTrue(config4.isA(MappingArchetypes.ENTITY_MAPPINGS));
    }

    /**
     * Verifies that lookup mappings can be created.
     */
    @Test
    public void testCreateMappingConfigurationForLookup() {
        IMObject configuration = mappingService.createMappingConfiguration(Lookup.class);
        assertTrue(configuration.isA(MappingArchetypes.LOOKUP_MAPPINGS));
    }

    /**
     * Verifies that a {@link MappingException} is thrown if the type is unsupported.
     */
    @Test
    public void testCreateMappingConfigurationForUnsupportedType() {
        try {
            mappingService.createMappingConfiguration(Act.class);
            fail("Expected MappingException to be thrown");
        } catch (MappingException expected) {
            assertEquals("MAP-0100: Mapping from org.openvpms.component.model.act.Act is not supported",
                         expected.getMessage());
        }
    }

    /**
     * Lookup mappings.
     */
    @Test
    public void testLookupMappings() {
        IMObject config = mappingService.createMappingConfiguration(Lookup.class);
        SimpleTargets targets = new SimpleTargets("Species", "DOG", "CAT", "OTHER");
        Mappings<Lookup> mappings = mappingService.createMappings(config, Lookup.class, "lookup.species", "Species",
                                                                  targets);
        mappings.save();
    }
}
