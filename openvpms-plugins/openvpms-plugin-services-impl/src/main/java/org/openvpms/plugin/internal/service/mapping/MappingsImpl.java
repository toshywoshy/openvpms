/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.service.mapping;

import org.openvpms.component.business.domain.im.common.IMObjectReference;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.query.TypedQuery;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Path;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.mapping.exception.MappingException;
import org.openvpms.mapping.model.Cardinality;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Source;
import org.openvpms.mapping.model.Target;
import org.openvpms.mapping.model.Targets;
import org.openvpms.plugin.service.i18n.MappingMessages;

import javax.persistence.Tuple;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of {@link Mappings} for {@link Entity} and {@link Lookup} instances.
 *
 * @author Tim Anderson
 */
class MappingsImpl<T extends IMObject> implements Mappings<T> {

    /**
     * The mapping configuration.
     */
    private final IMObjectBean config;

    /**
     * The source type.
     */
    private final Class<T> type;

    /**
     * The source object archetype. This is used to filter mappings as the mapping configuration may contain
     * mappings for multiple archetypes.
     */
    private final String sourceArchetype;

    /**
     * The mapping display name.
     */
    private final String displayName;

    /**
     * The source object type display name.
     */
    private final String sourceDisplayName;

    /**
     * The targets.
     */
    private final Targets targets;

    /**
     * The mappings, keyed on source reference. This is stored as string, as the linkId is needed for reference
     * equality.
     */
    private final Map<String, List<Mapping>> bySource = new LinkedHashMap<>();

    /**
     * The mappings, keyed on target identifier.
     */
    private final Map<String, List<Mapping>> byTarget = new HashMap<>();

    /**
     * The cardinality.
     */
    private final Cardinality cardinality;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;


    /**
     * Constructs an {@link MappingsImpl}.
     *
     * @param config            the mapping configuration
     * @param sourceArchetype   the mapping source archetype
     * @param displayName       the mapping display name
     * @param sourceDisplayName the source object type display name
     * @param targets           the targets
     * @param service           the archetype service
     */
    MappingsImpl(IMObject config, Class<T> type, String sourceArchetype, String displayName, String sourceDisplayName,
                 Targets targets, ArchetypeService service) {
        this.config = service.getBean(config);
        this.type = type;
        this.sourceArchetype = sourceArchetype;
        this.displayName = displayName;
        this.sourceDisplayName = sourceDisplayName;
        this.targets = targets;
        this.service = service;
        cardinality = getCardinality(this.config);
        // the config may contain objects of different archetypes. Exclude those that don't match the sourceArchetype
        for (Relationship relationship : this.config.getValues("mappings", Relationship.class)) {
            Reference source = relationship.getTarget();
            if (source != null && source.isA(sourceArchetype)) {
                Target target = getTarget(service.getBean(relationship));
                Mapping mapping = createMapping(relationship, source, target);
                add(mapping);
            }
        }
    }

    /**
     * Returns the type being mapped.
     *
     * @return the type
     */
    @Override
    public Class<T> getType() {
        return type;
    }

    /**
     * Returns the mapping cardinality.
     *
     * @return the mapping cardinality
     */
    @Override
    public Cardinality getCardinality() {
        return cardinality;
    }

    /**
     * Returns a display name for the mappings.
     *
     * @return the display name
     */
    @Override
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns a display name for the source object.
     *
     * @return the display name
     */
    @Override
    public String getSourceDisplayName() {
        return sourceDisplayName;
    }

    /**
     * Returns a display name for the target object.
     *
     * @return the display name
     */
    @Override
    public String getTargetDisplayName() {
        return targets.getDisplayName();
    }

    /**
     * Returns the mappings.
     *
     * @return the mappings
     */
    @Override
    public List<Mapping> getMappings() {
        List<Mapping> result = new ArrayList<>();
        for (List<Mapping> mappings : bySource.values()) {
            result.addAll(mappings);
        }
        return result;
    }

    /**
     * Returns a mapping given the source.
     * <p/>
     * If there are multiple mappings, the first available will be returned.
     *
     * @param source the source
     * @return the corresponding mapping, or {@code null} if none is found
     */
    @Override
    public Mapping getMapping(T source) {
        return getMapping(source.getObjectReference());
    }

    /**
     * Returns a mapping given the source.
     * <p/>
     * If there are multiple mappings, the first available will be returned.
     *
     * @param source the source
     * @return the corresponding mapping, or {@code null} if none is found
     */
    @Override
    public Mapping getMapping(Reference source) {
        List<Mapping> mappings = bySource.get(toString(source));
        return mappings != null && !mappings.isEmpty() ? mappings.get(0) : null;
    }

    /**
     * Returns a mapping given the target identifier.
     * <p/>
     * If there are multiple mappings, the first available will be returned.
     *
     * @param target the target identifier
     * @return the corresponding mapping, or {@code null} if none is found
     */
    @Override
    public Mapping getMapping(String target) {
        List<Mapping> mappings = getMappings(target);
        return !mappings.isEmpty() ? mappings.get(0) : null;
    }

    /**
     * Returns all mappings for the given target identifier.
     *
     * @param target the target identifier
     * @return the corresponding mappings
     */
    @Override
    public List<Mapping> getMappings(String target) {
        List<Mapping> mappings = byTarget.get(target);
        return mappings != null ? mappings : Collections.emptyList();
    }

    /**
     * Adds a mapping between a source object and a target.
     *
     * @param source the source object
     * @param target the target
     * @return the mapping
     */
    @Override
    public Mapping add(T source, Target target) {
        if (!source.isA(sourceArchetype)) {
            throw new MappingException(MappingMessages.invalidArchetype(source.getArchetype(), sourceArchetype));
        }
        Relationship relationship = config.addTarget("mappings", source);
        updateRelationship(relationship, target);
        Mapping mapping = createMapping(relationship, source.getObjectReference(), target);
        add(mapping);
        return mapping;
    }

    /**
     * Updates an existing mapping.
     *
     * @param mapping the mapping to update
     * @param source  the new source
     * @param target  the new target
     */
    @Override
    public void replace(Mapping mapping, T source, Target target) {
        if (!(mapping instanceof IMObjectMapping)) {
            throw new IllegalArgumentException("Unsupported mapping: " + mapping);
        }
        remove(mapping.getSource(), mapping.getTarget());

        IMObjectMapping impl = (IMObjectMapping) mapping;
        Relationship relationship = (Relationship) impl.getConfig();
        relationship.setTarget(source.getObjectReference());
        updateRelationship(relationship, target);
        impl.setSource(source.getObjectReference());
        impl.setTarget(target);

        add(impl);
    }

    /**
     * Removes a mapping.
     *
     * @param mapping the mapping to remove
     */
    @Override
    public void remove(Mapping mapping) {
        remove(mapping.getSource(), mapping.getTarget());
        IMObject relationship = ((IMObjectMapping) mapping).getConfig();
        config.removeValue("mappings", relationship);
    }

    /**
     * Makes the mapping changes persistent.
     *
     * @throws MappingException if cardinality constraints are not met
     */
    @Override
    public void save() {
        for (Map.Entry<String, List<Mapping>> entry : bySource.entrySet()) {
            if (entry.getValue().size() > 1) {
                Mapping mapping = entry.getValue().get(0);
                Reference source = mapping.getSource();
                String name = getName(source);
                throw new MappingException(MappingMessages.multipleMappingsExistForSource(source.getId(), name));
            }
        }
        if (getCardinality() == Cardinality.ONE_TO_ONE) {
            for (Map.Entry<String, List<Mapping>> entry : byTarget.entrySet()) {
                if (entry.getValue().size() > 1) {
                    Mapping mapping = entry.getValue().get(0);
                    Target target = mapping.getTarget();
                    throw new MappingException(MappingMessages.multipleMappingsExistForTarget(target.getId(),
                                                                                              target.getName()));
                }
            }
        }
        config.save();
    }

    /**
     * Returns the source for a given target.
     *
     * @param target the target identifier
     * @return the source, or {@code null} if no mapping exists
     */
    @Override
    public Reference getSource(String target) {
        Mapping mapping = getMapping(target);
        return mapping != null ? mapping.getSource() : null;
    }

    /**
     * Returns the target for the given source.
     *
     * @param source the source
     * @return the target, or {@code null} if no mapping exists
     */
    @Override
    public Target getTarget(T source) {
        return getTarget(source.getObjectReference());
    }

    /**
     * Returns the target for the given source.
     *
     * @param source the source
     * @return the target, or {@code null} if no mapping exists
     */
    @Override
    public Target getTarget(Reference source) {
        Mapping mapping = getMapping(source);
        return (mapping != null) ? mapping.getTarget() : null;
    }

    /**
     * Returns the target given its identifier.
     *
     * @param target the target identifier
     */
    @Override
    public Target getTarget(String target) {
        return targets.getTarget(target);
    }

    /**
     * Returns a count of the targets matching the criteria.
     *
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of targets matching the criteria
     */
    @Override
    public int getTargetCount(String name, boolean unmapped) {
        return targets.count(this, name, unmapped);
    }

    /**
     * Returns the available sources.
     *
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve
     * @return the available sources
     */
    @Override
    public List<Source> getSources(String name, boolean unmapped, int firstResult, int maxResults) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<T> root = query.from(type, sourceArchetype);

        Path<Object> idPath = root.get("id").alias("id");
        Path<String> namePath = root.<String>get("name").alias("name");
        query.multiselect(idPath, namePath);

        addConstraints(query, root, name, namePath, unmapped, builder);
        query.orderBy(builder.asc(namePath), builder.asc(idPath));

        List<Source> result = new ArrayList<>();
        TypedQuery<Tuple> typedQuery = service.createQuery(query);
        typedQuery.setFirstResult(firstResult);
        typedQuery.setMaxResults(maxResults);
        for (Tuple tuple : typedQuery.getResultList()) {
            Reference reference = new IMObjectReference(sourceArchetype, tuple.get("id", Long.class));
            result.add(new SourceImpl(reference, tuple.get("name", String.class)));
        }
        return result;
    }

    /**
     * Returns a count of the sources matching the criteria.
     *
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @return the count of sources matching the criteria
     */
    @Override
    public long getSourceCount(String name, boolean unmapped) {
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> root = query.from(type, sourceArchetype);
        query.select(builder.count(root));

        addConstraints(query, root, name, null, unmapped, builder);
        long result = service.createQuery(query).getSingleResult();
        return (int) result;
    }

    /**
     * Returns the available targets.
     *
     * @param name        a partial name to filter results on, or {@code null} to not filter them
     * @param unmapped    if {@code true}, only include results where no mapping exists
     * @param firstResult the position of the first result to retrieve
     * @param maxResults  the maximum number of results to retrieve, or {@code -1} to retrieve all results
     * @return the available targets
     */
    @Override
    public List<Target> getTargets(String name, boolean unmapped, int firstResult, int maxResults) {
        return targets.getTargets(this, name, unmapped, firstResult, maxResults);
    }

    /**
     * Returns the name for a reference.
     *
     * @param reference the reference
     * @return the name
     */
    private String getName(Reference reference) {
        IMObject object = service.get(reference);
        return (object != null) ? object.getName() : "<unknown>";
    }

    /**
     * Returns the configured cardinality.
     *
     * @param config the configuration
     * @return the cardinality
     */
    private Cardinality getCardinality(IMObjectBean config) {
        String value = config.getString("cardinality");
        return ("N:1".equals(value)) ? Cardinality.MANY_TO_ONE : Cardinality.ONE_TO_ONE;
    }

    /**
     * Returns a string form of a source reference.
     *
     * @param source the source reference
     * @return the stringified reference
     */
    private String toString(Reference source) {
        return source.getArchetype() + ":" + source.getId();
    }

    /**
     * Adds a mapping.
     *
     * @param mapping the mapping
     */
    private void add(Mapping mapping) {
        Reference source = mapping.getSource();
        Target target = mapping.getTarget();
        List<Mapping> sources = bySource.computeIfAbsent(toString(source), k -> new ArrayList<>());
        sources.add(mapping);
        List<Mapping> targets = byTarget.computeIfAbsent(target.getId(), k -> new ArrayList<>());
        targets.add(mapping);
    }

    /**
     * Removes a mapping.
     *
     * @param source the source
     * @param target the target
     */
    private void remove(Reference source, Target target) {
        List<Mapping> sources = bySource.get(toString(source));
        if (sources != null) {
            sources.removeIf(mapping -> Objects.equals(target.getId(), mapping.getTarget().getId()));
        }
        List<Mapping> targets = byTarget.get(target.getId());
        if (targets != null) {
            targets.removeIf(mapping -> Objects.equals(source, mapping.getSource()));
        }
    }

    /**
     * Creates a new mapping.
     *
     * @param config the mapping configuration
     * @param source the source
     * @param target the target
     * @return a new mapping
     */
    private Mapping createMapping(IMObject config, Reference source, Target target) {
        return new IMObjectMapping(config, source, target);
    }

    /**
     * Updates a mapping relationship.
     *
     * @param relationship the relationship
     * @param target       the new target
     */
    private void updateRelationship(Relationship relationship, Target target) {
        IMObjectBean mapping = service.getBean(relationship);
        relationship.setActive(target.isActive());
        mapping.setValue("identity", target.getId());
        mapping.setValue("description", target.getName());
    }

    /**
     * Returns the target from a configuration.
     *
     * @param bean the configuration
     * @return the corresponding target
     */
    private Target getTarget(IMObjectBean bean) {
        return targets.create(bean.getString("identity"), bean.getString("description"), bean.getObject().isActive());
    }

    /**
     * Adds constraints to a query.
     *
     * @param query    the query
     * @param root     the query root
     * @param name     a partial name to filter results on, or {@code null} to not filter them
     * @param namePath the name path. May be {@code null}
     * @param unmapped if {@code true}, only include results where no mapping exists
     * @param builder  the criteria builder
     */
    private void addConstraints(CriteriaQuery<?> query, Root<T> root, String name, Path<String> namePath,
                                boolean unmapped, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(root.get("active"), true));
        if (name != null) {
            if (namePath == null) {
                namePath = root.<String>get("name").alias(name);
            }
            predicates.add(builder.like(namePath, name));
        }
        if (unmapped) {
            IMObject object = config.getObject();
            Root<?> mappingRoot = query.from(object.getClass(), object.getArchetype());
            Join<?, IMObject> mappings = mappingRoot.join("mappings");
            mappings.on(builder.equal(mappings.get("target"), root.reference()));
        }
        query.where(predicates);
    }

}
