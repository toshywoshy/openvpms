/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.apache.commons.lang.math.RandomUtils;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.model.organisation.Company;
import org.openvpms.deputy.internal.model.organisation.Employee;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.query.Field;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Tests implementation of {@link Deputy}.
 *
 * @author Tim Anderson
 */
class TestDeputy implements Deputy {

    /**
     * Predicate used to trigger overlap errors. May be {@code null}.
     */
    private final Predicate<RosterData> overlap;

    /**
     * The rosters.
     */
    private final Map<Long, Roster> rosters = new HashMap<>();

    /**
     * The roster id start id.
     */
    private long startId;

    /**
     * Default constructor.
     */
    TestDeputy() {
        this(RandomUtils.nextLong());
    }

    /**
     * Constructs a {@link TestDeputy}.
     *
     * @param startId the roster id start id
     */
    TestDeputy(long startId) {
        this(startId, null);
    }

    /**
     * Constructs a {@link TestDeputy}.
     *
     * @param startId the roster id start id
     * @param overlap predicate used to trigger overlap errors. May be {@code null}
     */
    TestDeputy(long startId, Predicate<RosterData> overlap) {
        this.startId = startId;
        this.overlap = overlap;
    }

    /**
     * Constructs a {@link TestDeputy}.
     *
     * @param rosters the rosters to pre-register
     */
    TestDeputy(Roster... rosters) {
        this(Arrays.asList(rosters));
    }


    /**
     * Constructs a {@link TestDeputy}.
     *
     * @param rosters the rosters to pre-register
     */
    TestDeputy(List<Roster> rosters) {
        this.startId = RandomUtils.nextLong();
        this.overlap = null;
        for (Roster roster : rosters) {
            this.rosters.put(roster.getId(), roster);
        }
    }

    /**
     * Returns the companies.
     *
     * @return the companies
     */
    @Override
    public List<Company> getCompanies() {
        return Collections.emptyList();
    }

    /**
     * Queries operational units.
     *
     * @param query the query
     * @return the matching operational units
     */
    @Override
    public List<OperationalUnit> getOperationalUnits(Query query) {
        return Collections.emptyList();
    }

    /**
     * Returns an operational unit given its identifier.
     *
     * @param id the operational unit identifier
     * @return the corresponding operational unit
     * @throws NotFoundException if the operational unit does not exist
     */
    @Override
    public OperationalUnit getOperationalUnit(long id) {
        OperationalUnit unit = new OperationalUnit();
        unit.setId(id);
        unit.setActive(true);
        unit.setOperationalUnitName("Area " + id);
        return unit;
    }

    /**
     * Queries employees.
     *
     * @param query the query
     * @return the matching rosters
     */
    @Override
    public List<Employee> getEmployees(Query query) {
        return Collections.emptyList();
    }

    /**
     * Returns an employee given its identifier.
     *
     * @param id the employee identifier
     * @return the corresponding employee
     * @throws NotFoundException if the employee does not exist
     */
    @Override
    public Employee getEmployee(long id) {
        Employee employee = new Employee();
        employee.setActive(true);
        employee.setId(id);
        employee.setDisplayName("Employee " + id);
        return employee;
    }

    /**
     * Queries rosters.
     *
     * @param query the query
     * @return the matching rosters
     */
    @Override
    public List<Roster> getRosters(Query query) {
        List<Roster> result = new ArrayList<>();
        Map<String, Field> criteria = query.getSearch();
        Field area = criteria.get("area");
        Field fromDate = criteria.get("from");
        Field toDate = criteria.get("to");
        if (area == null || fromDate == null || toDate == null) {
            throw new BadRequestException();
        }
        int discarded = 0;
        int firstResult = query.getFirstResults();
        int maxResults = query.getMaxResults();
        long operationalUnit = (Long) area.getData();
        long from = DeputyHelper.toUnixtimestamp((Date) fromDate.getData());
        long to = DeputyHelper.toUnixtimestamp((Date) toDate.getData());
        for (Roster roster : rosters.values()) {
            if ((roster.getOperationalUnit() == operationalUnit)
                && ((roster.getStartTime() < from && roster.getEndTime() > from)
                    || (roster.getStartTime() < to && roster.getEndTime() > to)
                    || roster.getStartTime() >= from && roster.getStartTime() <= to)) {
                if (firstResult > discarded) {
                    discarded++;
                } else {
                    result.add(roster);
                }
                if (result.size() >= maxResults) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Returns a roster given its identifier.
     *
     * @param id the roster identifier
     * @return the roster
     * @throws NotFoundException if the roster is not found
     */
    @Override
    public Roster getRoster(long id) {
        Roster roster = rosters.get(id);
        if (roster == null) {
            throw new NotFoundException();
        }
        return roster;
    }

    /**
     * Creates a new roster, or updates an existing roster.
     * <p/>
     * To update an existing roster, the {@link RosterData#getId()} must be set.
     *
     * @param data the roster data
     * @return the new roster
     * @throws BadRequestException if an overlap is detected
     */
    @Override
    public Roster roster(RosterData data) {
        if (overlap != null && overlap.test(data)) {
            throw new BadRequestException("Overlap detected!");
        }
        long id = data.getId();
        Roster roster;
        if (id == 0) {
            id = startId++;
            roster = new Roster();
            roster.setId(id);
            rosters.put(id, roster);
        } else {
            roster = rosters.get(id);
            if (roster == null) {
                throw new NotFoundException();
            }
        }
        roster.setStartTime(data.getStartTime());
        roster.setEndTime(data.getEndTime());
        roster.setEmployee(data.getEmployee());
        roster.setOperationalUnit(data.getOperationalUnit());
        roster.setModified(new Date());
        return roster;
    }

    /**
     * Deletes a roster.
     *
     * @param id the roster identifier
     * @return the deletion confirmation message
     * @throws NotFoundException if the roster does not exist
     */
    @Override
    public String removeRoster(long id) {
        Roster roster = rosters.remove(id);
        if (roster == null) {
            throw new NotFoundException();
        }
        return "Roster removed";
    }
}
