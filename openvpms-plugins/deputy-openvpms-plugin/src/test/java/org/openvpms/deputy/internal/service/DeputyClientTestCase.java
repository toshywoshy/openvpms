/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import org.junit.Rule;
import org.junit.Test;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.model.organisation.Company;
import org.openvpms.deputy.internal.model.organisation.Employee;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;
import java.util.Date;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.openvpms.deputy.internal.service.DeputyTestHelper.getISODatetime;

/**
 * Tests the {@link DeputyClient}.
 *
 * @author Tim Anderson
 */
public class DeputyClientTestCase {

    /**
     * Sets up a WireMock service.
     */
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Options.DYNAMIC_PORT);

    /**
     * Tests the {@link DeputyClient#getCompanies()} method.
     */
    @Test
    public void testGetCompanies() {
        String response = "[{\"Id\":6,\"Portfolio\":null,\"Code\":\"MRM\",\"Active\":true,\"ParentCompany\":0," +
                          "\"CompanyName\":\"OpenVPMS\",\"TradingName\":null,\"BusinessNumber\":null," +
                          "\"CompanyNumber\":null,\"IsWorkplace\":true,\"IsPayrollEntity\":true," +
                          "\"PayrollExportCode\":null,\"Address\":156,\"Contact\":null,\"Creator\":1," +
                          "\"Created\":\"2015-12-08T05:28:27+11:00\",\"Modified\":\"2019-08-10T19:36:41+10:00\"}]";
        stubFor(WireMock.get(urlEqualTo("/api/v1/resource/Company"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        DeputyClient client = createClient();
        List<Company> companies = client.getCompanies();
        assertEquals(1, companies.size());
        Company company = companies.get(0);
        assertEquals(6, company.getId());
        assertEquals("OpenVPMS", company.getName());
        assertTrue(company.getActive());
        assertEquals("OpenVPMS", company.getCompanyName());
    }

    /**
     * Tests the {@link DeputyClient#getOperationalUnits(Query)} method.
     */
    @Test
    public void testGetOperationalUnits() {
        String request = "{\"sort\":{\"Id\":\"asc\"}}";
        String response = "[{\"Id\":4,\"Creator\":1,\"Created\":\"2015-12-08T05:46:52+11:00\"," +
                          "\"Modified\":\"2018-06-21T16:40:48+10:00\",\"Company\":6," +
                          "\"ParentOperationalUnit\":0,\"OperationalUnitName\":\"Reception\\/Sales\"," +
                          "\"Active\":true,\"PayrollExportName\":\"\",\"Address\":0,\"Contact\":null," +
                          "\"RosterSortOrder\":0,\"ShowOnRoster\":true,\"Colour\":\"#35e4bd\"," +
                          "\"RosterActiveHoursSchedule\":null,\"DailyRosterBudget\":null," +
                          "\"OperationalUnitType\":null,\"CompanyCode\":\"MRM\",\"CompanyName\":\"OpenVPMS\"}" +
                          ",{\"Id\":17,\"Creator\":1,\"Created\":\"2015-12-08T11:21:57+11:00\"," +
                          "\"Modified\":\"2018-06-21T16:40:56+10:00\",\"Company\":6,\"ParentOperationalUnit\":0," +
                          "\"OperationalUnitName\":\"Workshop\",\"Active\":true,\"PayrollExportName\":\"\"," +
                          "\"Address\":0,\"Contact\":null,\"RosterSortOrder\":1,\"ShowOnRoster\":true," +
                          "\"Colour\":\"#bc51ff\",\"RosterActiveHoursSchedule\":null,\"DailyRosterBudget\":null," +
                          "\"OperationalUnitType\":null,\"CompanyCode\":\"MRM\",\"CompanyName\":\"OpenVPMS\"}," +
                          "{\"Id\":18,\"Creator\":1,\"Created\":\"2015-12-08T11:22:22+11:00\"," +
                          "\"Modified\":\"2018-06-21T16:41:03+10:00\",\"Company\":6," +
                          "\"ParentOperationalUnit\":0,\"OperationalUnitName\":\"Accounts\",\"Active\":true," +
                          "\"PayrollExportName\":\"\",\"Address\":0,\"Contact\":null,\"RosterSortOrder\":2," +
                          "\"ShowOnRoster\":true,\"Colour\":\"#f93c3c\",\"RosterActiveHoursSchedule\":null," +
                          "\"DailyRosterBudget\":null,\"OperationalUnitType\":null,\"CompanyCode\":\"MRM\"," +
                          "\"CompanyName\":\"OpenVPMS\"}]";

        stubFor(WireMock.post(urlEqualTo("/api/v1/resource/OperationalUnit/QUERY"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        DeputyClient client = createClient();

        Query query = new Query();
        query.orderBy("Id", true);
        List<OperationalUnit> units = client.getOperationalUnits(query);
        assertEquals(3, units.size());

        OperationalUnit unit1 = units.get(0);
        assertEquals(4, unit1.getId());
        assertEquals("Reception/Sales", unit1.getName());
        assertTrue(unit1.getActive());

        OperationalUnit unit2 = units.get(1);
        assertEquals(17, unit2.getId());
        assertEquals("Workshop", unit2.getName());
        assertTrue(unit2.getActive());

        OperationalUnit unit3 = units.get(2);
        assertEquals(18, unit3.getId());
        assertEquals("Accounts", unit3.getName());
        assertTrue(unit3.getActive());

        // check serialisation of the request
        List<LoggedRequest> requests = WireMock.findAll(postRequestedFor(
                urlEqualTo("/api/v1/resource/OperationalUnit/QUERY")));
        assertEquals(1, requests.size());
        assertEquals(request, requests.get(0).getBodyAsString());
    }

    /**
     * Tests the {@link DeputyClient#getOperationalUnit(long)} method.
     */
    @Test
    public void testGetOperationalUnit() {
        String response = "{\"Id\":4,\"Creator\":1,\"Created\":\"2015-12-08T05:46:52+11:00\"," +
                          "\"Modified\":\"2018-06-21T16:40:48+10:00\",\"Company\":6,\"ParentOperationalUnit\":0," +
                          "\"OperationalUnitName\":\"Reception\\/Sales\",\"Active\":true,\"PayrollExportName\":\"\"," +
                          "\"Address\":0,\"Contact\":null,\"RosterSortOrder\":0,\"ShowOnRoster\":true," +
                          "\"Colour\":\"#35e4bd\",\"RosterActiveHoursSchedule\":null,\"DailyRosterBudget\":null," +
                          "\"OperationalUnitType\":null,\"CompanyCode\":\"MRM\",\"CompanyName\":\"OpenVPMS\"}";
        stubFor(WireMock.get(urlEqualTo("/api/v1/resource/OperationalUnit/4"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));
        stubFor(WireMock.get(urlEqualTo("/api/v1/resource/OperationalUnit/99"))
                        .willReturn(aResponse()
                                            .withStatus(404)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody("{\"error\":{\"code\":404,\"message\":\"Object not found\"}}")));
        DeputyClient client = createClient();
        OperationalUnit unit = client.getOperationalUnit(4);
        assertEquals(4, unit.getId());
        assertEquals("Reception/Sales", unit.getName());
        assertTrue(unit.getActive());

        // verify a NotFoundException is generated for a non-existent operational unit
        try {
            client.getOperationalUnit(99);
            fail("Expected call to fail");
        } catch (NotFoundException exception) {
            assertEquals("404 - Object not found", exception.getMessage());
        }
    }

    /**
     * Tests the {@link DeputyClient#getEmployees(Query)} method.
     */
    @Test
    public void testGetEmployees() {
        String request = "{\"sort\":{\"DisplayName\":\"asc\",\"Id\":\"asc\"}}";
        String response = "[{\"Id\":1,\"Company\":6,\"FirstName\":\"Joe\",\"LastName\":\"Bloggs\"," +
                          "\"DisplayName\":\"Joe Bloggs\",\"OtherName\":null,\"Salutation\":null," +
                          "\"MainAddress\":null,\"PostalAddress\":null,\"Contact\":500," +
                          "\"EmergencyAddress\":null,\"DateOfBirth\":null,\"Gender\":null,\"Photo\":null," +
                          "\"UserId\":1,\"JobAppId\":null,\"Active\":true," +
                          "\"StartDate\":\"2019-08-10T00:00:00+10:00\",\"TerminationDate\":null," +
                          "\"StressProfile\":1,\"Position\":null,\"HigherDuty\":null,\"Role\":1," +
                          "\"AllowAppraisal\":true,\"HistoryId\":2566,\"CustomFieldData\":null," +
                          "\"Creator\":1,\"Created\":\"2019-08-10T19:36:32+10:00\"," +
                          "\"Modified\":\"2019-08-10T19:36:40+10:00\"}]";
        stubFor(WireMock.post(urlEqualTo("/api/v1/resource/Employee/QUERY"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        DeputyClient client = createClient();
        Query query = new Query();
        query.orderBy("DisplayName", true);
        query.orderBy("Id", true);
        List<Employee> employees = client.getEmployees(query);
        assertEquals(1, employees.size());

        Employee employee = employees.get(0);
        assertEquals(1, employee.getId());
        assertEquals(6, employee.getCompany());
        assertEquals("Joe Bloggs", employee.getName());
        assertTrue(employee.getActive());

        // check serialisation of the request
        List<LoggedRequest> requests = WireMock.findAll(postRequestedFor(
                urlEqualTo("/api/v1/resource/Employee/QUERY")));
        assertEquals(1, requests.size());
        assertEquals(request, requests.get(0).getBodyAsString());
    }

    /**
     * Tests the {@link Deputy#getEmployee(long)} method.
     */
    @Test
    public void testGetEmployee() {
        String response = "{\"Id\":1,\"Company\":6,\"FirstName\":\"Joe\",\"LastName\":\"Bloggs\"," +
                          "\"DisplayName\":\"Joe Bloggs\",\"OtherName\":null,\"Salutation\":null," +
                          "\"MainAddress\":null,\"PostalAddress\":null,\"Contact\":500," +
                          "\"EmergencyAddress\":null,\"DateOfBirth\":null,\"Gender\":null,\"Photo\":null," +
                          "\"UserId\":1,\"JobAppId\":null,\"Active\":true," +
                          "\"StartDate\":\"2019-08-10T00:00:00+10:00\",\"TerminationDate\":null," +
                          "\"StressProfile\":1,\"Position\":null,\"HigherDuty\":null,\"Role\":1," +
                          "\"AllowAppraisal\":true,\"HistoryId\":2566,\"CustomFieldData\":null," +
                          "\"Creator\":1,\"Created\":\"2019-08-10T19:36:32+10:00\"," +
                          "\"Modified\":\"2019-08-10T19:36:40+10:00\"}";
        stubFor(WireMock.get(urlEqualTo("/api/v1/resource/Employee/1"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));
        stubFor(WireMock.get(urlEqualTo("/api/v1/resource/Employee/99"))
                        .willReturn(aResponse()
                                            .withStatus(404)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody("{\"error\":{\"code\":404,\"message\":\"Object not found\"}}")));

        DeputyClient client = createClient();
        Employee employee = client.getEmployee(1);
        assertNotNull(employee);
        assertEquals(1, employee.getId());
        assertEquals(6, employee.getCompany());
        assertEquals("Joe Bloggs", employee.getName());
        assertTrue(employee.getActive());

        // verify a NotFoundException is generated for a non-existent employee
        try {
            client.getEmployee(99);
            fail("Expected call to fail");
        } catch (NotFoundException exception) {
            assertEquals("404 - Object not found", exception.getMessage());
        }
    }

    /**
     * Tests the {@link DeputyClient#getRosters(Query)} method.
     */
    @Test
    public void testGetRosters() {
        String request = "{\"search\":{\"area\":{\"field\":\"OperationalUnit\",\"type\":\"eq\",\"data\":4}," +
                         "\"from\":{\"field\":\"Date\",\"type\":\"ge\",\"data\":\"2019-08-10\"}," +
                         "\"to\":{\"field\":\"Date\",\"type\":\"lt\",\"data\":\"2019-08-11\"}}," +
                         "\"sort\":{\"Id\":\"asc\"},\"start\":0,\"max\":500}";
        String response = "[{\"Id\":31260,\"Date\":\"2019-08-10T00:00:00+10:00\",\"StartTime\":1565391600," +
                          "\"EndTime\":1565422200,\"Mealbreak\":\"2019-08-10T00:30:00+10:00\",\"Slots\":[]," +
                          "\"TotalTime\":8,\"Cost\":200,\"OperationalUnit\":4,\"Employee\":390," +
                          "\"Comment\":\"\",\"Warning\":\"\",\"WarningOverrideComment\":\"\",\"Published\":true" +
                          ",\"MatchedByTimesheet\":0,\"Open\":false,\"ConfirmStatus\":0,\"ConfirmComment\":\"\"," +
                          "\"ConfirmBy\":0,\"ConfirmTime\":0,\"SwapStatus\":0,\"SwapManageBy\":null," +
                          "\"ShiftTemplate\":null,\"ConnectStatus\":null,\"Creator\":1," +
                          "\"Created\":\"2019-07-21T14:01:18+10:00\",\"Modified\":\"2019-08-10T19:39:07+10:00\"," +
                          "\"OnCost\":200,\"StartTimeLocalized\":\"2019-08-10T09:00:00+10:00\"," +
                          "\"EndTimeLocalized\":\"2019-08-10T17:30:00+10:00\",\"ExternalId\":null," +
                          "\"ConnectCreator\":null},{\"Id\":31263,\"Date\":\"2019-08-10T00:00:00+10:00\"," +
                          "\"StartTime\":1565391600,\"EndTime\":1565422200," +
                          "\"Mealbreak\":\"2019-08-10T00:30:00+10:00\",\"Slots\":[],\"TotalTime\":8,\"Cost\":200," +
                          "\"OperationalUnit\":4,\"Employee\":394,\"Comment\":\"\",\"Warning\":\"\"," +
                          "\"WarningOverrideComment\":\"\",\"Published\":true,\"MatchedByTimesheet\":0," +
                          "\"Open\":false,\"ConfirmStatus\":0,\"ConfirmComment\":\"\",\"ConfirmBy\":0," +
                          "\"ConfirmTime\":0,\"SwapStatus\":0,\"SwapManageBy\":null,\"ShiftTemplate\":null," +
                          "\"ConnectStatus\":null,\"Creator\":1,\"Created\":\"2019-07-21T14:01:19+10:00\"," +
                          "\"Modified\":\"2019-08-10T19:39:07+10:00\",\"OnCost\":200," +
                          "\"StartTimeLocalized\":\"2019-08-10T09:00:00+10:00\"," +
                          "\"EndTimeLocalized\":\"2019-08-10T17:30:00+10:00\",\"ExternalId\":null," +
                          "\"ConnectCreator\":null}]";

        Query query = new Query();
        query.addSearch("area", "OperationalUnit", "eq", 4);
        query.addSearch("from", "Date", "ge", "2019-08-10");
        query.addSearch("to", "Date", "lt", "2019-08-11");
        query.orderBy("Id", true);
        query.setFirstResult(0);
        query.setMaxResults(500);

        stubFor(WireMock.post(urlEqualTo("/api/v1/resource/Roster/QUERY"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        DeputyClient client = createClient();
        List<Roster> rosters = client.getRosters(query);

        assertEquals(2, rosters.size());
        Roster roster1 = rosters.get(0);
        assertEquals(31260, roster1.getId());
        assertEquals(1565391600, roster1.getStartTime());
        assertEquals(1565422200, roster1.getEndTime());
        assertEquals(4, roster1.getOperationalUnit());
        assertEquals(390, roster1.getEmployee());
        assertEquals(getISODatetime("2019-08-10T19:39:07+10:00"), roster1.getModified());

        Roster roster2 = rosters.get(1);
        assertEquals(31263, roster2.getId());
        assertEquals(1565391600, roster2.getStartTime());
        assertEquals(1565422200, roster2.getEndTime());
        assertEquals(4, roster2.getOperationalUnit());
        assertEquals(394, roster2.getEmployee());
        assertEquals(getISODatetime("2019-08-10T19:39:07+10:00"), roster1.getModified());

        // check serialisation of the request
        List<LoggedRequest> requests = WireMock.findAll(postRequestedFor(urlEqualTo("/api/v1/resource/Roster/QUERY")));
        assertEquals(1, requests.size());
        assertEquals(request, requests.get(0).getBodyAsString());
    }

    /**
     * Tests the {@link Deputy#getRoster(long)} method.
     */
    @Test
    public void testGetRoster() {
        String response = "{\"Id\":31623,\"Date\":\"2019-08-12T00:00:00+10:00\",\"StartTime\":1565564400," +
                          "\"EndTime\":1565595000,\"Mealbreak\":\"2019-08-12T00:00:00+10:00\",\"Slots\":[]," +
                          "\"TotalTime\":8.5,\"Cost\":0,\"OperationalUnit\":4,\"Employee\":1,\"Comment\":null," +
                          "\"Warning\":\"\",\"WarningOverrideComment\":null,\"Published\":false," +
                          "\"MatchedByTimesheet\":0,\"Open\":false,\"ConfirmStatus\":0,\"ConfirmComment\":\"\"," +
                          "\"ConfirmBy\":0,\"ConfirmTime\":0,\"SwapStatus\":0,\"SwapManageBy\":null," +
                          "\"ShiftTemplate\":null,\"ConnectStatus\":null,\"Creator\":1," +
                          "\"Created\":\"2019-08-11T12:06:21+10:00\",\"Modified\":\"2019-08-11T12:06:21+10:00\"," +
                          "\"OnCost\":0,\"StartTimeLocalized\":\"2019-08-12T09:00:00+10:00\"," +
                          "\"EndTimeLocalized\":\"2019-08-12T17:30:00+10:00\",\"ExternalId\":null," +
                          "\"ConnectCreator\":null}";
        stubFor(WireMock.get(urlEqualTo("/api/v1/resource/Roster/31623"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        DeputyClient client = createClient();
        Roster roster = client.getRoster(31623);
        assertEquals(31623, roster.getId());
        assertEquals(4, roster.getOperationalUnit());
        assertEquals(1, roster.getEmployee());
        assertEquals(1565564400, roster.getStartTime());
        assertEquals(1565595000, roster.getEndTime());
    }

    /**
     * Tests the {@link DeputyClient#roster(RosterData)} method.
     */
    @Test
    public void testCreateRoster() {
        String request = "{\"intStartTimestamp\":1565564400,\"intEndTimestamp\":1565595000,\"intRosterEmployee\":1," +
                         "\"blnPublish\":0,\"blnOpen\":false,\"strComment\":null,\"blnForceOverwrite\":1," +
                         "\"intMealbreakMinute\":0,\"intOpunitId\":4,\"intRosterId\":0}";
        String response = "{\"Id\":31623,\"Date\":\"2019-08-12T00:00:00+10:00\",\"StartTime\":1565564400," +
                          "\"EndTime\":1565595000,\"Mealbreak\":\"2019-08-12T00:00:00+10:00\",\"Slots\":[]," +
                          "\"TotalTime\":8.5,\"Cost\":0,\"OperationalUnit\":4,\"Employee\":1,\"Comment\":null," +
                          "\"Warning\":\"\",\"WarningOverrideComment\":null,\"Published\":false," +
                          "\"MatchedByTimesheet\":0,\"Open\":false,\"ConfirmStatus\":0,\"ConfirmComment\":\"\"," +
                          "\"ConfirmBy\":0,\"ConfirmTime\":0,\"SwapStatus\":0,\"SwapManageBy\":null," +
                          "\"ShiftTemplate\":null,\"ConnectStatus\":null,\"Creator\":1," +
                          "\"Created\":\"2019-08-11T12:06:21+10:00\",\"Modified\":\"2019-08-11T12:06:21+10:00\"," +
                          "\"OnCost\":0,\"StartTimeLocalized\":\"2019-08-12T09:00:00+10:00\"," +
                          "\"EndTimeLocalized\":\"2019-08-12T17:30:00+10:00\",\"ExternalId\":null," +
                          "\"ConnectCreator\":null}";

        stubFor(WireMock.post(urlEqualTo("/api/v1/supervise/roster"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        RosterData data = new RosterData();

        data.setOperationalUnit(4);
        data.setEmployee(1);
        data.setStartTime(DeputyTestHelper.getUnixtimestamp("2019-08-12T09:00:00+10:00"));
        data.setEndTime(DeputyTestHelper.getUnixtimestamp("2019-08-12T17:30:00+10:00"));
        data.setForceOverwrite(1);

        DeputyClient client = createClient();
        Roster roster = client.roster(data);

        // check serialisation of the request
        List<LoggedRequest> requests = WireMock.findAll(postRequestedFor(urlEqualTo("/api/v1/supervise/roster")));
        assertEquals(1, requests.size());
        assertEquals(request, requests.get(0).getBodyAsString());

        // check response
        assertEquals(31623, roster.getId());
        assertEquals(4, roster.getOperationalUnit());
        assertEquals(1, roster.getEmployee());
        assertEquals(1565564400, roster.getStartTime());
        assertEquals(1565595000, roster.getEndTime());

        Date datetime = getISODatetime("2019-08-11T12:06:21+10:00");
        assertEquals(datetime, roster.getCreated());
        assertEquals(datetime, roster.getModified());
    }

    /**
     * Simulates creation of an overlapping roster, and verifies that a {@link BadRequestException} is thrown when
     * a 400 status is returned.
     */
    @Test
    public void testCreateOverlappingRoster() {
        String request = "{\"intStartTimestamp\":1565564400,\"intEndTimestamp\":1565595000,\"intRosterEmployee\":1," +
                         "\"blnPublish\":0,\"blnOpen\":false,\"strComment\":null,\"blnForceOverwrite\":1," +
                         "\"intMealbreakMinute\":0,\"intOpunitId\":4,\"intRosterId\":0}";
        String response = "{\"error\":{\"code\":400,\"message\":\"Overlap detected! Joe Bloggs already working on " +
                          "Mon 12/08/19 09:00 AM to 05:30 PM at [MRM] Reception/Sales\"}}";

        stubFor(WireMock.post(urlEqualTo("/api/v1/supervise/roster"))
                        .willReturn(aResponse()
                                            .withStatus(400)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        RosterData create = new RosterData();
        create.setOperationalUnit(4);
        create.setEmployee(1);
        create.setStartTime(DeputyTestHelper.getUnixtimestamp("2019-08-12T09:00:00+10:00"));
        create.setEndTime(DeputyTestHelper.getUnixtimestamp("2019-08-12T17:30:00+10:00"));
        create.setForceOverwrite(1);

        DeputyClient client = createClient();

        try {
            client.roster(create);
            fail("Expected roster to fail");
        } catch (BadRequestException expected) {
            assertEquals("400 - Overlap detected! Joe Bloggs already working on Mon 12/08/19 09:00 AM to 05:30 PM " +
                         "at [MRM] Reception/Sales", expected.getMessage());
        }

        // check serialisation of the request
        List<LoggedRequest> requests = WireMock.findAll(postRequestedFor(urlEqualTo("/api/v1/supervise/roster")));
        assertEquals(1, requests.size());
        assertEquals(request, requests.get(0).getBodyAsString());
    }

    /**
     * Tests the {@link DeputyClient#roster(RosterData)} method when updating a roster.
     */
    @Test
    public void testUpdateRoster() {
        String request = "{\"intStartTimestamp\":1565395200,\"intEndTimestamp\":1565425800,\"intRosterEmployee\":1," +
                         "\"blnPublish\":0,\"blnOpen\":false,\"strComment\":null,\"blnForceOverwrite\":1," +
                         "\"intMealbreakMinute\":0,\"intOpunitId\":4,\"intRosterId\":31623}";

        String response = "{\"Id\":31623,\"Date\":\"2019-08-10T00:00:00+10:00\",\"StartTime\":1565395200," +
                          "\"EndTime\":1565425800,\"Mealbreak\":\"2019-08-10T00:00:00+10:00\",\"Slots\":[]," +
                          "\"TotalTime\":8.5,\"Cost\":0,\"OperationalUnit\":4,\"Employee\":1,\"Comment\":null," +
                          "\"Warning\":\"\",\"WarningOverrideComment\":null,\"Published\":false," +
                          "\"MatchedByTimesheet\":0,\"Open\":false,\"ConfirmStatus\":0,\"ConfirmComment\":\"\"," +
                          "\"ConfirmBy\":0,\"ConfirmTime\":0,\"SwapStatus\":0,\"SwapManageBy\":null," +
                          "\"ShiftTemplate\":null,\"ConnectStatus\":null,\"Creator\":1," +
                          "\"Created\":\"2019-08-11T12:06:21+10:00\",\"Modified\":\"2019-08-11T12:06:22+10:00\"," +
                          "\"OnCost\":0,\"StartTimeLocalized\":\"2019-08-10T10:00:00+10:00\"," +
                          "\"EndTimeLocalized\":\"2019-08-10T18:30:00+10:00\",\"ExternalId\":null," +
                          "\"ConnectCreator\":null}";

        stubFor(WireMock.post(urlEqualTo("/api/v1/supervise/roster"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        RosterData data = new RosterData();

        data.setOperationalUnit(4);
        data.setEmployee(1);
        data.setStartTime(DeputyTestHelper.getUnixtimestamp("2019-08-10T10:00:00+10:00"));
        data.setEndTime(DeputyTestHelper.getUnixtimestamp("2019-08-10T18:30:00+10:00"));
        data.setForceOverwrite(1);
        data.setId(31623);

        DeputyClient client = createClient();
        Roster updated = client.roster(data);

        // check serialisation of the request
        List<LoggedRequest> requests = WireMock.findAll(postRequestedFor(urlEqualTo("/api/v1/supervise/roster")));
        assertEquals(1, requests.size());
        assertEquals(request, requests.get(0).getBodyAsString());

        // check response
        assertEquals(4, updated.getOperationalUnit());
        assertEquals(1, updated.getEmployee());
        assertEquals(1565395200, updated.getStartTime());
        assertEquals(1565425800, updated.getEndTime());
    }

    /**
     * Tests the {@link DeputyClient#removeRoster(long)} method.
     */
    @Test
    public void testRemoveRoster() {
        RosterData data = new RosterData();
        String response = "\"Deleted 31623 Joe Bloggs from Sun 11/08/19 09:00 AM to 05:30 PM @ [OpenVPMS] " +
                          "Reception/Sales\"";

        data.setOperationalUnit(4);
        data.setEmployee(1);
        data.setStartTime(DeputyTestHelper.getUnixtimestamp("2019-08-12T09:00:00+10:00"));
        data.setEndTime(DeputyTestHelper.getUnixtimestamp("2019-08-12T17:30:00+10:00"));
        data.setForceOverwrite(1);
        data.setId(31623);

        stubFor(WireMock.delete(urlEqualTo("/api/v1/resource/Roster/31623"))
                        .willReturn(aResponse()
                                            .withStatus(200)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody(response)));

        stubFor(WireMock.delete(urlEqualTo("/api/v1/resource/Roster/2"))
                        .willReturn(aResponse()
                                            .withStatus(404)
                                            .withHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                            .withBody("{\"error\":{\"code\":404,\"message\":\"Object not found\"}}")));

        DeputyClient client = createClient();
        String result = client.removeRoster(31623);
        assertEquals(response, result);

        // now verify a NotFoundException is thrown when the roster doesn't exist
        try {
            client.removeRoster(2);
            fail("Expected remove to fail");
        } catch (NotFoundException expected) {
            assertEquals("404 - Object not found", expected.getMessage());
        }
    }

    /**
     * Creates a new client.
     *
     * @return a new client
     */
    private DeputyClient createClient() {
        String url = "http://localhost:" + wireMockRule.port() + "/";
        return new DeputyClient(url, "anaccesstoken");
    }

}
