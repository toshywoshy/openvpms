/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Identity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.Archetypes;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.i18n.DeputyMessages;
import org.openvpms.deputy.internal.model.organisation.Employee;
import org.openvpms.deputy.internal.model.organisation.OperationalUnit;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.deputy.internal.model.roster.RosterData;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.openvpms.deputy.internal.service.DeputyHelper.fromUnixtimestamp;
import static org.openvpms.deputy.internal.service.DeputyHelper.getRosterId;
import static org.openvpms.deputy.internal.service.DeputyHelper.getTargetId;
import static org.openvpms.deputy.internal.service.DeputyHelper.toUnixtimestamp;
import static org.openvpms.deputy.internal.service.SyncStatus.ERROR;
import static org.openvpms.deputy.internal.service.SyncStatus.SYNC;

/**
 * Synchronises roster updates between OpenVPMS and Deputy.
 *
 * @author Tim Anderson
 */
class RosterSynchroniser {

    /**
     * The area mappings.
     */
    private final Mappings<Entity> areas;

    /**
     * The user mappings.
     */
    private final Mappings<User> users;

    /**
     * The Deputy service.
     */
    private final Deputy deputy;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The query service.
     */
    private final QueryService queryService;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(RosterSynchroniser.class);


    /**
     * Constructs a {@link RosterSynchroniser}.
     *
     * @param areas        the area mappings
     * @param users        the user mappings
     * @param deputy       the Deputy service
     * @param queryService the query service
     * @param service      the archetype service
     */
    RosterSynchroniser(Mappings<Entity> areas, Mappings<User> users, Deputy deputy, QueryService queryService,
                       ArchetypeService service) {
        this.areas = areas;
        this.users = users;
        this.deputy = deputy;
        this.service = service;
        this.queryService = queryService;
    }

    /**
     * Invoked when an <em>act.rosterEvent</em> is changed.
     * <p/>
     * If the event has never been synchronised, this will create a new roster in Deputy, otherwise
     * it will attempt to update the existing roster.<br/>
     * If the Deputy roster no longer exists, then the event itself will be deleted.
     *
     * @param event the roster event
     * @return the roster, or {@code null} if the event was deleted or the roster could not be created
     */
    Roster synchroniseFromEvent(Act event) {
        return synchroniseFromEvent(service.getBean(event));
    }

    /**
     * Invoked when an <em>act.rosterEvent</em> is changed.
     * <p/>
     * If the event has never been synchronised, this will create a new roster in Deputy, otherwise
     * it will attempt to update the existing roster.<br/>
     * If the Deputy roster no longer exists, then the event itself will be deleted.
     *
     * @param bean the roster event bean
     * @return the roster, or {@code null} if the event was deleted or the roster could not be created
     */
    Roster synchroniseFromEvent(IMObjectBean bean) {
        return synchroniseFromEvent(bean, new Rosters(deputy));
    }

    /**
     * Invoked when an <em>act.rosterEvent</em> is changed.
     * <p/>
     * If the event has never been synchronised, this will create a new roster in Deputy, otherwise
     * it will attempt to update the existing roster.<br/>
     * If the Deputy roster no longer exists, then the event itself will be deleted.
     *
     * @param event   the roster event
     * @param rosters the cache of Deputy rosters
     * @return the roster, or {@code null} if the event was deleted or the roster could not be created
     */
    Roster synchroniseFromEvent(Act event, Rosters rosters) {
        IMObjectBean bean = service.getBean(event);
        return synchroniseFromEvent(bean, rosters);
    }

    /**
     * Invoked when an <em>act.rosterEvent</em> is changed.
     * <p/>
     * If the event has never been synchronised, this will create a new roster in Deputy, otherwise
     * it will attempt to update the existing roster.<br/>
     * If the Deputy roster no longer exists, then the event itself will be deleted.
     *
     * @param bean    the roster event bean
     * @param rosters the cache of Deputy rosters
     * @return the roster, or {@code null} if the event was deleted or the roster could not be created
     */
    Roster synchroniseFromEvent(IMObjectBean bean, Rosters rosters) {
        Roster result = null;
        Reference areaRef = bean.getTargetRef("schedule");
        if (areaRef != null) {
            Target area = areas.getTarget(areaRef);
            Identity identity = DeputyHelper.getSynchronisationId(bean);
            if (area != null) {
                Reference userRef = bean.getTargetRef("user");
                Target employee = (userRef != null) ? users.getTarget(userRef) : null;
                if (!hasSynchronised(identity)) {
                    if (userRef != null && employee == null) {
                        if (setError(bean, DeputyMessages.noMappingForUser(queryService.getName(userRef)))) {
                            bean.save();
                        }
                    } else {
                        result = createRoster(bean, area, employee, rosters, identity);
                    }
                } else {
                    result = updateIfChanged(bean, area, userRef, employee, identity, rosters);
                }
            } else {
                // area not synchronised. If the event has been synced previously, need to raise an error
                if (hasSynchronised(identity)) {
                    String name = queryService.getName(areaRef);
                    if (setError(bean, DeputyMessages.shiftMovedToUnmappedOpenVPMSArea(name))) {
                        bean.save();
                    }
                }
            }
        }
        return result;
    }

    /**
     * Updates OpenVPMS with a Deputy roster.
     * <p/>
     * If the roster is linked to an existing event, this will be updated. This could lead to overlaps,
     * but this should be corrected by synchronising more roster<br/>
     * If the roster is not linked to an event, the it will be checked to see if it overlaps an existing event.<br/>
     * If so and the overlap:
     * <ul>
     * <li>is synced to another event, a sync error will be raised.</li>
     * <li>is not synced, and the times match, then the roster will be linked to it.</li>
     * <li>is not synced, and the times don't match, then a sync error will be raised</li>
     * </ul>
     *
     * @param roster  the roster
     * @param rosters the cache of rosters
     * @return the corresponding event, or {@code null} if no event was updated
     */
    Act synchroniseFromRoster(Roster roster, Rosters rosters) {
        Act result = null;
        Reference areaRef = getReferenceForDeputyId(areas, roster.getOperationalUnit());
        long employee = roster.getEmployee();
        Reference userRef = getReferenceForDeputyId(users, employee);

        Act event = queryService.getEvent(roster);
        if (event != null) {
            // roster has been previously synced
            IMObjectBean bean = service.getBean(event);
            ActIdentity identity = DeputyHelper.getSynchronisationId(bean);
            if (identity == null) {
                // this shouldn't occur, as the act was matched on identity
                log.error("Cannot update event=" + event.getId() + ". Missing synchronisation identifier");
            } else {
                IMObjectBean idBean = service.getBean(identity);
                if (ERROR.equals(idBean.getString("status"))
                    || !Objects.equals(roster.getModified(), idBean.getDate("modified"))) {
                    if (updateEvent(roster, bean, identity, areaRef, userRef, rosters)) {
                        result = event;
                    }
                }
            }
        } else {
            // not synced
            if (areaRef != null) {
                if (employee == 0 || userRef != null) {
                    // only create an event when both the area and employee are mapped. Open events have employee == 0
                    Date startTime = fromUnixtimestamp(roster.getStartTime());
                    Date endTime = fromUnixtimestamp(roster.getEndTime());
                    if (employee != 0) {
                        // syncing from an employee roster
                        result = syncFromEmployeeRoster(roster, areaRef, userRef, startTime, endTime);
                    } else {
                        result = syncFromOpenRoster(roster, areaRef, startTime, endTime);
                    }
                } else {
                    if (log.isInfoEnabled()) {
                        log.info("Not syncing roster=" + roster.getId() + " as employee=" + roster.getEmployee()
                                 + " not mapped");
                    }
                }
            } else {
                if (log.isInfoEnabled()) {
                    log.info("Not syncing roster=" + roster.getId() + " as operational unit="
                             + roster.getOperationalUnit() + " not mapped");
                }
            }
        }
        return result;
    }

    /**
     * Updates Deputy when an <em>act.rosterEvent</em> is removed.
     *
     * @param event the event
     * @return {@code true} if Deputy was updated
     */
    boolean remove(Act event) {
        IMObjectBean bean = service.getBean(event);
        return remove(bean);
    }

    /**
     * Updates Deputy when an <em>act.rosterEvent</em> is removed.
     *
     * @param bean the event bean
     * @return {@code true} if Deputy was updated
     */
    boolean remove(IMObjectBean bean) {
        boolean result = false;
        Reference areaRef = bean.getTargetRef("schedule");
        if (areaRef != null) {
            // make sure the area is being synchronised
            Target area = areas.getTarget(areaRef);
            if (area != null) {
                long operationalUnit = getTargetId(area);
                Reference userRef = bean.getTargetRef("user");
                Target user = (userRef != null) ? users.getTarget(userRef) : null;
                long employee = getTargetId(user);
                long id = getRosterId(bean);
                if (id != -1) {
                    Roster roster = getRoster(id);
                    if (roster != null) {
                        if (roster.getOperationalUnit() == operationalUnit && roster.getEmployee() == employee) {
                            // only remove the roster if it is for the same area and user
                            try {
                                String response = deputy.removeRoster(id);
                                result = true;
                                log.info("Removed roster=" + id + " linked to deleted event="
                                         + bean.getObject().getId() + ": " + response);
                            } catch (Throwable exception) {
                                log.info("Failed to remove roster=" + id + " linked to deleted event="
                                         + bean.getObject().getId() + ": " + exception.getMessage(), exception);
                            }
                        } else {
                            log.info("Not removing Deputy roster=" + id + " linked to deleted event="
                                     + bean.getObject().getId() + ". The local copy had operationalUnit="
                                     + operationalUnit + ", employee= " + employee
                                     + ", whereas Deputy has operationalUnit=" + roster.getOperationalUnit()
                                     + ", employee=" + roster.getEmployee());
                        }
                    } else {
                        log.info("Not removing roster=" + id + " linked to deleted event=" + bean.getObject().getId()
                                 + " as it could not be retrieved from Deputy");
                    }
                }
            }
        }
        return result;
    }

    /**
     * Updates OpenVPMS with a Deputy roster for a specific employee that has not been previously synced.
     *
     * @param roster    the roster
     * @param areaRef   the area reference
     * @param userRef   the user reference, or {@code null} if the roster is an open shift
     * @param startTime the start time
     * @param endTime   the end time
     * @return the corresponding event, or {@code null} if no event was updated
     */
    private Act syncFromEmployeeRoster(Roster roster, Reference areaRef, Reference userRef, Date startTime,
                                       Date endTime) {
        Act result = null;
        Act overlap = queryService.getOverlappingEvent(startTime, endTime, areaRef, userRef);
        if (overlap == null) {
            result = createEvent(roster, areaRef, userRef, startTime, endTime);
        } else {
            IMObjectBean bean = service.getBean(overlap);
            Identity identity = DeputyHelper.getSynchronisationId(bean);
            if (hasSynchronised(identity)) {
                // event is linked to a different roster
                if (setError(bean, DeputyMessages.syncedShiftOverlapsRoster(startTime, endTime))) {
                    bean.save();
                }
            } else if (startTime.equals(overlap.getActivityStartTime())
                       && endTime.equals(overlap.getActivityEndTime())) {
                // match on all details, so link the event to the roster
                updateSyncId(bean, identity, roster);
                bean.save();
                result = overlap;
            } else {
                if (setError(bean, DeputyMessages.unsyncedShiftOverlapsRoster(startTime, endTime))) {
                    bean.save();
                }
            }
        }
        return result;
    }

    /**
     * Updates OpenVPMS with a Deputy roster with no employee that has not been previously synced.
     * <p/>
     * If an unsynced event exists with the same times, it will be linked to the roster, otherwise a new event will be
     * created
     *
     * @param roster    the roster
     * @param areaRef   the area reference
     * @param startTime the start time
     * @param endTime   the end time
     * @return the corresponding event, or {@code null} if no event was updated
     */
    private Act syncFromOpenRoster(Roster roster, Reference areaRef, Date startTime, Date endTime) {
        Act result = null;
        List<Act> overlaps = queryService.getMatchingOpenEvents(startTime, endTime, areaRef);
        if (overlaps.isEmpty()) {
            result = createEvent(roster, areaRef, null, startTime, endTime);
        } else {
            for (Act overlap : overlaps) {
                IMObjectBean bean = service.getBean(overlap);
                Identity identity = DeputyHelper.getSynchronisationId(bean);
                if (!hasSynchronised(identity)) {
                    if (startTime.equals(overlap.getActivityStartTime())
                        && endTime.equals(overlap.getActivityEndTime())) {
                        // match on all details, so link the event to the roster
                        updateSyncId(bean, identity, roster);
                        bean.save();
                        result = overlap;
                        break;
                    }
                }
            }
            if (result == null) {
                // cannot link the roster to an existing event, so create a new one
                result = createEvent(roster, areaRef, null, startTime, endTime);
            }
        }
        return result;
    }

    /**
     * Determines if an event has been synchronised previously.
     * <p/>
     * Any event may have the identity, but with no roster id assigned. This occurs if synchronisation fails and an
     * error was recorded.
     *
     * @param identity the synchronisation identifier from the event. May be {@code null}
     * @return {@code true} if the event has been synchronised previously
     */
    private boolean hasSynchronised(Identity identity) {
        return identity != null && identity.getIdentity() != null;
    }

    /**
     * Creates a synchronisation identity for a roster.
     *
     * @param roster the roster
     * @return a new identity
     */
    private ActIdentity createIdentity(Roster roster) {
        ActIdentity identity = createIdentity();
        updateSyncId(identity, roster);
        return identity;
    }

    /**
     * Updates a synchronisation identity to indicate its roster has been synchronised.
     *
     * @param identity the synchronisation identity to update. If {@code null}, one will be created
     * @param roster   the roster. The modified time is stored in the identity
     */
    private void updateSyncId(IMObjectBean bean, Identity identity, Roster roster) {
        if (identity == null) {
            identity = createIdentity(roster);
            bean.addValue("synchronisation", identity);
        } else {
            updateSyncId(identity, roster);
        }
    }

    /**
     * Updates a synchronisation identity to indicate its roster has been synchronised.
     *
     * @param identity the synchronisation identity to update
     * @param roster   the roster. The modified time is stored in the identity
     */
    private void updateSyncId(Identity identity, Roster roster) {
        IMObjectBean bean = service.getBean(identity);
        identity.setIdentity(Long.toString(roster.getId()));
        bean.setValue("modified", roster.getModified());
        bean.setValue("status", SYNC);
        bean.setValue("error", null);
    }

    /**
     * Sets an error due to an exception.
     *
     * @param event     the event
     * @param exception the exception
     */
    private void error(IMObjectBean event, Throwable exception) {
        String message = exception.getMessage();
        if (message == null) {
            message = exception.getClass().getName() + ": No reason given";
        }
        try {
            if (setError(event, message)) {
                event.save();
            }
        } catch (Throwable throwable) {
            log.error("Failed to update event=" + event.getObject().getId() + " with error=" + message, exception);
        }
    }

    /**
     * Clears any synchronisation error.
     *
     * @param event the event to update
     * @return {@code true} if the event was updated
     */
    private boolean clearError(IMObjectBean event) {
        return setError(event, (String) null);
    }

    /**
     * Sets or clears the synchronisation error.
     *
     * @param event   the event to update
     * @param message the error message
     * @return {@code true} if the event was updated
     */
    private boolean setError(IMObjectBean event, Message message) {
        return setError(event, message.toString());
    }

    /**
     * Sets or clears the synchronisation error.
     *
     * @param event   the event to update
     * @param message the error message, or {@code null} if there is no error
     * @return {@code true} if the event was updated
     */
    private boolean setError(IMObjectBean event, String message) {
        boolean updated = false;
        String status = null;
        ActIdentity identity = DeputyHelper.getSynchronisationId(event);
        if (identity == null) {
            identity = createIdentity();
            event.addValue("synchronisation", identity);
        }
        IMObjectBean bean = service.getBean(identity);
        if (message != null) {
            int maxLength = bean.getMaxLength("error");
            if (message.length() > maxLength) {
                message = message.substring(0, maxLength - 3) + "...";
            }
            status = ERROR;
        }
        if (!Objects.equals(status, bean.getString("status"))) {
            bean.setValue("status", status);
            updated = true;
        }
        if (!Objects.equals(message, bean.getString("error"))) {
            bean.setValue("error", message);
            updated = true;
        }
        return updated;
    }

    /**
     * Updates an event from a {@link Roster}.
     *
     * @param roster   the roster
     * @param bean     the event bean
     * @param identity the roster identity
     * @param areaRef  the area reference
     * @param userRef  the user reference
     * @param rosters  the rosters
     * @return {@code true} if the event was updated
     */
    private boolean updateEvent(Roster roster, IMObjectBean bean, Identity identity, Reference areaRef,
                                Reference userRef, Rosters rosters) {
        boolean updated = false;
        boolean error = false;
        Date startTime = fromUnixtimestamp(roster.getStartTime());
        Date endTime = fromUnixtimestamp(roster.getEndTime());
        if (areaRef == null) {
            error = true;
            String name = getOperationalUnitName(roster.getOperationalUnit());
            if (setError(bean, DeputyMessages.shiftMovedToUnmappedDeputyArea(name))) {
                updated = true;
            }
        } else {
            if (!Objects.equals(bean.getDate("startTime"), startTime)) {
                bean.setValue("startTime", startTime);
                updated = true;
            }
            if (!Objects.equals(bean.getDate("endTime"), endTime)) {
                bean.setValue("endTime", endTime);
                updated = true;
            }
            if (userRef == null && roster.getEmployee() != 0) {
                updated |= setEmployeeMappingError(bean, roster.getEmployee());
                error = true;
            } else if (!Objects.equals(userRef, bean.getTargetRef("user"))) {
                if (userRef == null) {
                    bean.removeValues("user");
                } else {
                    bean.setTarget("user", userRef);
                }
                updated = true;
            }

            if (!Objects.equals(areaRef, bean.getTargetRef("schedule"))) {
                Entity area = (Entity) service.get(areaRef);
                if (area == null) {
                    updated = setError(bean, DeputyMessages.failedToRetrieveRosterArea(areaRef));
                    error = true;
                } else {
                    Reference location = getLocation(area);
                    bean.setTarget("schedule", area);
                    bean.setTarget("location", location);
                }
            }
            if (!error) {
                updated |= clearError(bean);
                rosters.processed(roster);
            }
        }
        if (updated) {
            if (!error) {
                updateSyncId(identity, roster);
            }
            bean.save();
        }
        return updated;
    }

    /**
     * Returns the location for an area.
     *
     * @param area the area
     * @return the location reference, or {@code null} if none is found
     */
    private Reference getLocation(Entity area) {
        IMObjectBean bean = service.getBean(area);
        return bean.getTargetRef("location");
    }

    /**
     * Creates an empty identity.
     *
     * @return a new identity
     */
    private ActIdentity createIdentity() {
        return service.create(Archetypes.EVENT_ID, ActIdentity.class);
    }

    /**
     * Returns an operational unit name given its id.
     *
     * @param id the employee id
     * @return the employee name
     */
    private String getOperationalUnitName(long id) {
        String name = null;
        try {
            OperationalUnit unit = deputy.getOperationalUnit(id);
            if (unit != null) {
                name = unit.getName();
            }
        } catch (Throwable exception) {
            log.error("Failed to retrieve OperationalUnit with identifier=" + id + ": " + exception.getMessage(),
                      exception);
        }
        if (name == null) {
            name = Long.toString(id);
        }
        return name;
    }

    /**
     * Returns an employee name given the employee id.
     *
     * @param id the employee id
     * @return the employee name
     */
    private String getEmployeeName(long id) {
        String name = null;
        try {
            Employee employee = deputy.getEmployee(id);
            if (employee != null) {
                name = employee.getName();
            }
        } catch (Throwable exception) {
            log.error("Failed to retrieve employee with identifier=" + id + ": " + exception.getMessage(),
                      exception);
        }
        if (name == null) {
            name = Long.toString(id);
        }
        return name;
    }

    /**
     * Creates a roster in Deputy.
     * <p/>
     * If the event cannot be created, its status will be set to ERROR.
     *
     * @param bean     the event bean
     * @param area     the Deputy area
     * @param employee the Deputy employee. May be {@code null}
     * @param rosters  the Deputy rosters
     * @param identity the existing synchronisation identity. May be {@code null}
     * @return the roster, if one was created, otherwise {@code null}
     */
    private Roster createRoster(IMObjectBean bean, Target area, Target employee, Rosters rosters, Identity identity) {
        Roster result = null;
        RosterData data = new RosterData();
        data.setStartTime(toUnixtimestamp(bean.getDate("startTime")));
        data.setEndTime(toUnixtimestamp(bean.getDate("endTime")));
        data.setOperationalUnit(getTargetId(area));
        data.setEmployee(getTargetId(employee));
        data.setForceOverwrite(1);
        try {
            Roster created = deputy.roster(data);
            updateSyncId(bean, identity, created);
            bean.save();
            rosters.processed(created);
            result = created;
        } catch (Throwable exception) {
            log.error("Failed to create roster in Deputy: " + exception.getMessage(), exception);
            error(bean, exception);
        }
        return result;
    }

    /**
     * Creates an event from a roster.
     * <p/>
     * This should only be called if both the Deputy area and employee are mapped, or the employee is
     * {@code 0} indicating an open shift.
     *
     * @param roster    the roster
     * @param areaRef   the area reference
     * @param userRef   the user reference. May be {@code null} indicating an open shift.
     * @param startTime the event start time
     * @param endTime   the event end time
     * @return the new event, or {@code null} if it couldn't be created
     */
    private Act createEvent(Roster roster, Reference areaRef, Reference userRef, Date startTime, Date endTime) {
        Act result = null;
        Act act = service.create(Archetypes.ROSTER_EVENT, Act.class);
        IMObjectBean bean = service.getBean(act);

        Entity area = (Entity) service.get(areaRef);
        if (area != null) {
            ActIdentity identity = createIdentity(roster);
            act.addIdentity(identity);
            bean.setValue("startTime", startTime);
            bean.setValue("endTime", endTime);
            bean.setTarget("schedule", areaRef);
            bean.setTarget("user", userRef);

            IMObjectBean areaBean = service.getBean(area);
            Reference location = areaBean.getTargetRef("location");
            if (location != null) {
                bean.setTarget("location", location);
                bean.save();
                result = act;
            } else {
                // shouldn't occur
                log.error("Can't synchronise roster=" + roster.getId()
                          + ": can't determine location with area: " + area.getObjectReference());
            }
        } else {
            // shouldn't occur
            log.error("Can't synchronise roster=" + roster.getId() + ": failed to retrieve area: " + areaRef);
        }
        return result;
    }

    /**
     * Records an error on the event when an employee is not mapped.
     *
     * @param bean     the event bean
     * @param employee the employee id
     * @return {@code true} if the event was updated
     */
    private boolean setEmployeeMappingError(IMObjectBean bean, long employee) {
        String name = getEmployeeName(employee);
        return setError(bean, DeputyMessages.noMappingForEmployee(name));
    }

    /**
     * Updates a roster or event, if required.
     * <p/>
     * If the roster corresponding to the identity no longer exists, the event will be deleted.<br/>
     * If the event is different to the roster and:
     * <ul>
     * <li>Deputy has a newer instance, then the event will be updated</li>
     * <li>OpenVPMS has a changed instance, then Deputy will be updated</li>
     * </ul>
     * If the roster cannot be updated, the event status will be set to ERROR.
     *
     * @param bean     the event bean
     * @param area     the Deputy area
     * @param userRef  the user reference. May be {@code null}
     * @param employee the Deputy employee. May be {@code null}
     * @param identity the Deputy identity for the roster
     * @param rosters  the cache of Deputy rosters
     * @return the roster, or {@code null} if the event was deleted or the roster could not be created
     */
    private Roster updateIfChanged(IMObjectBean bean, Target area, Reference userRef, Target employee,
                                   Identity identity, Rosters rosters) {
        Roster result = null;
        long id = DeputyHelper.getId(identity);
        Roster current = (id != -1) ? rosters.get(id) : null;
        if (current == null) {
            // event no longer exists in Deputy
            log.info("Removing event=" + bean.getObject().getId() + " linked to roster=" + id
                     + " as roster no longer exists");
            service.remove(bean.getObject());
        } else {
            if (deputyIsNewer(current, identity)) {
                // treat Deputy as the master, as OpenVPMS doesn't have a modified timestamp to determine if it is
                // newer than Deputy
                userRef = getReferenceForDeputyId(users, current.getEmployee());
                Reference areaRef = getReferenceForDeputyId(areas, current.getOperationalUnit());
                updateEvent(current, bean, identity, areaRef, userRef, rosters);
                rosters.processed(current);
                result = current;
            } else {
                if (userRef != null && employee == null) {
                    // user is not mapped, so can't synchronise
                    if (setError(bean, DeputyMessages.noMappingForUser(queryService.getName(userRef)))) {
                        bean.save();
                    }
                    rosters.processed(current); // don't want to re-process later
                } else {
                    RosterData data = createRosterData(current.getId(), bean, area, employee);
                    if (isDifferent(current, data)) {
                        result = updateRoster(data, bean, identity, rosters);
                    } else {
                        rosters.processed(current);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Determines if a roster from Deputy is newer than what was last synced.
     *
     * @param current  the current roster from Deputy
     * @param identity the synchronisation identity
     * @return {@code true} if the current instance is newer
     */
    private boolean deputyIsNewer(Roster current, Identity identity) {
        IMObjectBean bean = service.getBean(identity);
        Date remote = current.getModified();
        Date local = bean.getDate("modified");
        if (remote == null) {
            return false;
        }
        return local == null || local.compareTo(remote) < 0;
    }

    /**
     * Creates a {@link RosterData}.
     *
     * @param id   the roster id
     * @param bean the event bean
     * @param area the roster area
     * @param user the roster user
     * @return a new {@link RosterData}
     */
    private RosterData createRosterData(long id, IMObjectBean bean, Target area, Target user) {
        long startTime = toUnixtimestamp(bean.getDate("startTime"));
        long endTime = toUnixtimestamp(bean.getDate("endTime"));
        long operationalUnit = getTargetId(area);
        long employee = getTargetId(user);
        RosterData event = new RosterData();
        event.setId(id);
        event.setStartTime(startTime);
        event.setEndTime(endTime);
        event.setOperationalUnit(operationalUnit);
        event.setEmployee(employee);
        return event;
    }

    /**
     * Updates a roster in Deputy.
     *
     * @param data     the data to send
     * @param bean     the event bean
     * @param identity the roster identity
     * @param rosters  the rosters
     * @return the updated roster, or {@code null} if it couldn't be updated
     */
    private Roster updateRoster(RosterData data, IMObjectBean bean, Identity identity, Rosters rosters) {
        Roster result = null;
        try {
            Roster updated = deputy.roster(data);
            updateSyncId(identity, updated);
            bean.save();
            rosters.processed(updated);
            result = updated;
        } catch (Throwable exception) {
            log.error("Failed to update roster in Deputy: " + exception.getMessage(), exception);
            error(bean, exception);
        }
        return result;
    }

    /**
     * Determines if the existing roster is different to that specified.
     *
     * @param existing the existing roster
     * @param data     the roster data
     * @return {@code true} if the existing roster is different to the specified data
     */
    private boolean isDifferent(Roster existing, RosterData data) {
        boolean result = false;
        if (existing.getStartTime() != data.getStartTime() || existing.getEndTime() != data.getEndTime()
            || existing.getOperationalUnit() != data.getOperationalUnit()
            || existing.getEmployee() != data.getEmployee()) {
            result = true;
        }
        return result;
    }

    /**
     * Returns a roster given its id.
     *
     * @param id the roster identifier
     * @return the corresponding roster, or {@code null} if it cannot be found
     */
    private Roster getRoster(long id) {
        Roster result = null;
        try {
            result = deputy.getRoster(id);
        } catch (NotFoundException exception) {
            log.info("Roster not found: " + id);

        } catch (Throwable exception) {
            log.info("Failed to get roster=" + id + ": " + exception.getMessage(), exception);
        }
        return result;
    }

    /**
     * Returns the reference to an OpenVPMS object corresponding to a Deputy identifier.
     *
     * @param mappings the mappings
     * @param id       the Deputy identifier. A value of 0 indicates {@code null}.
     * @return the corresponding object reference, or {@code null} if no mapping exists
     */
    private Reference getReferenceForDeputyId(Mappings mappings, long id) {
        return (id != 0) ? mappings.getSource(Long.toString(id)) : null;
    }

}
