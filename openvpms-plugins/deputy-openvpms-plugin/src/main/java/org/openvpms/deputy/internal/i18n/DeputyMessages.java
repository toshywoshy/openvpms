/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;
import org.openvpms.component.model.object.Reference;

import java.util.Date;

/**
 * Deputy messages.
 *
 * @author Tim Anderson
 */
public class DeputyMessages {

    /**
     * The messages.
     */
    private static final Messages messages = new Messages("DEPUTY", DeputyMessages.class);

    /**
     * Returns a message indicating that Deputy is not configured.
     *
     * @return the message
     */
    public static Message notConfigured() {
        return messages.create(1);
    }

    /**
     * Returns a message indicating that there is no mapping for an OpenVPMS user.
     *
     * @param name the user name
     * @return the message
     */
    public static Message noMappingForUser(String name) {
        return messages.create(50, name);
    }


    /**
     * Returns a message indicating that there is no mapping for a Deputy employee.
     *
     * @param name the employee name
     * @return the message
     */
    public static Message noMappingForEmployee(String name) {
        return messages.create(51, name);
    }

    /**
     * Returns a message indicating that an existing shift has moved to an unmapped area in Deputy.
     *
     * @param name the area name
     * @return the message
     */
    public static Message shiftMovedToUnmappedDeputyArea(String name) {
        return messages.create(52, name);
    }

    /**
     * Returns a message indicating that an existing shift has moved to an unmapped area in OpenVPMS.
     *
     * @param name the area name
     * @return the message
     */
    public static Message shiftMovedToUnmappedOpenVPMSArea(String name) {
        return messages.create(53, name);
    }

    /**
     * Returns a message indicating that a roster area could not be retrieved.
     *
     * @param reference the area reference
     * @return the message
     */
    public static Message failedToRetrieveRosterArea(Reference reference) {
        return messages.create(54, reference.toString());
    }

    /**
     * Returns a message indicating that a synchronised shift overlaps another roster.
     *
     * @param startTime the roster start time
     * @param endTime   the roster end time
     * @return the message
     */
    public static Message syncedShiftOverlapsRoster(Date startTime, Date endTime) {
        return messages.create(55, startTime, endTime);
    }

    /**
     * Returns a message indicating that a unsynchronised shift overlaps another roster.
     *
     * @param startTime the roster start time
     * @param endTime   the roster end time
     * @return the message
     */
    public static Message unsyncedShiftOverlapsRoster(Date startTime, Date endTime) {
        return messages.create(56, startTime, endTime);
    }

}
