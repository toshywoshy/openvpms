/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.deputy.internal.service;

import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.deputy.internal.api.Deputy;
import org.openvpms.deputy.internal.model.query.Query;
import org.openvpms.deputy.internal.model.roster.Roster;
import org.openvpms.mapping.model.Mapping;
import org.openvpms.mapping.model.Mappings;
import org.openvpms.mapping.model.Target;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Manages synchronisation of rosters between OpenVPMS and Deputy.
 *
 * @author Tim Anderson
 */
class SynchronisationManager implements Runnable {

    /**
     * The number of days to synchronise.
     */
    private final int days;

    /**
     * The area mappings.
     */
    private final Mappings<Entity> areas;

    /**
     * The Deputy service.
     */
    private final Deputy deputy;

    /**
     * The query service.
     */
    private final QueryService queryService;

    /**
     * Maximum number of rosters to retrieve in a single call.
     */
    private final int maxResults;

    /**
     * The roster synchroniser.
     */
    private final RosterSynchroniser rosterSynchroniser;

    /**
     * Flag to indicate that the synchroniser should stop.
     */
    private AtomicBoolean stop = new AtomicBoolean();

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(SynchronisationManager.class);

    /**
     * Constructs a {@link SynchronisationManager}.
     *
     * @param days         the number of days to synchronise
     * @param areas        the area mappings
     * @param users        the user mappings
     * @param deputy       the Deputy service
     * @param queryService the query service
     * @param service      the archetype service
     */
    SynchronisationManager(int days, Mappings<Entity> areas, Mappings<User> users, Deputy deputy,
                           QueryService queryService, ArchetypeService service) {
        this(days, areas, users, deputy, queryService, service, QueryHelper.MAX_RESULTS);
    }

    /**
     * Constructs a {@link SynchronisationManager}.
     *
     * @param days         the number of days to synchronise
     * @param areas        the area mappings
     * @param users        the user mappings
     * @param deputy       the Deputy service
     * @param queryService the query service
     * @param service      the archetype service
     */
    SynchronisationManager(int days, Mappings<Entity> areas, Mappings<User> users, Deputy deputy,
                           QueryService queryService, ArchetypeService service, int maxResults) {
        this.days = days;
        this.areas = areas;
        this.deputy = deputy;
        this.queryService = queryService;
        this.maxResults = maxResults;
        rosterSynchroniser = new RosterSynchroniser(areas, users, deputy, queryService, service);
    }

    /**
     * Runs synchronisation from the current date.
     */
    public void run() {
        LocalDate initial = LocalDate.now();
        run(initial);
    }

    /**
     * Runs synchronisation from an initial date.
     *
     * @param initial the date
     */
    void run(LocalDate initial) {
        long time = System.currentTimeMillis();
        int count = 0;
        List<Mapping> areasMappings = Collections.emptyList();
        try {
            LocalDate start = initial;
            LocalDate end = start.plusDays(1);
            ZoneId zone = TimeZone.getDefault().toZoneId();
            areasMappings = getActiveAreas();

            for (count = 0; count < days; ++count) {
                Date from = Date.from(start.atStartOfDay().atZone(zone).toInstant());
                Date to = Date.from(end.atStartOfDay().atZone(zone).toInstant());

                for (Mapping mapping : areasMappings) {
                    if (stopped()) {
                        break;
                    }
                    Rosters rosters = getDeputyRosters(mapping, from, to);
                    if (stopped()) {
                        break;
                    }
                    synchronise(mapping, from, to, rosters);
                }
                start = end;
                end = start.plusDays(1);
            }
        } catch (Throwable exception) {
            if (stopped()) {
                log.debug("Roster synchronisation terminated");
            } else {
                log.error("Roster synchronisation terminated due to error: " + exception.getMessage(), exception);
            }
        } finally {
            if (log.isInfoEnabled()) {
                long elapsed = System.currentTimeMillis() - time;
                Duration duration = Duration.ofMillis(elapsed);
                long hours = duration.toHours();
                long minutes = duration.minusHours(hours).toMinutes();
                long seconds = duration.minusHours(hours).minusMinutes(minutes).getSeconds();
                log.info("Synchronised " + areasMappings.size() + " areas for " + count + " days starting from "
                         + initial + " in " + hours + ":" + minutes + ":" + seconds);
            }
        }
    }

    /**
     * Flags synchronisation to stop.
     */
    void stop() {
        stop.set(true);
    }

    /**
     * Returns the active areas from the area mappings.
     * <p/>
     * This ignores if a target is inactive as that should be highlighted in the UI.
     *
     * @return the active areas
     */
    private List<Mapping> getActiveAreas() {
        List<Mapping> result = new ArrayList<>();
        for (Mapping mapping : areas.getMappings()) {
            Reference source = mapping.getSource();
            if (queryService.isActive(source)) {
                result.add(mapping);
            } else {
                log.warn("Excluding inactive area=" + source + " from synchronisation");
            }
        }
        return result;
    }

    /**
     * Determines if synchronisation has been flagged to stop.
     *
     * @return {@code true} if synchronisation should stop
     */
    private boolean stopped() {
        return stop.get() || Thread.currentThread().isInterrupted();
    }

    /**
     * Sends rosters between the specified date range to Deputy, if they haven't been uploaded previously.
     *
     * @param mapping the area mapping
     * @param start   the start of the date range
     * @param end     the end of the date range
     * @param rosters the cache of Deputy rosters for the date range
     */
    private void synchronise(Mapping mapping, Date start, Date end, Rosters rosters) {
        List<Act> events = queryService.getEvents(mapping.getSource(), start, end);

        // for each OpenVPMS roster event, update Deputy where applicable
        for (Act event : events) {
            if (stopped()) {
                break;
            }
            rosterSynchroniser.synchroniseFromEvent(event, rosters);
        }

        // for each unprocessed Deputy roster, update OpenVPMS
        if (!stopped()) {
            for (Roster roster : rosters.getUnprocessed()) {
                if (stopped()) {
                    break;
                }
                rosterSynchroniser.synchroniseFromRoster(roster, rosters);
            }
        }
    }

    /**
     * Returns all Deputy rosters in the specified date range for an area.
     *
     * @param mapping the area mapping
     * @param from    the start of the date range
     * @param to      the end of the date range
     * @return the rosters
     */
    private Rosters getDeputyRosters(Mapping mapping, Date from, Date to) {
        Query query = new Query();
        Target target = mapping.getTarget();
        query.addSearch("area", "OperationalUnit", "eq", Long.valueOf(target.getId()));
        query.addSearch("from", "Date", "ge", from);
        query.addSearch("to", "Date", "lt", to);
        query.orderBy("Id", true);

        List<Roster> matches = QueryHelper.query(query, maxResults, deputy::getRosters, stop);
        return new Rosters(matches, deputy);
    }

}
