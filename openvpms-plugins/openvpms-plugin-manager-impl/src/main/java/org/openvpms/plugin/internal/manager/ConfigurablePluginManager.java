/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.component.business.dao.im.plugin.PluginDAO;
import org.openvpms.plugin.manager.PluginManager;
import org.openvpms.plugin.manager.PluginManagerListener;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A {@link PluginManager} that enables/disables plugins based on the party.organisationPractice enablePlugins flag.
 * <p>
 * The plugin manager is started after Spring has fully initialised, to avoid deadlocks.
 *
 * @author Tim Anderson
 */
public class ConfigurablePluginManager implements PluginManager, DisposableBean,
        ApplicationListener<ContextRefreshedEvent> {

    /**
     * The plugin service provider;
     */
    private final HostComponentProvider provider;

    /**
     * The plugin DAO.
     */
    private final PluginDAO pluginDAO;

    /**
     * The servlet container context.
     */
    private final ServletContainerContext context;

    /**
     * The practice service.
     */
    private final PracticeService practiceService;

    /**
     * The listeners.
     */
    private final Set<PluginManagerListener> listeners = Collections.synchronizedSet(new HashSet<>());

    /**
     * The plugin manager.
     */
    private volatile PluginManager manager;

    /**
     * The logger.
     */
    private static final Logger log = LoggerFactory.getLogger(ConfigurablePluginManager.class);

    /**
     * Constructs a {@link ConfigurablePluginManager}.
     *
     * @param provider        the plugin service provider
     * @param pluginDAO       the plugin DAO
     * @param context         the servlet container context
     * @param practiceService the practice service
     */
    public ConfigurablePluginManager(HostComponentProvider provider, PluginDAO pluginDAO,
                                     ServletContainerContext context, PracticeService practiceService) {
        this.provider = provider;
        this.pluginDAO = pluginDAO;
        this.context = context;
        this.practiceService = practiceService;
    }

    /**
     * Returns the first service implementing the specified interface.
     *
     * @param type the interface
     * @return the first service implementing the interface, or {@code null} if none was found
     */
    @Override
    public <T> T getService(Class<T> type) {
        PluginManager current = manager;
        return (current != null) ? current.getService(type) : null;
    }

    /**
     * Returns all services implementing the specified interface.
     *
     * @param type the interface
     * @return the services implementing the interface
     */
    @Override
    public <T> List<T> getServices(Class<T> type) {
        PluginManager current = manager;
        return (current != null) ? current.getServices(type) : Collections.emptyList();
    }

    /**
     * Returns the bundle context, or {@code null} if the manager is not running.
     *
     * @return the bundle context. May be {@code null}
     */
    @Override
    public BundleContext getBundleContext() {
        PluginManager current = manager;
        return (current != null) ? current.getBundleContext() : null;
    }

    /**
     * Returns a list of all installed bundles.
     *
     * @return the installed bundles
     */
    @Override
    public Bundle[] getBundles() {
        PluginManager current = manager;
        return (current != null) ? current.getBundles() : new Bundle[0];
    }

    /**
     * Determines if the plugin manager is started.
     *
     * @return {@code true} if the plugin manager is started
     */
    @Override
    public boolean isStarted() {
        PluginManager current = manager;
        return current != null && current.isStarted();
    }

    /**
     * Starts the plugin manager.
     */
    @Override
    public synchronized void start() {
        if (manager == null && practiceService.pluginsEnabled()) {
            // NOTE that the manager needs to be assigned here before it has completed startup so that listeners can
            // access the BundleContext
            manager = new PluginManagerImpl(provider, pluginDAO, context, practiceService);
            for (PluginManagerListener listener : listeners) {
                manager.addListener(listener);
            }
            manager.start();
        }
    }

    /**
     * Stops the plugin manager.
     * <p>
     * This method will wait until the manager shuts down.
     */
    @Override
    public synchronized void stop() {
        if (manager != null) {
            manager.stop();
            manager = null;
        }
    }

    /**
     * Installs a plugin from a file.
     * <p>
     * The plugin manager must be started for this operation to be successful.
     *
     * @param file the file
     * @throws BundleException if the plugin cannot be installed
     */
    @Override
    public void install(File file) throws BundleException {
        PluginManager current = manager;
        if (current == null) {
            throw new BundleException("Plugin cannot be installed: PluginManager is not running",
                                      BundleException.INVALID_OPERATION);
        }
        current.install(file);
    }

    /**
     * Determines if a bundle can be uninstalled.
     *
     * @param bundle the bundle
     * @return {@code true} if the bundle is a plugin that can be uninstalled
     */
    @Override
    public boolean canUninstall(Bundle bundle) {
        PluginManager current = manager;
        return current != null && current.canUninstall(bundle);
    }

    /**
     * Uninstalls a bundle.
     * <p>
     * The plugin manager must be started for this operation to be successful.
     *
     * @param bundle the bundle
     * @throws BundleException if the bundle cannot be uninstalled
     */
    @Override
    public void uninstall(Bundle bundle) throws BundleException {
        PluginManager current = manager;
        if (current == null) {
            throw new BundleException("Plugin cannot be uninstalled: PluginManager is not running",
                                      BundleException.INVALID_OPERATION);
        }
        current.uninstall(bundle);
    }

    /**
     * Determines if a bundle can be restarted.
     *
     * @param bundle the bundle
     * @return {@code true} if the bundle is a plugin that can be restarted
     */
    @Override
    public boolean canRestart(Bundle bundle) {
        PluginManager current = manager;
        return current != null && current.canRestart(bundle);
    }

    /**
     * Start a bundle.
     *
     * @param bundle the bundle to start
     */
    @Override
    public void start(Bundle bundle) {
        PluginManager current = manager;
        if (current == null) {
            throw new IllegalStateException("Plugin cannot be started: PluginManager is not running");
        }
        current.start(bundle);
    }

    /**
     * Stop a bundle.
     *
     * @param bundle the bundle to stop
     */
    @Override
    public void stop(Bundle bundle) {
        PluginManager current = manager;
        if (current != null) {
            current.stop(bundle);
        }
    }

    /**
     * Scan for and deploy new plugins.
     *
     * @return the number of new plugins found
     */
    @Override
    public int scanForNewPlugins() {
        PluginManager current = manager;
        return current != null ? current.scanForNewPlugins() : 0;
    }

    /**
     * Adds a listener to be notified of plugin manager events.
     *
     * @param listener the listener to notify
     */
    @Override
    public void addListener(PluginManagerListener listener) {
        listeners.add(listener);
        PluginManager current = manager;
        if (current != null) {
            current.addListener(listener);
        }
    }

    /**
     * Removes a listener.
     *
     * @param listener the listener to remove
     */
    @Override
    public void removeListener(PluginManagerListener listener) {
        listeners.remove(listener);
        PluginManager current = manager;
        if (current != null) {
            current.removeListener(listener);
        }
    }

    /**
     * Handle an application event.
     *
     * @param event the event to respond to
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
            start();
        } catch (Throwable exception) {
            log.error("Failed to start the plugin manager", exception);
        }
    }

    /**
     * Invoked by a BeanFactory on destruction of a singleton.
     *
     * @throws Exception in case of shutdown errors.
     *                   Exceptions will get logged but not rethrown to allow
     *                   other beans to release their resources too.
     */
    @Override
    public void destroy() throws Exception {
        try {
            stop();
        } finally {
            listeners.clear();
        }
    }

}
