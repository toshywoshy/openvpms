/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.spring;

import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import java.util.List;
import java.util.Map;

/**
 * A {@code FactoryBean} for creating {@link SpringPluginServiceProvider} instances.
 *
 * @author Tim Anderson
 */
public class PluginServiceProviderFactoryBean extends AbstractFactoryBean {

    /**
     * The services.
     */
    private Map<String, List<String>> services = null;

    /**
     * Sets the service names to expose to plugins.
     *
     * @param services the services
     */
    public void setServices(Map<String, List<String>> services) {
        this.services = services;
    }

    /**
     * Return the type of object that this FactoryBean creates.
     *
     * @return {@code HostComponentProvider.class}
     */
    @Override
    public Class<?> getObjectType() {
        return HostComponentProvider.class;
    }

    /**
     * Creates an instance of {@link SpringPluginServiceProvider}.
     *
     * @return the object returned by this factory
     */
    @Override
    protected Object createInstance() {
        PluginServiceProviderConfig config = PluginServiceProviderConfig.getConfig(getBeanFactory());
        if (config != null) {
            if (services == null) {
                services = config.getServices();
            } else {
                services.putAll(config.getServices());
            }
        }
        return new SpringPluginServiceProvider(getBeanFactory(), services);
    }

}
