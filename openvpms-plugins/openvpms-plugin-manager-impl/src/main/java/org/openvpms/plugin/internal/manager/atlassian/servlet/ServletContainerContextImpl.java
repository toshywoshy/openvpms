/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager.atlassian.servlet;

import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.sal.api.web.context.HttpContext;
import org.openvpms.plugin.internal.manager.ServletContainerContext;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Map;

/**
 * Provides access to the servlet context and current request/response, for plugins.
 *
 * @author Tim Anderson
 */
public class ServletContainerContextImpl implements ServletContainerContext, ServletContextAware,
        ServletContextFactory, HttpContext {

    /**
     * The servlet context
     */
    private ServletContext servletContext;

    /**
     * Constructs a {@link ServletContainerContextImpl}.
     */
    public ServletContainerContextImpl() {
        super();
    }

    /**
     * Set the {@link ServletContext} that this object runs in.
     * <p>Invoked after population of normal bean properties but before an init
     * callback like InitializingBean's {@code afterPropertiesSet} or a
     * custom init-method. Invoked after ApplicationContextAware's
     * {@code setApplicationContext}.
     *
     * @param servletContext ServletContext object to be used by this object
     */
    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Returns the servlet context.
     *
     * @return servlet context
     */
    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }

    /**
     * Returns the HTTP request for the current thread.
     *
     * @return the request. May be {@code null}
     */
    @Override
    public HttpServletRequest getRequest() {
        return ServletContextThreadLocal.getRequest();
    }

    /**
     * Returns the HTTP response for the current thread.
     *
     * @return the response. May be {@code null}
     */
    @Override
    public HttpServletResponse getResponse() {
        return ServletContextThreadLocal.getResponse();
    }

    /**
     * Returns the current {@code HttpSession} associated with this request or, if there is no
     * current session and {@code create} is true, returns a new session.
     *
     * @param create if {@code true} create a session if none exists
     * @return the session or {@code null} if there is no current request, or there is a request but no session and
     * {@code create == false}
     */
    @Override
    public HttpSession getSession(boolean create) {
        HttpServletRequest request = getRequest();
        return request != null ? request.getSession(create) : null;
    }

    /**
     * Returns a cache for the current request.
     *
     * @return the cache. May be {@code null}
     */
    @Override
    public Map<String, Object> getRequestCache() {
        return ServletContextThreadLocal.getRequestCache();
    }

    /**
     * Returns the current user's locale.
     *
     * @return the current user's locale, or the system default if there is none.
     */
    @Override
    public Locale getLocale() {
        HttpServletRequest request = getRequest();
        return (request != null) ? request.getLocale() : Locale.getDefault();
    }

}
