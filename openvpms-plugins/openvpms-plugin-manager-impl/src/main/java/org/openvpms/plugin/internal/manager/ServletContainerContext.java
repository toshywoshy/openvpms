/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.internal.manager;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;

/**
 * Servlet container context.
 *
 * @author Tim Anderson
 */
public interface ServletContainerContext {

    /**
     * Returns the servlet context.
     *
     * @return servlet context
     */
    ServletContext getServletContext();

    /**
     * Returns the HTTP request for the current thread.
     *
     * @return the request. May be {@code null}
     */
    HttpServletRequest getRequest();

    /**
     * Returns a cache for the current request.
     *
     * @return the cache. May be {@code null}
     */
    Map<String, Object> getRequestCache();

    /**
     * Returns the current user's locale.
     *
     * @return the current user's locale, or the system default if there is none.
     */
    Locale getLocale();

}
