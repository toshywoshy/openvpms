<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>org.openvpms</groupId>
        <artifactId>openvpms-plugins</artifactId>
        <version>2.3.0.8-SNAPSHOT</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <artifactId>openvpms-plugin-framework-bundles</artifactId>

    <description>Packaging of bundles for OpenVPMS plugin framework</description>
    <packaging>pom</packaging>

    <dependencies>
        <dependency>
            <groupId>org.apache.felix</groupId>
            <artifactId>org.apache.felix.scr</artifactId>
            <version>${felix.scr.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bundles</groupId>
            <artifactId>velocity</artifactId>
            <version>1.6.2-1</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <!-- ================================================================================= -->
            <!-- WARNING - the following packages different versions of jersey for plugin use      -->
            <!-- ================================================================================= -->
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-module</artifactId>
            <version>6.1.2</version>
            <exclusions>
                <exclusion>
                    <!-- to keep maven-enforcer-plugin quiet -->
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-annotations</artifactId>
                </exclusion>
                <exclusion>
                    <!-- to keep maven-enforcer-plugin quiet -->
                    <groupId>javax.ws.rs</groupId>
                    <artifactId>jsr311-api</artifactId>
                </exclusion>
            </exclusions>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.httpclient</groupId>
            <artifactId>atlassian-httpclient-plugin</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.templaterenderer</groupId>
            <artifactId>atlassian-template-renderer-api</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.templaterenderer</groupId>
            <artifactId>atlassian-template-renderer-velocity16-plugin</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-framework-bundles</artifactId>
            <version>${atlassian.plugins.version}</version>
            <type>zip</type>
            <classifier>spring50x</classifier>
        </dependency>
        <dependency>
            <groupId>org.openvpms</groupId>
            <artifactId>openvpms-auth-plugin</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptors>
                        <descriptor>src/main/assembly/assembly.xml</descriptor>
                    </descriptors>
                    <appendAssemblyId>false</appendAssemblyId>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
