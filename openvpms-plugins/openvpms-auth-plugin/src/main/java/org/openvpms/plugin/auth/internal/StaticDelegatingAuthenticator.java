/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.plugin.auth.internal;

import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.config.SecurityConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Map;

/**
 * Lifted from the Atlassian Reference Application.
 * <p/>
 * TODO - this uses a static for serap-config.xml. See if it can be removed.
 *
 * @author Tim Anderson
 */
public class StaticDelegatingAuthenticator implements Authenticator {

    private static Authenticator authenticator;

    public void init(Map<String, String> params, SecurityConfig securityConfig) {
        authenticator.init(params, securityConfig);
    }

    public String getRemoteUser(HttpServletRequest request) {
        return authenticator.getRemoteUser(request);
    }

    public Principal getUser(HttpServletRequest request, HttpServletResponse response) {
        return authenticator.getUser(request, response);
    }

    public Principal getUser(HttpServletRequest request) {
        return authenticator.getUser(request);
    }

    public boolean login(HttpServletRequest request, HttpServletResponse response, String username, String password, boolean cookie)
            throws AuthenticatorException {
        return authenticator.login(request, response, username, password, cookie);
    }

    public boolean login(HttpServletRequest request, HttpServletResponse response, String username, String password) throws AuthenticatorException {
        return authenticator.login(request, response, username, password);
    }

    public boolean logout(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticatorException {
        return authenticator.logout(request, response);
    }

    public void destroy() {
        authenticator.destroy();
    }

    static void setAuthenticator(Authenticator authenticator) {
        StaticDelegatingAuthenticator.authenticator = authenticator;
    }

}
