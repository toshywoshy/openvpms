/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

/**
 * EFTPOS transaction display, performed by the the EFTPOS provider in the browser.
 * <p/>
 * The provider generates a URL for the transaction which will be displayed in an <em>iframe</em>.
 *
 * @author Tim Anderson
 */
public interface WebTransactionDisplay extends TransactionDisplay {

    /**
     * Returns the transaction display URL.
     *
     * @return the transaction display URL
     */
    String getUrl();

    /**
     * Returns the window width.
     *
     * @return the window width, in pixels
     */
    int getWidth();

    /**
     * Returns the window height.
     *
     * @return the window height, in pixels
     */
    int getHeight();

}
