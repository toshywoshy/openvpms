/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.transaction;

import org.openvpms.eftpos.transaction.Transaction.Status;

/**
 * Updates the state of a {@link Transaction}.
 *
 * @author Tim Anderson
 */
public interface TransactionUpdater {

    /**
     * Sets the status.
     *
     * @param status the status
     * @return this
     */
    TransactionUpdater status(Status status);

    /**
     * Sets the status and message.
     *
     * @param status  the status
     * @param message the status message. May be {@code null}
     * @return this
     */
    TransactionUpdater status(Status status, String message);

    /**
     * Sets the transaction identifier, issued by the provider.
     * <p>
     * A transaction can have a single identifier issued by a provider. To avoid duplicates, each EFTPOS service must
     * provide a unique archetype.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.EFTPOSTransaction</em> prefix.
     * @param id        the transaction identifier
     * @return this
     */
    TransactionUpdater transactionId(String archetype, String id);

    /**
     * Sets the card type.
     *
     * @param cardType the card type. May be {@code null}
     * @return this
     */
    TransactionUpdater cardType(String cardType);

    /**
     * Sets the authorisation code.
     *
     * @param authorisationCode the authorisation code. May be {@code null}
     * @return this
     */
    TransactionUpdater authorisationCode(String authorisationCode);

    /**
     * Sets the response code and message.
     * <p/>
     * The code is issued by the provider and indicates the reason for the success or failure of a transaction.
     * <p/>
     * The text is a human readable interpretation of the code.
     *
     * @param code the response code. May be {@code null}
     * @param text the response text. May be {@code null}
     * @return this
     */
    TransactionUpdater response(String code, String text);

    /**
     * Sets the masked card number.
     * <p/>
     * This is the card number with most digits elided for security.
     * <p/>
     * Also known as the elided PAN (primary account number).
     *
     * @param maskedCardNumber the masked card number
     * @return this
     */
    TransactionUpdater maskedCardNumber(String maskedCardNumber);

    /**
     * Sets the retrieval reference number (RRN).
     *
     * @param retrievalReferenceNumber the retrieval reference number. May be {@code null}
     * @return this
     */
    TransactionUpdater retrievalReferenceNumber(String retrievalReferenceNumber);

    /**
     * Sets the system trace audit number (STAN).
     *
     * @param systemTraceAuditNumber the system trace audit number. May be {@code null}
     * @return this
     */
    TransactionUpdater systemTraceAuditNumber(String systemTraceAuditNumber);

    /**
     * Adds a merchant receipt.
     *
     * @param receipt           the merchant receipt. May be {@code null}
     * @param signatureRequired if {@code true} a signature is required
     * @return this
     */
    TransactionUpdater addMerchantReceipt(String receipt, boolean signatureRequired);

    /**
     * Adds a customer receipt.
     *
     * @param receipt the customer receipt. May be {@code null}
     * @return this
     */
    TransactionUpdater addCustomerReceipt(String receipt);

    /**
     * Commits any changes.
     */
    void update();
}
