/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.service;

import org.openvpms.eftpos.exception.EFTPOSException;

import java.util.List;
import java.util.Map;

/**
 * Terminal registration, managed by OpenVPMS.
 * <p/>
 * This prompts for one or more fields, that are submitted on completion.
 *
 * @author Tim Anderson
 */
public interface ManagedTerminalRegistrar extends TerminalRegistrar {

    class Field {

        private final String name;

        private final String displayName;

        private final Class<?> type;

        private final boolean password;

        private final String description;

        private Field(String name, String displayName, Class<?> type, boolean password, String description) {
            this.name = name;
            this.displayName = displayName;
            this.type = type;
            this.password = password;
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public Class<?> getType() {
            return type;
        }

        public boolean isPassword() {
            return password;
        }

        public String getDescription() {
            return description;
        }

        public static Field string(String name, String displayName) {
            return string(name, displayName, null);
        }

        public static Field string(String name, String displayName, String description) {
            return new Field(name, displayName, String.class, false, description);
        }

        public static Field password(String name, String displayName) {
            return password(name, displayName, null);
        }

        public static Field password(String name, String displayName, String description) {
            return new Field(name, displayName, String.class, true, description);
        }

        public static Field number(String name, String displayName) {
            return number(name, displayName, null);
        }

        public static Field number(String name, String displayName, String description) {
            return new Field(name, displayName, Long.class, false, description);
        }
    }

    /**
     * Returns a prompt to display to the user.
     *
     * @return a prompt. May be {@code null}
     * @throws EFTPOSException for any error
     */
    String getMessage();

    /**
     * Returns the fields.
     *
     * @return the fields
     * @throws EFTPOSException for any error
     */
    List<Field> getFields();

    /**
     * Registers the terminal.
     *
     * @param values the field values, keyed on name
     * @throws EFTPOSException for any error
     */
    void register(Map<String, Object> values);
}
