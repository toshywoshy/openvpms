/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.event;

import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.eftpos.transaction.Receipt;
import org.openvpms.eftpos.transaction.Transaction;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Default implementation of {@link EventMonitor}.
 *
 * @author Tim Anderson
 */
public class EFTPOSEventMonitor implements EventMonitor {

    /**
     * The terminal listeners.
     */
    private final Map<Terminal, List<WeakReference<Consumer<TerminalStatus>>>> terminals;

    /**
     * The transaction listeners.
     */
    private final Map<Transaction, List<WeakReference<Consumer<Transaction>>>> transactions;

    /**
     * The print listeners.
     */
    private final Map<Transaction, List<WeakReference<Consumer<Receipt>>>> print;

    /**
     * Constructs an {@link EFTPOSEventMonitor}.
     */
    public EFTPOSEventMonitor() {
        terminals = new HashMap<>();
        transactions = new HashMap<>();
        print = new HashMap<>();
    }

    /**
     * Adds a listener for updates to a terminal.
     *
     * @param terminal the terminal
     * @param listener the listener
     */
    public synchronized void addTerminalListener(Terminal terminal, Consumer<TerminalStatus> listener) {
        addListener(terminal, listener, terminals);
    }

    /**
     * Removes a listener from receiving updates to a terminal.
     *
     * @param terminal the terminal
     * @param listener the listener
     */
    public synchronized void removeTerminalListener(Terminal terminal, Consumer<TerminalStatus> listener) {
        removeListener(terminal, listener, terminals);
    }

    /**
     * Adds a listener for updates to a transaction.
     *
     * @param transaction the transaction
     * @param listener    the listener
     */
    public synchronized void addTransactionListener(Transaction transaction, Consumer<Transaction> listener) {
        addListener(transaction, listener, transactions);
    }

    /**
     * Removes a listener from receiving updates to a transaction.
     *
     * @param transaction the transaction
     * @param listener    the listener
     */
    public synchronized void removeTransactionListener(Transaction transaction, Consumer<Transaction> listener) {
        removeListener(transaction, listener, transactions);
    }

    /**
     * Adds a listener to receive merchant receipt print notifications.
     *
     * @param transaction the transaction
     * @param listener    the listener
     */
    public synchronized void addPrintListener(Transaction transaction, Consumer<Receipt> listener) {
        addListener(transaction, listener, print);
    }

    /**
     * Removes a listener from receiving print notifications.
     *
     * @param transaction the transaction
     * @param listener    the listener
     */
    public synchronized void removePrintListener(Transaction transaction, Consumer<Receipt> listener) {
        removeListener(transaction, listener, print);
    }

    /**
     * Notifies that the status of an EFTPOS terminal has been updated.
     *
     * @param terminal the terminal
     * @param status   the terminal status
     */
    @Override
    public void terminalUpdated(Terminal terminal, TerminalStatus status) {
        notifyAll(terminal, status, terminals);
    }

    /**
     * Indicates that a merchant receipt needs to printed for signature verification purposes.
     * <p/>
     * This should be invoked when the terminal does not support printing.
     *
     * @param transaction the transaction the receipt is for
     * @param receipt     the receipt
     */
    @Override
    public void printMerchantReceipt(Transaction transaction, Receipt receipt) {
        notifyAll(transaction, receipt, print);
    }

    /**
     * Invoked when a transaction is updated.
     *
     * @param transaction the transaction
     */
    @Override
    public void completed(Transaction transaction) {
        notifyAll(transaction, transaction, transactions);
    }

    private <K, T> void notifyAll(K key, T value, Map<K, List<WeakReference<Consumer<T>>>> listeners) {
        List<Consumer<T>> matches = getListeners(key, listeners);
        for (Consumer<T> listener : matches) {
            listener.accept(value);
        }
    }

    private <K, T> void addListener(K key, Consumer<T> listener, Map<K, List<WeakReference<Consumer<T>>>> listeners) {
        List<WeakReference<Consumer<T>>> list
                = listeners.computeIfAbsent(key, k -> new ArrayList<>());
        list.add(new WeakReference<>(listener));
    }

    private <K, T> void removeListener(K key, Consumer<T> listener, Map<K, List<WeakReference<Consumer<T>>>> listeners) {
        List<WeakReference<Consumer<T>>> list = listeners.get(key);
        if (list != null) {
            ListIterator<WeakReference<Consumer<T>>> iterator = list.listIterator();
            while (iterator.hasNext()) {
                WeakReference<Consumer<T>> next = iterator.next();
                Consumer<T> consumer = next.get();
                if (consumer == null || consumer.equals(listener)) {
                    iterator.remove();
                }
            }
            if (list.isEmpty()) {
                listeners.remove(key);
            }
        }
    }

    private synchronized <K, T> List<Consumer<T>> getListeners(K key, Map<K, List<WeakReference<Consumer<T>>>> listeners) {
        List<Consumer<T>> matches = new ArrayList<>();
        List<WeakReference<Consumer<T>>> list = listeners.get(key);
        if (list != null) {
            ListIterator<WeakReference<Consumer<T>>> iterator = list.listIterator();
            while (iterator.hasNext()) {
                WeakReference<Consumer<T>> next = iterator.next();
                Consumer<T> listener = next.get();
                if (listener == null) {
                    iterator.remove();
                } else {
                    matches.add(listener);
                }
            }
            if (list.isEmpty()) {
                listeners.remove(key);
            }
        }
        return matches;
    }
}
