/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.eftpos.internal.terminal;

import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.business.service.archetype.helper.TypeHelper;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.eftpos.internal.event.EventMonitor;
import org.openvpms.eftpos.terminal.Terminal;
import org.openvpms.eftpos.terminal.TerminalStatus;
import org.openvpms.eftpos.terminal.Terminals;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of {@link Terminals}.
 *
 * @author Tim Anderson
 */
public class TerminalsImpl implements Terminals {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The domain service.
     */
    private final DomainService domainService;

    /**
     * The event monitor.
     */
    private final EventMonitor monitor;

    /**
     * Constructs a {@link TerminalsImpl}.
     *
     * @param monitor the event monitor
     */
    public TerminalsImpl(ArchetypeService service, DomainService domainService, EventMonitor monitor) {
        this.service = service;
        this.domainService = domainService;
        this.monitor = monitor;
    }

    /**
     * Returns a terminal given its reference.
     *
     * @param reference the terminal reference
     * @return the corresponding terminal or {@code null} if none is found. The returned terminal may be inactive
     */
    @Override
    public Terminal getTerminal(Reference reference) {
        return domainService.get(reference, Terminal.class);
    }

    /**
     * Returns all terminals with the specified archetype.
     *
     * @param archetype  the terminal archetype. Must be an <em>entity.eftposTerminal*</em>
     * @param activeOnly if {@code true}, only return active terminals
     * @return the terminals
     */
    @Override
    public List<Terminal> getTerminals(String archetype, boolean activeOnly) {
        List<Terminal> result = new ArrayList<>();
        if (!TypeHelper.matches(archetype, EFTPOSArchetypes.TERMINALS)) {
            throw new IllegalStateException("Invalid terminal archetype: " + archetype);
        }
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Entity> query = builder.createQuery(Entity.class);
        Root<Entity> root = query.from(Entity.class, archetype);
        if (activeOnly) {
            query.where(builder.equal(root.get("active"), true));
        }
        query.orderBy(builder.asc(root.get("id")));
        for (Entity entity : service.createQuery(query).getResultList()) {
            result.add(domainService.create(entity, Terminal.class));
        }
        return result;
    }

    /**
     * Notifies that the status of an EFTPOS terminal has been updated.
     * <p/>
     * This should be invoked whenever a terminal is registered, or when registration fails.
     *
     * @param terminal the terminal
     * @param status   the terminal status
     */
    @Override
    public void terminalUpdated(Terminal terminal, TerminalStatus status) {
        monitor.terminalUpdated(terminal, status);
    }
}
