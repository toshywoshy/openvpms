package org.openvpms.component.security.crypto;

/**
 * Service interface for the symmetric encryption of passwords.
 *
 * @author Tim Anderson
 */
public interface PasswordEncryptor {

    /**
     * Encrypts a password.
     *
     * @param password the password
     */
    String encrypt(String password);

    /**
     * Decrypts a password.
     *
     * @param encryptedPassword the encrypted password
     */
    String decrypt(String encryptedPassword);
}
