/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.math;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.openvpms.component.math.Weight.ONE_KILO_IN_POUNDS;
import static org.openvpms.component.math.Weight.ONE_POUND_IN_GRAMS;
import static org.openvpms.component.math.Weight.ONE_POUND_IN_KILOS;
import static org.openvpms.component.math.Weight.ONE_THOUSAND;
import static org.openvpms.component.math.Weight.convert;
import static org.openvpms.component.math.WeightUnits.GRAMS;
import static org.openvpms.component.math.WeightUnits.KILOGRAMS;
import static org.openvpms.component.math.WeightUnits.POUNDS;

/**
 * Tests the {@link Weight} class.
 *
 * @author Tim Anderson
 */
public class WeightTestCase {

    /**
     * Tests the {@link Weight#convert(WeightUnits)} and {@link Weight#toKilograms()} methods.
     */
    @Test
    public void testConvert() {
        Weight kg = new Weight(BigDecimal.TEN, KILOGRAMS);

        checkEquals(BigDecimal.TEN, kg.toKilograms());
        checkEquals(BigDecimal.TEN, kg.convert(KILOGRAMS));
        checkEquals(new BigDecimal("10000"), kg.convert(WeightUnits.GRAMS));
        checkEquals(new BigDecimal("22.04622622"), kg.convert(WeightUnits.POUNDS));

        Weight g = new Weight(BigDecimal.TEN, WeightUnits.GRAMS);
        checkEquals(new BigDecimal("0.01"), g.toKilograms());
        checkEquals(new BigDecimal("0.01"), g.convert(KILOGRAMS));
        checkEquals(BigDecimal.TEN, g.convert(WeightUnits.GRAMS));
        checkEquals(new BigDecimal("0.02204623"), g.convert(WeightUnits.POUNDS));

        Weight lb = new Weight(BigDecimal.TEN, WeightUnits.POUNDS);
        checkEquals(new BigDecimal("4.53592370"), lb.toKilograms());
        checkEquals(new BigDecimal("4.53592370"), lb.convert(KILOGRAMS));
        checkEquals(new BigDecimal("4535.9237"), lb.convert(WeightUnits.GRAMS));
        checkEquals(BigDecimal.TEN, lb.convert(WeightUnits.POUNDS));
    }

    /**
     * Tests the {@link Weight#isZero()} methods.
     */
    @Test
    public void testIsZero() {
        Assert.assertTrue(Weight.ZERO.isZero());
        Weight weight1 = new Weight(BigDecimal.TEN, WeightUnits.KILOGRAMS);
        Weight weight2 = new Weight(BigDecimal.ZERO, WeightUnits.KILOGRAMS);
        assertFalse(weight1.isZero());
        Assert.assertTrue(weight2.isZero());
    }

    /**
     * Tests the {@link Weight#between(BigDecimal, BigDecimal, WeightUnits)} method.
     */
    @Test
    public void testBetween() {
        Weight kg = new Weight(BigDecimal.ONE, KILOGRAMS);
        assertTrue(kg.between(valueOf(0.5), valueOf(2), KILOGRAMS));
        assertFalse(kg.between(valueOf(0), valueOf(1), KILOGRAMS));
        assertTrue(kg.between(valueOf(1), valueOf(2), KILOGRAMS));

        assertTrue(kg.between(valueOf(500), valueOf(2000), GRAMS));
        assertFalse(kg.between(valueOf(0), valueOf(1000), GRAMS));
        Assert.assertTrue(kg.between(valueOf(1000), valueOf(2000), GRAMS));

        assertTrue(kg.between(valueOf(2), valueOf(3), POUNDS));
        assertFalse(kg.between(valueOf(0), valueOf(2.20462), POUNDS));
        assertTrue(kg.between(valueOf(2.20462), valueOf(3), POUNDS));
    }

    /**
     * Tests the {@link Weight#compareTo(Weight)} method.
     */
    @Test
    public void testCompareTo() {
        Weight kg = new Weight(BigDecimal.ONE, KILOGRAMS);
        assertEquals(0, kg.compareTo(kg));

        Weight lb = kg.to(POUNDS);
        assertEquals(POUNDS, lb.getUnits());
        assertEquals(0, kg.compareTo(lb));
        assertEquals(0, lb.compareTo(kg));

        Weight kg2 = new Weight(2, KILOGRAMS);
        assertEquals(-1, lb.compareTo(kg2));
        assertEquals(1, kg2.compareTo(lb));
    }

    /**
     * Tests the {@link Weight#convert(BigDecimal, WeightUnits, WeightUnits)} method.
     */
    @Test
    public void testConvertFromTo() {
        checkEquals(ZERO, convert(ZERO, KILOGRAMS, KILOGRAMS));
        checkEquals(ZERO, convert(ZERO, GRAMS, KILOGRAMS));
        checkEquals(ZERO, convert(ZERO, POUNDS, KILOGRAMS));

        // test Kg conversion
        checkEquals(ONE, convert(ONE, KILOGRAMS, KILOGRAMS));
        checkEquals(ONE_THOUSAND, convert(ONE, KILOGRAMS, GRAMS));
        checkEquals(ONE_KILO_IN_POUNDS, convert(ONE, KILOGRAMS, POUNDS));

        // test gram conversion
        checkEquals(ONE_THOUSAND, convert(ONE_THOUSAND, GRAMS, GRAMS));
        checkEquals(ONE, convert(ONE_THOUSAND, GRAMS, KILOGRAMS));
        checkEquals(ONE_KILO_IN_POUNDS, convert(ONE_THOUSAND, GRAMS, POUNDS));

        // test pound conversion
        checkEquals(ONE, convert(ONE, POUNDS, POUNDS));
        checkEquals(ONE_POUND_IN_KILOS, convert(ONE, POUNDS, KILOGRAMS));
        checkEquals(ONE_POUND_IN_GRAMS, convert(ONE, POUNDS, GRAMS));
    }

    /**
     * Helper to compare two {@code BigDecimal}s.
     *
     * @param expected the expected value
     * @param actual   the actual value
     */
    private void checkEquals(BigDecimal expected, BigDecimal actual) {
        assertEquals("Expected " + expected + ", but got " + actual, 0, actual.compareTo(expected));
    }

}
