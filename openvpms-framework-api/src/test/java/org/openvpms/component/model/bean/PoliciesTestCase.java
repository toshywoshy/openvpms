/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.component.model.bean;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.openvpms.component.model.entity.EntityRelationship;
import org.openvpms.component.model.lookup.LookupRelationship;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.object.SequencedRelationship;

import java.util.Comparator;
import java.util.Date;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.openvpms.component.model.bean.Policy.State.ACTIVE;
import static org.openvpms.component.model.bean.Policy.State.ANY;

/**
 * Tests the {@link Policies} class.
 *
 * @author Tim Anderson
 */
public class PoliciesTestCase {

    /**
     * Tests the {@link Policies#active()}, {@link Policies#active(Date)}, {@link Policies#active(Date, boolean)},
     * {@link Policies#active(Predicate)}, {@link Policies#active(Class)}
     */
    @Test
    public void testActive() {
        Date now = new Date();
        Date tomorrow = DateUtils.addDays(now, 1);
        check(Policies.active(), Predicates.activeNow(), ACTIVE, false, Relationship.class);
        check(Policies.active(now), Predicates.activeAt(now), ACTIVE, false, Relationship.class);
        check(Policies.active(now, false), Predicates.activeAt(now), ANY, false, Relationship.class);
        check(Policies.active(Predicates.activeAt(tomorrow)), Predicates.activeAt(tomorrow), ACTIVE, false,
              Relationship.class);
        check(Policies.active(EntityRelationship.class), Predicates.activeNow(), ACTIVE, false,
              EntityRelationship.class);

        Comparator<EntityRelationship> comparator = (o1, o2) -> 0;
        check(Policies.active(EntityRelationship.class, comparator), Predicates.activeNow(), ACTIVE, true,
              EntityRelationship.class);
        check(Policies.active(now, EntityRelationship.class, comparator), Predicates.activeAt(now), ACTIVE, true,
              EntityRelationship.class);
    }

    /**
     * Tests the {@link Policies#any()}, {@link Policies#any(Class)}, {@link Policies#any(Predicate)} and
     * {@link Policies#any(Class, Predicate)} methods.
     */
    @Test
    public void testAny() {
        check(Policies.any(), null, ANY, false, Relationship.class);
        check(Policies.any(EntityRelationship.class), null, ANY, false, EntityRelationship.class);
        check(Policies.any(Predicates.isA("foo")), Predicates.isA("foo"), ANY, false, Relationship.class);
        check(Policies.any(EntityRelationship.class, Predicates.isA("bar")), Predicates.isA("bar"), ANY, false,
              EntityRelationship.class);
    }

    /**
     * Tests the {@link Policies#all()},  {@link Policies#all(Comparator)}, {@link Policies#all(Class, Comparator)}
     * and {@link Policies#all(Predicate)} methods.
     */
    @Test
    public void testAll() {
        check(Policies.all(), null, ANY, false, Relationship.class);
        check(Policies.all((o1, o2) -> 0), null, ANY, true, Relationship.class);
        check(Policies.all(EntityRelationship.class, (o1, o2) -> 0), null, ANY, true, EntityRelationship.class);
        check(Policies.all(Predicates.activeNow()), Predicates.activeNow(), ANY, false, Relationship.class);
    }

    /**
     * Tests the {@link Policies#orderBySequence()} method.
     */
    @Test
    public void testOrderedBySequence() {
        check(Policies.orderBySequence(), null, ANY, true, SequencedRelationship.class);
    }

    /**
     * Verifies that policies can be compared.
     * <p/>
     * Note that this is only applicable where the contained predicates and comparators provide their own equals()
     * methods.
     */
    @Test
    public void testEquality() {
        assertEquals(Policies.active(), Policies.active());
        assertEquals(Policies.active(), Policies.newPolicy(Relationship.class).active().build());
        assertNotEquals(Policies.active(), Policies.newPolicy(LookupRelationship.class).active().build());

        Date now = new Date();
        Date tomorrow = DateUtils.addDays(now, 1);
        assertEquals(Policies.active(now), Policies.active(now));
        assertEquals(Policies.active(now), Policies.newPolicy(Relationship.class).active(now).build());
        assertNotEquals(Policies.active(now), Policies.active(tomorrow));

        assertEquals(Policies.active(now, true), Policies.active(now, true));
        assertEquals(Policies.active(now, true),
                     Policies.newPolicy(Relationship.class).activeObjects().active(now).build());
        assertNotEquals(Policies.active(now, true), Policies.active(now, false));

        assertEquals(Policies.active(Predicates.isA("party.patientpet")),
                     Policies.active(Predicates.isA("party.patientpet")));
        assertNotEquals(Policies.active(Predicates.isA("party.patientpet")),
                        Policies.active(Predicates.isA("party.customerperson")));

        assertEquals(Policies.all(), Policies.any());
        assertEquals(Policies.all(), Policies.newPolicy(Relationship.class).build());

        assertEquals(Policies.orderBySequence(), Policies.orderBySequence());
    }

    /**
     * Checks a policy matches that expected.
     *
     * @param policy        the policy
     * @param predicate     the expected predicate
     * @param state         the expected state
     * @param hasComparator determines if a comparator should be present or not
     * @param type          the expected relationship type
     */
    private <R extends Relationship> void check(Policy<R> policy, Predicate<R> predicate, Policy.State state,
                                                boolean hasComparator, Class<? extends R> type) {
        assertEquals(predicate, policy.getPredicate());
        assertEquals(state, policy.getState());
        assertEquals(hasComparator, policy.getComparator() != null);
        assertEquals(type, policy.getType());
    }
}