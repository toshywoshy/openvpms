/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.junit.Test;
import org.openvpms.archetype.test.ArchetypeServiceTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Tests the {@link PrinterReference} class.
 *
 * @author Tim Anderson
 */
public class PrinterReferenceTestCase extends ArchetypeServiceTest {

    /**
     * Tests the {@link PrinterReference#toString()} method.
     */
    @Test
    public void testToString() {
        assertEquals("::printer:", new PrinterReference(null, "printer").toString());
        assertEquals("entity.printerService1::printer:",
                     new PrinterReference("entity.printerService1", "printer").toString());
        assertEquals("entity.printerService1:PrinterService:printer:",
                     new PrinterReference("entity.printerService1", "PrinterService", "printer", null).toString());
        assertEquals("entity.printerService1:PrinterService:printer:Printer",
                     new PrinterReference("entity.printerService1", "PrinterService", "printer", "Printer").toString());

        // check escaping of colons
        assertEquals("entity.printerService1:PrinterService\\:1:printer\\:a:Printer\\:A",
                     new PrinterReference("entity.printerService1", "PrinterService:1", "printer:a", "Printer:A")
                             .toString());
    }

    /**
     * Tests the {@link PrinterReference#fromString(String)} method.
     */
    @Test
    public void testFromString() {
        // check invalid references
        assertNull(PrinterReference.fromString(null));
        assertNull(PrinterReference.fromString(""));
        assertNull(PrinterReference.fromString(":"));
        assertNull(PrinterReference.fromString("::"));
        assertNull(PrinterReference.fromString(":::"));
        assertNull(PrinterReference.fromString("entity.printerService1:PrinterService:printer"));
        assertNull(PrinterReference.fromString("entity.printerService1:PrinterService:printer:"));

        // check valid references
        checkFromString("printer", null, null, "printer", "printer");
        checkFromString(":printer", null, null, "printer", "printer");
        checkFromString("entity.printerService1:printer", "entity.printerService1", null, "printer", "printer");
        checkFromString("entity.printerService1:PrinterService:printer:PrinterA", "entity.printerService1",
                        "PrinterService", "printer", "PrinterA");
        checkFromString("entity.printerService1::printer:PrinterA", "entity.printerService1", null, "printer",
                        "PrinterA");

        // verify escaping
        checkFromString("entity.printerService1:PrinterService:printer\\:B:Printer\\:B", "entity.printerService1",
                        "PrinterService", "printer:B", "Printer:B");
    }

    /**
     * Checks the {@link PrinterReference#fromString(String)} method.
     *
     * @param value       the value to parse
     * @param archetype   the expected archetype
     * @param serviceName the expected service name
     * @param id          the expected printer id
     * @param name        the expected printer name
     */
    private void checkFromString(String value, String archetype, String serviceName, String id, String name) {
        PrinterReference reference = PrinterReference.fromString(value);
        assertNotNull(reference);
        assertEquals(archetype, reference.getArchetype());
        assertEquals(serviceName, reference.getServiceName());
        assertEquals(id, reference.getId());
        assertEquals(name, reference.getName());
    }
}
