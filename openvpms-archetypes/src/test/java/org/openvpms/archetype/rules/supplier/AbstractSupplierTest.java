/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.supplier;

import org.junit.Before;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.common.IMObjectRelationship;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.ActRelationship;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Predicates;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Base class for supplier test cases.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSupplierTest extends ArchetypeServiceTest {

    /**
     * Package units for act items.
     */
    protected static final String PACKAGE_UNITS = "BOX";

    /**
     * UN/CEFACT unit code corresponding to the package units.
     */
    protected static final String PACKAGE_UNIT_CODE = "BX";

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * The product.
     */
    private Product product;

    /**
     * The practice.
     */
    private Party practice;

    /**
     * The practice location.
     */
    private Party practiceLocation;


    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        product = TestHelper.createProduct();
        stockLocation = createStockLocation();

        // create a practice for currency and tax calculation purposes
        practiceLocation = TestHelper.createLocation();
        IMObjectBean locBean = getBean(practiceLocation);
        locBean.addTarget("stockLocations", stockLocation, "locations");
        practice = TestHelper.getPractice();
        IMObjectBean pracBean = getBean(practice);
        pracBean.addTarget("locations", practiceLocation, "practice");
        save(practice, practice, stockLocation);

        supplier = TestHelper.createSupplier();

        // set up a unit of measure
        Lookup uom = TestHelper.getLookup("lookup.uom", PACKAGE_UNITS);
        IMObjectBean bean = getBean(uom);
        bean.setValue("unitCode", PACKAGE_UNIT_CODE);
        bean.save();
    }

    /**
     * Returns the supplier.
     *
     * @return the supplier
     */
    protected Party getSupplier() {
        return supplier;
    }

    /**
     * Returns the product.
     *
     * @return the product
     */
    protected Product getProduct() {
        return product;
    }

    /**
     * Returns the practice.
     *
     * @return the practice
     */
    protected Party getPractice() {
        return practice;
    }

    /**
     * Returns the practice location.
     *
     * @return the practice location
     */
    protected Party getPracticeLocation() {
        return practiceLocation;
    }

    /**
     * Returns the stock location.
     *
     * @return the stock location
     */
    protected Party getStockLocation() {
        return stockLocation;
    }

    /**
     * Creates an order associated with order items.
     *
     * @param supplier   the supplier
     * @param orderItems the order item
     * @return a new order
     */
    protected FinancialAct createOrder(Party supplier, FinancialAct... orderItems) {
        return createOrder(supplier, stockLocation, orderItems);
    }

    /**
     * Creates an order associated with order items.
     *
     * @param supplier      the supplier
     * @param stockLocation the stock location
     * @param orderItems    the order item
     * @return a new order
     */
    protected FinancialAct createOrder(Party supplier, Party stockLocation, FinancialAct... orderItems) {
        List<Act> toSave = new ArrayList<>();
        IMObjectBean bean = createAct(SupplierArchetypes.ORDER, supplier, stockLocation);
        BigDecimal total = BigDecimal.ZERO;
        for (FinancialAct item : orderItems) {
            bean.addTarget("items", item, "order");
            total = total.add(item.getTotal());
            toSave.add(item);
        }
        bean.setValue("amount", total);
        toSave.add((Act) bean.getObject());
        save(toSave);
        return (FinancialAct) bean.getObject();
    }

    /**
     * Creates an order item.
     *
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @return a new order item
     */
    protected FinancialAct createOrderItem(Product product, BigDecimal quantity, int packageSize,
                                           BigDecimal unitPrice) {
        return createItem(SupplierArchetypes.ORDER_ITEM, product, quantity, packageSize, unitPrice, BigDecimal.ZERO);
    }

    /**
     * Creates and saves a delivery.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price. May be {@code null}
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice) {
        FinancialAct acts = createActs(SupplierArchetypes.DELIVERY, SupplierArchetypes.DELIVERY_ITEM,
                                       supplier, product, quantity, packageSize, unitPrice, BigDecimal.ZERO);
        IMObjectBean bean = getBean(acts);
        bean.setValue("supplierNotes", "Some notes");
        return acts;
    }

    /**
     * Creates and saves a delivery.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice, BigDecimal listPrice) {
        return createActs(SupplierArchetypes.DELIVERY, SupplierArchetypes.DELIVERY_ITEM,
                          supplier, product, quantity, packageSize, unitPrice, listPrice);
    }

    /**
     * Creates and posts a delivery.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new delivery
     */
    protected FinancialAct postDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice, BigDecimal listPrice) {
        FinancialAct delivery = createDelivery(supplier, product, quantity, packageSize, unitPrice, listPrice);
        delivery.setStatus(ActStatus.POSTED);
        save(delivery);
        return delivery;
    }

    /**
     * Creates and saves a delivery, associated with an order item.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the delivery quantity
     * @param packageSize the delivery package size
     * @param unitPrice   the delivery unit price
     * @param orderItem   the order item
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, Product product, BigDecimal quantity, int packageSize,
                                          BigDecimal unitPrice, FinancialAct orderItem) {
        FinancialAct item = createDeliveryItem(product, quantity, packageSize, unitPrice, orderItem);
        return createDelivery(supplier, item);
    }

    /**
     * Creates and saves a delivery, associated with a delivery item.
     *
     * @param supplier      the supplier
     * @param deliveryItems the delivery items
     * @return a new delivery
     */
    protected FinancialAct createDelivery(Party supplier, FinancialAct... deliveryItems) {
        IMObjectBean bean = createAct(SupplierArchetypes.DELIVERY, supplier);
        BigDecimal amount = BigDecimal.ZERO;
        BigDecimal tax = BigDecimal.ZERO;
        List<Act> toSave = new ArrayList<>();
        toSave.add((Act) bean.getObject());
        for (FinancialAct deliveryItem : deliveryItems) {
            bean.addTarget("items", deliveryItem, "delivery");
            amount = amount.add(deliveryItem.getTotal());
            tax = tax.add(deliveryItem.getTaxAmount());
            toSave.add(deliveryItem);
        }
        bean.setValue("amount", amount);
        bean.setValue("tax", tax);
        save(toSave);

        return (FinancialAct) bean.getObject();
    }

    /**
     * Creates a delivery item, associated with an order item.
     *
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param orderItem   the order item
     * @return a new delivery item
     */
    protected FinancialAct createDeliveryItem(Product product, BigDecimal quantity, int packageSize,
                                              BigDecimal unitPrice, FinancialAct orderItem) {
        return createItem(SupplierArchetypes.DELIVERY_ITEM, product, quantity, packageSize, unitPrice,
                          orderItem);
    }

    /**
     * Creates a return.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @return a new return
     */
    protected Act createReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                               BigDecimal unitPrice) {
        return createReturn(supplier, product, quantity, packageSize, unitPrice, BigDecimal.ZERO);
    }

    /**
     * Creates a return.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new return
     */
    protected Act createReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                               BigDecimal unitPrice, BigDecimal listPrice) {
        return createActs(SupplierArchetypes.RETURN, SupplierArchetypes.RETURN_ITEM,
                          supplier, product, quantity, packageSize, unitPrice, listPrice);
    }

    /**
     * Creates and posts a return.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new return
     */
    protected Act postReturn(Party supplier, Product product, BigDecimal quantity, int packageSize,
                               BigDecimal unitPrice, BigDecimal listPrice) {
        Act result = createReturn(supplier, product, quantity, packageSize, unitPrice, listPrice);
        result.setStatus(ActStatus.POSTED);
        save(result);
        return result;
    }

    /**
     * Create a new return, associated with an order item.
     *
     * @param supplier    the supplier
     * @param product     the product
     * @param quantity    the return quantity
     * @param packageSize the return package size
     * @param unitPrice   the unit price
     * @param orderItem   the order item
     * @return a new return
     */
    protected Act createReturn(Party supplier, Product product, BigDecimal quantity, int packageSize, BigDecimal
            unitPrice, FinancialAct orderItem) {
        IMObjectBean bean = createAct(SupplierArchetypes.RETURN, supplier);
        Act item = createReturnItem(product, quantity, packageSize, unitPrice, orderItem);
        bean.addTarget("items", item, "return");
        bean.save();
        return (Act) bean.getObject();
    }

    /**
     * Creates a new supplier act.
     *
     * @param shortName the act short name
     * @param supplier  the supplier
     * @return a new act
     */
    protected IMObjectBean createAct(String shortName, Party supplier) {
        return createAct(shortName, supplier, stockLocation);
    }

    /**
     * Creates a new supplier act.
     *
     * @param shortName the act short name
     * @param supplier  the supplier
     * @return a new act
     */
    protected IMObjectBean createAct(String shortName, Party supplier, Party stockLocation) {
        Act act = create(shortName, Act.class);
        IMObjectBean bean = getBean(act);
        bean.setTarget("supplier", supplier);
        if (bean.hasNode("stockLocation")) {
            bean.setTarget("stockLocation", stockLocation);
        }
        return bean;
    }

    /**
     * Creates a new supplier act item associated with an order item.
     *
     * @param shortName   the act short name
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param orderItem   the order item
     * @return a new act
     */
    protected FinancialAct createItem(String shortName, Product product, BigDecimal quantity,
                                      int packageSize, BigDecimal unitPrice, FinancialAct orderItem) {
        FinancialAct act = createItem(shortName, product, quantity, packageSize, unitPrice, BigDecimal.ZERO);
        IMObjectBean bean = getBean(act);
        Relationship relationship = bean.addTarget("order", orderItem);
        orderItem.addActRelationship((ActRelationship) relationship);
        save(act, orderItem);
        return act;
    }

    /**
     * Creates a new supplier act item.
     *
     * @param shortName   the act short name
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param listPrice   the list price
     * @return a new act
     */
    protected FinancialAct createItem(String shortName, Product product, BigDecimal quantity, int packageSize,
                                      BigDecimal unitPrice, BigDecimal listPrice) {
        FinancialAct item = create(shortName, FinancialAct.class);
        IMObjectBean bean = getBean(item);
        bean.setTarget("product", product);
        item.setQuantity(quantity);
        bean.setValue("packageSize", packageSize);
        bean.setValue("packageUnits", PACKAGE_UNITS);
        bean.setValue("unitPrice", unitPrice);
        bean.setValue("listPrice", listPrice);
        getArchetypeService().deriveValues(item);
        return item;
    }

    /**
     * Creates and saves a new stock location.
     *
     * @return a new stock location
     */
    protected Party createStockLocation() {
        return SupplierTestHelper.createStockLocation();
    }

    /**
     * Verifies that the <em>entityLink.productStockLocation</em> associated with the product and stock location matches
     * that expected.
     *
     * @param product  the product
     * @param quantity the expected quantity, or {@code null} if the relationship shouldn't exist
     */
    protected void checkProductStockLocationRelationship(Product product, BigDecimal quantity) {
        product = get(product);  // make sure the latest instance is used
        IMObjectBean bean = getBean(product);
        List<IMObjectRelationship> values = bean.getValues("stockLocations", IMObjectRelationship.class,
                                                           Predicates.targetEquals(stockLocation));
        if (quantity == null) {
            assertTrue(values.isEmpty());
        } else {
            assertEquals(1, values.size());
            IMObjectBean relBean = getBean(values.get(0));
            checkEquals(quantity, relBean.getBigDecimal("quantity"));
        }
    }

    /**
     * Creates a return item, associated with an order item.
     *
     * @param product     the product
     * @param quantity    the quantity
     * @param packageSize the package size
     * @param unitPrice   the unit price
     * @param orderItem   the order item
     * @return a new return item
     */
    private FinancialAct createReturnItem(Product product, BigDecimal quantity, int packageSize, BigDecimal unitPrice,
                                          FinancialAct orderItem) {
        return createItem(SupplierArchetypes.RETURN_ITEM, product, quantity, packageSize, unitPrice,
                          orderItem);
    }

    /**
     * Creates and saves a new supplier act associated with an act item.
     *
     * @param shortName             the act short name
     * @param itemShortName         the act item short name
     * @param supplier              the supplier
     * @param product               the product
     * @param quantity              the item quantity
     * @param packageSize           the item package size
     * @param unitPrice             the item unit price
     * @param listPrice             the item list price
     * @return a new act
     */
    private FinancialAct createActs(String shortName, String itemShortName, Party supplier, Product product,
                                    BigDecimal quantity, int packageSize, BigDecimal unitPrice, BigDecimal listPrice) {
        IMObjectBean bean = createAct(shortName, supplier);
        FinancialAct item = createItem(itemShortName, product, quantity, packageSize, unitPrice, listPrice);

        ActRelationship relationship = (ActRelationship) bean.addTarget("items", item);
        item.addActRelationship(relationship);
        bean.save(item);
        return (FinancialAct) bean.getObject();
    }

}
