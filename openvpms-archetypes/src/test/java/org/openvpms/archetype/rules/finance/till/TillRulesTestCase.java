/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.till;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.archetype.rules.finance.deposit.DepositArchetypes;
import org.openvpms.archetype.rules.finance.deposit.DepositHelper;
import org.openvpms.archetype.rules.finance.deposit.DepositTestHelper;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.act.ActRelationship;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.business.domain.im.common.Entity;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Relationship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.openvpms.archetype.rules.act.ActStatus.POSTED;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.CantAddActToTill;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.ClearedTill;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.InvalidTillArchetype;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.InvalidTransferTill;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.MissingRelationship;
import static org.openvpms.archetype.rules.finance.till.TillRuleException.ErrorCode.MissingTill;
import static org.openvpms.archetype.test.TestHelper.createTill;


/**
 * Tests the {@link TillRules} class.
 *
 * @author Tim Anderson
 */
public class TillRulesTestCase extends AbstractTillRulesTest {

    /**
     * The till.
     */
    private Entity till;

    /**
     * The till business rules.
     */
    private TillRules rules;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        till = createTill();
        save(till);
        rules = new TillRules((IArchetypeRuleService) getArchetypeService(), transactionManager);
    }

    /**
     * Tests the {@link TillRules#clearTill)} method, with a zero cash float.
     * This should not create an <em>act.tillBalanceAdjustment</e>.
     */
    @Test
    public void testClearTillWithNoAdjustment() {
        BigDecimal cashFloat = BigDecimal.ZERO;
        checkClearTillForUnclearedBalance(cashFloat, cashFloat);
    }

    /**
     * Tests the {@link TillRules#clearTill)} method, with an initial cash float
     * of {@code 40.0} and new cash float of {@code 20.0}.
     * This should create a credit adjustment of {@code 20.0}
     */
    @Test
    public void testClearTillWithCreditAdjustment() {
        BigDecimal cashFloat = new BigDecimal(40);
        BigDecimal newCashFloat = new BigDecimal(20);
        checkClearTillForUnclearedBalance(cashFloat, newCashFloat);
    }

    /**
     * Tests the {@link TillRules#clearTill)} method, with an initial cash float
     * of {@code 40.0} and new cash float of {@code 100.0}.
     * This should create a debit adjustment of {@code 60.0}
     */
    @Test
    public void testClearTillWithDebitAdjustment() {
        BigDecimal cashFloat = new BigDecimal(40);
        BigDecimal newCashFloat = new BigDecimal(100);
        checkClearTillForUnclearedBalance(cashFloat, newCashFloat);
    }

    /**
     * Verifies that an act doesn't get added to a new till balance if it
     * is subsequently saved after the till has been cleared.
     */
    @Test
    public void testClearTillAndReSave() {
        List<FinancialAct> payment = createPayment(till);
        payment.get(0).setStatus(POSTED);
        FinancialAct balance = checkAddToTillBalance(till, payment, false, BigDecimal.ONE);

        // clear the till
        Party account = DepositTestHelper.createDepositAccount();
        rules.clearTill(balance, BigDecimal.ZERO, account);

        // reload the act and save it to force TillRules.addToTill() to run
        // again. However the act should not be added to a new balance as
        // there is an existing actRelationship.tillBalanceItem relationship.
        Act latest = get(payment.get(0));
        save(latest);
        latest = get(latest);
        IMObjectBean bean = getBean(latest);
        List<ActRelationship> relationships = bean.getValues("items", ActRelationship.class);
        assertEquals(1, relationships.size());
    }

    /**
     * Tests the {@link TillRules#transfer)} method.
     */
    @Test
    public void testTransfer() {
        List<FinancialAct> payment = createPayment(till);
        payment.get(0).setStatus(POSTED);
        FinancialAct balance = checkAddToTillBalance(till, payment, false, BigDecimal.ONE);

        // make sure using the latest version of the act (i.e, with relationship
        // to balance)
        Act act = get(payment.get(0));

        Entity newTill = createTill();
        save(newTill);
        rules.transfer(balance, act, newTill);

        // reload the balance and make sure the payment has been removed
        balance = (FinancialAct) get(balance.getObjectReference());
        assertEquals(0, countRelationships(balance, payment.get(0)));

        // balance should now be zero.
        checkEquals(BigDecimal.ZERO, balance.getTotal());

        // make sure the payment has been added to a new balance
        FinancialAct newBalance = TillHelper.getUnclearedTillBalance(newTill, getArchetypeService());
        assertNotNull(newBalance);
        assertEquals(1, countRelationships(newBalance, payment.get(0)));
        assertEquals(TillBalanceStatus.UNCLEARED, newBalance.getStatus());

        checkEquals(BigDecimal.ONE, newBalance.getTotal());
    }

    /**
     * Verifies that {@link TillRules#transfer} throws TillRuleException if
     * invoked for an invalid balance.
     */
    @Test
    public void testTransferWithInvalidBalance() {
        FinancialAct act = create(DepositArchetypes.BANK_DEPOSIT, FinancialAct.class);
        List<FinancialAct> payment = createPayment(till);
        save(payment);
        try {
            rules.transfer(act, payment.get(0), till);
        } catch (TillRuleException expected) {
            assertEquals(InvalidTillArchetype, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillRules#transfer} throws TillRuleException if
     * invoked for an invalid act.
     */
    @Test
    public void testTransferWithInvalidAct() {
        Act act = TillHelper.createTillBalance(till, getArchetypeService());
        FinancialAct payment = create(DepositArchetypes.BANK_DEPOSIT, FinancialAct.class);
        try {
            rules.transfer(act, payment, till);
        } catch (TillRuleException expected) {
            assertEquals(CantAddActToTill, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillRules#transfer} throws TillRuleException if
     * invoked with a cleared balance.
     */
    @Test
    public void testTransferWithClearedBalance() {
        Act act = TillHelper.createTillBalance(till, getArchetypeService());
        act.setStatus(TillBalanceStatus.CLEARED);
        List<FinancialAct> payment = createPayment(till);
        payment.get(0).setStatus(POSTED);
        getArchetypeService().save(payment);
        try {
            rules.transfer(act, payment.get(0), createTill());
        } catch (TillRuleException expected) {
            assertEquals(ClearedTill, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillRules#transfer} throws TillRuleException if
     * there is no relationship between the balance and act to transfer.
     */
    @Test
    public void testTransferWithNoRelationship() {
        Act act = TillHelper.createTillBalance(till, getArchetypeService());
        List<FinancialAct> payment = createPayment(till);
        payment.get(0).setStatus(POSTED);
        getArchetypeService().save(payment);
        try {
            rules.transfer(act, payment.get(0), createTill());
        } catch (TillRuleException expected) {
            assertEquals(MissingRelationship, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillRules#transfer} throws TillRuleException if
     * the act to be transferred has no till participation.
     */
    @Test
    public void testTransferWithNoParticipation() {
        Act balance = TillHelper.createTillBalance(till, getArchetypeService());
        Party party = TestHelper.createCustomer();

        FinancialAct payment = create(CustomerAccountArchetypes.PAYMENT, FinancialAct.class);
        payment.setStatus(POSTED);
        IMObjectBean bean = getBean(payment);
        bean.setTarget("customer", party);

        IMObjectBean balanceBean = getBean(balance);
        balanceBean.addTarget("items", payment, "tillBalance");

        try {
            rules.transfer(balance, payment, createTill());
        } catch (TillRuleException expected) {
            assertEquals(MissingTill, expected.getErrorCode());
        }
    }

    /**
     * Verifies that {@link TillRules#transfer} throws TillRuleException if
     * an attempt is made to transfer to the same till.
     */
    @Test
    public void testTransferToSameTill() {
        List<FinancialAct> payment = createPayment(till);
        payment.get(0).setStatus(POSTED);
        Act balance = checkAddToTillBalance(till, payment, false, BigDecimal.ONE);

        try {
            rules.transfer(balance, payment.get(0), till);
        } catch (TillRuleException expected) {
            assertEquals(InvalidTransferTill, expected.getErrorCode());
        }
    }

    /**
     * Tests the {@link TillRules#isClearInProgress(Entity)} method.
     */
    @Test
    public void testIsClearInProgress() {
        assertFalse(rules.isClearInProgress(till));

        FinancialAct balance = createBalance(till, TillBalanceStatus.UNCLEARED);
        save(balance);
        assertFalse(rules.isClearInProgress(till));
        balance.setStatus(TillBalanceStatus.IN_PROGRESS);
        save(balance);
        assertTrue(rules.isClearInProgress(till));

        balance.setStatus(TillBalanceStatus.CLEARED);
        save(balance);
        assertFalse(rules.isClearInProgress(till));
    }

    /**
     * Tests the {@link TillRules#startClearTill(FinancialAct, BigDecimal)} method.
     */
    @Test
    public void testStartClearTill() {
        FinancialAct balance1 = createBalance(till, TillBalanceStatus.UNCLEARED);
        rules.startClearTill(balance1, BigDecimal.ZERO);
        balance1 = get(balance1);
        assertEquals(TillBalanceStatus.IN_PROGRESS, balance1.getStatus());

        List<FinancialAct> payment = createPayment(till);
        payment.get(0).setStatus(POSTED);
        save(payment);

        // make sure the payment went into a new till balance
        FinancialAct balance2 = TillHelper.getUnclearedTillBalance(till, getArchetypeService());
        assertNotNull(balance2);
        assertNotEquals(balance1.getId(), balance2.getId());
        IMObjectBean bean = getBean(balance2);
        assertTrue(bean.hasTarget("items", payment.get(0)));
    }

    /**
     * Tests the {@link TillRules#startClearTill} method folli, with a zero cash float.
     * This should not create an <em>act.tillBalanceAdjustment</e>.
     */
    @Test
    public void testStartClearTillWithNoAdjustment() {
        BigDecimal cashFloat = BigDecimal.ZERO;
        checkStartClearTill(cashFloat, cashFloat);
    }

    /**
     * Tests the {@link TillRules#startClearTill} method, with an initial cash float
     * of {@code 40.0} and new cash float of {@code 20.0}.
     * This should create a credit adjustment of {@code 20.0}
     */
    @Test
    public void testStartClearTillWithCreditAdjustment() {
        BigDecimal cashFloat = new BigDecimal(40);
        BigDecimal newCashFloat = new BigDecimal(20);
        checkStartClearTill(cashFloat, newCashFloat);
    }

    /**
     * Tests the {@link TillRules#startClearTill)} method, with an initial cash float
     * of {@code 40.0} and new cash float of {@code 100.0}.
     * This should create a debit adjustment of {@code 60.0}
     */
    @Test
    public void testStartClearTillWithDebitAdjustment() {
        BigDecimal cashFloat = new BigDecimal(40);
        BigDecimal newCashFloat = new BigDecimal(100);
        checkStartClearTill(cashFloat, newCashFloat);
    }

    /**
     * Checks the behaviour of the {@link TillRules#clearTill(FinancialAct, BigDecimal, Party)} method.
     *
     * @param initialCashFloat the initial cash float value
     * @param newCashFloat     the new cash float value
     */
    private void checkClearTillForUnclearedBalance(BigDecimal initialCashFloat, BigDecimal newCashFloat) {
        setTillCashFloat(initialCashFloat);

        Party account = DepositTestHelper.createDepositAccount();
        FinancialAct balance = createBalance(till, TillBalanceStatus.UNCLEARED);
        save(balance);
        assertNull(balance.getActivityEndTime());

        // make sure there is no uncleared deposit for the account
        FinancialAct deposit = DepositHelper.getUndepositedDeposit(account, getArchetypeService());
        assertNull(deposit);

        // clear the till
        rules.clearTill(balance, newCashFloat, account);

        // make sure the balance is updated
        BigDecimal expectedBalance = checkBalance(initialCashFloat, newCashFloat, balance, TillBalanceStatus.CLEARED);
        checkDeposit(account, balance, expectedBalance);
    }

    /**
     * Verifies the deposit matches that expected.
     *
     * @param account         the deposit account
     * @param balance         the till balance act
     * @param expectedBalance the expected balance
     */
    private void checkDeposit(Party account, FinancialAct balance, BigDecimal expectedBalance) {
        FinancialAct deposit = DepositHelper.getUndepositedDeposit(account, getArchetypeService());
        // make sure a new uncleared bank deposit exists, with a relationship to the till balance
        assertNotNull(deposit);
        IMObjectBean depBean = getBean(deposit);
        assertTrue(depBean.hasTarget("items", balance));
        checkEquals(expectedBalance, deposit.getTotal());
    }

    /**
     * Sets the till cash float.
     *
     * @param tillFloat the till cash float
     */
    private void setTillCashFloat(BigDecimal tillFloat) {
        IMObjectBean tillBean = getBean(till);
        tillBean.setValue("tillFloat", tillFloat);
        tillBean.save();
    }

    /**
     * Checks the behaviour of the {@link TillRules#startClearTill(FinancialAct, BigDecimal)} method and
     * {@link TillRules#clearTill(FinancialAct, Party)} methods.
     *
     * @param initialCashFloat the initial cash float value
     * @param newCashFloat     the new cash float value
     */
    private void checkStartClearTill(BigDecimal initialCashFloat, BigDecimal newCashFloat) {
        setTillCashFloat(initialCashFloat);
        Party account = DepositTestHelper.createDepositAccount();

        FinancialAct balance = createBalance(till, TillBalanceStatus.UNCLEARED);
        save(balance);
        assertNull(balance.getActivityEndTime());

        // start clearing the till
        rules.startClearTill(balance, newCashFloat);

        BigDecimal expectedBalance = checkBalance(initialCashFloat, newCashFloat, balance,
                                                  TillBalanceStatus.IN_PROGRESS);

        rules.clearTill(balance, account);
        checkDeposit(account, balance, expectedBalance);
    }


    private BigDecimal checkBalance(BigDecimal initialCashFloat, BigDecimal newCashFloat, FinancialAct balance,
                                    String status) {
        // make sure the balance is updated
        assertEquals(status, balance.getStatus());
        // end time should be > startTime < now
        Date startTime = balance.getActivityStartTime();
        Date endTime = balance.getActivityEndTime();
        if (TillBalanceStatus.CLEARED.equals(status)) {
            // CLEARED balances have an end time
            assertTrue(endTime.compareTo(startTime) >= 0);
            assertTrue(endTime.compareTo(new Date()) <= 0);
        } else {
            // IN_PROGRESS balances do not
            assertNull(endTime);
        }

        BigDecimal total = newCashFloat.subtract(initialCashFloat);

        if (initialCashFloat.compareTo(newCashFloat) != 0) {
            // expect a till balance adjustment to have been made
            assertEquals(1, balance.getSourceActRelationships().size());
            IMObjectBean balBean = getBean(balance);
            Act target = balBean.getTarget("items", Act.class);
            assertTrue(target.isA(TillArchetypes.TILL_BALANCE_ADJUSTMENT));
            IMObjectBean adjBean = getBean(target);
            BigDecimal amount = adjBean.getBigDecimal("amount");

            boolean credit = (newCashFloat.compareTo(initialCashFloat) < 0);
            BigDecimal adjustmentTotal = total.abs();
            checkEquals(adjustmentTotal, amount);
            assertEquals(credit, adjBean.getBoolean("credit"));
        } else {
            // no till balance adjustment should have been generated
            assertTrue(balance.getSourceActRelationships().isEmpty());
        }

        // check the till balance.
        BigDecimal expectedBalance = total.negate();
        checkEquals(expectedBalance, balance.getTotal());

        // make sure the till is updated
        till = get(till);
        IMObjectBean bean = getBean(till);
        BigDecimal currentFloat = bean.getBigDecimal("tillFloat");
        Date lastCleared = bean.getDate("lastCleared");
        Date now = new Date();

        checkEquals(currentFloat, newCashFloat);
        assertTrue(now.compareTo(lastCleared) >= 0); // expect now >= lastCleared
        return expectedBalance;
    }

    /**
     * Counts the no. of times an act appears as the target act in a set
     * of act relationships.
     *
     * @param source the source act
     * @param target the target act
     * @return the no. of times {@code target} appears as a target
     */
    private int countRelationships(Act source, Act target) {
        int found = 0;
        for (Relationship relationship : source.getSourceActRelationships()) {
            if (relationship.getTarget().equals(target.getObjectReference())) {
                found++;
            }
        }
        return found;
    }

}
