/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.verifier.customer.account;

import org.openvpms.archetype.rules.finance.account.FinancialTestHelper;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.Participation;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.user.User;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.openvpms.archetype.test.TestHelper.checkEquals;

/**
 * Verifies an invoice item matches that expected.
 *
 * @author Tim Anderson
 */
public class TestInvoiceItemVerifier {
    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The expected patient.
     */
    private Reference patient;

    /**
     * The expected product.
     */
    private Reference product;

    /**
     * The expected quantity.
     */
    private BigDecimal quantity = BigDecimal.ONE;

    /**
     * The expected fixed price.
     */
    private BigDecimal fixedPrice = BigDecimal.ZERO;

    /**
     * The expected unit price.
     */
    private BigDecimal unitPrice = BigDecimal.ZERO;

    /**
     * The expected discount.
     */
    private BigDecimal discount = BigDecimal.ZERO;

    /**
     * The expected total.
     */
    private BigDecimal total = BigDecimal.ZERO;

    /**
     * The expected product template.
     */
    private Reference template;

    /**
     * The expected product template expansion group.
     */
    private Integer group;

    /**
     * The expected user that created the item.
     */
    private Reference createdBy;

    /**
     * Constructs a {@link TestInvoiceItemVerifier}.
     *
     * @param service the archetype service
     */
    public TestInvoiceItemVerifier(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Sets the expected patient.
     *
     * @param patient the expected patient
     * @return this
     */
    public TestInvoiceItemVerifier patient(Party patient) {
        this.patient = (patient != null) ? patient.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected product.
     *
     * @param product the expected product
     * @return this
     */
    public TestInvoiceItemVerifier product(Product product) {
        this.product = (product != null) ? product.getObjectReference() : null;
        return this;
    }

    /**
     * Sets the expected  quantity.
     *
     * @param quantity the expected  quantity
     * @return this
     */
    public TestInvoiceItemVerifier quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }

    /**
     * Sets the expected quantity.
     *
     * @param quantity the expected quantity
     * @return this
     */
    public TestInvoiceItemVerifier quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestInvoiceItemVerifier fixedPrice(int fixedPrice) {
        return fixedPrice(BigDecimal.valueOf(fixedPrice));
    }

    /**
     * Sets the expected fixed price.
     *
     * @param fixedPrice the expected fixed price
     * @return this
     */
    public TestInvoiceItemVerifier fixedPrice(BigDecimal fixedPrice) {
        this.fixedPrice = fixedPrice;
        return this;
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public TestInvoiceItemVerifier unitPrice(int unitPrice) {
        return unitPrice(BigDecimal.valueOf(unitPrice));
    }

    /**
     * Sets the expected unit price.
     *
     * @param unitPrice the expected unit price
     * @return this
     */
    public TestInvoiceItemVerifier unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public TestInvoiceItemVerifier discount(int discount) {
        return discount(BigDecimal.valueOf(discount));
    }

    /**
     * Sets the expected discount.
     *
     * @param discount the expected discount
     * @return this
     */
    public TestInvoiceItemVerifier discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public TestInvoiceItemVerifier total(int total) {
        return total(BigDecimal.valueOf(total));
    }

    /**
     * Sets the expected total.
     *
     * @param total the expected total
     * @return this
     */
    public TestInvoiceItemVerifier total(BigDecimal total) {
        this.total = total;
        return this;
    }

    /**
     * Sets the expected product template.
     *
     * @param template the template. May be {@code null}
     * @return this
     */
    public TestInvoiceItemVerifier template(Product template) {
        this.template = (template != null) ? template.getObjectReference() : null;
        if (template == null) {
            group = null;
        }
        return this;
    }

    /**
     * Sets the expected template expansion group.
     *
     * @param group the expected template expansion group
     * @return this
     */
    public TestInvoiceItemVerifier group(int group) {
        this.group = group;
        return this;
    }

    /**
     * Sets the expected created-by user.
     *
     * @param createdBy the expected created-by user
     * @return this
     */
    public TestInvoiceItemVerifier createdBy(User createdBy) {
        this.createdBy = (createdBy != null) ? createdBy.getObjectReference() : null;
        return this;
    }

    /**
     * Verifies that the estimate item matches that expected.
     *
     * @param item the estimate item
     */
    public void verify(Act item) {
        IMObjectBean bean = service.getBean(item);
        assertEquals(patient, bean.getTargetRef("patient"));
        assertEquals(product, bean.getTargetRef("product"));
        assertEquals(template, bean.getTargetRef("template"));
        checkEquals(quantity, bean.getBigDecimal("quantity"));
        checkEquals(fixedPrice, bean.getBigDecimal("fixedPrice"));
        checkEquals(unitPrice, bean.getBigDecimal("unitPrice"));
        checkEquals(discount, bean.getBigDecimal("discount"));
        checkEquals(total, bean.getBigDecimal("total"));
        Participation participation = bean.getObject("template", Participation.class);
        if (participation == null) {
            assertNull(group);
        } else {
            IMObjectBean participationBean = service.getBean(participation);
            assertEquals(group, participationBean.getValue("group"));
        }
        assertEquals(createdBy, item.getCreatedBy());
    }

    /**
     * Verifies there is an estimate item that matches the criteria.
     *
     * @param items the estimate items
     */
    public void verify(List<Act> items) {
        Act item = FinancialTestHelper.find(items, patient, product, null);
        if (item == null) {
            fail("Failed to find estimate item for patient=" + patient + ", product=" + product);
        }
        verify(item);
    }

}
