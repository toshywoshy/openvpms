/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Builds <em>party.supplierorganisation</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestSupplierOrganisationBuilder extends AbstractTestPartyBuilder<Party, TestSupplierOrganisationBuilder> {

    /**
     * Constructs a {@link TestSupplierOrganisationBuilder}.
     *
     * @param service   the archetype service
     */
    public TestSupplierOrganisationBuilder(ArchetypeService service) {
        super(SupplierArchetypes.SUPPLIER_ORGANISATION, Party.class, service);
        name(ValueStrategy.random("zsupplier"));
    }
}