/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountRules;
import org.openvpms.archetype.test.builder.eft.TestEFTPOSPaymentBuilder;
import org.openvpms.archetype.test.builder.eft.TestEFTPOSRefundBuilder;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for creating customer account acts.
 *
 * @author Tim Anderson
 */
public class TestCustomerAccountFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The customer account rules.
     */
    private final CustomerAccountRules rules;

    /**
     * Constructs a {@link TestCustomerAccountFactory}.
     *
     * @param service the archetype service
     * @param rules   the customer account rules
     */
    public TestCustomerAccountFactory(ArchetypeService service, CustomerAccountRules rules) {
        this.service = service;
        this.rules = rules;
    }

    /**
     * Returns a builder for a new counter sale.
     *
     * @return a a counter sale builder
     */
    public TestCounterSaleBuilder newCounterSale() {
        return new TestCounterSaleBuilder(service);
    }

    /**
     * Returns a builder for a new estimate.
     *
     * @return an estimate builder
     */
    public TestEstimateBuilder newEstimate() {
        return new TestEstimateBuilder(service, rules);
    }

    /**
     * Returns a builder for a new estimate item.
     *
     * @return an estimate item builder
     */
    public TestEstimateItemBuilder newEstimateItem() {
        return new TestEstimateItemBuilder(null, service, rules);
    }

    /**
     * Returns a builder for a new customer invoice.
     *
     * @return a customer invoice builder
     */
    public TestInvoiceBuilder newInvoice() {
        return new TestInvoiceBuilder(service);
    }

    /**
     * Returns a builder for a new customer credit.
     *
     * @return a customer credit builder
     */
    public TestCreditBuilder newCredit() {
        return new TestCreditBuilder(service);
    }

    /**
     * Returns a builder for a new customer payment.
     *
     * @return a customer payment builder
     */
    public TestPaymentBuilder newPayment() {
        return new TestPaymentBuilder(service);
    }

    /**
     * Returns a builder for a new customer cash payment item.
     *
     * @return a customer cash payment item builder
     */
    public TestCashPaymentItemBuilder newCashPaymentItem() {
        return new TestCashPaymentItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new customer EFT payment item.
     *
     * @return a customer EFT payment item builder
     */
    public TestEFTPaymentItemBuilder newEFTPaymentItem() {
        return new TestEFTPaymentItemBuilder(null, service);
    }

    /**
     * Returns a builder for a new EFTPOS payment.
     *
     * @return an EFTPOS payment builder
     */
    public TestEFTPOSPaymentBuilder newEFTPOSPayment() {
        return new TestEFTPOSPaymentBuilder(service);
    }

    /**
     * Returns a builder for a new EFTPOS refund.
     *
     * @return an EFTPOS refund builder
     */
    public TestEFTPOSRefundBuilder newEFTPOSRefund() {
        return new TestEFTPOSRefundBuilder(service);
    }

    /**
     * Returns a builder for a new customer refund.
     *
     * @return a refund payment builder
     */
    public TestRefundBuilder newRefund() {
        return new TestRefundBuilder(service);
    }

    /**
     * Returns a builder for a new customer cash refund item.
     *
     * @return a customer cash refund item builder
     */
    public TestCashRefundItemBuilder newCashRefundItem() {
        return new TestCashRefundItemBuilder(null, service);
    }


    /**
     * Returns a builder for a new customer EFT refund item.
     *
     * @return a customer EFT refund item builder
     */
    public TestEFTRefundItemBuilder newEFTRefundItem() {
        return new TestEFTRefundItemBuilder(null, service);
    }
}
