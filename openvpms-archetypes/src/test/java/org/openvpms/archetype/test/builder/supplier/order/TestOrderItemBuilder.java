/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier.order;

import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Builds <em>act.supplierOrderItem</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestOrderItemBuilder extends AbstractTestActBuilder<FinancialAct, TestOrderItemBuilder> {

    /**
     * The parent builder.
     */
    private final TestOrderBuilder parent;

    /**
     * The product.
     */
    private Product product;

    /**
     * The reorder code.
     */
    private String reorderCode;

    /**
     * The reorder description.
     */
    private String reorderDescription;

    /**
     * The package size.
     */
    private Integer packageSize;

    /**
     * The package units.
     */
    private String packageUnits;

    /**
     * The quantity.
     */
    private BigDecimal quantity;

    /**
     * The received quantity.
     */
    private BigDecimal receivedQuantity;

    /**
     * The cancelled quantity.
     */
    private BigDecimal cancelledQuantity;

    /**
     * The unit price.
     */
    private BigDecimal unitPrice;

    /**
     * The list price.
     */
    private BigDecimal listPrice;

    /**
     * The tax.
     */
    private BigDecimal tax;

    /**
     * Constructs a {@link TestOrderItemBuilder}.
     *
     * @param service the archetype service
     */
    public TestOrderItemBuilder(TestOrderBuilder parent, ArchetypeService service) {
        super(SupplierArchetypes.ORDER_ITEM, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Sets the product.
     *
     * @param product the product
     * @return this
     */
    public TestOrderItemBuilder product(Product product) {
        this.product = product;
        return this;
    }

    /**
     * Sets the reorder code.
     *
     * @param reorderCode the reorder code
     * @return this
     */
    public TestOrderItemBuilder reorderCode(String reorderCode) {
        this.reorderCode = reorderCode;
        return this;
    }

    /**
     * Sets the reorder description.
     *
     * @param reorderDescription the reorder description
     * @return this
     */
    public TestOrderItemBuilder reorderDescription(String reorderDescription) {
        this.reorderDescription = reorderDescription;
        return this;
    }

    /**
     * Sets the package size.
     *
     * @param packageSize the package size
     * @return this
     */
    public TestOrderItemBuilder packageSize(int packageSize) {
        this.packageSize = packageSize;
        return this;
    }

    /**
     * Sets the package units.
     *
     * @param packageUnits the package units
     * @return this
     */
    public TestOrderItemBuilder packageUnits(String packageUnits) {
        this.packageUnits = packageUnits;
        return this;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestOrderItemBuilder quantity(int quantity) {
        return quantity(BigDecimal.valueOf(quantity));
    }


    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     * @return this
     */
    public TestOrderItemBuilder quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    /**
     * Sets the received quantity.
     *
     * @param receivedQuantity the received quantity
     * @return this
     */
    public TestOrderItemBuilder receivedQuantity(int receivedQuantity) {
        return receivedQuantity(BigDecimal.valueOf(receivedQuantity));
    }

    /**
     * Sets the received quantity.
     *
     * @param receivedQuantity the received quantity
     * @return this
     */
    public TestOrderItemBuilder receivedQuantity(BigDecimal receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
        return this;
    }

    /**
     * Sets the cancelled quantity.
     *
     * @param cancelledQuantity the cancelled quantity
     */
    public TestOrderItemBuilder cancelledQuantity(int cancelledQuantity) {
        return cancelledQuantity(BigDecimal.valueOf(cancelledQuantity));
    }

    /**
     * Sets the cancelled quantity.
     *
     * @param cancelledQuantity the cancelled quantity
     */
    public TestOrderItemBuilder cancelledQuantity(BigDecimal cancelledQuantity) {
        this.cancelledQuantity = cancelledQuantity;
        return this;
    }

    /**
     * Sets the unit price.
     *
     * @param unitPrice the unit price
     * @return this
     */
    public TestOrderItemBuilder unitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    /**
     * Sets the list price.
     *
     * @param listPrice the list price
     * @return this
     */
    public TestOrderItemBuilder listPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
        return this;
    }

    /**
     * Sets the tax.
     *
     * @param tax the tax
     * @return this
     */
    public TestOrderItemBuilder tax(BigDecimal tax) {
        this.tax = tax;
        return this;
    }

    /**
     * Adds the item to the order.
     *
     * @return the order builder
     */
    public TestOrderBuilder add() {
        FinancialAct item = build(false);
        parent.add(item);
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (product != null) {
            bean.setTarget("product", product);
        }
        if (reorderCode != null) {
            bean.setValue("reorderCode", reorderCode);
        }
        if (reorderDescription != null) {
            bean.setValue("reorderDescription", reorderDescription);
        }
        if (packageSize != null) {
            bean.setValue("packageSize", packageSize);
        }
        if (packageUnits != null) {
            bean.setValue("packageUnits", packageUnits);
        }
        if (quantity != null) {
            bean.setValue("quantity", quantity);
        }
        if (receivedQuantity != null) {
            bean.setValue("receivedQuantity", receivedQuantity);
        }
        if (cancelledQuantity != null) {
            bean.setValue("cancelledQuantity", cancelledQuantity);
        }
        if (unitPrice != null) {
            bean.setValue("unitPrice", unitPrice);
        }
        if (listPrice != null) {
            bean.setValue("listPrice", listPrice);
        }
        if (tax != null) {
            bean.setValue("tax", tax);
        }
        getService().deriveValues(object);
    }
}