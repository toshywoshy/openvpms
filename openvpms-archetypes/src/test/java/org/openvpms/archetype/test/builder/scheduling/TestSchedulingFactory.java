/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.scheduling;

import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * Factory for scheduling archetypes.
 *
 * @author Tim Anderson
 */
public class TestSchedulingFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Constructs a {@link TestSchedulingFactory}.
     *
     * @param service the archetype service
     */
    public TestSchedulingFactory(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Creates a new schedule at a practice location.
     *
     * @param location the practice location
     * @return the schedule
     */
    public Entity createSchedule(Party location) {
        return newSchedule().location(location).build();
    }

    /**
     * Returns a builder to create a new schedule.
     *
     * @return the builder
     */
    public TestScheduleBuilder newSchedule() {
        return new TestScheduleBuilder(service);
    }

    /**
     * Creates an appointment type.
     *
     * @return the appointment type
     */
    public Entity createAppointmentType() {
        return newAppointmentType().build();
    }

    /**
     * Returns a builder to create an appointment type.
     *
     * @return the builder
     */
    public TestAppointmentTypeBuilder newAppointmentType() {
        return new TestAppointmentTypeBuilder(service);
    }

    /**
     * Creates a new roster area at a practice location.
     *
     * @param location the practice location
     * @return the roster area
     */
    public Entity createRosterArea(Party location) {
        return newRosterArea().location(location).build();
    }

    /**
     * Creates a new roster area at a practice location.
     *
     * @param location  the practice location
     * @param schedules the schedules associated with the roster area
     * @return the roster area
     */
    public Entity createRosterArea(Party location, Entity... schedules) {
        return newRosterArea()
                .location(location)
                .schedules(schedules)
                .build();
    }

    /**
     * Returns a builder to create a new roster area.
     *
     * @return the builder
     */
    public TestRosterAreaBuilder newRosterArea() {
        return new TestRosterAreaBuilder(service);
    }

    /**
     * Creates a new schedule view.
     *
     * @param schedules the schedules
     * @return the schedule view
     */
    public Entity createScheduleView(Entity... schedules) {
        return newScheduleView().schedules(schedules).build();
    }

    /**
     * Returns a builder to create a new schedule view.
     *
     * @return the builder
     */
    public TestScheduleViewBuilder newScheduleView() {
        return new TestScheduleViewBuilder(service);
    }
}
