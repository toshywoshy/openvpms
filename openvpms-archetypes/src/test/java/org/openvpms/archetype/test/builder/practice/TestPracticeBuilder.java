/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.practice;

import org.openvpms.archetype.rules.practice.PracticeArchetypes;
import org.openvpms.archetype.test.builder.lookup.TestCurrencyBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.archetype.test.builder.party.AbstractTestPartyBuilder;
import org.openvpms.archetype.test.builder.user.TestUserBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Contact;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.model.user.User;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>party.organisationPractice</em> instances, for testing purposes.
 * <p/>
 * Note that the <em>party.organisationPractice</em> is a singleton, so any saved instance will be returned.
 *
 * @author Tim Anderson
 */
public class TestPracticeBuilder extends AbstractTestPartyBuilder<Party, TestPracticeBuilder> {

    /**
     * The tax rates.
     */
    private final List<Lookup> taxes = new ArrayList<>();

    /**
     * The currency.
     */
    private String currency = "AUD";

    /**
     * The service user.
     */
    private User serviceUser;

    /**
     * The practice locations.
     */
    private Party[] locations;

    /**
     * Constructs a {@link TestPracticeBuilder}.
     *
     * @param service the archetype service
     */
    public TestPracticeBuilder(ArchetypeService service) {
        super(PracticeArchetypes.PRACTICE, Party.class, service);
        name("zpractice");
    }

    /**
     * Sets the practice locations.
     *
     * @param locations the locations
     * @return this
     */
    public TestPracticeBuilder locations(Party... locations) {
        this.locations = locations;
        return this;
    }

    /**
     * Sets the currency.
     *
     * @param currency the currency
     * @return this
     */
    public TestPracticeBuilder currency(String currency) {
        this.currency = currency;
        return this;
    }

    /**
     * Adds a tax type.
     *
     * @param rate the tax rate
     * @return this
     */
    public TestPracticeBuilder addTaxType(BigDecimal rate) {
        Lookup lookup = new TestLookupBuilder("lookup.taxType", getService())
                .code(ValueStrategy.random("ZTAXTYPE").toString())
                .build(false);
        IMObjectBean bean = getBean(lookup);
        bean.setValue("rate", rate);
        bean.save();
        taxes.add(lookup);
        return this;
    }

    /**
     * Sets the service user.
     * <p/>
     * This creates a random user.
     *
     * @return this
     */
    public TestPracticeBuilder serviceUser() {
        return serviceUser(new TestUserBuilder(getService()).build());
    }

    /**
     * Sets the service user.
     *
     * @param serviceUser the service user
     * @return this
     */
    public TestPracticeBuilder serviceUser(User serviceUser) {
        this.serviceUser = serviceUser;
        return this;
    }

    /**
     * Returns the object to build.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    @Override
    protected Party getObject(String archetype) {
        ArchetypeService service = getService();
        CriteriaBuilder builder = service.getCriteriaBuilder();
        CriteriaQuery<Party> query = builder.createQuery(Party.class);
        Root<Party> root = query.from(Party.class, archetype);
        query.where(builder.equal(root.get("active"), true));
        Party practice = service.createQuery(query).getFirstResult();
        if (practice != null) {
            IMObjectBean bean = service.getBean(practice);
            // remove contacts
            for (Contact contact : bean.getValues("contacts", Contact.class)) {
                practice.removeContact(contact);
            }

            // remove locations
            for (Party location : bean.getTargets("locations", Party.class)) {
                bean.removeTarget("locations", location);
            }

            // remove taxes
            for (Lookup tax : bean.getValues("taxes", Lookup.class)) {
                bean.removeValue("taxes", tax);
            }

            bean.setValue("useLocationProducts", false);
            bean.setValue("useLoggedInClinician", true);
            bean.removeValues("serviceUser");
        } else {
            practice = super.getObject(archetype);
        }
        return practice;
    }

    /**
     * Builds the party.
     *
     * @param object the party to build
     * @param bean   a bean wrapping the party
     * @param toSave objects to save, if the entity is to be saved
     */
    @Override
    protected void build(Party object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (locations != null) {
            for (Party location : locations) {
                bean.addTarget("locations", location, "practice");
                toSave.add(location);
            }
            locations = null; // can't reuse
        }
        Lookup currencyLookup = new TestCurrencyBuilder(getService()).code(currency).build();
        bean.setValue("currency", currencyLookup.getCode());
        if (serviceUser != null) {
            bean.setTarget("serviceUser", serviceUser);
        }
        for (Lookup tax : taxes) {
            object.addClassification(tax);
        }
        taxes.clear();
    }
}
