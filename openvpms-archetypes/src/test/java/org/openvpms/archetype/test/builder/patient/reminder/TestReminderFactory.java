/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2022 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient.reminder;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.rules.patient.reminder.ReminderTestHelper;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.archetype.test.builder.doc.TestDocumentFactory;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.InputStream;

import static org.junit.Assert.assertNotNull;

/**
 * Factory for creating patient reminders.
 *
 * @author Tim Anderson
 */
public class TestReminderFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document factory.
     */
    private final TestDocumentFactory documentFactory;

    /**
     * Creates a new {@link TestReminderFactory}.
     *
     * @param service         the archetype service
     * @param documentFactory the document factory
     */
    public TestReminderFactory(ArchetypeService service, TestDocumentFactory documentFactory) {
        this.service = service;
        this.documentFactory = documentFactory;
    }

    /**
     * Creates a reminder type with a one-month interval.
     *
     * @return a new reminder type
     */
    public Entity createReminderType() {
        return newReminderType().defaultInterval(1, DateUnits.MONTHS).build();
    }

    /**
     * Returns a builder for a new reminder type.
     *
     * @return a reminder type builder
     */
    public TestReminderTypeBuilder newReminderType() {
        return new TestReminderTypeBuilder(service);
    }

    /**
     * Creates an <em>entity.documentTemplate</em> for a vaccination reminder.
     *
     * @return a new vaccination certificate
     */
    public Entity createVaccinationReminderTemplate() {
        String file = "/Vaccination Reminders.jrxml";
        String mimeType = "text/xml";
        InputStream stream = ReminderTestHelper.class.getResourceAsStream(file);
        assertNotNull(stream);

        return documentFactory.newTemplate()
                .type(PatientArchetypes.DOCUMENT_FORM)
                .document(file, mimeType)
                .build();
    }
}