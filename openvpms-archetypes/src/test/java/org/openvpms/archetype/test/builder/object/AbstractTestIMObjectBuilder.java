/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.object;

import org.openvpms.component.model.act.ActIdentity;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A builder of {@link IMObject} instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestIMObjectBuilder<T extends IMObject, B extends AbstractTestIMObjectBuilder<T, B>> {

    /**
     * The type.
     */
    private final Class<T> type;

    /**
     * The object to update.
     */
    private final T existing;

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Collects objects to be saved.
     */
    private final Set<IMObject> collector = new LinkedHashSet<>();

    /**
     * The archetype to build.
     */
    private String archetype;

    /**
     * The name value strategy;
     */
    private ValueStrategy name = ValueStrategy.defaultValue();

    /**
     * The description value strategy.
     */
    private ValueStrategy description = ValueStrategy.defaultValue();

    /**
     * Determines if the object is active or not.
     */
    private Boolean active;

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param type    the type
     * @param service the archetype service
     */
    public AbstractTestIMObjectBuilder(Class<T> type, ArchetypeService service) {
        this(null, null, type, service);
    }

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param archetype the archetype to build
     * @param type      the type
     * @param service   the archetype service
     */
    public AbstractTestIMObjectBuilder(String archetype, Class<T> type, ArchetypeService service) {
        this(null, archetype, type, service);
    }

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    @SuppressWarnings("unchecked")
    public AbstractTestIMObjectBuilder(T object, ArchetypeService service) {
        this(object, object.getArchetype(), (Class<T>) object.getClass(), service);
    }

    /**
     * Constructs an {@link AbstractTestIMObjectBuilder}.
     *
     * @param object    the object to update. May be {@code null}
     * @param archetype the archetype. May be {@code null}
     * @param type      the type
     * @param service   the archetype service
     */
    private AbstractTestIMObjectBuilder(T object, String archetype, Class<T> type, ArchetypeService service) {
        this.existing = object;
        this.archetype = archetype;
        this.type = type;
        this.service = service;
    }

    /**
     * Sets the object name.
     *
     * @param name the name
     * @return this
     */
    public B name(String name) {
        return name(ValueStrategy.value(name));
    }

    /**
     * Sets the object name.
     *
     * @param name the name
     * @return this
     */
    public B name(ValueStrategy name) {
        this.name = name;
        return getThis();
    }

    /**
     * Sets the object description.
     *
     * @param description the description
     * @return this
     */
    public B description(String description) {
        return description(ValueStrategy.value(description));
    }

    /**
     * Sets the object description.
     *
     * @param description the description
     * @return this
     */
    public B description(ValueStrategy description) {
        this.description = description;
        return getThis();
    }

    /**
     * Determines if the object is active or not.
     *
     * @param active if {@code true} the object is active, otherwise it is inactive
     * @return this
     */
    public B active(boolean active) {
        this.active = active;
        return getThis();
    }

    /**
     * Collect objects that need to be saved when this is saved.
     * <p/>
     * This can be used by child builders to pass built objects back.
     */
    public B collect(Collection<IMObject> objects) {
        collector.addAll(objects);
        return getThis();
    }

    /**
     * Builds the object.
     * <p/>
     * This implementation saves it.
     *
     * @return the object
     */
    public T build() {
        return build(true);
    }

    /**
     * Builds the object.
     *
     * @param save if {@code true}, save the object, and any related objects
     * @return the entity
     */
    public T build(boolean save) {
        Set<IMObject> toSave = new HashSet<>();
        T entity = build(toSave);
        if (save) {
            service.save(toSave);
        }
        return entity;
    }

    /**
     * Builds the object, collecting the objects built without saving them.
     *
     * @param objects collects the built objects
     * @return the primary object
     */
    public T build(Set<IMObject> objects) {
        T entity = getObject(archetype);
        IMObjectBean bean = service.getBean(entity);
        build(entity, bean, objects);
        objects.add(entity);
        if (!collector.isEmpty()) {
            objects.addAll(collector);
            collector.clear();
        }
        return entity;
    }

    /**
     * Returns the object to build.
     * <p/>
     * This implementation returns the existing instance, if one was supplied at construction and matches
     * the specified archetype else it returns a new instance.
     * <p/>
     * For builders that require unique instances, it may return an existing instance.
     *
     * @param archetype the archetype
     * @return the object to build
     */
    protected T getObject(String archetype) {
        T result;
        if (existing != null && existing.isA(archetype)) {
            result = existing;
        } else {
            result = service.create(archetype, type);
        }
        return result;
    }

    /**
     * Sets the archetype to create.
     *
     * @param archetype the archetype
     * @return this
     */
    protected B archetype(String archetype) {
        this.archetype = archetype;
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    protected void build(T object, IMObjectBean bean, Set<IMObject> toSave) {
        name.setValue(bean, "name");
        description.setValue(bean, "description");
        if (active != null) {
            object.setActive(active);
        }
    }

    /**
     * Returns the name value strategy.
     *
     * @return the name value strategy
     */
    protected ValueStrategy getName() {
        return name;
    }

    /**
     * Returns this.
     * <p/>
     * This is used to avoid a proliferation of unchecked casts.
     *
     * @return this
     */
    @SuppressWarnings("unchecked")
    protected B getThis() {
        return (B) this;
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected ArchetypeService getService() {
        return service;
    }

    /**
     * Returns a bean for an object.
     *
     * @param object the object
     * @return the bean
     */
    protected IMObjectBean getBean(IMObject object) {
        return service.getBean(object);
    }

    /**
     * Helper to create an {@link EntityIdentity}.
     *
     * @param archetype the identity archetype
     * @param id        the identity
     * @return a new entity identity
     */
    protected EntityIdentity createEntityIdentity(String archetype, ValueStrategy id) {
        EntityIdentity identity = service.create(archetype, EntityIdentity.class);
        id.setValue(getBean(identity), "identity");
        return identity;
    }

    /**
     * Helper to create an {@link ActIdentity}.
     *
     * @param archetype the identity archetype
     * @param id        the identity
     * @return a new act identity
     */
    protected ActIdentity createActIdentity(String archetype, ValueStrategy id) {
        ActIdentity identity = service.create(archetype, ActIdentity.class);
        id.setValue(getBean(identity), "identity");
        return identity;
    }

}
