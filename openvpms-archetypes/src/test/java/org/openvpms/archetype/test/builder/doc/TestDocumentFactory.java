/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.doc;

import org.apache.commons.io.IOUtils;
import org.openvpms.archetype.rules.doc.DocumentArchetypes;
import org.openvpms.archetype.rules.doc.DocumentHandler;
import org.openvpms.archetype.rules.doc.DocumentHandlers;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.document.Document;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertNotNull;

/**
 * Factory for creating documents and document templates, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestDocumentFactory {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The document handlers.
     */
    private final DocumentHandlers handlers;

    /**
     * Constructs an {@link TestDocumentFactory}.
     *
     * @param service  the archetype service
     * @param handlers the document handlers
     */
    public TestDocumentFactory(ArchetypeService service, DocumentHandlers handlers) {
        this.service = service;
        this.handlers = handlers;
    }

    /**
     * Creates a blank document.
     *
     * @return a blank document
     */
    public Document createBlankDocument() {
        return createDocument("/template-blank.jrxml");
    }

    /**
     * Creates a document from a resource.
     *
     * @param path the resource path
     * @return a new document
     */
    public Document createDocument(String path) {
        DocumentHandler handler = handlers.get(path, null);
        assertNotNull(handler);
        InputStream stream = getClass().getResourceAsStream(path);
        assertNotNull(stream);
        return handler.create(path, stream, null, -1);
    }

    /**
     * Creates an <em>entity.documentTemplate</em> with a blank document.
     *
     * @param type the document template type
     * @return a new template
     */
    public Entity createTemplate(String type) {
        return newTemplate().type(type).blankDocument().build();
    }

    /**
     * Returns a builder for a template.
     *
     * @return a template builder
     */
    public TestDocumentTemplateBuilder newTemplate() {
        return new TestDocumentTemplateBuilder(service, handlers);
    }

    /**
     * Returns a builder to update a template.
     *
     * @param template the template
     * @return a template builder
     */
    public TestDocumentTemplateBuilder updateTemplate(Entity template) {
        return new TestDocumentTemplateBuilder(template, service, handlers);
    }

    /**
     * Returns a builder for a user email template.
     *
     * @return an email template builder
     */
    public TestEmailTemplateBuilder newEmailTemplate() {
        return newEmailTemplate(DocumentArchetypes.USER_EMAIL_TEMPLATE);
    }

    /**
     * Returns a builder for an email template.
     *
     * @param archetype the email template archetype
     * @return an email template builder
     */
    public TestEmailTemplateBuilder newEmailTemplate(String archetype) {
        return new TestEmailTemplateBuilder(archetype, service, handlers);
    }

    /**
     * Returns a builder to update an email template.
     *
     * @param template the template
     * @return a template builder
     */
    public TestEmailTemplateBuilder updateEmailTemplate(Entity template) {
        return new TestEmailTemplateBuilder(template, service, handlers);
    }

    /**
     * Attaches a document to a template.
     *
     * @param template the template. May be an <em>entity.documentTemplate</em> or
     *                 <em>entity.documentTemplateEmail*</em>
     * @param document the document
     * @return the template act
     */
    public DocumentAct attachDocument(Entity template, Document document) {
        AbstractTestDocumentTemplateBuilder<?> builder;
        if (template.isA(DocumentArchetypes.DOCUMENT_TEMPLATE)) {
            builder = updateTemplate(template);
        } else if (template.isA(DocumentArchetypes.EMAIL_TEMPLATES)) {
            builder = updateEmailTemplate(template);
        } else {
            throw new IllegalArgumentException("Unsupported template " + template.getArchetype());
        }
        builder.document(document).build();
        return builder.getTemplateAct();
    }

    /**
     * Returns a builder for an SMS template.
     *
     * @param archetype the SMS template archetype
     * @return a new builder
     */
    public TestSMSTemplateBuilder newSMSTemplate(String archetype) {
        return new TestSMSTemplateBuilder(archetype, service);
    }

    /**
     * Converts a document to string.
     *
     * @param document the document
     * @return the string form of the document
     * @throws IOException for any I/O error
     */
    public String toString(Document document) throws IOException {
        return IOUtils.toString(handlers.get(document).getContent(document), StandardCharsets.UTF_8);
    }

}
