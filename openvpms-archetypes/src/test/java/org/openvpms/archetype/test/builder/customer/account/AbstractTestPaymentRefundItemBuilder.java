/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Builds <em>act.customerAccountPayment*</em> and <em>act.customerAccountRefund*</em></emn> item instances, for testing
 * purposes.
 *
 * @author Tim Anderson
 */
public class AbstractTestPaymentRefundItemBuilder<P extends AbstractTestPaymentRefundBuilder<P>,
        B extends AbstractTestPaymentRefundItemBuilder<P, B>>
        extends AbstractTestIMObjectBuilder<FinancialAct, B> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The amount.
     */
    private BigDecimal amount;

    /**
     * Constructs a {@link AbstractTestPaymentRefundItemBuilder}.
     *
     * @param parent    the parent builder
     * @param archetype the item archetype
     * @param service   the archetype service
     */
    public AbstractTestPaymentRefundItemBuilder(P parent, String archetype, ArchetypeService service) {
        super(archetype, FinancialAct.class, service);
        this.parent = parent;
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     * @return this
     */
    public B amount(int amount) {
        return amount(BigDecimal.valueOf(amount));
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     * @return this
     */
    public B amount(BigDecimal amount) {
        this.amount = amount;
        return getThis();
    }

    /**
     * Adds the item to the parent payment.
     *
     * @return the parent payment builder
     */
    public P add() {
        Set<IMObject> objects = new HashSet<>();
        FinancialAct item = build(objects);
        parent.add(item);
        parent.collect(objects);
        return parent;
    }

    /**
     * Returns the parent builder.
     *
     * @return the parent builder
     */
    public P getParent() {
        return parent;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (amount != null) {
            object.setTotal(amount);
        }
    }
}
