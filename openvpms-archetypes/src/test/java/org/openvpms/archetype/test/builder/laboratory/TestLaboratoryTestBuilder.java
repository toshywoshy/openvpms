/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.laboratory;

import org.openvpms.archetype.rules.laboratory.LaboratoryArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.entity.TestEntityIdentityBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityIdentity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.laboratoryTest</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestLaboratoryTestBuilder extends AbstractTestEntityBuilder<Entity, TestLaboratoryTestBuilder> {

    /**
     * The test code archetype.
     */
    private String testCodeArchetype;

    /**
     * The test code.
     */
    private String testCode;

    /**
     * The test code name.
     */
    private ValueStrategy testCodeName = ValueStrategy.defaultValue();

    /**
     * The investigation type.
     */
    private Entity investigationType;

    /**
     * Constructs a {@link TestLaboratoryTestBuilder}.
     *
     * @param service the archetype service
     */
    public TestLaboratoryTestBuilder(ArchetypeService service) {
        super(LaboratoryArchetypes.TEST, Entity.class, service);
        name("ztest");
    }

    /**
     * Sets the test code.
     *
     * @param archetype the test code archetype
     * @param code      the test code
     * @return this
     */
    public TestLaboratoryTestBuilder code(String archetype, String code) {
        testCodeArchetype = archetype;
        testCode = code;
        return this;
    }

    /**
     * Sets the test code.
     *
     * @param archetype the test code archetype
     * @param code      the test code
     * @param name the test code name
     * @return this
     */
    public TestLaboratoryTestBuilder code(String archetype, String code, String name) {
        code(archetype, code);
        testCodeName = ValueStrategy.value(name);
        return this;
    }

    /**
     * Sets the investigation type.
     *
     * @param investigationType the investigation type
     * @return this
     */
    public TestLaboratoryTestBuilder investigationType(Entity investigationType) {
        this.investigationType = investigationType;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (testCode != null) {
            EntityIdentity identity = new TestEntityIdentityBuilder(testCodeArchetype, getService())
                    .identity(testCode)
                    .name(testCodeName)
                    .build();
            object.addIdentity(identity);
        }
        if (investigationType != null) {
            bean.setTarget("investigationType", investigationType);
        }
    }
}
