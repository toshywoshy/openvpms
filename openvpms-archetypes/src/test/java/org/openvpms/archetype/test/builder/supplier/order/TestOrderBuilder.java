/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.supplier.order;

import org.openvpms.archetype.rules.supplier.DeliveryStatus;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.builder.act.AbstractTestActBuilder;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builds <em>act.supplierOrder</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestOrderBuilder extends AbstractTestActBuilder<FinancialAct, TestOrderBuilder> {

    /**
     * The items.
     */
    private final List<FinancialAct> items = new ArrayList<>();

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The stock location.
     */
    private Party stockLocation;

    /**
     * The delivery status.
     */
    private DeliveryStatus deliveryStatus;

    /**
     * Constructs a {@link TestOrderBuilder}.
     *
     * @param service the archetype service
     */
    public TestOrderBuilder(ArchetypeService service) {
        super(SupplierArchetypes.ORDER, FinancialAct.class, service);
    }

    /**
     * Sets the supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public TestOrderBuilder supplier(Party supplier) {
        this.supplier = supplier;
        return this;
    }

    /**
     * Sets the stock location.
     *
     * @param stockLocation the stock location
     * @return this
     */
    public TestOrderBuilder stockLocation(Party stockLocation) {
        this.stockLocation = stockLocation;
        return this;
    }

    /**
     * Sets the delivery status.
     *
     * @param deliveryStatus the delivery status
     * @return this
     */
    public TestOrderBuilder deliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
        return this;
    }

    /**
     * Returns a builder to add an order item.
     *
     * @return an order item builder
     */
    public TestOrderItemBuilder item() {
        return new TestOrderItemBuilder(this, getService());
    }

    /**
     * Adds an order item.
     *
     * @param item the order item
     * @return this
     */
    public TestOrderBuilder add(FinancialAct item) {
        items.add(item);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (supplier != null) {
            bean.setTarget("supplier", supplier);
        }
        if (stockLocation != null) {
            bean.setTarget("stockLocation", stockLocation);
        }
        if (deliveryStatus != null) {
            object.setStatus2(deliveryStatus.toString());
        }
        BigDecimal tax = BigDecimal.ZERO;
        BigDecimal total = BigDecimal.ZERO;
        for (FinancialAct item : items) {
            bean.addTarget("items", item, "order");
            toSave.add(item);
            tax = tax.add(item.getTaxAmount());
            total = total.add(item.getTotal());
        }
        object.setTotal(tax);
        object.setTotal(total);
        items.clear(); // can't reuse
    }
}