/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.customer.account;

import org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes;
import org.openvpms.component.business.domain.im.act.FinancialAct;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builds <em>act.customerAccountInvoiceItem</em> instances, for testing purposes
 *
 * @author Tim Anderson
 */
public class TestInvoiceItemBuilder extends AbstractTestCustomerChargeItemBuilder<TestInvoiceBuilder,
        TestInvoiceItemBuilder> {

    /**
     * The medication act.
     */
    private Act medication;

    /**
     * Constructs a {@link TestInvoiceItemBuilder}.
     *
     * @param parent  the parent builder
     * @param service the archetype service
     */
    public TestInvoiceItemBuilder(TestInvoiceBuilder parent, ArchetypeService service) {
        super(parent, CustomerAccountArchetypes.INVOICE_ITEM, service);
    }

    /**
     * Associates a medication act with the invoice item.
     *
     * @param medication the medication act
     * @return this
     */
    public TestInvoiceItemBuilder medication(Act medication) {
        this.medication = medication;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(FinancialAct object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (medication != null) {
            bean.addTarget("dispensing", medication, "invoiceItem");
            toSave.add(medication);
        }
    }
}
