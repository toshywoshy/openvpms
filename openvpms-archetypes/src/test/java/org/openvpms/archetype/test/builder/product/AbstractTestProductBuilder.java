/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Builder for <em>product.*</em> archetypes, for testing purposes.
 *
 * @author Tim Anderson
 */
public abstract class AbstractTestProductBuilder<T extends AbstractTestProductBuilder<T>>
        extends AbstractTestEntityBuilder<Product, T> {

    /**
     * The prices.
     */
    private final List<ProductPrice> prices = new ArrayList<>();

    /**
     * The product suppliers.
     */
    private final List<EntityLink> productSuppliers = new ArrayList<>();

    /**
     * The laboratory tests.
     */
    private Entity[] tests;

    /**
     * The product discounts.
     */
    private Entity[] discounts;

    /**
     * The product type.
     */
    private Entity type;

    /**
     * Constructs a {@link AbstractTestProductBuilder}.
     *
     * @param archetype the product archetype
     * @param service   the archetype service
     */
    public AbstractTestProductBuilder(String archetype, ArchetypeService service) {
        super(archetype, Product.class, service);
        name("zproduct");
    }

    /**
     * Constructs an {@link AbstractTestProductBuilder}.
     *
     * @param object  the object to update
     * @param service the archetype service
     */
    public AbstractTestProductBuilder(Product object, ArchetypeService service) {
        super(object, service);
    }

    /**
     * Adds a fixed price.
     *
     * @param fixedPrice the fixed price
     * @return this
     */
    public T fixedPrice(BigDecimal fixedPrice) {
        return fixedPrice()
                .price(fixedPrice)
                .add();
    }

    /**
     * Returns a builder to add a fixed price.
     *
     * @return a fixed price builder
     */
    public TestFixedPriceBuilder<T> fixedPrice() {
        return new TestFixedPriceBuilder<T>(getThis(), getService());
    }

    /**
     * Adds a unit price.
     *
     * @param unitPrice the fixed price
     * @return this
     */
    public T unitPrice(BigDecimal unitPrice) {
        return unitPrice()
                .price(unitPrice)
                .add();
    }

    /**
     * Returns a builder to add a unit price.
     *
     * @return a fixed price builder
     */
    public TestUnitPriceBuilder<T> unitPrice() {
        return new TestUnitPriceBuilder<>(getThis(), getService());
    }

    /**
     * Adds a price.
     *
     * @param price the price
     */
    public T addPrice(ProductPrice price) {
        prices.add(price);
        return getThis();
    }

    /**
     * Sets the product type.
     *
     * @param type the product type. An <em>entity.productType</em>
     * @return this
     */
    public T type(Entity type) {
        this.type = type;
        return getThis();
    }

    /**
     * Sets the laboratory tests.
     *
     * @param tests the tests
     * @return this
     */
    public T tests(Entity... tests) {
        this.tests = tests;
        return getThis();
    }

    /**
     * Sets the product discounts.
     *
     * @param discounts the discounts
     * @return this
     */
    public T discounts(Entity... discounts) {
        this.discounts = discounts;
        return getThis();
    }

    /**
     * Returns a builder for a product-supplier relationship.
     *
     * @return a new product-supplier builder
     */
    public TestProductSupplierBuilder<T> newProductSupplier() {
        return new TestProductSupplierBuilder<T>(getThis(), getService());
    }

    /**
     * Adds a product-supplier relationship.
     *
     * @param productSupplier the product-supplier relationship
     * @return this
     */
    public T addProductSupplier(EntityLink productSupplier) {
        productSuppliers.add(productSupplier);
        return getThis();
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Product object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        for (ProductPrice price : prices) {
            object.addProductPrice(price);
        }
        if (type != null) {
            bean.setTarget("type", type);
        }
        if (tests != null) {
            for (Entity test : tests) {
                bean.addTarget("tests", test);
            }
        }
        if (discounts != null) {
            for (Entity discount : discounts) {
                bean.addTarget("discounts", discount);
            }
        }
        for (EntityLink productSupplier : productSuppliers) {
            productSupplier.setSource(object.getObjectReference());
            object.addEntityLink(productSupplier);
        }

        // can't re-use
        prices.clear();
        productSuppliers.clear();
    }
}
