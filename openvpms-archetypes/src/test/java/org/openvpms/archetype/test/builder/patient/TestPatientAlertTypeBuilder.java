/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.patient;

import org.openvpms.archetype.rules.patient.PatientArchetypes;
import org.openvpms.archetype.test.builder.entity.AbstractTestEntityBuilder;
import org.openvpms.archetype.test.builder.lookup.TestLookupBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.lookup.Lookup;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entity.patientAlertType</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestPatientAlertTypeBuilder extends AbstractTestEntityBuilder<Entity, TestPatientAlertTypeBuilder> {

    /**
     * The alert priority.
     */
    private String priority;

    /**
     * Determines if the alert is mandatory.
     */
    private Boolean mandatory;

    /**
     * Determines if the alert is interactive.
     */
    private Boolean interactive;

    /**
     * The alert type code.
     */
    private String alertTypeCode;

    /**
     * Constructs a {@link TestPatientAlertTypeBuilder}.
     *
     * @param service the archetype service
     */
    public TestPatientAlertTypeBuilder(ArchetypeService service) {
        super(PatientArchetypes.ALERT_TYPE, Entity.class, service);
        name(ValueStrategy.random("zalerttype"));
    }

    /**
     * Sets the alert priority.
     *
     * @param priority the alert priority
     * @return this
     */
    public TestPatientAlertTypeBuilder priority(String priority) {
        this.priority = priority;
        return this;
    }

    /**
     * Determines if the alert is mandatory.
     *
     * @param mandatory if {@code true}, the alert is mandatory, else it is optional
     * @return this
     */
    public TestPatientAlertTypeBuilder mandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    /**
     * Determines if the alert is interactive.
     *
     * @param interactive if {@code true}, the alert is interactive
     * @return this
     */
    public TestPatientAlertTypeBuilder interactive(boolean interactive) {
        this.interactive = interactive;
        return this;
    }

    /**
     * Sets the alert type code.
     *
     * @param alertTypeCode the alert type code.
     * @return this
     */
    public TestPatientAlertTypeBuilder alertType(String alertTypeCode) {
        this.alertTypeCode = alertTypeCode;
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(Entity object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (priority != null) {
            bean.setValue("priority", priority);
        }

        if (mandatory != null) {
            bean.setValue("mandatory", mandatory);
        }

        if (interactive != null) {
            bean.setValue("interactive", interactive);
        }

        if (alertTypeCode != null) {
            Lookup lookup = new TestLookupBuilder(PatientArchetypes.ALERT_TYPE_LOOKUP, getService())
                    .code(alertTypeCode).build();
            object.addClassification(lookup);
        }
    }
}
