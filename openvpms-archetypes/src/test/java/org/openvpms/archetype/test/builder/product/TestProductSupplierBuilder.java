/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.test.builder.product;

import org.openvpms.archetype.rules.product.ProductArchetypes;
import org.openvpms.archetype.test.builder.object.AbstractTestIMObjectBuilder;
import org.openvpms.archetype.test.builder.object.ValueStrategy;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.EntityLink;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.Set;

/**
 * Builder for <em>entityLink.productSupplier</em> instances, for testing purposes.
 *
 * @author Tim Anderson
 */
public class TestProductSupplierBuilder<P extends AbstractTestProductBuilder<P>>
        extends AbstractTestIMObjectBuilder<EntityLink, TestProductSupplierBuilder<P>> {

    /**
     * The parent builder.
     */
    private final P parent;

    /**
     * The supplier.
     */
    private Party supplier;

    /**
     * The package size.
     */
    private ValueStrategy packageSize = ValueStrategy.defaultValue();

    /**
     * The auto-price-update flag.
     */
    private ValueStrategy autoPriceUpdate = ValueStrategy.defaultValue();

    /**
     * Constructs a {@link TestProductSupplierBuilder}.
     *
     * @param service the archetype service
     */
    public TestProductSupplierBuilder(P parent, ArchetypeService service) {
        super(ProductArchetypes.PRODUCT_SUPPLIER_RELATIONSHIP, EntityLink.class, service);
        this.parent = parent;
    }

    /**
     * Sets the supplier.
     *
     * @param supplier the supplier
     * @return this
     */
    public TestProductSupplierBuilder<P> supplier(Party supplier) {
        this.supplier = supplier;
        return this;
    }

    /**
     * Builds the object.
     *
     * @return the object
     */
    @Override
    public EntityLink build() {
        return build(false);
    }

    /**
     * Adds the relationship to the parent product.
     *
     * @return the parent product builder
     */
    public P add() {
        parent.addProductSupplier(build());
        return parent;
    }

    /**
     * Sets the package size.
     *
     * @param packageSize the package size
     * @return this
     */
    public TestProductSupplierBuilder<P> packageSize(int packageSize) {
        this.packageSize = ValueStrategy.value(packageSize);
        return this;
    }

    /**
     * Sets the auto-price-update flag.
     *
     * @param autoPriceUpdate the auto-price-update flag
     * @return this
     */
    public TestProductSupplierBuilder<P> autoPriceUpdate(boolean autoPriceUpdate) {
        this.autoPriceUpdate = ValueStrategy.value(autoPriceUpdate);
        return this;
    }

    /**
     * Builds the object.
     *
     * @param object the object
     * @param bean   a bean wrapping the object
     * @param toSave objects to save, if the object is to be saved
     */
    @Override
    protected void build(EntityLink object, IMObjectBean bean, Set<IMObject> toSave) {
        super.build(object, bean, toSave);
        if (supplier != null) {
            object.setTarget(supplier.getObjectReference());
        }
        packageSize.setValue(bean, "packageSize");
        autoPriceUpdate.setValue(bean, "autoPriceUpdate");
    }
}