/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.i18n;

import org.junit.Test;
import org.openvpms.archetype.rules.supplier.SupplierArchetypes;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.component.business.domain.im.act.Act;
import org.openvpms.component.business.domain.im.party.Party;
import org.openvpms.component.i18n.Message;

import static org.junit.Assert.assertEquals;

/**
 * Tests the {@link ArchetypeMessages} class.
 *
 * @author Tim Anderson
 */
public class ArchetypeMessagesTestCase extends ArchetypeServiceTest {

    /**
     * Tests the {@link ArchetypeMessages#priceCreatedByDelivery} method.
     */
    @Test
    public void testPriceCreatedByDelivery() {
        Party supplier = create(SupplierArchetypes.SUPPLIER_ORGANISATION, Party.class);
        supplier.setName("Cenvet");
        Act delivery = create(SupplierArchetypes.DELIVERY, Act.class);
        delivery.setId(1005);
        delivery.setActivityStartTime(TestHelper.getDate("2020-02-17"));
        check("ARCH-1501: Price created by delivery 1005 from supplier Cenvet on 17/02/2020",
              ArchetypeMessages.priceCreatedByDelivery(delivery, supplier));
    }

    /**
     * Verifies a message matches that expected.
     *
     * @param expected the expected message
     * @param actual   the actual message
     */
    private void check(String expected, Message actual) {
        assertEquals(expected, actual.toString());
    }
}
