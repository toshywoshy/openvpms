/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.function.eftpos;

import org.apache.commons.jxpath.ExpressionContext;
import org.openvpms.archetype.rules.finance.eft.EFTPOSArchetypes;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.DocumentAct;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.bean.Policies;
import org.openvpms.component.model.bean.Policy;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.object.SequencedRelationship;
import org.openvpms.component.query.criteria.CriteriaBuilder;
import org.openvpms.component.query.criteria.CriteriaQuery;
import org.openvpms.component.query.criteria.Join;
import org.openvpms.component.query.criteria.Root;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.component.system.common.jxpath.FunctionHelper;

import java.util.ArrayList;
import java.util.List;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.PAYMENT;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.REFUND;
import static org.openvpms.component.model.bean.Predicates.targetIsA;

/**
 * JXPath extension functions for EFTPOS transactions.
 *
 * @author Tim Anderson
 */
public class EFTPOSFunctions {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * Policy to return customer receipts ordered by sequence.
     */
    private static final Policy<SequencedRelationship> RECEIPTS = Policies.newPolicy(SequencedRelationship.class)
            .anyObject()
            .predicate(targetIsA(EFTPOSArchetypes.CUSTOMER_RECEIPT))
            .orderBySequence()
            .build();

    /**
     * Printer node.
     */
    private static final String PRINTER = "printer";

    /**
     * Constructs an {@link EFTPOSFunctions}.
     *
     * @param service the archetype service
     */
    public EFTPOSFunctions(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Returns the printable EFTPOS receipts for a payment or refund.
     * <p/>
     * These are the customer receipts that aren't printed by a terminal.
     *
     * @param context the expression context. Expected to refer to a payment or refund
     * @return the EFTPOS receipts
     */
    public Iterable<DocumentAct> printableReceipts(ExpressionContext context) {
        return printableReceipts(context.getContextNodePointer().getValue());
    }

    /**
     * Returns the EFTPOS receipts for a payment or refund for terminals that don't support printing.
     * <p/>
     * These are the customer receipts that aren't printed by a terminal.
     *
     * @param object the payment/refund
     * @return the EFTPOS receipts
     */
    public Iterable<DocumentAct> printableReceipts(Object object) {
        object = FunctionHelper.unwrap(object);
        List<DocumentAct> result = new ArrayList<>();
        if (object instanceof FinancialAct && ((FinancialAct) object).isA(PAYMENT, REFUND)) {
            FinancialAct act = (FinancialAct) object;
            CriteriaBuilder builder = service.getCriteriaBuilder();
            CriteriaQuery<Act> query = builder.createQuery(Act.class);
            Root<Act> root = query.from(Act.class, EFTPOSArchetypes.PAYMENT, EFTPOSArchetypes.REFUND);
            query.select(root);
            // need to query by parentId as declined EFTPOS transaction may have been removed from the payment
            Join<Act, IMObject> parentId = root.join("parentId");
            parentId.on(builder.equal(parentId.get("identity"), Long.toString(act.getId())));
            query.orderBy(builder.asc(root.get("id")));
            for (Act transaction : service.createQuery(query).getResultList()) {
                IMObjectBean bean = service.getBean(transaction);
                Entity terminal = bean.getTarget("terminal", Entity.class);
                if (terminal != null) {
                    IMObjectBean terminalBean = service.getBean(terminal);
                    if (terminalBean.hasNode(PRINTER) && !terminalBean.getBoolean(PRINTER)) {
                        result.addAll(bean.getTargets("receipts", DocumentAct.class, RECEIPTS));
                    }
                }
            }
        }
        return result;
    }
}
