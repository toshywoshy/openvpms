/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.laboratory;

/**
 * Statuses for <em>act.laboratoryOrder</em>.
 *
 * @author Tim Anderson
 */
public class LaboratoryOrderStatus {

    /**
     * Pending order.
     */
    public static final String PENDING = "PENDING";

    /**
     * Order queued for send.
     */
    public static final String QUEUED = "QUEUED";

    /**
     * Order awaiting confirmation before send.
     */
    public static final String CONFIRM = "CONFIRM";

    /**
     * Order is being submitted.
     */
    public static final String SUBMITTING = "SUBMITTING";

    /**
     * Order has been submitted.
     */
    public static final String SUBMITTED = "SUBMITTED";

    /**
     * Order is in error.
     */
    public static final String ERROR = "ERROR";

    /**
     * Order has been completed.
     */
    public static final String COMPLETED = "COMPLETED";

    /**
     * Order has been cancelled.
     */
    public static final String CANCELLED = "CANCELLED";

    /**
     * Default constructor.
     */
    private LaboratoryOrderStatus() {

    }
}
