/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.product;

import org.openvpms.component.model.object.IMObject;
import org.openvpms.component.model.product.Product;
import org.openvpms.component.model.product.ProductPrice;
import org.openvpms.component.service.archetype.ArchetypeService;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper to collect new or updated prices.
 *
 * @author Tim Anderson
 */
public class PriceCollector {

    /**
     * The archetype service.
     */
    private final ArchetypeService service;

    /**
     * The products and their associated prices.
     */
    private final Map<Product, List<ProductPrice>> products = new LinkedHashMap<>();

    /**
     * The prices, in the order they were added.
     */
    private final List<ProductPrice> prices = new ArrayList<>();

    /**
     * Constructs a {@link PriceCollector}.
     *
     * @param service the archetype service
     */
    public PriceCollector(ArchetypeService service) {
        this.service = service;
    }

    /**
     * Adds a product and its new and updated prices.
     *
     * @param product the product
     * @param prices  the prices
     */
    public void add(Product product, List<ProductPrice> prices) {
        List<ProductPrice> current = products.computeIfAbsent(product, k -> new ArrayList<>());
        current.addAll(prices);
        this.prices.addAll(prices);
    }

    /**
     * Returns the new and updated prices.
     *
     * @return the prices
     */
    public List<ProductPrice> getPrices() {
        return prices;
    }

    /**
     * Saves new and updated prices.
     *
     * @return the prices
     */
    public List<ProductPrice> save() {
        List<IMObject> toSave = prepareSave();
        if (!toSave.isEmpty()) {
            service.save(toSave);
        }
        return prices;
    }

    /**
     * Derives values in new and updated prices.
     * <p/>
     * Use this if:
     * <ul>
     *     <li>prices need to be displayed without being saved</li>
     *     <li>registration of new prices on products should be deferred</li>
     * </ul>
     *
     * @return the prices
     */
    public List<ProductPrice> deriveValues() {
        for (ProductPrice price : prices) {
            service.deriveValues(price);
        }
        return prices;
    }

    /**
     * Prepares products and prices for save.
     * <p/>
     * Use this if objects are being saved independently.<br/>
     * This adds new prices to their associated products.
     *
     * @return the products and prices to save
     */
    public List<IMObject> prepareSave() {
        List<IMObject> toSave = new ArrayList<>();
        for (Map.Entry<Product, List<ProductPrice>> entry : products.entrySet()) {
            Product product = entry.getKey();
            boolean saveProduct = false;
            for (ProductPrice price : entry.getValue()) {
                if (price.getProduct() == null) {
                    product.addProductPrice(price);
                    saveProduct = true;
                }
            }
            if (saveProduct) {
                toSave.add(product);             // need to save the product to add new prices
            } else {
                toSave.addAll(entry.getValue()); // just save the prices
            }
        }
        return toSave;
    }

}
