/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.doc;

import org.openvpms.component.service.archetype.ArchetypeService;

/**
 * A {@link DocumentHandler} for <em>document.temporary</em> documents.
 * <p/>
 * These are not compressed.
 *
 * @author Tim Anderson
 */
public class TemporaryDocumentHandler extends AbstractDocumentHandler {

    /**
     * Constructs an {@link AbstractDocumentHandler}.
     *
     * @param service the archetype service
     */
    public TemporaryDocumentHandler(ArchetypeService service) {
        super(DocumentArchetypes.TEMPORARY_DOCUMENT, service, false);
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param name      the document name
     * @param archetype the document archetype
     * @param mimeType  the mime type of the document. May be {@code null}
     * @return {@code true} if this handler supports the document
     */
    @Override
    public boolean canHandle(String name, String archetype, String mimeType) {
        return DocumentArchetypes.TEMPORARY_DOCUMENT.equals(archetype);
    }

    /**
     * Determines if this handler supports a document.
     *
     * @param name     the document name
     * @param mimeType the mime type of the document. May be {@code null}
     * @return {@code false}. Use {@link #canHandle(String, String, String)}
     */
    @Override
    public boolean canHandle(String name, String mimeType) {
        return false;
    }
}
