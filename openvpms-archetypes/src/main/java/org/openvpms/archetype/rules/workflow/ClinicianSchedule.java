/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import org.openvpms.archetype.rules.workflow.roster.RosterService;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.user.User;
import org.openvpms.component.system.common.cache.IMObjectCache;
import org.openvpms.component.system.common.cache.SoftRefIMObjectCache;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Caches clinician roster and appointment data, between two date ranges.
 *
 * @author Tim Anderson
 * @see FreeSlotQuery
 * @see FreeSlotIterator
 */
class ClinicianSchedule {

    /**
     * The clinician.
     */
    private final User clinician;

    /**
     * The start of the date range.
     */
    private final Date from;

    /**
     * The end of the date range.
     */
    private final Date to;

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * The roster service.
     */
    private final RosterService rosterService;

    /**
     * The appointment service.
     */
    private final AppointmentService appointmentService;

    /**
     * The appointment rules.
     */
    private final AppointmentRules rules;

    /**
     * The cache of areas.
     */
    private final IMObjectCache areas;

    /**
     * Roster events (aka shifts).
     */
    private List<RosterService.UserEvent> events;

    /**
     * Appointments the clinician is scheduled for, during the period.
     */
    private RangeSet<Date> appointments;

    /**
     * The practice location, used to determine if the cache of {@link #events} needs to be refreshed.
     */
    private Reference location;

    /**
     * Constructs a {@link ClinicianSchedule}.
     *
     * @param clinician          the clinician
     * @param from               the start of the date range
     * @param to                 the end of the date range
     * @param service            the archetype service
     * @param rosterService      the roster service
     * @param appointmentService the appointment service
     * @param rules              the appointment rules
     */
    ClinicianSchedule(User clinician, Date from, Date to, IArchetypeService service, RosterService rosterService,
                      AppointmentService appointmentService, AppointmentRules rules) {
        this.clinician = clinician;
        this.from = from;
        this.to = to;
        this.service = service;
        this.rosterService = rosterService;
        this.appointmentService = appointmentService;
        this.rules = rules;
        areas = new SoftRefIMObjectCache(service);
    }

    /**
     * Returns the shifts that the clinician is rostered on to, for the given schedule.
     *
     * @param schedule the schedule
     * @return the shifts
     */
    public RangeSet<Date> getShifts(Entity schedule) {
        RangeSet<Date> shifts = TreeRangeSet.create();
        Reference newLocation = getLocation(schedule);
        if (newLocation != null) {
            if (events == null || !Objects.equals(location, newLocation)) {
                events = rosterService.getUserEvents(clinician, newLocation, from, to);
                location = newLocation;
            }
            for (RosterService.UserEvent event : events) {
                Entity area = (Entity) areas.get(event.getArea());
                if (area != null && rules.rosterAreaHasSchedule(area, schedule)) {
                    Range<Date> range = Range.closed(event.getStartTime(), event.getEndTime());
                    shifts.add(range);
                }
            }
        }
        return shifts;
    }

    /**
     * Returns the appointments the clinician is scheduled for, in the date range.
     *
     * @return the appointments
     */
    public RangeSet<Date> getAppointments() {
        if (appointments == null) {
            appointments = TreeRangeSet.create();
            List<ScheduleTimes> times = appointmentService.getAppointmentsForClinician(clinician, from, to);
            for (Times range : times) {
                appointments.add(Range.closed(range.getStartTime(), range.getEndTime()));
            }
        }
        return appointments;
    }

    /**
     * Returns the location of a schedule.
     *
     * @param schedule the schedule
     * @return the practice location
     */
    private Reference getLocation(Entity schedule) {
        IMObjectBean bean = service.getBean(schedule);
        return bean.getTargetRef("location");
    }
}
