/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.workflow;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.iterators.AbstractUntypedIteratorDecorator;
import org.apache.commons.collections4.iterators.FilterIterator;
import org.apache.commons.collections4.iterators.IteratorChain;
import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.joda.time.Period;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.rules.util.DateUnits;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.Constraints;
import org.openvpms.component.system.common.query.NamedQuery;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSet;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Date;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.openvpms.component.system.common.query.Constraints.shortName;

/**
 * An iterator over free slots for a schedule.
 *
 * @author Tim Anderson
 */
class FreeSlotIterator implements Iterator<Slot> {

    /**
     * The underlying iterator.
     */
    private final Iterator<Slot> iterator;

    /**
     * Start time node name.
     */
    private static final String START_TIME = "startTime";

    /**
     * End time node name.
     */
    private static final String END_TIME = "endTime";

    /**
     * Schedule id column.
     */
    private static final String SCHEDULE_ID = "scheduleId";

    /**
     * Constructs a {@link FreeSlotIterator}.
     *
     * @param schedule          the schedule
     * @param fromDate          the date to query from
     * @param toDate            the date to query to
     * @param fromTime          the time to query from. May be {@code null}
     * @param toTime            the time to query to. May be {@code null}
     * @param clinicianSchedule the clinician schedule data, or {@code null} if not filtering slots by clinician
     * @param service           the archetype service
     */
    FreeSlotIterator(Entity schedule, Date fromDate, Date toDate, Period fromTime, Period toTime,
                     ClinicianSchedule clinicianSchedule, IArchetypeService service) {
        IMObjectBean bean = service.getBean(schedule);

        long scheduleStart = getScheduleTime(bean.getDate(START_TIME)); // the time that the schedule starts at
        long scheduleEnd = getScheduleTime(bean.getDate(END_TIME));     // the time that the schedule ends at

        Iterator<ObjectSet> queryIterator = createFreeSlotIterator(schedule, fromDate, toDate, service);
        Iterator<Slot> slotIterator = createFreeSlotAdapter(queryIterator);
        Iterator<Slot> first = createFirstLastFreeSlotIterator(slotIterator, schedule, fromDate, toDate, service);
        if (scheduleStart != -1 || scheduleEnd != -1) {
            // filter free slots outside the schedule opening and closing times, and split those slots that span
            // multiple opening/closing times
            first = new TimeRangeSlotIterator(first, scheduleStart, scheduleEnd);
        }
        if (fromTime != null || toTime != null) {
            // filter free slots outside the time range
            long from = (fromTime != null) ? fromTime.toStandardDuration().getMillis() : -1;
            long to = (toTime != null) ? toTime.toStandardDuration().getMillis() : -1;
            first = new TimeRangeSlotIterator(first, from, to);
        }
        if (clinicianSchedule != null) {
            first = new ClinicianSlotIterator(first, schedule, clinicianSchedule);
        }
        iterator = first;
    }

    /**
     * Returns {@code true} if the iteration has more elements.
     *
     * @return {@code true} if the iteration has more elements
     */
    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    @Override
    public Slot next() {
        return iterator.next();
    }

    /**
     * Removes from the underlying collection the last element returned
     * by this iterator (optional operation).
     *
     * @throws UnsupportedOperationException if invoked
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    /**
     * Creates an iterator to handle the first and last free slot.
     * <p/>
     * These cannot be determined by the findFreeSlots named query without using unions which are slow, so two other
     * queries are issued to find:
     * <ul>
     * <li>the start time of the earliest appointment intersecting the start of the date range (before)</li>
     * <li>the end time of the latest appointment intersecting the end of the date range (after)</li>
     * </ul>
     * If there is an appointment at the start, this is used to add a slot (fromDate, before).
     * <p/>
     * If there is an appointment at the end, this is used to add a slot (after, toDate).
     * <p/>
     * If there are no appointments, then a slot (fromDate, toDate) is added.
     *
     * @param iterator the slot iterator
     * @return an iterator that handles the first free slot in the date range
     */
    private Iterator<Slot> createFirstLastFreeSlotIterator(Iterator<Slot> iterator, Entity schedule, Date fromDate,
                                                           Date toDate, IArchetypeService service) {
        IteratorChain<Slot> result = new IteratorChain<>();
        Date appointmentBefore = getAppointmentBefore(schedule, fromDate, toDate, service);
        Date appointmentAfter = getAppointmentAfter(schedule, fromDate, toDate, service);
        if (appointmentBefore != null || appointmentAfter != null) {
            if (appointmentBefore != null && appointmentBefore.compareTo(fromDate) > 0) {
                Slot slot = new Slot(schedule.getId(), fromDate, appointmentBefore);
                result.addIterator(Collections.singletonList(slot).iterator());
            }
            result.addIterator(iterator);
            if (appointmentAfter != null && appointmentAfter.compareTo(toDate) < 0) {
                Slot slot = new Slot(schedule.getId(), appointmentAfter, toDate);
                result.addIterator(Collections.singletonList(slot).iterator());
            }
        } else {
            // no appointments
            Slot slot = new Slot(schedule.getId(), fromDate, toDate);
            result.addIterator(Collections.singletonList(slot).iterator());
        }
        return result;
    }

    /**
     * Creates an iterator that adapts {@link ObjectSet}s returned by the <em>findFreeSlots</em> query,
     * to {@link Slot}s.
     *
     * @param queryIterator the query iterator to adapt
     * @return the iterator adapter
     */
    private Iterator<Slot> createFreeSlotAdapter(final Iterator<ObjectSet> queryIterator) {
        return new AbstractUntypedIteratorDecorator<ObjectSet, Slot>(queryIterator) {
            @Override
            public Slot next() {
                ObjectSet set = getIterator().next();
                return new Slot(set.getLong(SCHEDULE_ID), set.getDate(START_TIME), set.getDate(END_TIME));
            }
        };
    }

    /**
     * Creates an iterator that uses the <em>findFreeSlots</em> named query, to find free slots.
     *
     * @param schedule the schedule
     * @param fromDate the date to query from
     * @param toDate   the date to query to
     * @param service  the archetype service
     * @return a new iterator over the free slots
     */
    private Iterator<ObjectSet> createFreeSlotIterator(Entity schedule, Date fromDate, Date toDate,
                                                       IArchetypeService service) {
        NamedQuery query = new NamedQuery("findFreeSlots", SCHEDULE_ID, START_TIME, END_TIME);
        query.setParameter("from", fromDate);
        query.setParameter("to", toDate);
        query.setParameter(SCHEDULE_ID, schedule.getId());
        return new ObjectSetQueryIterator(service, query);
    }

    /**
     * Returns the start time of the earliest appointment intersecting the start of the date range.
     *
     * @param schedule the schedule
     * @param fromDate the start of the date range
     * @param toDate   the end of the date range
     * @param service  the archetype service
     * @return the start time of the appointment at the start of the date range, or {@code null} if none exists
     */
    private Date getAppointmentBefore(Entity schedule, Date fromDate, Date toDate, IArchetypeService service) {
        Date result = null;
        ArchetypeQuery query = createAppointmentQuery(schedule, fromDate, toDate);
        query.add(Constraints.sort(START_TIME));
        Iterator<ObjectSet> iter = new ObjectSetQueryIterator(service, query);
        if (iter.hasNext()) {
            ObjectSet set = iter.next();
            result = set.getDate("a.startTime");
        }
        return result;
    }

    /**
     * Returns the end time of the latest appointment intersecting the end of the date range.
     *
     * @param schedule the schedule
     * @param fromDate the start of the date range
     * @param toDate   the end of the date range
     * @param service  the archetype service
     * @return the end time of the appointment after the date range, or {@code null} if none exists
     */
    private Date getAppointmentAfter(Entity schedule, Date fromDate, Date toDate, IArchetypeService service) {
        Date result = null;
        ArchetypeQuery query = createAppointmentQuery(schedule, fromDate, toDate);
        query.add(Constraints.sort(END_TIME, false));
        Iterator<ObjectSet> iter = new ObjectSetQueryIterator(service, query);
        if (iter.hasNext()) {
            ObjectSet set = iter.next();
            result = set.getDate("a.endTime");
        }
        return result;
    }

    /**
     * Creates a query for the start and end times of an appointment falling in the specified date range.
     *
     * @param schedule the appointment schedule
     * @param fromDate the date to query from
     * @param toDate   the date to query to
     * @return a new query
     */
    private ArchetypeQuery createAppointmentQuery(Entity schedule, Date fromDate, Date toDate) {
        ArchetypeQuery query = new ArchetypeQuery(shortName("a", ScheduleArchetypes.APPOINTMENT));
        query.add(new NodeSelectConstraint("a.startTime"));
        query.add(new NodeSelectConstraint("a.endTime"));
        query.add(Constraints.join("schedule").add(Constraints.eq("entity", schedule)));
        query.add(Constraints.or(Constraints.between(START_TIME, fromDate, toDate),
                                 Constraints.between(END_TIME, fromDate, toDate)));
        query.setMaxResults(1);
        return query;
    }

    /**
     * Returns a schedule start or end time, in milliseconds.
     *
     * @param date the date/time
     * @return the time, in milliseconds, or {@code -1} if the date is {@code null} or is 00:00 or 24:00
     */
    private long getScheduleTime(Date date) {
        long result = -1;
        if (date != null) {
            DateTime dateTime = new DateTime(date);
            if (dateTime.getDayOfMonth() < 2) {     // 24 hour schedule represented as 1970-02-01 0:0:0
                result = dateTime.getMillisOfDay();
                if (result == 0) {
                    result = -1;
                }
            }
        }
        return result;
    }


    /**
     * An iterator that filters slots that fall outside a time range, and splits slots that overlap the time range.
     */
    private static class TimeRangeSlotIterator implements Iterator<Slot> {

        /**
         * The filtering iterator. This filters out slots that aren't in the time range.
         */
        private final Iterator<Slot> filter;

        /**
         * The underlying iterator. A push-back iterator is used to handle the case where a free slot spans multiple
         * time ranges. In this case, the slot is split into two or more slots, and pushed back onto the iterator.
         */
        private final Deque<Slot> slots = new ArrayDeque<>();

        /**
         * The start of the time range, in milliseconds, or {@code -1} if there is no start (in which case, the range
         * effectively starts at 12 AM).
         */
        private final long rangeStart;

        /**
         * The end of the time range, in milliseconds, or {@code -1} if there is no end ((in which case, the range
         * effectively ends at 12AM the following day).
         */
        private final long rangeEnd;

        /**
         * Constructs a {@link TimeRangeSlotIterator}.
         *
         * @param iterator   the underlying slot iterator
         * @param rangeStart the start of the time range
         * @param rangeEnd   the end of the time range
         */
        public TimeRangeSlotIterator(Iterator<Slot> iterator, long rangeStart, long rangeEnd) {
            filter = new FilterIterator<>(iterator, new TimeRangePredicate());
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            while (slots.isEmpty() && filter.hasNext()) {
                Slot slot = filter.next();
                add(slot);
            }
            return !slots.isEmpty();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Slot next() {
            return slots.pop();
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator (optional operation).
         *
         * @throws UnsupportedOperationException if invoked
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        /**
         * Adds a slot.
         * <p/>
         * For slots that don't span multiple days, this ensures that the slot start and end times don't exceed those
         * of the range.
         * <p/>
         * Slots that span multiple days are split into a slot per day.
         *
         * @param slot the slot to add
         */
        private void add(Slot slot) {
            Date from = DateRules.getDate(slot.getStartTime());
            Date to = DateRules.getDate(slot.getEndTime());
            if (from.compareTo(to) != 0) {
                add(slot.getSchedule(), getSlotStart(slot.getStartTime()), getRangeEnd(from));
                while ((from = DateRules.getDate(from, 1, DateUnits.DAYS)).compareTo(to) < 0) {
                    add(slot.getSchedule(), getRangeStart(from), getRangeEnd(from));
                }
                add(slot.getSchedule(), getRangeStart(to), getSlotEnd(slot.getEndTime()));
            } else {
                add(slot.getSchedule(), getSlotStart(slot.getStartTime()), getSlotEnd(slot.getEndTime()));
            }
        }

        /**
         * Adds a slot, if it is valid.
         *
         * @param scheduleId the schedule identifier.
         * @param slotStart  the slot start time
         * @param slotEnd    the slot end time
         */
        private void add(long scheduleId, Date slotStart, Date slotEnd) {
            if (slotStart.compareTo(slotEnd) < 0) {
                slots.add(new Slot(scheduleId, slotStart, slotEnd));
            }
        }

        /**
         * Returns the time range start for a particular date.
         *
         * @param date the date
         * @return the schedule start for the date
         */
        private Date getRangeStart(Date date) {
            if (rangeStart == -1) {
                return date;
            }
            return getDateTime(date, rangeStart);
        }

        /**
         * Returns the time range end for a particular date.
         *
         * @param date the date
         * @return the schedule end for the date
         */
        private Date getRangeEnd(Date date) {
            if (rangeEnd == -1) {
                return DateRules.getDate(date, 1, DateUnits.DAYS);
            }
            return getDateTime(date, rangeEnd);
        }

        /**
         * Adjusts a slot start time, if it is before the range start time.
         *
         * @param startTime the slot start time
         * @return the adjusted start time, or {@code startTime}  if it didn't need adjusting
         */
        private Date getSlotStart(Date startTime) {
            Date result = startTime;
            if (rangeStart != -1) {
                long slotStart = getTime(startTime);
                if (slotStart < rangeStart) {
                    result = getDateTime(startTime, rangeStart);
                }
            }
            return result;
        }

        /**
         * Adjusts a slot end time, if it is after the range end time.
         *
         * @param endTime the slot end time
         * @return the adjusted end time, or {@code endTime}  if it didn't need adjusting
         */
        private Date getSlotEnd(Date endTime) {
            Date result = endTime;
            if (rangeEnd != -1) {
                long slotEnd = getTime(endTime);
                if (slotEnd >= rangeEnd) {
                    result = getDateTime(endTime, rangeEnd);
                }
            }
            return result;
        }

        /**
         * Returns a new date/time from the specified date and time
         *
         * @param date the date
         * @param time the time, in milliseconds
         * @return a new date/time
         */
        private Date getDateTime(Date date, long time) {
            MutableDateTime dateTime = new MutableDateTime(date);
            dateTime.setMillisOfDay((int) time);
            return dateTime.toDate();
        }

        /**
         * Returns the time portion of a date/time, in milliseconds.
         *
         * @param date the date/time
         * @return the time, in milliseconds, or {@code -1} if the date is {@code null}
         */
        private long getTime(Date date) {
            if (date != null) {
                return new DateTime(date).getMillisOfDay();
            }
            return -1;
        }

        /**
         * Predicate to exclude slots outside a time range.
         */
        private class TimeRangePredicate implements Predicate<Slot> {

            /**
             * Use the specified parameter to perform a test that returns true or false.
             *
             * @param slot the object to evaluate
             * @return true or false
             */
            @Override
            public boolean evaluate(Slot slot) {
                Date slotStart = slot.getStartTime();
                Date slotEnd = slot.getEndTime();
                Date start = rangeStart != -1 ? getRangeStart(slotStart) : DateRules.getDate(slotStart);
                Date end = rangeEnd != -1 ? getRangeEnd(slotEnd)
                                          : DateRules.getDate(DateRules.getDate(slotStart), 1, DateUnits.DAYS);
                return DateRules.intersects(slotStart, slotEnd, start, end);
            }
        }

    }

    /**
     * An iterator that filters slots that the clinician is rostered on to, excluding any appointments they already
     * have.
     */
    private static class ClinicianSlotIterator implements Iterator<Slot> {

        /**
         * The underlying slot iterator.
         */
        private final Iterator<Slot> iterator;

        /**
         * The schedule.
         */
        private final Entity schedule;

        /**
         * The current slots being processed.
         */
        private final Deque<Slot> slots = new ArrayDeque<>();

        /**
         * The date ranges of the clinician's shifts, within the search range.
         */
        private RangeSet<Date> shifts;


        /**
         * The date ranges of appointments that the clinician is scheduled for within the search range.
         */
        private RangeSet<Date> appointments;


        /**
         * Constructs a {@link ClinicianSlotIterator}.
         *
         * @param iterator the underlying slot iterator
         * @param schedule the schedule
         * @param cache    the cached schedule for the clincian
         */
        ClinicianSlotIterator(Iterator<Slot> iterator, Entity schedule, ClinicianSchedule cache) {
            this.iterator = iterator;
            this.schedule = schedule;

            // determine if the clinician is rostered on to the schedule, within the date range
            shifts = cache.getShifts(schedule);

            // determine any appointments they are scheduled to work on within the date range
            if (!shifts.isEmpty()) {
                // don't bother look for appointments if the clinician isn't rostered on to the schedule
                appointments = cache.getAppointments();
            }
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            boolean result = false;
            if (!shifts.isEmpty()) {
                while (slots.isEmpty() && iterator.hasNext()) {
                    Slot slot = iterator.next();
                    add(slot);
                }
                result = !slots.isEmpty();
            }
            return result;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public Slot next() {
            return slots.pop();
        }

        /**
         * Adds a slot.
         * <p/>
         * This only adds the parts of slot that the clinician is rostered on for, and where they don't have existing
         * appointments.
         *
         * @param slot the slot
         */
        private void add(Slot slot) {
            Date startTime = slot.getStartTime();
            Date endTime = slot.getEndTime();

            // process the slot if the clinician is rostered on during the slot period
            RangeSet<Date> intersectingShifts = shifts.subRangeSet(Range.closed(startTime, endTime));
            for (Range<Date> shift : intersectingShifts.asRanges()) {
                if (!shift.isEmpty()) {
                    // restrict the slot to the shift
                    Date start = DateRules.max(startTime, shift.lowerEndpoint());
                    Date end = DateRules.min(endTime, shift.upperEndpoint());
                    if (start.compareTo(end) < 0) {
                        if (appointments != null && !appointments.isEmpty()) {
                            // The clinician has appointments. Check if any appointments intersect the slot
                            RangeSet<Date> intersects = appointments.subRangeSet(Range.closed(start, end));
                            if (!intersects.isEmpty()) {
                                // need to remove intersecting appointments
                                RangeSet<Date> newRange = TreeRangeSet.create();
                                newRange.add(Range.closed(start, end));
                                newRange.removeAll(intersects);
                                if (!newRange.isEmpty()) {
                                    for (Range<Date> range : newRange.asRanges()) {
                                        if (!range.isEmpty()) {
                                            slots.add(new Slot(schedule.getId(), range.lowerEndpoint(),
                                                               range.upperEndpoint()));
                                        }
                                    }
                                }
                            } else {
                                slots.add(new Slot(schedule.getId(), start, end));
                            }
                        } else {
                            slots.add(new Slot(schedule.getId(), start, end));
                        }
                    }
                }
            }
        }
    }

}
