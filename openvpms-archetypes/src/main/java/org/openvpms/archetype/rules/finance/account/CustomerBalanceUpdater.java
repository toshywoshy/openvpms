/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.account;

import org.apache.commons.collections4.Predicate;
import org.apache.commons.collections4.iterators.FilterIterator;
import org.openvpms.archetype.rules.act.ActStatus;
import org.openvpms.archetype.rules.insurance.InsuranceRules;
import org.openvpms.component.business.service.archetype.ArchetypeServiceException;
import org.openvpms.component.business.service.archetype.IArchetypeService;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.act.FinancialAct;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.object.Reference;
import org.openvpms.component.model.object.Relationship;
import org.openvpms.component.system.common.query.ArchetypeQuery;
import org.openvpms.component.system.common.query.IMObjectQueryIterator;
import org.openvpms.component.system.common.query.NodeSelectConstraint;
import org.openvpms.component.system.common.query.ObjectSetQueryIterator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.DEBITS_CREDITS;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountArchetypes.INVOICE;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountRuleException.ErrorCode.CannotCreateInitialBalance;
import static org.openvpms.archetype.rules.finance.account.CustomerAccountRuleException.ErrorCode.MissingCustomer;


/**
 * Updates the customer account balance.
 *
 * @author Tim Anderson
 */
public class CustomerBalanceUpdater {

    /**
     * The archetype service.
     */
    private final IArchetypeService service;

    /**
     * Balance calculator.
     */
    private final BalanceCalculator calculator;

    /**
     * The insurance rules.
     */
    private final InsuranceRules rules;

    /**
     * Customer node name.
     */
    private static final String CUSTOMER = "customer";

    /**
     * Account balance node name.
     */
    private static final String ACCOUNT_BALANCE = "accountBalance";

    /**
     * Constructs a {@link CustomerBalanceUpdater}.
     *
     * @param service the archetype service
     */
    public CustomerBalanceUpdater(IArchetypeService service) {
        this(service, null);
    }

    /**
     * Constructs a {@link CustomerBalanceUpdater}.
     *
     * @param service the archetype service
     * @param rules   the insurance rules, used to determine if invoices are in gap claims. May be {@code null}
     */
    public CustomerBalanceUpdater(IArchetypeService service, InsuranceRules rules) {
        this.service = service;
        calculator = new BalanceCalculator(service);
        this.rules = rules;
    }

    /**
     * Determines if a customer has any account acts.
     *
     * @param customer the customer reference
     * @return {@code true} if the customer has any account acts, otherwise {@code false}
     */
    public boolean hasAccountActs(Reference customer) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createQuery(customer,
                                                                       CustomerAccountArchetypes.ACCOUNT_ACTS);
        query.setMaxResults(1);
        query.add(new NodeSelectConstraint("id"));
        ObjectSetQueryIterator iterator = new ObjectSetQueryIterator(service, query);
        return iterator.hasNext();
    }

    /**
     * Verifies that a customer has no account acts.
     * <p/>
     * This should be invoked prior to saving an initial balance act for the first time.
     *
     * @param initialBalance the initial balance act
     * @throws CustomerAccountRuleException if there are account acts present
     */
    public void checkInitialBalance(FinancialAct initialBalance) {
        if (initialBalance.isNew()) {
            IMObjectBean bean = service.getBean(initialBalance);
            Reference customer = bean.getTargetRef(CUSTOMER);
            if (customer != null && hasAccountActs(customer)) {
                throw new CustomerAccountRuleException(CannotCreateInitialBalance);
            }
        }
    }

    /**
     * Adds an act to the customer balance.
     * <p/>
     * Should be invoked prior to the act being saved.
     *
     * @param act the act to add
     * @throws CustomerAccountRuleException if the act is posted but contains no customer
     */
    public void addToBalance(FinancialAct act) {
        IMObjectBean bean = service.getBean(act);
        if (hasBalanceParticipation(bean)) {
            if (calculator.isAllocated(act)) {
                // will occur if a non-zero act is changed to a zero act
                bean.removeValues(ACCOUNT_BALANCE);
            }
        } else if (!calculator.isAllocated(act)) {
            addBalanceParticipation(bean);
        }
    }

    /**
     * Updates the balance for the customer associated with the supplied act, if:
     * <ul>
     * <li>the act is POSTED; and</li>
     * <li>it has a <em>participation.customerAccountBalance</em>; and</li>
     * <li>{@link #include} indicates it should be included</li>
     * </ul>
     *
     * @param act the act
     * @throws ArchetypeServiceException    for any archetype service error
     * @throws CustomerAccountRuleException if the act is posted but contains no customer
     */
    public void updateBalance(FinancialAct act) {
        if (ActStatus.POSTED.equals(act.getStatus())) {
            IMObjectBean bean = service.getBean(act);
            if (hasBalanceParticipation(act)) {
                Reference customer = bean.getTargetRef(CUSTOMER);
                if (customer == null) {
                    throw new CustomerAccountRuleException(MissingCustomer, act);
                }
                updateBalance(act, customer);
            }
        }
    }

    /**
     * Determines if an act is already in the customer account balance.
     *
     * @param act the act
     * @return {@code true} if the act has no <em>act.customerAccountBalance</em> participation and has been fully
     * allocated
     */
    public boolean inBalance(FinancialAct act) {
        boolean result = hasBalanceParticipation(act);
        if (!result) {
            result = calculator.isAllocated(act);
        }
        return result;
    }

    /**
     * Calculates the balance for a customer.
     * <p/>
     * This saves the updated acts.
     * <br/>
     * Use this to selectively allocate acts. Unlike {@link #updateBalance(FinancialAct)}, this will not exclude
     * acts.
     * <br/>
     * All acts must be {@code POSTED}.
     *
     * @param act         the act that triggered the update. May be {@code null}
     * @param unallocated the unallocated acts
     * @return a list of the acts that were updated
     */
    public List<FinancialAct> updateBalance(FinancialAct act, Iterator<FinancialAct> unallocated) {
        return updateBalance(act, unallocated, true);
    }

    /**
     * Calculates the balance for a customer.
     * <p/>
     * Use this to selectively allocate acts. Unlike {@link #updateBalance(FinancialAct)}, this will not exclude
     * acts.
     * <br/>
     * All acts must be {@code POSTED}.
     *
     * @param act         the act that triggered the update. May be {@code null}
     * @param unallocated the unallocated acts
     * @param save        if {@code true}, save the updated acts
     * @return a list of the acts that were updated
     */
    public List<FinancialAct> updateBalance(FinancialAct act, Iterator<FinancialAct> unallocated, boolean save) {
        List<BalanceAct> debits = new ArrayList<>();
        List<BalanceAct> credits = new ArrayList<>();

        if (act != null && ActStatus.POSTED.equals(act.getStatus())) {
            add(act, debits, credits);
        }
        while (unallocated.hasNext()) {
            FinancialAct a = unallocated.next();
            if (ActStatus.POSTED.equals(a.getStatus())) {
                add(a, debits, credits);
            }
        }
        List<FinancialAct> modified = new ArrayList<>();
        for (BalanceAct credit : credits) {
            for (ListIterator<BalanceAct> iter = debits.listIterator();
                 iter.hasNext(); ) {
                BalanceAct debit = iter.next();
                allocate(credit, debit);
                if (debit.isAllocated()) {
                    iter.remove();
                }
                if (debit.isDirty()) {
                    modified.add(debit.getAct());
                }
            }
            if (credit.isDirty()) {
                modified.add(credit.getAct());
            }
        }
        if (save && !modified.isEmpty()) {
            // save all updates in the one transaction
            service.save(modified);
        }
        return modified;
    }

    protected void add(FinancialAct act, List<BalanceAct> debits, List<BalanceAct> credits) {
        if (act.isCredit()) {
            if (act.getTotal().signum() != -1) {
                credits.add(new BalanceAct(act));
            } else {
                debits.add(new BalanceAct(act));
            }
        } else {
            if (act.getTotal().signum() != -1) {
                debits.add(new BalanceAct(act));
            } else {
                credits.add(new BalanceAct(act));
            }
        }
    }

    /**
     * Returns unallocated acts for a customer.
     *
     * @param customer the customer
     * @param exclude  the act to exclude. May be {@code null}
     * @return unallocated acts for the customer
     * @throws ArchetypeServiceException for any archetype service error
     */
    protected Iterator<FinancialAct> getUnallocatedActs(Reference customer, Act exclude) {
        ArchetypeQuery query = CustomerAccountQueryFactory.createUnallocatedQuery(customer, DEBITS_CREDITS, exclude);
        return new IMObjectQueryIterator<>(service, query);
    }

    /**
     * Returns the archetype service.
     *
     * @return the archetype service
     */
    protected IArchetypeService getService() {
        return service;
    }

    /**
     * Returns a predicate to determine if an act should be included in balance allocation.
     * <p/>
     * Subclasses may override this to return a predicate that maintains state.
     *
     * @return a predicate that uses {@link #include(FinancialAct)}.
     */
    protected Predicate<FinancialAct> includes() {
        return this::include;
    }

    /**
     * Determines if an act should be included in the balance allocation.
     * <p/>
     * This implementation excludes invoices that are associated with gap claims that have not been CANCELLED, SETTLED,
     * or DECLINED.
     *
     * @param act the act
     * @return {@code true} if the act should be included
     */
    protected boolean include(FinancialAct act) {
        return !isInvoiceInGapClaim(act);
    }

    /**
     * Determines if an act is an invoice in a current gap claim.
     * <p/>
     * If so, this should be excluded from automatic balance allocation.
     *
     * @param act the invoice
     * @return {@code true} if the invoice is in a current gap claim
     */
    private boolean isInvoiceInGapClaim(FinancialAct act) {
        boolean result = false;
        if (rules != null && act.isA(INVOICE)) {
            result = rules.hasCurrentGapClaims(act);
        }
        return result;
    }

    /**
     * Calculates the balance for the supplied customer.
     * <p/>
     * This only allocates acts that are included via the {@link #includes()} predicate.
     *
     * @param act      the act that triggered the update
     * @param customer the customer
     * @throws ArchetypeServiceException for any archetype service error
     */
    private void updateBalance(FinancialAct act, Reference customer) {
        Predicate<FinancialAct> includes = includes();
        if (includes.evaluate(act)) {
            Iterator<FinancialAct> unallocated = getUnallocatedActs(customer, act);

            // wrap the iterator to filter any acts that should be excluded from allocation
            Iterator<FinancialAct> filtered = new FilterIterator<>(unallocated, includes);

            updateBalance(act, filtered);
        }
    }

    /**
     * Adds an <em>participation.customerAccountBalance</em> to an act.
     *
     * @param act the act
     */
    private void addBalanceParticipation(IMObjectBean act) {
        Reference customer = act.getTargetRef(CUSTOMER);
        if (customer == null) {
            throw new CustomerAccountRuleException(MissingCustomer, act.getObject());
        }
        act.setTarget(ACCOUNT_BALANCE, customer);
    }

    /**
     * Determines if an act has an <em>participation.customerAccountBalance<em>.
     *
     * @param act the act
     * @return {@code true} if the participation is present
     */
    private boolean hasBalanceParticipation(FinancialAct act) {
        IMObjectBean bean = service.getBean(act);
        return hasBalanceParticipation(bean);
    }

    /**
     * Determines if an act has an <em>participation.customerAccountBalance<em>.
     *
     * @param act the act
     * @return {@code true} if the participation is present
     */
    private boolean hasBalanceParticipation(IMObjectBean act) {
        return act.getTargetRef(ACCOUNT_BALANCE) != null;
    }

    /**
     * Allocates an amount from a credit to a debit.
     *
     * @param credit the credit act
     * @param debit  the debit act
     */
    private void allocate(BalanceAct credit, BalanceAct debit) {
        BigDecimal creditToAlloc = credit.getAllocatable();
        if (creditToAlloc.compareTo(BigDecimal.ZERO) > 0) {
            // have money to allocate
            BigDecimal debitToAlloc = debit.getAllocatable();
            if (creditToAlloc.compareTo(debitToAlloc) <= 0) {
                // can allocate all the credit
                debit.addAllocated(creditToAlloc);
                debit.addRelationship(credit, creditToAlloc);
                credit.addAllocated(creditToAlloc);
            } else {
                // can allocate some of the credit
                debit.addAllocated(debitToAlloc);
                debit.addRelationship(credit, debitToAlloc);
                credit.addAllocated(debitToAlloc);
            }
        }
    }

    /**
     * Wrapper for performing operations on an act that affects the customer
     * account balance.
     */
    class BalanceAct {

        /**
         * The act to delegate to.
         */
        private final FinancialAct act;

        /**
         * Determines if the act has been modified.
         */
        private boolean dirty;

        BalanceAct(FinancialAct act) {
            this.act = act;
        }

        /**
         * Adds an <em>actRelationship.customerAccountAllocation</em>.
         *
         * @param credit    the credit act
         * @param allocated the allocated amount
         */
        public void addRelationship(BalanceAct credit, BigDecimal allocated) {
            IMObjectBean debitBean = service.getBean(act);
            Relationship relationship = debitBean.addTarget("allocation", credit.getAct(), "allocation");
            IMObjectBean relBean = service.getBean(relationship);
            relBean.setValue("allocatedAmount", allocated);
        }

        /**
         * Determines if the act is a credit or debit.
         *
         * @return {@code true} if the act is a credit, {@code false}
         * if it is a debit
         */
        public boolean isCredit() {
            return act.isCredit();
        }

        /**
         * Returns the underlying act.
         *
         * @return the underlying act
         */
        public FinancialAct getAct() {
            return act;
        }

        /**
         * Determines if the act has been modified.
         *
         * @return {@code true} if the act has been modified
         */
        public boolean isDirty() {
            return dirty;
        }

        /**
         * Returns the amount of this act yet to be allocated.
         *
         * @return the amount yet to be allocated
         */
        BigDecimal getAllocatable() {
            return calculator.getAllocatable(act);
        }

        /**
         * Determines if the act has been fully allocated.
         *
         * @return {@code true} if the act has been full allocated
         */
        boolean isAllocated() {
            return calculator.isAllocated(act);
        }

        /**
         * Adds to the allocated amount. If the act is fully allocated, the
         * <em>participation.customerAccountBalance</em> participation is removed.
         *
         * @param allocated the allocated amount
         */
        void addAllocated(BigDecimal allocated) {
            BigDecimal value = act.getAllocatedAmount().add(allocated);
            act.setAllocatedAmount(value);
            if (isAllocated()) {
                IMObjectBean bean = service.getBean(act);
                bean.removeValues(ACCOUNT_BALANCE);
            }
            dirty = true;
        }

    }

}
