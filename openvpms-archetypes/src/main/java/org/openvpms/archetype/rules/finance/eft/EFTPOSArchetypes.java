/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.archetype.rules.finance.eft;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * EFTPOS archetypes.
 *
 * @author Tim Anderson
 */
public class EFTPOSArchetypes {

    /**
     * EFTPOS terminals.
     */
    public static final String TERMINALS = "entity.EFTPOSTerminal*";

    /**
     * The EFTPOS payment.
     */
    public static final String PAYMENT = "act.EFTPOSPayment";

    /**
     * The EFTPOS refund.
     */
    public static final String REFUND = "act.EFTPOSRefund";

    /**
     * EFTPOS transactions.
     */
    public static final List<String> TRANSACTIONS = Collections.unmodifiableList(Arrays.asList(PAYMENT, REFUND));

    /**
     * Provider transaction identifiers.
     */
    public static final String TRANSACTION_IDS = "actIdentity.EFTPOSTransaction*";

    /**
     * Merchant receipt archetype.
     */
    public static final String MERCHANT_RECEIPT = "act.EFTPOSReceiptMerchant";

    /**
     * Customer receipt archetype.
     */
    public static final String CUSTOMER_RECEIPT = "act.EFTPOSReceiptCustomer";

    /**
     * Parent transaction identifier archetype.
     */
    public static final String PARENT_ID = "actIdentity.EFTPOSParentId";

    /**
     * Authorisation code identifier archetype.
     */
    public static final String AUTH_CODE_ID = "actIdentity.EFTPOSAuthCode";

    /**
     * Retrieval reference number archetype.
     */
    public static final String RRN_ID = "actIdentity.EFTPOSRRN";

    /**
     * System trace audit number archetype.
     */
    public static final String STAN_ID = "actIdentity.EFTPOSSTAN";

    /**
     * Default constructor.
     */
    private EFTPOSArchetypes() {

    }

}
