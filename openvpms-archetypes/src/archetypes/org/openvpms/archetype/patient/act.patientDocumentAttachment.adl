<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="act.patientDocumentAttachment.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.act.DocumentAct"
               displayName="Attachment">
        <node name="id" path="/id" type="java.lang.Long" hidden="true" readOnly="true"/>
        <node name="created" type="java.util.Date" path="/created" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="createdBy" path="/createdBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="updated" type="java.util.Date" path="/updated" minCardinality="0" maxCardinality="1" hidden="true"
              readOnly="true"/>
        <node name="updatedBy" path="/updatedBy" minCardinality="0" maxCardinality="1" hidden="true" readOnly="true"
              type="org.openvpms.component.business.domain.im.common.IMObjectReference" filter="security.user"/>
        <node name="name" type="java.lang.String" path="/name" hidden="true"
              minCardinality="1" derived="true"
              derivedValue="'Patient Attachment'"/>
        <node name="startTime" displayName="Date" path="/activityStartTime" type="java.util.Date"
              minCardinality="1" defaultValue="java.util.Date.new()"/>
        <node name="description" type="java.lang.String" path="/description"/>
        <node name="status" path="/status" type="java.lang.String" minCardinality="1" defaultValue="'IN_PROGRESS'">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="IN_PROGRESS" value="In Progress"/>
                    <property name="COMPLETED" value="Completed"/>
                    <property name="POSTED" value="Finalised"/>
                </propertyList>
                <errorMessage>Invalid Status</errorMessage>
            </assertion>
        </node>
        <node name="printed" path="/printed" type="java.lang.Boolean" defaultValue="false()"/>
        <node name="clinician" path="/participations" type="java.util.HashSet" minCardinality="0" maxCardinality="1"
              filter="participation.clinician"/>
        <node displayName="Attachment" name="document" path="/document" minCardinality="0"
              maxCardinality="1" type="org.openvpms.component.business.domain.im.common.IMObjectReference"
              readOnly="true"/>
        <node name="patient" path="/participations" type="java.util.HashSet"
              minCardinality="1" maxCardinality="1"
              filter="participation.patient" hidden="true"/>
        <node name="fileName" type="java.lang.String" path="/fileName" hidden="true"/>
        <node name="mimeType" type="java.lang.String" path="/mimeType" readOnly="true" hidden="true"/>
        <node name="identities" path="/identities" type="java.util.HashSet" minCardinality="0" maxCardinality="*"
              filter="actIdentity.*" hidden="true"/>
        <node name="versions" path="/sourceActRelationships" type="java.util.HashSet" minCardinality="0"
              maxCardinality="*" filter="actRelationship.patientDocumentAttachmentVersion"/>
        <node name="event" path="/targetActRelationships" type="java.util.HashSet" baseName="TargetActRelationship"
              minCardinality="0" maxCardinality="1" hidden="true" filter="actRelationship.patientClinicalEventItem"/>
        <node name="problem" path="/targetActRelationships" type="java.util.HashSet" baseName="TargetActRelationship"
              minCardinality="0" maxCardinality="1" hidden="true" filter="actRelationship.patientClinicalProblemItem"/>
    </archetype>
</archetypes>
