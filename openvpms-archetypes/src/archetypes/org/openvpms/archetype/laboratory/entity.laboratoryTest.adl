<!--
  ~ Version: 1.0
  ~
  ~ The contents of this file are subject to the OpenVPMS License Version
  ~ 1.0 (the 'License'); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.openvpms.org/license/
  ~
  ~ Software distributed under the License is distributed on an 'AS IS' basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ Copyright 2019 (C) OpenVPMS Ltd. All Rights Reserved.
  -->

<archetypes>
    <archetype name="entity.laboratoryTest.1.0" latest="true"
               type="org.openvpms.component.business.domain.im.common.Entity" displayName="Laboratory Test">
        <node name="id" path="/id" type="java.lang.Long" readOnly="true"/>
        <node name="name" type="java.lang.String" path="/name" minCardinality="1" maxLength="100"/>
        <node name="description" type="java.lang.String" path="/description"/>
        <node name="code" path="/identities" type="java.util.HashSet" baseName="Identity" minCardinality="0"
              maxCardinality="1" filter="entityIdentity.laboratoryTest*" readOnly="true"/>
        <node name="active" path="/active" type="java.lang.Boolean" defaultValue="true()"/>
        <node name="turnaround" path="/details/turnaround" type="java.lang.String" minCardinality="0"
              maxLength="255"/>
        <node name="specimen" path="/details/specimen" type="java.lang.String" minCardinality="0"
              maxLength="5000"/>
        <node name="investigationType" path="/entityLinks" type="java.util.HashSet" baseName="EntityLink"
              minCardinality="1" maxCardinality="1" filter="entityLink.laboratoryTestInvestigationType"/>
        <node name="group" path="/details/group" type="java.lang.Boolean" defaultValue="false()" readOnly="true"/>
        <node name="useDevice" path="/details/useDevice" type="java.lang.String" minCardinality="1"
              readOnly="true" defaultValue="'NO'">
            <assertion name="lookup.local">
                <propertyList name="entries">
                    <property name="YES" value="Yes"/>
                    <property name="NO" value="No"/>
                    <property name="OPTIONAL" value="Optional"/>
                </propertyList>
            </assertion>
        </node>
        <node name="identities" path="/identities" type="java.util.HashSet" baseName="Identity"
              minCardinality="0" maxCardinality="*" readOnly="true" hidden="true"
              filter="entityIdentity.*"/> <!-- for query purposes -->
    </archetype>
</archetypes>
