/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.component.dispatcher.Queues;

import java.util.Collections;
import java.util.List;

/**
 * SMS message queue.
 * <p/>
 * This implementation only supports a single queue.
 *
 * @author Tim Anderson
 */
class SMSQueues implements Queues<SMSDispatcher, SMSQueue> {

    /**
     * The queue.
     */
    private final SMSQueue queue;

    /**
     * The queue in a list.
     */
    private final List<SMSQueue> queues;

    /**
     * Constructs an {@link SMSQueues}.
     *
     * @param dispatcher the SMS message dispatcher
     */
    public SMSQueues(SMSDispatcher dispatcher) {
        queue = new SMSQueue(dispatcher);
        queues = Collections.singletonList(queue);
    }

    /**
     * Returns the queues.
     *
     * @return the queues
     */
    @Override
    public List<SMSQueue> getQueues() {
        return queues;
    }

    /**
     * Returns a queue given its owner.
     *
     * @param owner the queue owner
     * @return the queue
     */
    @Override
    public SMSQueue getQueue(SMSDispatcher owner) {
        return owner == queue.getOwner() ? queue : null;
    }

    /**
     * Destroys the queues.
     */
    @Override
    public void destroy() {
        // no-op
    }
}
