/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.service;

import org.openvpms.archetype.component.dispatcher.Queue;
import org.openvpms.component.model.act.Act;

/**
 * SMS message queue.
 *
 * @author Tim Anderson
 */
class SMSQueue extends Queue<Act, SMSDispatcher> {

    /**
     * Constructs an {@link Queue}.
     *
     * @param owner the owner of the queue
     */
    public SMSQueue(SMSDispatcher owner) {
        super(owner);
    }

    /**
     * Returns the interval to wait after a failure.
     *
     * @return the interval, in seconds
     */
    @Override
    public int getRetryInterval() {
        return 30;
    }

    /**
     * Retrieves the next object.
     *
     * @param owner the owner of the queue
     * @return the next object, or {@code null} if there is none
     */
    @Override
    protected Act getNext(SMSDispatcher owner) {
        return owner.getNext();
    }
}
