/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.mail;

import org.apache.commons.lang3.time.DateUtils;
import org.openvpms.archetype.rules.util.DateRules;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.component.model.act.Act;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.sms.internal.SMSArchetypes;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.OutboundMessage;

import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Base class for SMS tests requiring the archetype service.
 *
 * @author Tim Anderson
 */
public abstract class AbstractSMSTest extends ArchetypeServiceTest {

    /**
     * Creates an <em>entity.SMSConfigEmailGeneric</em>.
     *
     * @param from           the from address
     * @param fromExpression the 'from' address expression
     * @param to             the 'to' address
     * @param toExpression   the 'to' address expression
     * @param textExpression the text expression
     * @return a new configuration
     */
    protected Entity createConfig(String from, String fromExpression, String to, String toExpression,
                                  String textExpression) {
        Entity entity = create(SMSArchetypes.GENERIC_SMS_EMAIL_CONFIG, Entity.class);
        IMObjectBean bean = getBean(entity);
        bean.setValue("name", "Test");
        bean.setValue("from", from);
        bean.setValue("fromExpression", fromExpression);
        bean.setValue("to", to);
        bean.setValue("toExpression", toExpression);
        bean.setValue("textExpression", textExpression);
        return entity;
    }

    /**
     * Verifies an inbound message matches that expected.
     *
     * @param message  the inbound message
     * @param id       the expected provider id. May be {@code null}
     * @param phone    the expected phone
     * @param text     the expected message text
     * @param customer the expected customer. May be {@code null}
     * @param patient  the expected patient. May be {@code null}
     * @param location the expected practice location. May be {@code null}
     * @param outbound the associated outbound message. MAy be {@code null}
     */
    protected void checkMessage(InboundMessage message, String id, String phone, String text, Party customer,
                                Party patient, Party location, OutboundMessage outbound) {
        assertEquals(id, message.getProviderId());
        assertEquals(phone, message.getPhone());
        assertEquals(text, message.getMessage());
        Act act = getAct(message);
        IMObjectBean bean = getBean(act);
        assertEquals(customer, bean.getTarget("customer"));
        assertEquals(patient, bean.getTarget("patient"));
        assertEquals(location, bean.getTarget("location"));
        if (outbound != null) {
            assertEquals(outbound.getId(), message.getOutboundId());
            Act outboundAct = getAct(outbound);
            assertEquals(outboundAct, bean.getSource("sms"));
        } else {
            assertEquals(-1, message.getOutboundId());
        }
    }


    /**
     * Returns the <em>act.smsMessage/em>> associated with a {@link OutboundMessage}.
     *
     * @param message the message. Must be saved
     * @return the corresponding
     */
    protected Act getAct(OutboundMessage message) {
        Act act = getArchetypeService().get(SMSArchetypes.MESSAGE, message.getId(), Act.class);
        assertNotNull(act);
        return act;
    }

    /**
     * Returns the <em>act.smsReply/em>> associated with a {@link InboundMessage}.
     *
     * @param message the message. Must be saved
     * @return the corresponding
     */
    protected Act getAct(InboundMessage message) {
        Act act = getArchetypeService().get(SMSArchetypes.REPLY, message.getId(), Act.class);
        assertNotNull(act);
        return act;
    }

    /**
     * Backdate the updated timestamp in order to detect changes.
     *
     * @param act the outbound message act
     * @return the updated timestamp
     */
    protected OffsetDateTime backdateUpdated(Act act) {
        Date updated = act.getActivityStartTime();
        assertNotNull(updated);
        updated = DateUtils.truncate(updated, Calendar.SECOND); // need to remove millis a MySQL doesn't store them
        updated = new Date(updated.getTime() - 10000);
        act.setActivityStartTime(updated);
        return DateRules.toOffsetDateTime(updated);
    }

}
