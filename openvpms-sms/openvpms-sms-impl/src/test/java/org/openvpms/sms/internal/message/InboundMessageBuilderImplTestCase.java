/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.internal.message;

import org.junit.Before;
import org.junit.Test;
import org.openvpms.archetype.test.builder.customer.TestCustomerFactory;
import org.openvpms.archetype.test.builder.patient.TestPatientFactory;
import org.openvpms.archetype.test.builder.practice.TestPracticeFactory;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.sms.exception.DuplicateSMSException;
import org.openvpms.sms.internal.mail.AbstractSMSTest;
import org.openvpms.sms.message.InboundMessage;
import org.openvpms.sms.message.InboundMessageBuilder;
import org.openvpms.sms.message.OutboundMessage;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.OffsetDateTime;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

/**
 * Tests the {@link InboundMessageBuilderImpl} class.
 *
 * @author Tim Anderson
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class InboundMessageBuilderImplTestCase extends AbstractSMSTest {

    /**
     * The customer factory.
     */
    @Autowired
    private TestCustomerFactory customerFactory;

    /**
     * The patient factory.
     */
    @Autowired
    private TestPatientFactory patientFactory;

    /**
     * The practice factory.
     */
    @Autowired
    private TestPracticeFactory practiceFactory;

    /**
     * The domain service.
     */
    @Autowired
    private DomainService domainService;

    /**
     * The messages service.
     */
    private MessagesImpl messages;

    /**
     * The message builder.
     */
    private InboundMessageBuilder builder;

    /**
     * The test customer.
     */
    private Party customer;

    /**
     * The test patient.
     */
    private Party patient;

    /**
     * The test location.
     */
    private Party location;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        ArchetypeService service = getArchetypeService();
        messages = new MessagesImpl(service, domainService);
        builder = new InboundMessageBuilderImpl(getArchetypeService(), domainService, messages);
        customer = customerFactory.createCustomer();
        patient = patientFactory.createPatient();
        location = practiceFactory.createLocation();
    }

    /**
     * Test messages that are not responses to an outbound message.
     */
    @Test
    public void testInboundMessages() {
        InboundMessage message1 = builder
                .phone("1123456")
                .message("a message")
                .build();

        checkMessage(message1, null, "1123456", "a message", null, null, null, null);

        String id2 = UUID.randomUUID().toString();
        InboundMessage message2 = builder
                .providerId("actIdentity.smsTest", id2)
                .phone("1123456")
                .message("a message")
                .build();

        checkMessage(message2, id2, "1123456", "a message", null, null, null, null);

        String id3 = UUID.randomUUID().toString();
        InboundMessage message3 = builder
                .providerId("actIdentity.smsTest", id3)
                .phone("1123456")
                .message("a message with location")
                .location(location)
                .build();

        checkMessage(message3, id3, "1123456", "a message with location", null, null, location, null);
    }

    /**
     * Tests a message that is a reply to an outbound message.
     * <p/>
     * This should change the {@link OutboundMessage#getUpdated()} timestamp.
     */
    @Test
    public void testReply() {
        OutboundMessageImpl message = (OutboundMessageImpl) messages.getOutboundMessageBuilder()
                .recipient(customer)
                .customer(customer)
                .patient(patient)
                .phone("12345678")
                .message("a message")
                .location(location)
                .build();
        String outboundId = UUID.randomUUID().toString();
        message.state()
                .providerId("actIdentity.smsTest", outboundId)
                .status(OutboundMessage.Status.SENT)
                .build();
        OffsetDateTime updated = backdateUpdated(message.getAct());
        save(message.getAct());  // force the updated timestamp to be in the past in order to detect changes

        String inboundId = UUID.randomUUID().toString();
        InboundMessage reply = builder
                .providerId("actIdentity.smsTest", inboundId)
                .outboundProviderId("actIdentity.smsTest", outboundId)
                .phone("12345678")
                .message("a reply")
                .build();

        checkMessage(reply, inboundId, "12345678", "a reply", customer, patient, location, message);

        // reload the message and verify its updated timestamp has changed
        message = (OutboundMessageImpl) messages.getOutboundMessage(message.getId());
        assertNotEquals(updated, message.getUpdated());
    }

    /**
     * Verifies that a duplicate provider id triggers a {@link DuplicateSMSException}.
     */
    @Test
    public void testDuplicate() {
        String id = UUID.randomUUID().toString();
        builder.providerId("actIdentity.smsTest", id)
                .phone("1123456")
                .message("message 1")
                .build();
        try {
            builder.providerId("actIdentity.smsTest", id)
                    .phone("1123456")
                    .message("message 1")
                    .build();
            fail("Expected DuplicateSMSException");
        } catch (DuplicateSMSException exception) {
            assertEquals("SMS-0030: There is already an inbound message with identifier: " + id,
                         exception.getMessage());
        }
    }
}