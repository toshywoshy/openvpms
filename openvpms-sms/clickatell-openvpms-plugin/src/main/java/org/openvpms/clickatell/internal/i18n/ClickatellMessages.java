/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.i18n;

import org.openvpms.component.i18n.Message;
import org.openvpms.component.i18n.Messages;

/**
 * Messages reported by the Clickatell plugin.
 *
 * @author Tim Anderson
 */
public class ClickatellMessages {

    /**
     * The messages.
     */
    private static Messages messages = new Messages("CLICKATELL", ClickatellMessages.class);

    /**
     * Creates a message for when an SMS cannot be sent to Clickatell due to an exception.
     *
     * @param exception the exception
     * @return a new message
     */
    public static Message exceptionSendingMessage(Throwable exception) {
        return messages.create(1, exception.getMessage());
    }

    /**
     * Creates a message for when an SMS cannot be sent to Clickatell.
     *
     * @param errorCode   the Clickatell error code
     * @param error       the error
     * @param description the error description
     * @return a new message
     */
    public static Message errorSendingMessage(String errorCode, String error, String description) {
        return messages.create(2, errorCode, error, description);
    }

}
