/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell.internal.service;

import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.plugin.service.archetype.ArchetypeInstaller;

/**
 * Installs Clickatell archetypes.
 *
 * @author Tim Anderson
 */
public class ClickatellArchetypeInstaller {

    /**
     * Constructs a {@link ClickatellArchetypeInstaller}.
     *
     * @param installer the archetype installer
     */
    public ClickatellArchetypeInstaller(ArchetypeInstaller installer) {
        install(installer, ClickatellArchetypes.CONFIG);
        install(installer, ClickatellArchetypes.ID);
    }

    /**
     * Installs an archetype.
     *
     * @param installer the installer
     * @param archetype the archetype to install
     */
    private void install(ArchetypeInstaller installer, String archetype) {
        installer.install(getClass(), "/org/openvpms/clickatell/internal/archetype/" + archetype + ".adl");
    }


}
