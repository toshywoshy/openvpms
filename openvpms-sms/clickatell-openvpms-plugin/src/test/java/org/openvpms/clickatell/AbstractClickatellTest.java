/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.clickatell;

import org.junit.Before;
import org.openvpms.archetype.rules.practice.PracticeService;
import org.openvpms.archetype.test.ArchetypeServiceTest;
import org.openvpms.archetype.test.TestHelper;
import org.openvpms.clickatell.internal.ClickatellArchetypes;
import org.openvpms.clickatell.internal.service.ClickatellArchetypeInstaller;
import org.openvpms.component.business.service.archetype.rule.IArchetypeRuleService;
import org.openvpms.component.model.bean.IMObjectBean;
import org.openvpms.component.model.entity.Entity;
import org.openvpms.component.model.party.Party;
import org.openvpms.component.service.archetype.ArchetypeService;
import org.openvpms.domain.internal.factory.DomainService;
import org.openvpms.domain.internal.factory.DomainServiceImpl;
import org.openvpms.plugin.internal.service.archetype.ArchetypeInstallerImpl;
import org.openvpms.plugin.internal.service.archetype.PluginArchetypeService;
import org.openvpms.sms.internal.message.MessagesImpl;
import org.openvpms.sms.message.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Base class for Clickatell tests.
 *
 * @author Tim Anderson
 */
public abstract class AbstractClickatellTest extends ArchetypeServiceTest {

    /**
     * The practice service
     */
    @Autowired
    private PracticeService practiceService;

    /**
     * The transaction manager.
     */
    @Autowired
    private PlatformTransactionManager transactionManager;

    /**
     * The plugin archetype service.
     */
    private ArchetypeService service;

    /**
     * The messages service.
     */
    private Messages messages;

    /**
     * Sets up the test case.
     */
    @Before
    public void setUp() {
        Party practice = practiceService.getPractice();
        if (practiceService.getServiceUser() == null) {
            IMObjectBean bean = getBean(practice);
            bean.setTarget("serviceUser", TestHelper.createUser());
        }

        IArchetypeRuleService ruleService = (IArchetypeRuleService) getArchetypeService();
        service = new PluginArchetypeService(ruleService, getLookupService(), practiceService);
        DomainService domainService = new DomainServiceImpl(service);
        messages = new MessagesImpl(ruleService, domainService);

        ArchetypeService service = getPluginArchetypeService();

        // install archetypes
        new ClickatellArchetypeInstaller(new ArchetypeInstallerImpl(service, transactionManager));

    }

    /**
     * Returns the plugin archetype service.
     *
     * @return the plugin archetype service
     */
    protected ArchetypeService getPluginArchetypeService() {
        return service;
    }

    /**
     * Returns the messages service.
     *
     * @return the messages service
     */
    protected Messages getMessages() {
        return messages;
    }

    /**
     * Creates a Clickatell configuration.
     *
     * @return a new config
     */
    protected Entity createConfig() {
        return createConfig(null);
    }

    /**
     * Creates a Clickatell configuration.
     *
     * @param from the from phone number, used for 2-way messaging. May be {@code null}
     * @return a new config
     */
    protected Entity createConfig(String from) {
        Entity config = create(ClickatellArchetypes.CONFIG, Entity.class);

        IMObjectBean configBean = getBean(config);
        String apiKey = "anAPIKey==";
        configBean.setValue("apiKey", apiKey);
        configBean.setValue("from", from);
        configBean.setValue("countryPrefix", "61");
        configBean.setValue("areaPrefix", "0");
        return config;
    }

}
