/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

import org.openvpms.sms.exception.SMSException;

import java.time.OffsetDateTime;

/**
 * SMS messages service.
 *
 * @author Tim Anderson
 */
public interface Messages {

    /**
     * Returns the number of parts that a message comprises.
     * <p/>
     * The number of parts is determined by the length of the message, and how the message will be encoded.
     * <p/>
     * A message that can be encoded using 7-bit GSM characters will use:
     * <ul>
     * <li>a single part for up to 160 characters</li>
     * <li>multiples of 153 characters for multi-part messages</li>
     * </ul>
     * A message encoded using UCS-2 will use:
     * <ul>
     * <li>a single part for up to 70 characters</li>
     * <li>multiples of 67 characters for multi-part messages</li>
     * </ul>
     *
     * @param message the message
     * @return the number of parts
     */
    int getParts(String message);

    /**
     * Returns an outbound message given its OpenVPMS identifier.
     *
     * @param id the message identifier
     * @return the corresponding message, or {@code null} if none is found
     */
    OutboundMessage getOutboundMessage(long id);

    /**
     * Returns a message given its provider identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return the corresponding message, or {@code null} if none is found
     * @throws SMSException for any SMS error
     */
    OutboundMessage getOutboundMessage(String archetype, String id);

    /**
     * Returns a builder to build outbound messages.
     *
     * @return a message builder
     */
    OutboundMessageBuilder getOutboundMessageBuilder();

    /**
     * Returns messages that have been sent from the specified time.
     * <p/>
     * Note that the returned iterator may be paged; when iterating over the messages, any status changes will
     * affect paging.
     *
     * @param from      the messages
     * @param archetype the provider SMS id archetype
     * @return the sent messages
     */
    Iterable<OutboundMessage> getSent(OffsetDateTime from, String archetype);

    /**
     * Returns an inbound message given its OpenVPMS identifier.
     *
     * @param id the message identifier
     * @return the corresponding message, or {@code null} if none is found
     */
    InboundMessage getInboundMessage(long id);

    /**
     * Returns an inbound message given its provider identifier.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return the corresponding inbound message, or {@code null} if none is found
     * @throws SMSException for any SMS error
     */
    InboundMessage getInboundMessage(String archetype, String id);

    /**
     * Returns a builder for an inbound message.
     * <p/>
     * NOTE: this should only be used for inbound messages that are not replies.<br/>
     * For replies, use {@link OutboundMessage#reply()}.
     *
     * @return a new builder
     */
    InboundMessageBuilder getInboundMessageBuilder();
}
