/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2021 (C) OpenVPMS Ltd. All Rights Reserved.
 */

package org.openvpms.sms.message;

/**
 * Builds an {@link InboundMessage} that is not in reply to an existing {@link OutboundMessage}.
 *
 * @author Tim Anderson
 */
public interface InboundMessageBuilder extends ReceivedMessageBuilder<InboundMessageBuilder> {

    /**
     * Sets the OpenVPMS identifier of the outbound message, if this message is a reply to an existing message.
     *
     * @param id the OpenVPMS message identifier
     * @return this
     */
    InboundMessageBuilder outboundId(long id);

    /**
     * Sets the provider identifier of the outbound message, if this message is a reply to an existing message.
     *
     * @param archetype the identifier archetype. Must have an <em>actIdentity.sms</em> prefix.
     * @param id        the message identifier
     * @return this
     */
    InboundMessageBuilder outboundProviderId(String archetype, String id);

}
